/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-12T09:54:25+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMSTUB_HPP__
#define RFMSTUB_HPP__

#include <cstdint>
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include <DIMParamList.h>
#include <DIMSuperCommand.h>

#include "EACSTypes.hpp"

namespace stub {

class RFMStub : public ntof::dim::DIMSuperCommand
{
public:
    struct EventInfo
    {
        ntof::eacs::EventNumber timingEvent;
        ntof::eacs::EventNumber validatedEvent;
        uint64_t fileSize;
    };

    struct RunInfo
    {
        RunInfo();
        std::string experiment;
        bool approved;
        bool standalone;
        std::vector<int> daqs;
    };

    typedef std::map<ntof::eacs::EventNumber, EventInfo> EventsMap;
    RFMStub();
    ~RFMStub() override;

    void commandReceived(ntof::dim::DIMCmd &cmd) override;

    void setCurrentRunNumber(ntof::eacs::RunNumber runNumber);

    EventsMap events() const;
    ntof::eacs::RunNumber runNumber() const;
    bool isStarted() const;
    RunInfo info() const;

protected:
    mutable std::mutex m_lock;
    ntof::eacs::RunNumber m_runNumber;
    EventsMap m_events;
    bool m_runStarted;
    RunInfo m_info;
    std::unique_ptr<ntof::dim::DIMParamList> m_currentSvc;
};

} // namespace stub

#endif
