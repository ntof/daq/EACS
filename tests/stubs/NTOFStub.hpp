/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-07T15:54:04+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef NTOFSTUB_HPP__
#define NTOFSTUB_HPP__

#include <map>
#include <memory>
#include <string>

#include "AddhStub.hpp"
#include "FilterStationStub.hpp"
#include "HVCardStub.hpp"
#include "RFMStub.hpp"
#include "TimingStub.hpp"
#include "daq/DaqStub.hpp"

namespace stub {

class NTOFStub
{
public:
    typedef std::unique_ptr<DaqStub> DaqStubPtr;
    typedef std::map<std::string, DaqStubPtr> DaqStubMap;
    typedef std::map<uint32_t, std::unique_ptr<HVCardStub>> HVMap;

    NTOFStub();
    ~NTOFStub();

    DaqStubMap daqStubs;
    HVMap hvStubs;
    std::unique_ptr<TimingStub> timing;
    std::unique_ptr<AddhStub> addh;
    std::unique_ptr<RFMStub> rfm;
    std::unique_ptr<FilterStationStub> filterStation;
};

} // namespace stub

#endif
