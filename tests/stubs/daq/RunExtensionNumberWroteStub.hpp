//
// Created by matteof on 7/29/20.
//

#ifndef EACS_RUNEXTENSIONNUMBERWROTESTUB_HPP
#define EACS_RUNEXTENSIONNUMBERWROTESTUB_HPP

#include <DIMXMLService.h>

using namespace ntof::dim;

class RunExtensionNumberWroteStub : public DIMXMLService
{
public:
    explicit RunExtensionNumberWroteStub(const std::string &hostname);

    void buildRunExtNum(int64_t runNumber, int64_t eventNumber, uint64_t size);

    void resetExtNum();

protected:
    std::string m_hostname;
};

#endif // EACS_RUNEXTENSIONNUMBERWROTESTUB_HPP
