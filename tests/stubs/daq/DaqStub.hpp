//
// Created by matteof on 7/23/20.
//

#ifndef EACS_DAQSTUB_HPP
#define EACS_DAQSTUB_HPP

#include <list>
#include <map>
#include <memory>
#include <utility>

#include <DIMState.h>
#include <EventReader.h>
#include <Worker.hpp>

#include "AcqStateStub.hpp"
#include "CardStub.hpp"
#include "ChannelConfigStub.hpp"
#include "DaqStateStub.hpp"
#include "ListDaqElementsStub.hpp"
#include "RunExtensionNumberWroteStub.hpp"
#include "WriterStateStub.hpp"
#include "ZeroSuppressionStub.hpp"

using namespace ntof::dim;

typedef std::unique_ptr<ListDaqElementsStub> ListDaqElementsStubPtr;
typedef std::unique_ptr<RunExtensionNumberWroteStub>
    RunExtensionNumberWroteStubPtr;
typedef std::unique_ptr<stub::ZeroSuppressionStub> ZeroSuppressionStubPtr;
typedef std::unique_ptr<CardStub> CardStubPtr;

class DaqStub : public DIMSuperCommand, public EventHandler
{
public:
    DaqStub(const std::string &hostname, uint32_t chassisId);
    ~DaqStub() override;

    static void processDimCmdParam(std::vector<std::string> &res,
                                   std::string str,
                                   std::string delimiter);

    void trigger(const EventReader::Data &event);

protected:
    void commandReceived(DIMCmd &cmd) override;
    void eventReceived(const EventReader::Data &event) override;
    void noLink() override;

    void processCommand(const std::string &cmd,
                        int64_t runNumber,
                        uint32_t cmdCounter);
    void cmdReset(uint32_t cmdCounter);
    void cmdInitialization(uint32_t cmdCounter);
    void cmdStop(uint32_t cmdCounter);
    void cmdStart(uint32_t cmdCounter);

    void configEvent();

    bool isConfigured();

    void expectState(int32_t expected);

    static void waitFor(uint32_t ms);

    void checkCmdCounter(uint32_t cmdCounter) const;

    std::string m_hostname;
    uint32_t m_chassisId;

    ntof::utils::Worker m_worker;
    uint32_t m_cmdCounter;

    std::unique_ptr<EventReader> m_eventReader; //!< Object to parse the content
                                                //!< of the
    //!< service publish by the timing
    //!< machine in order to get the event
    //!< number

    // Services
    ListDaqElementsStubPtr m_listDaqStub;
    RunExtensionNumberWroteStubPtr m_runExtNum;
    ZeroSuppressionStubPtr m_zspStub;
    std::list<CardStubPtr> m_cards;

    // States
    DaqStateStub m_daqState;
    AcqStateStub m_writerState;
    WriterStateStub m_acqState;

    int64_t m_runNumber;
};

#endif // EACS_DAQSTUB_HPP
