//
// Created by matteof on 7/28/20.
//

#ifndef EACS_DAQSTATESTUB_HPP
#define EACS_DAQSTATESTUB_HPP

#include <DIMState.h>

using namespace ntof::dim;

class DaqStateStub : public DIMState
{
public:
    enum States
    {
        HARDWARE_INIT = 0,
        NOT_CONFIGURED = 1,
        CALIBRATING = 2,
        WAITING_FOR_COMMAND = 3,
        PROCESSING_COMMAND = 4,
        STOPPING_ACQ = 8,
        STARTING_ACQ = 9,
        RESETING = 10,
        INITIALIZING_ACQ = 11,
        WAITING_FOR_START_ACQ = 12,
        SUPER_USER = 13
    };

    explicit DaqStateStub(const std::string &hostname);

private:
    std::string m_hostname;
};

#endif // EACS_DAQSTATESTUB_HPP
