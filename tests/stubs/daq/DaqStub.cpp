//
// Created by matteof on 7/23/20.
//

#include "DaqStub.hpp"

#include <string>

#include <easylogging++.h>

#include "DaqStubExceptions.hpp"

#define NUM_CARDS 3
#define NUM_CHANNELS 4

using namespace ntof::dim;
using namespace stub;

DaqStub::DaqStub(const std::string &hostname, uint32_t chassisId) :
    DIMSuperCommand(hostname + "/Daq/Command"),
    m_hostname(hostname),
    m_chassisId(chassisId),
    m_worker("DaqStub", 1, 2048),
    m_cmdCounter(0),
    m_eventReader(new EventReader("Timing/event")),
    m_daqState(m_hostname),
    m_writerState(m_hostname),
    m_acqState(m_hostname),
    m_runNumber(-1)
{
    m_eventReader->registerHandler(this);
    m_listDaqStub.reset(new ListDaqElementsStub(m_hostname));
    m_runExtNum.reset(new RunExtensionNumberWroteStub(m_hostname));
    m_zspStub.reset(new ZeroSuppressionStub(m_hostname));

    for (uint32_t card = 0; card < NUM_CARDS; card++)
    {
        CardStubPtr p(
            new CardStub(m_hostname, card, NUM_CHANNELS, "S412",
                         "SPD-" + std::to_string(chassisId * 100 + card)));
        p->configureSignal.connect([this]() {
            this->m_worker.post([this]() { this->configEvent(); });
        });
        m_cards.emplace_back(std::move(p));
    }
    m_listDaqStub->buildListDaqElements(m_cards);
    m_runExtNum->resetExtNum();
    m_worker.post([this]() {
        waitFor(100);
        m_daqState.setValue(DaqStateStub::States::NOT_CONFIGURED);
    });
}

DaqStub::~DaqStub()
{
    m_eventReader.reset();
    m_worker.stop();
}

void DaqStub::processDimCmdParam(std::vector<std::string> &res,
                                 std::string str,
                                 std::string delimiter)
{
    // Clear the vector to be sure it contains only the results
    res.clear();

    size_t pos = 0;
    std::string token;
    while ((pos = str.find(delimiter)) != std::string::npos)
    {
        token = str.substr(0, pos);
        res.push_back(token);
        str.erase(0, pos + delimiter.length());
    }

    res.push_back(str);
}

void DaqStub::trigger(const EventReader::Data &event)
{
    if (m_daqState.getValue() != DaqStateStub::States::WAITING_FOR_COMMAND ||
        m_acqState.getValue() != AcqStateStub::States::WAITING_FOR_TRIGGER)
    {
        LOG(INFO) << "[DaqStub] " << m_hostname
                  << " TRIGGER ignored: acqState is " << m_acqState.getValue();
        return;
    }
    try
    {
        LOG(INFO) << "[DaqStub] " << m_hostname
                  << " event received eventNumber:" << event.evtNumber;
        int32_t cmdCounter = ++m_cmdCounter;

        m_acqState.setValue(AcqStateStub::States::DUMPING_CARDS);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_acqState.setValue(AcqStateStub::States::QUEUING_BUFFERS);
        m_writerState.setValue(WriterStateStub::States::PROCESSING_DATA);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_writerState.setValue(WriterStateStub::States::WRITING_DATA);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_runExtNum->buildRunExtNum(m_runNumber, event.evtNumber, 1024);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_writerState.setValue(WriterStateStub::States::FREEING_BUFFERS);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_writerState.setValue(WriterStateStub::States::WAITING_FOR_DATA);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_acqState.setValue(AcqStateStub::States::ARMING_TRIGGERS);
        waitFor(10);
        checkCmdCounter(cmdCounter);

        m_acqState.setValue(AcqStateStub::States::WAITING_FOR_TRIGGER);
    }
    catch (const NewCommandInQueue &e)
    {
        LOG(INFO) << "[DaqStub] NewCommandInQueue: " << e.what();
    }
}

void DaqStub::eventReceived(const EventReader::Data &event)
{
    // copy event while catching it
    m_worker.post([this, event]() { this->trigger(event); });
}

void DaqStub::noLink()
{
    LOG(ERROR) << "Connection lost with timing machine.";
}

void DaqStub::commandReceived(DIMCmd &cmdData)
{
    pugi::xml_node parent = cmdData.getData();
    std::string errMsg = "undefined error";

    if (!parent)
    {
        errMsg = "could not process command, xml file incorrect (parent node "
                 "is empty)";
        LOG(INFO) << errMsg;
        setError(cmdData.getKey(), 100, errMsg);
        return;
    }

    std::string cmd = parent.child("command").child_value();
    int64_t runNumber = m_runNumber;

    std::vector<std::string> cmdParameters;
    DaqStub::processDimCmdParam(cmdParameters, cmd, "?");
    std::vector<std::string> params;

    if (cmdParameters.size() > 1)
    {
        DaqStub::processDimCmdParam(params, cmdParameters[1], "&");
        // For each parameter separate the key and the value
        for (auto &p : params)
        {
            std::vector<std::string> extractedParam;
            DaqStub::processDimCmdParam(extractedParam, p, "=");
            std::transform(extractedParam[0].begin(), extractedParam[0].end(),
                           extractedParam[0].begin(), ::tolower);
            if (extractedParam[0] == "runnumber")
            {
                runNumber = (uint32_t) std::stoi(extractedParam[1]);
            }
        }
    }

    cmd = cmdParameters[0];

    if (cmd != "reset" && cmd != "initialization" && cmd != "stop" &&
        cmd != "start" && cmd != "calibrate" && cmd != "super_user")
    {
        // unknown command received
        errMsg = "Error command unknown !";
        LOG(ERROR) << errMsg;
        setError(cmdData.getKey(), 101, errMsg);
        return;
    }

    setOk(cmdData.getKey(), std::string("Done!"));
    uint32_t cmdCounter = ++m_cmdCounter;
    m_worker.post([this, cmd, runNumber, cmdCounter]() {
        this->processCommand(cmd, runNumber, cmdCounter);
    });
}

void DaqStub::processCommand(const std::string &cmd,
                             int64_t runNumber,
                             uint32_t cmdCounter)
{
    try
    {
        // Check if it's the last command queued
        checkCmdCounter(cmdCounter);

        if (cmd == "reset")
        {
            LOG(INFO) << "[DaqStub] CMD: reset";
            cmdReset(cmdCounter);
            return;
        }
        else if (cmd == "initialization")
        {
            LOG(INFO) << "[DaqStub] CMD: initialization for run number: "
                      << runNumber;
            m_runNumber = runNumber;
            expectState(DaqStateStub::States::WAITING_FOR_COMMAND);
            cmdInitialization(cmdCounter);
            return;
        }
        else if (cmd == "stop")
        {
            LOG(INFO) << "[DaqStub] CMD: stop";
            expectState(DaqStateStub::States::WAITING_FOR_COMMAND);
            cmdStop(cmdCounter);
            return;
        }
        else if (cmd == "start")
        {
            LOG(INFO) << "[DaqStub] CMD: start";
            expectState(DaqStateStub::States::WAITING_FOR_START_ACQ);
            cmdStart(cmdCounter);
            return;
        }
        // Do nothing commands
        else if (cmd == "calibrate")
        {
            LOG(INFO) << "[DaqStub] CMD: calibrate (do nothing)";
            expectState(DaqStateStub::States::WAITING_FOR_COMMAND);
            return;
        }
        else if (cmd == "super_user")
        {
            LOG(INFO) << "[DaqStub] CMD: super_user (do nothing)";
            return;
        }
    }
    catch (const DaqInvalidState &e)
    {
        LOG(ERROR) << "[DaqStub] Invalid State: " << e.what();
    }
    catch (const NewCommandInQueue &e)
    {
        LOG(INFO) << "[DaqStub] NewCommandInQueue: " << e.what();
    }
}

void DaqStub::cmdReset(uint32_t cmdCounter)
{
    m_daqState.setValue(DaqStateStub::States::RESETING);
    waitFor(100);
    checkCmdCounter(cmdCounter);
    m_acqState.setValue(AcqStateStub::States::IDLE);
    m_writerState.setValue(WriterStateStub::States::IDLE);

    std::for_each(m_cards.begin(), m_cards.end(),
                  [](CardStubPtr &c) { c->setConfigured(false); });
    m_daqState.setValue(DaqStateStub::States::NOT_CONFIGURED);
}

void DaqStub::cmdInitialization(uint32_t cmdCounter)
{
    m_runExtNum->resetExtNum();

    m_daqState.setValue(DaqStateStub::States::PROCESSING_COMMAND);
    waitFor(100);
    checkCmdCounter(cmdCounter);

    m_daqState.setValue(DaqStateStub::States::INITIALIZING_ACQ);
    m_writerState.setValue(WriterStateStub::States::INITIALIZATION);
    m_acqState.setValue(AcqStateStub::States::INITIALIZATION);
    waitFor(100);
    checkCmdCounter(cmdCounter);

    m_acqState.setValue(AcqStateStub::States::ALLOCATING_MEMORY);
    m_writerState.setValue(WriterStateStub::States::ALLOCATING_MEMORY);
    waitFor(100);
    checkCmdCounter(cmdCounter);

    m_acqState.setValue(AcqStateStub::States::WAITING_TO_START);
    m_writerState.setValue(WriterStateStub::States::WAITING_FOR_DATA);
    m_daqState.setValue(DaqStateStub::States::WAITING_FOR_START_ACQ);
}

void DaqStub::cmdStart(uint32_t cmdCounter)
{
    m_daqState.setValue(DaqStateStub::States::STARTING_ACQ);
    m_acqState.setValue(AcqStateStub::States::ARMING_TRIGGERS);
    waitFor(100);
    checkCmdCounter(cmdCounter);

    m_daqState.setValue(DaqStateStub::States::WAITING_FOR_COMMAND);
    m_acqState.setValue(AcqStateStub::States::WAITING_FOR_TRIGGER);
}

void DaqStub::cmdStop(uint32_t cmdCounter)
{
    m_daqState.setValue(DaqStateStub::States::WAITING_FOR_COMMAND);
    waitFor(100);
    checkCmdCounter(cmdCounter);
    m_daqState.setValue(DaqStateStub::States::STOPPING_ACQ);
    waitFor(100);
    checkCmdCounter(cmdCounter);

    m_acqState.setValue(AcqStateStub::States::IDLE);
    m_writerState.setValue(WriterStateStub::States::IDLE);

    std::for_each(m_cards.begin(), m_cards.end(),
                  [](CardStubPtr &c) { c->setConfigured(false); });

    m_daqState.setValue(DaqStateStub::States::NOT_CONFIGURED);
}

void DaqStub::configEvent()
{
    if (m_daqState.getValue() == DaqStateStub::States::NOT_CONFIGURED &&
        isConfigured())
    {
        waitFor(100);
        m_daqState.setValue(DaqStateStub::States::CALIBRATING);
        waitFor(100);
        m_daqState.setValue(DaqStateStub::States::WAITING_FOR_COMMAND);
    }
}

bool DaqStub::isConfigured()
{
    return std::all_of(m_cards.begin(), m_cards.end(),
                       [](const CardStubPtr &c) { return c->isConfigured(); });
}

void DaqStub::expectState(int32_t expected)
{
    int32_t currValue = m_daqState.getValue();
    if (currValue != expected)
    {
        throw DaqInvalidState(currValue, expected);
    }
}

void DaqStub::waitFor(uint32_t ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

void DaqStub::checkCmdCounter(uint32_t cmdCounter) const
{
    if (cmdCounter != m_cmdCounter)
    {
        throw NewCommandInQueue();
    }
}
