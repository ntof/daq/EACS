//
// Created by matteof on 7/29/20.
//

#ifndef EACS_DAQSTUBEXCEPTIONS_HPP
#define EACS_DAQSTUBEXCEPTIONS_HPP

#include <cstdint>
#include <exception>
#include <sstream>
#include <string>

class DaqInvalidState : public std::exception
{
public:
    explicit DaqInvalidState(int32_t currentState, int32_t expectedState) :
        m_current(currentState), m_expected(expectedState)
    {}

    ~DaqInvalidState() override = default;

    const char *what() const noexcept override
    {
        if (m_what.empty())
        {
            std::ostringstream msg;
            msg << " Invalid Daq State. Current State: " << m_current
                << " | Expected: " << m_expected;
            m_what = msg.str();
        }

        return m_what.c_str();
    }

protected:
    mutable std::string m_what;
    int32_t m_current;
    int32_t m_expected;
};

class NewCommandInQueue : public std::exception
{
public:
    NewCommandInQueue() = default;
    ~NewCommandInQueue() override = default;

    const char *what() const noexcept override
    {
        if (m_what.empty())
        {
            m_what = "There is a new command in queue, abort the current one";
        }
        return m_what.c_str();
    }

protected:
    mutable std::string m_what;
};

#endif // EACS_DAQSTUBEXCEPTIONS_HPP
