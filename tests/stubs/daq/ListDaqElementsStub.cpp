//
// Created by matteof on 7/23/20.
//

#include "ListDaqElementsStub.hpp"

#include <easylogging++.h>

using namespace ntof::dim;

ListDaqElementsStub::ListDaqElementsStub(const std::string &hostname) :
    DIMXMLService(hostname + "/ListDaqElements"), m_hostname(hostname)
{}

void ListDaqElementsStub::buildListDaqElements(
    const std::list<CardStubPtr> &cards)
{
    // Example
    /*
     <listDaqElements>
        <card0 nbChannel="4" type="S014" serialNumber="SPD-04680" />
        <card1 nbChannel="4" type="S014" serialNumber="SPD-H0001" />
        <card2 nbChannel="4" type="S014" serialNumber="SPD-H0002" />
     </listDaqElements>
    */
    pugi::xml_document doc;
    pugi::xml_node listDaqElementsNode = doc.append_child("listDaqElements");

    for (const auto &card : cards)
    {
        std::string cardNodeStr = "card" + std::to_string(card->getIndex());
        pugi::xml_node cardNode = listDaqElementsNode.append_child(
            cardNodeStr.c_str());
        cardNode.append_attribute("nbChannel").set_value(card->getNbChannels());
        cardNode.append_attribute("type").set_value(card->getType().c_str());
        cardNode.append_attribute("serialNumber")
            .set_value(card->getSn().c_str());
    }

    std::stringstream ss;
    doc.save(ss, "  ");
    LOG(INFO) << "[ListDaqElementsStub] Set Data on " << m_hostname
              << "/ListDaqElements :" << ss.str();
    setData(doc);
}
