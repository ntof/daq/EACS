//
// Created by matteof on 7/28/20.
//

#include "CardStub.hpp"

#include <NTOFLogging.hpp>

struct IsChannelConfigured
{
    IsChannelConfigured() = default;
    bool operator()(const ChannelConfigStubPtr &d) const
    {
        return d->isConfigured();
    }
};

CardStub::CardStub(const std::string &hostname,
                   uint32_t index,
                   uint32_t nbChannels,
                   const std::string &type,
                   const std::string &serialNumber) :
    m_hostname(hostname),
    m_index(index),
    m_nbChannels(nbChannels),
    m_type(type),
    m_sn(serialNumber)
{
    for (uint32_t channel = 0; channel < m_nbChannels; channel++)
    {
        ChannelConfigStubPtr p(
            new ChannelConfigStub(m_hostname, m_index, channel));
        p->configureSignal.connect([this]() {
            if (isConfigured())
            {
                this->configureSignal();
            }
        });
        m_channels.emplace_back(std::move(p));
    }
}

void CardStub::setConfigured(bool value)
{
    std::for_each(
        m_channels.begin(), m_channels.end(),
        [&value](ChannelConfigStubPtr &d) { d->setConfigured(value); });
}

bool CardStub::isConfigured()
{
    return std::all_of(
        m_channels.begin(), m_channels.end(),
        [](const ChannelConfigStubPtr &d) { return d->isConfigured(); });
}

uint32_t CardStub::getIndex() const
{
    return m_index;
}

uint32_t CardStub::getNbChannels() const
{
    return m_nbChannels;
}

const std::string &CardStub::getType() const
{
    return m_type;
}

const std::string &CardStub::getSn() const
{
    return m_sn;
}
