//
// Created by matteof on 7/28/20.
//

#ifndef EACS_ACQSTATESTUB_HPP
#define EACS_ACQSTATESTUB_HPP

#include <DIMState.h>

using namespace ntof::dim;

class AcqStateStub : public DIMState
{
public:
    enum States
    {
        IDLE = 0,
        INITIALIZATION,
        ALLOCATING_MEMORY,
        WAITING_TO_START,
        ARMING_TRIGGERS,
        WAITING_FOR_TRIGGER,
        DUMPING_CARDS,
        QUEUING_BUFFERS
    };

    explicit AcqStateStub(const std::string &hostname);

private:
    std::string m_hostname;
};

#endif // EACS_ACQSTATESTUB_HPP
