//
// Created by matteof on 7/29/20.
//

#include "RunExtensionNumberWroteStub.hpp"

#include <easylogging++.h>

using namespace ntof::dim;

RunExtensionNumberWroteStub::RunExtensionNumberWroteStub(
    const std::string &hostname) :
    DIMXMLService(hostname + "/WRITER/RunExtensionNumber"), m_hostname(hostname)
{}

void RunExtensionNumberWroteStub::buildRunExtNum(int64_t runNumber,
                                                 int64_t eventNumber,
                                                 uint64_t size)
{
    /*
     element RunExtensionNumberWrote {
       attribute run { xsd:long },
       attribute event { xsd:long },
       attribute size { xsd:unsignedLong }
     }
    */

    pugi::xml_document doc;
    pugi::xml_node extNum = doc.append_child("RunExtensionNumberWrote");
    extNum.append_attribute("run").set_value(runNumber);
    extNum.append_attribute("event").set_value(eventNumber);
    extNum.append_attribute("size").set_value(size);

    std::stringstream ss;
    doc.save(ss, "  ");
    LOG(INFO) << "[RunExtensionNumberWroteStub] Set Data: " << ss.str();
    setData(doc);
}

void RunExtensionNumberWroteStub::resetExtNum()
{
    buildRunExtNum(-1, -1, 0);
}
