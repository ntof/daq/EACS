//
// Created by matteof on 7/23/20.
//

#include "ZeroSuppressionStub.hpp"

namespace stub {

/* incoming values can be partial, this is used to rebuild it */
static const DIMData masterInfo(0, "master", "", 0);
static const DIMData snInfo(0, "sn", "", 0);
static const DIMData channelInfo(1, "channel", "", 0);
static const DIMData slaveInfo(0, "slave", "", 0);

ZeroSuppressionStub::ZeroSuppressionStub(const std::string &hostname) :
    DIMParamList(hostname + "/ZeroSuppression"),
    m_hostname(hostname),
    m_zsMode(ZSP::Mode::INDEPENDENT)
{
    /*
     Example for independant (simplest) configuration

     <parameters>
      <data name="Number of parameters" index="0" unit="" type="1" value="21"/>

      <data name="mode" index="1" unit="" type="5" value="0"
    valueName="independant"> <enum> <value value="0" name="independant"/> <value
    value="1" name="singlemaster"/> <value value="2" name="master"/>
        </enum>
      </data>

    </parameters>
     */

    DIMEnum mode;
    mode.addItem(ZSP::Mode::INDEPENDENT, "independent");
    mode.addItem(ZSP::Mode::SINGLE_MASTER, "singlemaster");
    mode.addItem(ZSP::Mode::MULTIPLE_MASTER, "master");
    mode.setValue(ZSP::Mode::INDEPENDENT);
    addParameter(Params::MODE, "mode", "", std::move(mode), AddMode::CREATE,
                 false);
    addParameter(Params::CONFIGURATION, "configuration", "", DIMData::List(),
                 AddMode::CREATE, false);

    setHandler(this);
    updateService();
}

int ZeroSuppressionStub::parameterChanged(DIMData::List &settingsChanged,
                                          const DIMParamList & /*list*/,
                                          int & /*errCode*/,
                                          std::string & /*errMsg*/)
{
    ZSP::Mode mode = GetMode();
    MasterList masterList;
    DIMData::List configData;

    for (DIMData &data : settingsChanged)
    {
        if (data.getIndex() == Params::MODE)
        {
            mode = static_cast<ZSP::Mode>(data.getValue<DIMEnum>().getValue());
        }
        else if (data.getIndex() == Params::CONFIGURATION)
        {
            configData = data.getNestedValue();
            for (DIMData &masterData : configData)
                masterList.push_back(std::move(getMaster(masterData)));
        }
    }

    switch (mode)
    {
    case ZSP::Mode::INDEPENDENT:
        if (!masterList.empty())
            throw DIMException("can't set masters in independent mode",
                               __LINE__);
        break;
    case ZSP::Mode::SINGLE_MASTER:
        if (masterList.size() != 1)
            throw DIMException(
                "a single master must be set in singlemaster mode", __LINE__);
        else if (!masterList[0].slaveList.empty())
            throw DIMException("slave list must be empty in singlemaster mode",
                               __LINE__);
        break;
    case ZSP::Mode::MULTIPLE_MASTER: break;
    default:
        throw DIMException("invalid mode: " + std::to_string(mode), __LINE__);
    }

    /* directly pre-replace content */
    setValue(Params::CONFIGURATION, std::move(configData), false);

    std::lock_guard<std::mutex> lock(m_mutex);
    m_masterList.swap(masterList);
    m_zsMode = mode;

    return 0;
}

ZSP::Master ZeroSuppressionStub::getMaster(DIMData &master) const
{
    ZSP::Master ret;
    ret.channel = -1;

    for (DIMData &data : master.getNestedValue())
    {
        if (data.getIndex() == ChannelParams::SN)
        {
            ret.masterSn = data.getValue<std::string>();
            data.copyParameterInfo(snInfo);
        }
        else if (data.getIndex() == ChannelParams::CHANNEL)
        {
            ret.channel = data.getValue<uint32_t>();
            data.copyParameterInfo(channelInfo);
        }
        else
        {
            ZSP::Slave slave;
            slave.index = -1;

            for (DIMData &slaveData : data.getNestedValue())
            {
                if (slaveData.getIndex() == 0)
                {
                    slave.sn = slaveData.getValue<std::string>();
                    slaveData.copyParameterInfo(snInfo);
                }
                else if (slaveData.getIndex() == 1)
                {
                    slave.index = slaveData.getValue<uint32_t>();
                    slaveData.copyParameterInfo(slaveInfo);
                }
            }

            if (slave.sn.empty() || (slave.index < 0))
                throw DIMException("invalid channel on master: " + ret.masterSn +
                                       std::to_string(slave.index),
                                   __LINE__);
            else if (slave.index == ret.channel && slave.sn == ret.masterSn)
                throw DIMException("channel " + ret.masterSn + ":" +
                                       std::to_string(ret.channel) +
                                       " alread set as master",
                                   __LINE__);

            ret.slaveList.push_back(std::move(slave));
        }
    }
    if (ret.masterSn.empty() || ret.channel < 0)
        throw DIMException("invalid master channel: " + ret.masterSn + ":" +
                               std::to_string(ret.channel),

                           __LINE__);

    master.copyParameterInfo(masterInfo);
    return ret;
}

void ZeroSuppressionStub::updateService()
{
    DIMData::List masterList;
    ZSP::Mode mode;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        mode = m_zsMode;

        for (MasterList::const_iterator masterIt = m_masterList.begin();
             masterIt != m_masterList.end(); ++masterIt)
        {
            DIMData::List masterData;
            masterData.emplace_back(0, "sn", "", masterIt->masterSn);
            masterData.emplace_back(1, "channel", "",
                                    int32_t(masterIt->channel));

            if (m_zsMode == ZSP::Mode::MULTIPLE_MASTER)
            {
                for (const ZSP::Slave &slave : masterIt->slaveList)
                {
                    DIMData::List slaveData;
                    slaveData.emplace_back(0, "sn", "", slave.sn);
                    slaveData.emplace_back(1, "channel", "",
                                           int32_t(slave.index));
                    masterData.emplace_back(masterData.size(), "slave", "",
                                            std::move(slaveData));
                }
            }

            masterList.emplace_back(masterList.size(), "master", "",
                                    std::move(masterData));
        }
    }

    lockParameterAt(Params::MODE)->getEnumValue().setValue(mode);
    setValue(Params::CONFIGURATION, std::move(masterList), true);
}

ZSP::Mode ZeroSuppressionStub::GetMode() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_zsMode;
}

ZeroSuppressionStub::MasterList ZeroSuppressionStub::GetMasterList() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_masterList;
}

} // namespace stub