//
// Created by matteof on 7/23/20.
//

#ifndef EACS_LISTDAQELEMENTSSTUB_HPP
#define EACS_LISTDAQELEMENTSSTUB_HPP

#include <DIMXMLService.h>

#include "CardStub.hpp"

using namespace ntof::dim;

typedef std::unique_ptr<CardStub> CardStubPtr;

class ListDaqElementsStub : public DIMXMLService
{
public:
    explicit ListDaqElementsStub(const std::string &hostname);

    void buildListDaqElements(const std::list<CardStubPtr> &cards);

protected:
    std::string m_hostname;
};

#endif // EACS_LISTDAQELEMENTSSTUB_HPP
