//

// Created by matteof on 7/28/20.
//

#include "AcqStateStub.hpp"
AcqStateStub::AcqStateStub(const std::string &hostname) :
    DIMState(hostname + "/AcquisitionState"), m_hostname(hostname)
{
    addStateValue(IDLE, "IDLE");
    addStateValue(INITIALIZATION, "INITIALIZATION");
    addStateValue(ALLOCATING_MEMORY, "ALLOCATING_MEMORY");
    addStateValue(WAITING_TO_START, "WAITING_TO_START");
    addStateValue(ARMING_TRIGGERS, "ARMING_TRIGGERS");
    addStateValue(WAITING_FOR_TRIGGER, "WAITING_FOR_TRIGGER");
    addStateValue(DUMPING_CARDS, "DUMPING_CARDS");
    addStateValue(QUEUING_BUFFERS, "QUEUING_BUFFERS");
    setValue(IDLE);
}
