//
// Created by matteof on 7/23/20.
//

#ifndef EACS_ZEROSUPPRESSIONSTUB_HPP
#define EACS_ZEROSUPPRESSIONSTUB_HPP

#include <DIMParamList.h>

using namespace ntof::dim;

namespace stub {
namespace ZSP {

struct Slave
{
    std::string sn;
    int cardIndex;
    int index;
};

struct Master
{
    int id;
    std::string masterSn;
    int cardIndex;
    int channel;
    std::vector<Slave> slaveList;
};

enum Mode : int
{
    INDEPENDENT = 0,
    SINGLE_MASTER = 1,
    MULTIPLE_MASTER = 2
};

} // namespace ZSP

class ZeroSuppressionStub : public DIMParamList, public DIMParamListHandler
{
public:
    typedef std::vector<ZSP::Master> MasterList;

    enum Params
    {
        MODE = 1,
        CONFIGURATION
    };

    enum ChannelParams
    {
        SN = 0,
        CHANNEL
    };

    explicit ZeroSuppressionStub(const std::string &hostname);

    ZSP::Mode GetMode() const;
    MasterList GetMasterList() const;

    /**
     * @brief parameter checking callback
     * @details will actually modify the service content
     */
    int parameterChanged(DIMData::List &settingsChanged,
                         const DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;

protected:
    ZSP::Master getMaster(DIMData &master) const;
    void updateService();

    std::string m_hostname;
    mutable std::mutex m_mutex;
    ZSP::Mode m_zsMode;
    MasterList m_masterList;
};
} // namespace stub

#endif // EACS_ZEROSUPPRESSIONSTUB_HPP
