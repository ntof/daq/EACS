//
// Created by matteof on 7/28/20.
//

#include "WriterStateStub.hpp"

WriterStateStub::WriterStateStub(const std::string &hostname) :
    DIMState(hostname + "/WriterState"), m_hostname(hostname)
{
    addStateValue(IDLE, "IDLE");
    addStateValue(INITIALIZATION, "INITIALIZATION");
    addStateValue(ALLOCATING_MEMORY, "ALLOCATING_MEMORY");
    addStateValue(WAITING_FOR_DATA, "WAITING_FOR_DATA");
    addStateValue(PROCESSING_DATA, "PROCESSING_DATA");
    addStateValue(WRITING_DATA, "WRITING_DATA");
    addStateValue(FREEING_BUFFERS, "FREEING_BUFFERS");
    addStateValue(STOPPING, "STOPPING");
    setValue(IDLE);
}
