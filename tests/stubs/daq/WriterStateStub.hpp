//
// Created by matteof on 7/28/20.
//

#ifndef EACS_WRITERSTATESTUB_HPP
#define EACS_WRITERSTATESTUB_HPP

#include <DIMState.h>

using namespace ntof::dim;

class WriterStateStub : public DIMState
{
public:
    enum States
    {
        IDLE = 0,
        INITIALIZATION,
        ALLOCATING_MEMORY,
        WAITING_FOR_DATA,
        PROCESSING_DATA,
        WRITING_DATA,
        FREEING_BUFFERS,
        STOPPING
    };

    explicit WriterStateStub(const std::string &hostname);

private:
    std::string m_hostname;
};

#endif // EACS_WRITERSTATESTUB_HPP
