//
// Created by matteof on 7/28/20.
//

#include "DaqStateStub.hpp"
DaqStateStub::DaqStateStub(const std::string &hostname) :
    DIMState(hostname + "/DaqState"), m_hostname(hostname)
{
    addStateValue(HARDWARE_INIT, "HARDWARE_INIT");
    addStateValue(NOT_CONFIGURED, "NOT_CONFIGURED");
    addStateValue(CALIBRATING, "CALIBRATING");
    addStateValue(WAITING_FOR_COMMAND, "WAITING_FOR_COMMAND");
    addStateValue(PROCESSING_COMMAND, "PROCESSING_COMMAND");
    addStateValue(STOPPING_ACQ, "STOPPING_ACQ");
    addStateValue(STARTING_ACQ, "STARTING_ACQ");
    addStateValue(RESETING, "RESETING");
    addStateValue(INITIALIZING_ACQ, "INITIALIZING_ACQ");
    addStateValue(WAITING_FOR_START_ACQ, "WAITING_FOR_START_ACQ");
    addStateValue(SUPER_USER, "SUPER_USER");
    setValue(HARDWARE_INIT);
}
