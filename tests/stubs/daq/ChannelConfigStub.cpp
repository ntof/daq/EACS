//
// Created by matteof on 7/23/20.
//

#include "ChannelConfigStub.hpp"

#include <cmath>

#include <NTOFLogging.hpp>
#include <easylogging++.h>

using namespace ntof::dim;

int ChannelConfigStubHandler::parameterChanged(
    std::vector<DIMData> &settingsChanged,
    const DIMParamList & /*list*/,
    int & /*errCode*/,
    std::string & /*errMsg*/)
{
    LOG(INFO) << "[ChannelConfigStubHandler] parameterChanged received";
    LOG(INFO) << ntof::log::debug()
              << "[ChannelConfigStubHandler] Changes : " << settingsChanged;
    return 0;
}

ChannelConfigStub::ChannelConfigStub(const std::string &hostname,
                                     uint32_t cardNumber,
                                     uint32_t channelNumber) :
    DIMParamList(hostname + "/CARD" + std::to_string(cardNumber) + "/CHANNEL" +
                 std::to_string(channelNumber)),
    m_calibService(hostname + "/CARD" + std::to_string(cardNumber) +
                   "/CHANNEL" + std::to_string(channelNumber) + "/Calibration"),
    m_hostname(hostname),
    m_cardNumber(cardNumber),
    m_channelNumber(cardNumber),
    m_isConfigured(false)
{
    setHandler(&m_handler);
    /*
     Example

     <parameters>
      <data name="Number of parameters" index="0" unit="" type="1" value="21"/>

      <data name="Is channel enabled?" index="1" unit="" type="9" value="0"/>
      <data name="Detector type" index="2" unit="" type="3" value="BAF2"/>
      <data name="Detector id" index="3" unit="" type="11" value="1"/>
      <data name="Module type" index="4" unit="" type="3" value="S412"/>
      <data name="Channel id" index="5" unit="" type="16" value="0"/>
      <data name="Module id" index="6" unit="" type="16" value="0"/>
      <data name="Chassis id" index="7" unit="" type="16" value="119"/>
      <data name="Stream id" index="8" unit="" type="16" value="1"/>
      <data name="Sample rate" index="9" unit="MS/s" type="8" value="1800"/>
      <data name="Sample size" index="10" unit="kS" type="11" value="128000"/>
      <data name="Full scale" index="11" unit="mV" type="8" value="5000"/>
      <data name="Delay time" index="12" unit="samples" type="1" value="0"/>
      <data name="Threshold" index="13" unit="mV" type="8" value="130"/>
      <data name="Threshold sign" index="14" unit="" type="1" value="1"/>
      <data name="Zero suppression start" index="15" unit="" type="11"
    value="0"/> <data name="Offset" index="16" unit="mV" type="8" value="0"/>
      <data name="Pre samples" index="17" unit="samples" type="11" value="512"/>
      <data name="Post samples" index="18" unit="samples" type="11"
    value="512"/> <data name="Clock state" index="19" unit="" type="3"
    value="INTC"/> <data name="Input impedance" index="20" unit="ohms" type="8"
    value="50"/>
    </parameters>
     */

    addParameter<bool>(Params::IS_CHANNEL_ENABLED, "Is channel enabled?", "",
                       false, AddMode::CREATE, false);
    addParameter<const std::string &>(Params::DETECTOR_TYPE, "Detector type",
                                      "", "BAF2", AddMode::CREATE, false);
    addParameter<uint32_t>(Params::DETECTOR_ID, "Detector id", "", 1,
                           AddMode::CREATE, false);
    addParameter<const std::string &>(Params::MODULE_TYPE, "Module type", "",
                                      "S412", AddMode::CREATE, false);
    addParameter<uint8_t>(Params::CHANNEL_ID, "Channel id", "", 0,
                          AddMode::CREATE, false);
    addParameter<uint8_t>(Params::MODULE_ID, "Module id", "", 0,
                          AddMode::CREATE, false);
    addParameter<uint8_t>(Params::CHASSIS_ID, "Chassis id", "", 119,
                          AddMode::CREATE, false);
    addParameter<uint8_t>(Params::STREAM_ID, "Stream id", "", 1,
                          AddMode::CREATE, false);
    addParameter<float>(Params::SAMPLE_RATE, "Sample rate", "MS/s", 1800,
                        AddMode::CREATE, false);
    addParameter<uint32_t>(Params::SAMPLE_SIZE, "Sample size", "kS", 128000,
                           AddMode::CREATE, false);
    addParameter<float>(Params::FULL_SCALE, "Full scale", "mV", 5000,
                        AddMode::CREATE, false);
    addParameter<int32_t>(Params::DELAY_TIME, "Delay time", "samples", 0,
                          AddMode::CREATE, false);
    addParameter<float>(Params::THRESHOLD, "Threshold", "mV", 130,
                        AddMode::CREATE, false);
    addParameter<int32_t>(Params::THRESHOLD_SIGN, "Threshold sign", "", 1,
                          AddMode::CREATE, false);
    addParameter<uint32_t>(Params::ZSP_START, "Zero suppression start", "", 0,
                           AddMode::CREATE, false);
    addParameter<float>(Params::OFFSET, "Offset", "mV", 0, AddMode::CREATE,
                        false);
    addParameter<uint32_t>(Params::PRE_SAMPLES, "Pre samples", "samples", 512,
                           AddMode::CREATE, false);
    addParameter<uint32_t>(Params::POST_SAMPLES, "Post samples", "samples", 512,
                           AddMode::CREATE, false);
    addParameter<const std::string &>(Params::CLOCK_STATE, "Clock state", "",
                                      "INTC", AddMode::CREATE, false);
    addParameter<float>(Params::INPUT_IMPEDANCE, "Input impedance", "ohms", 50,
                        AddMode::CREATE, true);

    // Calibration Service
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_FULL_SCALE,
                                  "Calibrated full scale", "mV", 0,
                                  AddMode::CREATE, false);
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_OFFSET,
                                  "Calibrated offset", "mV", 0, AddMode::CREATE,
                                  false);
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_THRESHOLD,
                                  "Calibrated threshold", "ADC", 0,
                                  AddMode::CREATE, true);
}

void ChannelConfigStub::commandReceived(DIMCmd &cmd)
{
    DIMParamList::commandReceived(cmd);
    if (m_ack->getStatus() == DIMAck::OK)
    {
        updateCalibration();
        setConfigured(true);
        configureSignal();
    }
}
void ChannelConfigStub::setConfigured(bool value)
{
    m_isConfigured = value;
}

bool ChannelConfigStub::isConfigured()
{
    return m_isConfigured;
}

void ChannelConfigStub::updateCalibration()
{
    float fullScale = getParameterAt(Params::FULL_SCALE).getValue<float>() + 50;
    float offset = getParameterAt(Params::OFFSET).getValue<float>() + 50;
    float threshold = getParameterAt(Params::THRESHOLD).getValue<float>();
    if (fullScale)
    {
        threshold = std::floor(threshold * (1 << 12) / fullScale);
    }
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_FULL_SCALE,
                                  "Calibrated full scale", "mV", fullScale,
                                  AddMode::UPDATE, false);
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_OFFSET,
                                  "Calibrated offset", "mV", offset,
                                  AddMode::UPDATE, false);
    m_calibService.addData<float>(ParamsCalib::CALIBRATED_THRESHOLD,
                                  "Calibrated threshold", "ADC", threshold,
                                  AddMode::UPDATE, true);
}
