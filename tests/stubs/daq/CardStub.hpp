//
// Created by matteof on 7/28/20.
//

#ifndef EACS_CARDSTUB_HPP
#define EACS_CARDSTUB_HPP

#include <list>

#include <Signals.hpp>

#include "ChannelConfigStub.hpp"

typedef std::unique_ptr<ChannelConfigStub> ChannelConfigStubPtr;

class CardStub
{
public:
    typedef ntof::utils::signal<void()> ConfigureSignal;

    CardStub(const std::string &hostname,
             uint32_t index,
             uint32_t nbChannels,
             const std::string &type,
             const std::string &serialNumber);

    void setConfigured(bool value);
    bool isConfigured();

    uint32_t getIndex() const;
    uint32_t getNbChannels() const;
    const std::string &getType() const;
    const std::string &getSn() const;

    ConfigureSignal configureSignal;

protected:
    std::string m_hostname;
    uint32_t m_index;
    uint32_t m_nbChannels;
    std::string m_type;
    std::string m_sn;

    std::list<ChannelConfigStubPtr> m_channels;
};

#endif // EACS_CARDSTUB_HPP
