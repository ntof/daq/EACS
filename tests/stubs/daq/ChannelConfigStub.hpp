//
// Created by matteof on 7/23/20.
//

#ifndef EACS_CHANNELCONFIGSTUB_HPP
#define EACS_CHANNELCONFIGSTUB_HPP

#include <DIMParamList.h>
#include <Signals.hpp>

using namespace ntof::dim;

enum ErrorCode
{
    CMD_UNKNOWN = 32,
    CMD_ERR = 33,
    CMD_EMPTY = 34
};

class ChannelConfigStubHandler : public DIMParamListHandler
{
    int parameterChanged(std::vector<DIMData> &settingsChanged,
                         const DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;
};

class ChannelConfigStub : public DIMParamList
{
public:
    typedef ntof::utils::signal<void()> ConfigureSignal;

    enum Params
    {
        IS_CHANNEL_ENABLED = 1,
        DETECTOR_TYPE,
        DETECTOR_ID,
        MODULE_TYPE,
        CHANNEL_ID,
        MODULE_ID,
        CHASSIS_ID,
        STREAM_ID,
        SAMPLE_RATE,
        SAMPLE_SIZE,
        FULL_SCALE,
        DELAY_TIME,
        THRESHOLD,
        THRESHOLD_SIGN,
        ZSP_START,
        OFFSET,
        PRE_SAMPLES,
        POST_SAMPLES,
        CLOCK_STATE,
        INPUT_IMPEDANCE
    };

    enum ParamsCalib
    {
        CALIBRATED_FULL_SCALE = 0,
        CALIBRATED_OFFSET,
        CALIBRATED_THRESHOLD
    };

    explicit ChannelConfigStub(const std::string &hostname,
                               uint32_t cardNumber,
                               uint32_t channelNumber);

    void setConfigured(bool value);
    bool isConfigured();

    ConfigureSignal configureSignal;

protected:
    void commandReceived(DIMCmd &cmd) override;

    void updateCalibration();

    DIMDataSet m_calibService;

    ChannelConfigStubHandler m_handler; //!< Timing handler

    std::string m_hostname;
    uint32_t m_cardNumber;
    uint32_t m_channelNumber;

    bool m_isConfigured;
};

#endif // EACS_CHANNELCONFIGSTUB_HPP
