/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-07T15:56:55+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "NTOFStub.hpp"

#include <cassert>

#include "Config.hpp"

using namespace ntof::eacs;
using namespace stub;

NTOFStub::NTOFStub()
{
    // Init daq stub
    const std::map<int32_t, std::string> &daqsList = Config::instance().getDaq();
    for (const auto &daq : daqsList)
    {
        int32_t chassisId = daq.first;
        const std::string &hostname = daq.second;

        DaqStubPtr daqStub(new DaqStub(hostname, chassisId));
        daqStubs.insert(
            std::pair<std::string, DaqStubPtr>(hostname, std::move(daqStub)));
    }

    const Config::HighVoltageMap &hvMap =
        Config::instance().getHighVoltageConfig();
    for (const auto &hvIt : hvMap)
    {
        std::unique_ptr<HVCardStub> &card = hvStubs[hvIt.first];
        if (!card)
        {
            card.reset(new HVCardStub(hvIt.first));
        }
        for (const auto &chanIt : hvIt.second)
        {
            card->addChannel(chanIt, false, "CHAN" + std::to_string(chanIt),
                             0.5, 15);
        }
    }

    timing.reset(new TimingStub());
    addh.reset(new AddhStub());
    rfm.reset(new RFMStub());
    filterStation.reset(new FilterStationStub());

    // Wait for some service to be started
    DimCurrentInfo tmWaiter(Config::instance().getTimeMachineName().c_str(),
                            const_cast<char *>(""));
    assert(tmWaiter.getData() != nullptr);
    DimCurrentInfo addhWaiter("ADDH/Events", const_cast<char *>(""));
    assert(addhWaiter.getData() != nullptr);
    DimCurrentInfo rfmWaiter("MERGER", const_cast<char *>(""));
    assert(rfmWaiter.getData() != nullptr);
    DimCurrentInfo filterWaiter("FilterStation", const_cast<char *>(""));
    assert(filterWaiter.getData() != nullptr);
}

NTOFStub::~NTOFStub()
{
    filterStation.reset();
    rfm.reset();
    addh.reset();
    timing.reset();
    daqStubs.clear();
}