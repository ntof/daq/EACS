/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-10T21:51:58+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "AddhStub.hpp"

#include <fstream>

#include <DaqTypes.h>
#include <easylogging++.h>

using namespace ntof::dim;

namespace stub {

AddhStub::AddhStub() : DIMSuperCommand("ADDH/Command")
{
    resetState(true);
    m_events.reset(new DIMXMLService("ADDH/Events"));
}

void AddhStub::resetState(bool restart)
{
    if (restart)
    {
        m_state.reset(new DIMState("ADDH/State"));
        m_state->addStateValue(1, "STARTED", false);
        m_state->setValue(1);
    }
    else
    {
        m_state.reset();
    }
}

void AddhStub::addWarning(int32_t code, const std::string &msg)
{
    m_state->addWarning(code, msg);
}

void AddhStub::removeWarning(int32_t code)
{
    m_state->removeWarning(code);
}

void AddhStub::commandReceived(ntof::dim::DIMCmd &cmd)
{
    pugi::xml_node parent = cmd.getData();

    const std::string name = parent.attribute("name").as_string();
    if (name == "clear")
    {
        m_state->clearWarnings();
        setOk(cmd.getKey(), std::string("ok"));
    }
    else if (name == "reset")
    {
        m_state->clearWarnings();
        m_state->clearErrors();
        setOk(cmd.getKey(), std::string("ok"));
    }
    else if (name == "write")
    {
        int32_t runNumber = parent.attribute("runNumber").as_int(-1);
        int32_t timingEvent = parent.attribute("timingEvent").as_int(-1);
        std::string filePath = parent.attribute("filePath").as_string("");

        std::size_t fileSize = writeFile(filePath);
        if (fileSize != 0)
        {
            sendEvent(runNumber, timingEvent, filePath, fileSize);
        }
        setOk(cmd.getKey(), std::string("ok"));
    }
    else
    {
        setError(cmd.getKey(), 1, "Parent node is empty");
    }
}

std::size_t AddhStub::writeFile(const std::string &filePath)
{
    std::ofstream out;

    out.open(filePath.c_str(), std::ios_base::out | std::ios_base::in);
    if (!out.good())
        out.open(filePath.c_str(), std::ios_base::out);
    if (!out.good())
    {
        m_state->setError(
            WRITE_ERR, "ADDH writer internal error : unable to open the file.");
        return 0;
    }

    out.seekp(0, std::ios_base::end);
    {
        AdditionalDataHeader hdr;
        out << hdr; /* dump an empty header */
    }

    size_t fileSize = out.tellp();
    LOG(INFO) << "[AddhStub] content size: " << fileSize;

    if (!out.good())
    {
        m_state->setError(WRITE_ERR, "Failed to write ADDH");
    }

    out.close();
    return fileSize;
}

void AddhStub::sendEvent(int32_t runNumber,
                         int32_t timingEvent,
                         const std::string &filePath,
                         std::size_t fileSize)
{
    // Build XML data to be sent
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("addh");
    pugi::xml_node event = root.append_child("event");
    event.append_attribute("runNumber").set_value(runNumber);
    event.append_attribute("timingEvent").set_value(timingEvent);
    event.append_attribute("filePath").set_value(filePath.c_str());
    event.append_attribute("fileSize").set_value(fileSize);

    /* send event */
    m_events->setData(doc);
}

} // namespace stub
