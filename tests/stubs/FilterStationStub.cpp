/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-07T14:03:17+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "FilterStationStub.hpp"

#include <LockRef.hpp>
#include <NTOFException.h>
#include <NTOFLogging.hpp>

using namespace stub;
using namespace ntof::dim;
using namespace ntof::utils;

const std::size_t FilterStationStub::FiltersCount = 8;

FilterStationStub::FilterStationStub() :
    m_worker("FilterStationStub"),
    m_moveDuration(10), // quite fast by default
    m_filters("FilterStation")
{
    m_filters.addParameter(AirPressure, "Air Pressure", "Bar", float(10),
                           AddMode::CREATE, false);

    DIMEnum filterValues;
    filterValues.addItem(PositionUnknown, "UNKNOWN");
    filterValues.addItem(PositionIn, "IN");
    filterValues.addItem(PositionOut, "OUT");
    filterValues.addItem(PositionMoving, "MOVING");
    filterValues.addItem(PositionInInterlocked, "IN_INTERLOCKED");
    filterValues.addItem(PositionOutInterlocked, "OUT_INTERLOCKED");
    filterValues.addItem(PositionMovingInterlocked, "MOVING_INTERLOCKED");
    filterValues.setValue(PositionOut);
    for (std::size_t i = 0; i < FiltersCount; ++i)
    {
        m_filters.addParameter(i + FilterFirst,
                               "Filter " + std::to_string(i + 1), "",
                               filterValues, AddMode::CREATE, false);
    }
    m_filters.setHandler(this);
}

FilterStationStub::~FilterStationStub()
{
    m_worker.stop();
}

void FilterStationStub::setAirPressure(float bar)
{
    m_filters.setValue(AirPressure, bar);
}

void FilterStationStub::setMoveDuration(std::chrono::milliseconds duration)
{
    m_moveDuration = duration;
}

int FilterStationStub::parameterChanged(DIMData::List &settingsChanged,
                                        const DIMParamList & /*list*/,
                                        int & /*errCode*/,
                                        std::string & /*errMsg*/)
{
    bool modified = false;
    std::vector<FilterPosition> positions;
    positions.reserve(8);

    for (std::size_t i = FilterFirst; i <= FilterLast; ++i)
    {
        positions.push_back(FilterPosition(
            m_filters.lockParameterAt(i)->getEnumValue().getValue()));
    }

    for (const DIMData::List::value_type &v : settingsChanged)
    {
        if (v.getIndex() >= FilterFirst && v.getIndex() <= FilterLast)
        {
            FilterPosition p = FilterPosition(v.getEnumValue().getValue());
            std::size_t index = v.getIndex() - FilterFirst;
            if (positions[index] != p)
            {
                positions[index] = p;
                modified = true;
            }
            else
            {
                LOG(ERROR)
                    << "[FilterStationStub] not moving filter " << (index + 1);
            }
        }
    }

    if (modified)
    {
        try
        {
            m_worker.post([this, positions]() {
                startMove(positions);
                std::this_thread::sleep_for(m_moveDuration);
                endMove(positions);
            });
        }
        catch (ntof::NTOFException &ex)
        {
            LOG(ERROR) << "[FilterStationStub] failed to queue task: "
                       << ex.getMessage();
        }
    }
    settingsChanged.clear(); // changes are async, will be done by the worker
    return 0;
}

void FilterStationStub::startMove(const std::vector<FilterPosition> &pos)
{
    bool updated = false;
    for (std::size_t i = 0; i < FiltersCount; ++i)
    {
        LockRef<DIMData> param(m_filters.lockParameterAt(FilterFirst + i));
        if (param->getEnumValue().getValue() != pos[i])
        {
            updated = true;
            param->getEnumValue().setValue(PositionMoving);
            LOG(INFO) << "[FilterStation] moving filter " << (i + 1);
        }
    }
    if (updated)
        m_filters.updateList();
}

void FilterStationStub::endMove(const std::vector<FilterPosition> &pos)
{
    bool updated = false;
    for (std::size_t i = 0; i < FiltersCount; ++i)
    {
        LockRef<DIMData> param(m_filters.lockParameterAt(FilterFirst + i));
        if (param->getEnumValue().getValue() == PositionMoving)
        {
            updated = true;
            param->getEnumValue().setValue(pos[i]);
            LOG(INFO) << "[FilterStation] filter " << (i + 1) << " moved "
                      << param->getEnumValue().getName();
        }
    }
    if (updated)
        m_filters.updateList();
}

FilterStationStub::FilterPosition FilterStationStub::getFilterPosition(
    std::size_t index)
{
    return FilterPosition(m_filters.lockParameterAt(FilterFirst + index)
                              ->getEnumValue()
                              .getValue());
}
