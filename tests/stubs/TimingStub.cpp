//
// Created by matteof on 7/13/20.
//

#include "TimingStub.hpp"

#include <chrono>

#include <DIMData.h>
#include <DIMUtils.hpp>
#include <NTOFLogging.hpp>
#include <easylogging++.h>

#include "Config.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;
using std::chrono::steady_clock;

TimingStub::TimingStub() : m_eventNumber(0), m_isGeneratorActive(false)
{
    m_timingEvents.reset(
        new DIMXMLService(Config::instance().getTimeMachineName() + "/event"));
    m_timing.reset(new DIMParamList(Config::instance().getTimeMachineName()));
    m_timing->setHandler(this);
    m_bctEvents.reset(new DIMDataSet(Config::instance().getBctServiceName()));

    DIMEnum mode;
    mode.addItem(Mode::DISABLED, "DISABLED");
    mode.addItem(Mode::AUTOMATIC, "AUTOMATIC");
    mode.addItem(Mode::CALIBRATION, "CALIBRATION");
    mode.setValue(Mode::DISABLED);
    m_timing->addParameter(Params::MODE, "mode", "", mode, AddMode::CREATE,
                           false);
    m_timing->addParameter(Params::CALIB_TRIGGER_REPEAT,
                           "Calibration trigger repeat", "", int32_t(0),
                           AddMode::CREATE, false);
    m_timing->addParameter(Params::CALIB_TRIGGER_PERIOD,
                           "Calibration trigger period", "", int32_t(0),
                           AddMode::CREATE, false);
    m_timing->addParameter(Params::CALIB_TRIGGER_PAUSE,
                           "Calibration trigger pause", "", int32_t(0),
                           AddMode::CREATE, false);
    m_timing->addParameter(Params::EVENT_NUMBER, "Event number", "",
                           int64_t(-1), AddMode::CREATE, false);

    LOG(INFO)
        << ntof::log::debug()
        << "[TimingStub] init : " << m_timing->getParameterAt(Params::MODE);

    DIMEnum outMode;
    outMode.addItem(EnableType::DISABLED, "DISABLED");
    outMode.addItem(EnableType::ENABLED, "ENABLED");
    outMode.setValue(EnableType::ENABLED);
    m_timing->addParameter(Params::CALIB_OUT_ENABLED, "CALIBRATION out enabled",
                           "", outMode, AddMode::CREATE, false);
    m_timing->addParameter(Params::PARASITIC_OUT_ENABLED,
                           "PARASITIC out enabled", "", outMode,
                           AddMode::CREATE, false);
    m_timing->addParameter(Params::PRIMARY_OUT_ENABLED, "PRIMARY out enabled",
                           "", outMode, AddMode::CREATE, false);
    m_timing->updateList();

    m_bctEvents->addData<int64_t>(1, "cycleStamp", "", -1, AddMode::CREATE,
                                  false);
    m_bctEvents->addData<float>(2, "totalIntensitySingle", "", 0,
                                AddMode::CREATE, true);

    LOG(INFO) << "Timing Stub initialized";
}

TimingStub::~TimingStub()
{
    stop();
    DIMLockGuard guard;
    m_timing.reset();
    m_bctEvents.reset();
    m_timingEvents.reset();
}

void TimingStub::stop()
{
    if (m_thread)
    {
        m_timing->lockParameterAt(Params::MODE)
            ->getEnumValue()
            .setValue(Mode::DISABLED);

        {
            std::lock_guard<std::mutex> lock(m_lock);
            m_cond.notify_all();
        }
        m_thread->join();
        m_thread.reset();
    }
}

TimingStub::Mode TimingStub::getMode()
{
    return Mode(
        m_timing->lockParameterAt(Params::MODE)->getEnumValue().getValue());
}

void TimingStub::thread_func()
{
    Mode mode;
    std::unique_lock<std::mutex> lock(m_lock);
    m_eventNumber = 0;

    LOG(INFO) << "[TimingStub] thread started";
    for (mode = getMode(); mode != Mode::DISABLED; mode = getMode())
    {
        std::chrono::milliseconds delay;
        if (mode == Mode::CALIBRATION)
            delay = std::chrono::milliseconds(
                m_timing->getIntValue(Params::CALIB_TRIGGER_PERIOD));
        else
            delay = std::chrono::milliseconds(1200);

        if (m_cond.wait_for(lock, delay) != std::cv_status::timeout)
            continue;

        int64_t cyclestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(
                                 steady_clock::now().time_since_epoch())
                                 .count();

        lock.unlock();
        timingEvent(cyclestamp, 0, incEventNumber());
        lock.lock();

        if (m_cond.wait_for(lock, std::chrono::milliseconds(30)) !=
            std::cv_status::timeout)
            continue;

        lock.unlock();
        sendBctEvent(cyclestamp, (mode == Mode::CALIBRATION) ? 0 : 100000);
        lock.lock();
        /* trigger Pause/Repeat not implemented */
    }
    LOG(INFO) << "[TimingStub] thread end";
}

int TimingStub::parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const DIMParamList & /*list*/,
                                 int & /*errCode*/,
                                 std::string & /*errMsg*/)
{
    LOG(INFO) << ntof::log::debug()
              << "[TimingStub] config changed: " << settingsChanged;
    stop();
    for (const DIMData &data : settingsChanged)
    {
        if (data.getIndex() == Params::MODE &&
            data.getEnumValue().getValue() != Mode::DISABLED)
        {
            // pre-update for the thread
            m_timing->lockParameterAt(Params::MODE)
                ->getEnumValue()
                .setValue(data.getEnumValue().getValue());
            m_thread.reset(new std::thread(&TimingStub::thread_func, this));
            break;
        }
    }
    return 0;
}

void TimingStub::sendTimingEvent(int64_t cyclestamp, int32_t periodNumber)
{
    timingEvent(cyclestamp, periodNumber, incEventNumber());
}

void TimingStub::timingEvent(int64_t cyclestamp,
                             int32_t periodNumber,
                             EventNumber eventNumber)
{
    pugi::xml_document doc;
    pugi::xml_node timingNode = doc.append_child("timing");
    pugi::xml_node eventNode = timingNode.append_child("event");
    eventNode.append_attribute("name").set_value("name");
    eventNode.append_attribute("timestamp").set_value(std::time(0));
    eventNode.append_attribute("cyclestamp").set_value(cyclestamp);
    eventNode.append_attribute("periodNB").set_value(periodNumber);
    eventNode.append_attribute("dest").set_value("dest");
    eventNode.append_attribute("dest2").set_value("dest2");
    eventNode.append_attribute("user").set_value("user");
    eventNode.append_attribute("eventNumber").set_value(eventNumber);
    eventNode.append_attribute("lsaCycle").set_value("CALIBRATION");
    m_timingEvents->setData(doc);
    LOG(INFO) << "[TimingStub] timing event: eventNumber:" << eventNumber;
}

void TimingStub::setEventNumber(EventNumber num)
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_eventNumber = num - 1;
}

EventNumber TimingStub::incEventNumber()
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_timing->setValue<int64_t>(Params::EVENT_NUMBER, ++m_eventNumber);
    return m_eventNumber;
}

void TimingStub::sendBctEvent(int64_t cyclestamp, float intensity)
{
    m_bctEvents->setValue(1, cyclestamp, false);
    m_bctEvents->setValue(2, intensity);
}

const DIMParamList *TimingStub::getTimingService() const
{
    return m_timing.get();
}
