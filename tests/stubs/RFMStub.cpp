/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-12T09:58:02+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "RFMStub.hpp"

#include <DIMCmd.h>
#include <easylogging++.h>

#include "../test_helpers.hpp"

using namespace stub;
using namespace ntof::dim;
using namespace ntof::eacs;

RFMStub::RunInfo::RunInfo() : approved(false), standalone(false) {}

RFMStub::RFMStub() :
    DIMSuperCommand("MERGER"), m_runNumber(InvalidRunNumber), m_runStarted(false)
{
    m_currentSvc.reset(new DIMParamList("MERGER/Current"));
    m_currentSvc->addParameter(1, "runNumber", "", uint32_t(0), AddMode::CREATE,
                               true);
    // Do not care about other parameters
}

RFMStub::~RFMStub()
{
    m_currentSvc.reset();
}

void RFMStub::setCurrentRunNumber(ntof::eacs::RunNumber runNumber)
{
    m_currentSvc->setValue<uint32_t>(1, runNumber, true);
    m_runNumber = runNumber;
}

void RFMStub::commandReceived(ntof::dim::DIMCmd &cmd)
{
    pugi::xml_node parent = cmd.getData();

    try
    {
        const std::string name = parent.attribute("name").as_string();
        if (name == "event")
        {
            std::lock_guard<std::mutex> lock(m_lock);
            RunNumber runNumber =
                parent.attribute("runNumber").as_uint(InvalidRunNumber);
            EventNumber timingEvent =
                parent.attribute("timingEvent").as_uint(InvalidEventNumber);
            EventNumber validatedEvent =
                parent.attribute("validatedEvent").as_uint(InvalidEventNumber);

            uint64_t fileSize = parent.attribute("fileSize").as_ullong(0);

            EQ(true, runNumber != InvalidRunNumber);
            EQ(m_runNumber, runNumber);
            EQ(true, timingEvent != InvalidEventNumber);
            EQ(true, validatedEvent != InvalidEventNumber);
            EQ(true, m_runStarted);

            EQ(true, m_events.count(validatedEvent) == 0);
            m_events[validatedEvent] = {timingEvent, validatedEvent, fileSize};
            setOk(cmd.getKey(), std::string("ok"));
        }
        else if (name == "runStop")
        {
            std::lock_guard<std::mutex> lock(m_lock);
            RunNumber runNumber =
                parent.attribute("runNumber").as_uint(InvalidRunNumber);

            EQ(true, runNumber != InvalidRunNumber);
            EQ(m_runNumber, runNumber);
            m_runStarted = false;
            setOk(cmd.getKey(), std::string("ok"));
        }
        else if (name == "runStart")
        {
            std::lock_guard<std::mutex> lock(m_lock);
            RunNumber runNumber =
                parent.attribute("runNumber").as_uint(InvalidRunNumber);
            m_info.experiment = parent.attribute("experiment").as_string();
            m_info.approved = parent.attribute("approved").as_bool(false);
            m_info.standalone = parent.attribute("standalone").as_bool(false);

            for (const pugi::xml_node &elt : parent.children("daq"))
            {
                int daq = elt.attribute("crateId").as_int(-1);
                EQ(true, daq != -1);
                m_info.daqs.push_back(daq);
            }

            EQ(true, runNumber != InvalidRunNumber);
            EQ(false, m_runStarted);
            EQ(false, m_info.experiment.empty());
            EQ(true, m_info.approved);
            EQ(false, m_info.standalone);
            m_runNumber = runNumber;
            m_runStarted = true;
            m_events.clear();
            setOk(cmd.getKey(), std::string("ok"));
        }
        else if (name == "fileIgnore" || name == "runDelete")
        {
            LOG(INFO) << "[RFMStub] not implemented: " << name;
        }
        else
        {
            LOG(ERROR) << "[RFMStub] unknown command received";
            setError(cmd.getKey(), 1, "Unknown command");
        }
    }
    catch (CppUnit::Exception &ex)
    {
        printException(ex);
        setError(cmd.getKey(), 1, ex.what());
        // won't save you on that one
        throw;
    }
}

RFMStub::EventsMap RFMStub::events() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_events;
}

ntof::eacs::RunNumber RFMStub::runNumber() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_runNumber;
}

bool RFMStub::isStarted() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_runStarted;
}

RFMStub::RunInfo RFMStub::info() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_info;
}
