/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-17T14:36:57+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "HVCardStub.hpp"

#include <NTOFLogging.hpp>

using namespace stub;
using ntof::dim::AddMode;

HVCardStub::HVCardStub(uint32_t index) :
    DIMDataSet("HV_" + std::to_string(index) + "/Acquisition"),
    m_boardInfo("HV_" + std::to_string(index) + "/BoardInformation")
{
    addData(0, "acqStamp", "", int64_t(0), AddMode::CREATE, false);
    addData(1, "cycleStamp", "", int64_t(0), AddMode::CREATE, false);
    addData(2, "cycleName", "", std::string(), AddMode::CREATE);

    ntof::dim::DIMDataSet &bi = m_boardInfo;
    bi.addData(0, "acqStamp", "", int64_t(0), AddMode::CREATE, false);
    bi.addData(1, "cycleStamp", "", int64_t(0), AddMode::CREATE, false);
    bi.addData(2, "cycleName", "", std::string(), AddMode::CREATE, false);
    bi.addData(3, "description", "", std::string(" 12 Ch Neg. 3KV 10uA "),
               AddMode::CREATE, false);
    bi.addData(4, "firmware", "", std::string("3.0"), AddMode::CREATE, false);
    bi.addData(5, "model", "", std::string("A1821H"), AddMode::CREATE, false);
    bi.addData(6, "serialNumber", "", int32_t(841), AddMode::CREATE);

    LOG(INFO) << "[HVCardStub]: hv on HV_" << index;
}

void HVCardStub::addChannel(uint32_t index,
                            bool enabled,
                            const std::string &name,
                            float iset,
                            float vset)
{
    const std::string prefix = "chan" + std::to_string(index);
    addData(prefix + "caenErrorCode", "", int32_t(0), AddMode::CREATE, false);
    addData(prefix + "caenErrorMessage", "", std::string(), AddMode::CREATE,
            false);
    addData(prefix + "channelName", "", name, AddMode::CREATE, false);
    addData(prefix + "channelStatusBitfield", "", int32_t(0), AddMode::CREATE,
            false);
    addData(prefix + "i0set", "", iset, AddMode::CREATE, false);
    addData(prefix + "i0set_max", "", iset + 1, AddMode::CREATE, false);
    addData(prefix + "i0set_min", "", float(0), AddMode::CREATE, false);
    addData(prefix + "imon", "", iset, AddMode::CREATE, false);
    addData(prefix + "pw", "", int32_t(enabled ? ON : OFF), AddMode::CREATE,
            false);
    addData(prefix + "rdwn", "", float(15), AddMode::CREATE, false);
    addData(prefix + "rdwn_max", "", float(500), AddMode::CREATE, false);
    addData(prefix + "rdwn_min", "", float(1), AddMode::CREATE, false);
    addData(prefix + "rup", "", float(1), AddMode::CREATE, false);
    addData(prefix + "rup_max", "", float(500), AddMode::CREATE, false);
    addData(prefix + "rup_min", "", float(1), AddMode::CREATE, false);
    addData(prefix + "svmax", "", float(vset + 1), AddMode::CREATE, false);
    addData(prefix + "svmax_max", "", float(3000), AddMode::CREATE, false);
    addData(prefix + "svmax_min", "", float(0), AddMode::CREATE, false);
    addData(prefix + "trip", "", float(2), AddMode::CREATE, false);
    addData(prefix + "trip_max", "", float(1000), AddMode::CREATE, false);
    addData(prefix + "trip_min", "", float(0), AddMode::CREATE, false);
    addData(prefix + "v0set", "", float(vset), AddMode::CREATE, false);
    addData(prefix + "v0set_max", "", float(vset + 1), AddMode::CREATE, false);
    addData(prefix + "v0set_min", "", float(0), AddMode::CREATE, false);
    addData(prefix + "vmon", "", float(vset), AddMode::CREATE);
}

void HVCardStub::setPower(uint32_t index, bool enabled)
{
    lockDataAt("chan" + std::to_string(index) + "pw")
        ->setValue(int32_t(enabled ? ON : OFF));
    updateData();
}

void HVCardStub::setStatusBitField(uint32_t index, int32_t statusBitField)
{
    lockDataAt("chan" + std::to_string(index) + "channelStatusBitfield")
        ->setValue(statusBitField);
    updateData();
}

void HVCardStub::setError(uint32_t index,
                          int32_t error,
                          const std::string &message)
{
    lockDataAt("chan" + std::to_string(index) + "caenErrorCode")->setValue(error);
    lockDataAt("chan" + std::to_string(index) + "caenErrorMessage")
        ->setValue(message);
    updateData();
}
