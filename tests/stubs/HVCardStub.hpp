/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-17T14:34:35+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef HVCARDSTUB_HPP__
#define HVCARDSTUB_HPP__

#include <DIMDataSet.h>

namespace stub {

class HVCardStub : public ntof::dim::DIMDataSet
{
public:
    explicit HVCardStub(uint32_t index);

    enum PowerMode
    {
        UNKNOWN = 0,
        ON = 1,
        OFF = 2
    };

    void addChannel(uint32_t index,
                    bool enabled,
                    const std::string &name,
                    float iset,
                    float vset);
    void setPower(uint32_t index, bool enabled);

    void setStatusBitField(uint32_t index, int32_t statusBitField);
    void setError(uint32_t index, int32_t error, const std::string &message);

protected:
    ntof::dim::DIMDataSet m_boardInfo;
};

} // namespace stub

#endif
