/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-07T13:56:26+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef FILTERSTATIONSTUB_HPP__
#define FILTERSTATIONSTUB_HPP__

#include <chrono>
#include <cstdint>
#include <string>
#include <vector>

#include <DIMParamList.h>
#include <DIMState.h>
#include <Worker.hpp>

namespace stub {

class FilterStationStub : public ntof::dim::DIMParamListHandler
{
public:
    FilterStationStub();
    ~FilterStationStub();

    enum Params
    {
        AirPressure = 1,
        FilterFirst = 2,
        FilterLast = FilterFirst + 7
    };

    enum FilterPosition
    {
        PositionUnknown = 0,
        PositionIn = 1,
        PositionOut = 2,
        PositionMoving = 3,
        PositionInInterlocked = 4,
        PositionOutInterlocked = 5,
        PositionMovingInterlocked = 6
    };

    static const std::size_t FiltersCount;

    /**
     * @brief configure the stub movement duration
     * @param[in] duration duration in ms
     */
    void setMoveDuration(std::chrono::milliseconds duration);

    /**
     * @brief modify the air pressure
     * @param[in] bar air pressure in bars
     */
    void setAirPressure(float bar);

    FilterPosition getFilterPosition(std::size_t index);

    int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                         const ntof::dim::DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;

protected:
    virtual void startMove(const std::vector<FilterPosition> &pos);
    virtual void endMove(const std::vector<FilterPosition> &pos);

    ntof::utils::Worker m_worker;
    std::chrono::milliseconds m_moveDuration;
    ntof::dim::DIMParamList m_filters;
};

} // namespace stub

#endif
