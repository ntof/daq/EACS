//
// Created by matteof on 7/13/20.
//

#ifndef EACS_TIMINGSTUB_HPP
#define EACS_TIMINGSTUB_HPP

#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>

#include <DIMParamList.h>
#include <DIMXMLService.h>

#include "EACSTypes.hpp"

using namespace ntof::dim;
using namespace ntof::eacs;

class TimingStub : public DIMParamListHandler
{
public:
    enum Params
    {
        MODE = 1,
        CALIB_TRIGGER_REPEAT,
        CALIB_TRIGGER_PERIOD,
        CALIB_TRIGGER_PAUSE,
        EVENT_NUMBER,
        CALIB_OUT_ENABLED,
        PARASITIC_OUT_ENABLED,
        PRIMARY_OUT_ENABLED
    };

    enum Mode
    {
        DISABLED = 0,
        AUTOMATIC = 1,
        CALIBRATION = 2
    };

    TimingStub();
    ~TimingStub();

    void stop();

    virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const ntof::dim::DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) override;

    void sendTimingEvent(int64_t cyclestamp, int32_t periodNumber);

    void sendBctEvent(int64_t cyclestamp, float intensity);

    const DIMParamList *getTimingService() const;

    void setEventNumber(EventNumber num);

    EventNumber incEventNumber();

protected:
    void timingEvent(int64_t cyclestamp,
                     int32_t periodNumber,
                     EventNumber eventNumber);
    Mode getMode();

    void thread_func();

    EventNumber m_eventNumber;

    std::mutex m_lock;
    std::condition_variable m_cond;
    std::unique_ptr<std::thread> m_thread;
    std::unique_ptr<DIMXMLService> m_timingEvents;
    std::unique_ptr<DIMParamList> m_timing;
    std::unique_ptr<DIMDataSet> m_bctEvents;

    bool m_isGeneratorActive;
};

#endif // EACS_TIMINGSTUB_HPP
