/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-10T21:47:58+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef ADDHSTUB_HPP__
#define ADDHSTUB_HPP__

#include <cstdint>
#include <string>

#include <DIMParamList.h>
#include <DIMState.h>
#include <DIMSuperCommand.h>
#include <DIMXMLService.h>

#include "EACSTypes.hpp"

namespace stub {

class AddhStub : public ntof::dim::DIMSuperCommand
{
public:
    enum ErrorsCodes
    {
        INTERNAL_ERR = 2,
        ACK_ERR = 30,
        CMD_UNKNOWN = 32,
        CMD_ERR = 33,
        CMD_EMPTY = 34,
        WRITE_ERR = 50,
    };

    AddhStub();

    void resetState(bool restart = false);
    void addWarning(int32_t code, const std::string &msg);
    void removeWarning(int32_t code);

    void commandReceived(ntof::dim::DIMCmd &cmd) override;

protected:
    std::size_t writeFile(const std::string &filePath);
    void sendEvent(int32_t runNumber,
                   int32_t timingEvent,
                   const std::string &filePath,
                   std::size_t fileSize);

    std::unique_ptr<ntof::dim::DIMState> m_state;
    std::unique_ptr<ntof::dim::DIMXMLService> m_events;
};

} // namespace stub

#endif
