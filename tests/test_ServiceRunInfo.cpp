/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-29T10:43:48+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <DIMData.h>
#include <DIMException.h>
#include <DIMParamListClient.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Eacs.hpp"
#include "db/SQLiteDatabase.hpp"
#include "local-config.h"
#include "misc.h"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

#include <dis.hxx>

using namespace ntof::eacs;
using namespace ntof::utils;
using ntof::dim::DIMData;
using ntof::dim::DIMException;

class TestServiceRunInfo : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestServiceRunInfo);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override
    {
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        Eacs::instance();
    }

    void tearDown() override
    {
        Eacs::destroy();
        m_dim.reset();
    }

    void simple()
    {
        // Prepare DB
        SQLiteDatabase &db = dynamic_cast<SQLiteDatabase &>(
            Database::instance());
        SignalWaiter dbUpdateWaiter;
        dbUpdateWaiter.listen(db.dbUpdatedSignal);

        std::time_t now = std::time(nullptr);
        RunInfo info;
        info.runNumber = 901234;
        info.title = "run title";
        info.description = "sample run";
        info.experiment = "myExperiment";
        info.detectorsSetupId = 1111;
        info.materialsSetupId = 2222;
        db.createRun(info, now);
        EQ(true, dbUpdateWaiter.wait(1));

        Eacs &eacs(Eacs::instance());
        eacs.initServices();
        EQ(true, dbUpdateWaiter.wait(2)); // Created run as crashed
        RunInfoWaiter infoWaiter;

        EQ(true, infoWaiter.wait([&info](DIMData::List &data) {
            // Check RunInfo is publishing the correct data
            return info.runNumber ==
                data.at(EacsRunInfo::RUN_NUMBER).getUIntValue() &&
                info.title == data.at(EacsRunInfo::TITLE).getStringValue() &&
                info.description ==
                data.at(EacsRunInfo::DESCRIPTION).getStringValue() &&
                info.experiment ==
                data.at(EacsRunInfo::EXPERIMENT).getStringValue() &&
                info.detectorsSetupId ==
                data.at(EacsRunInfo::DETECTORS_SETUP_ID).getLongValue() &&
                info.materialsSetupId ==
                data.at(EacsRunInfo::MATERIALS_SETUP_ID).getLongValue();
        }));

        // Send a Parameter modification
        ntof::dim::DIMParamListClient params("EACS/RunInfo");
        std::vector<DIMData> args;
        args.emplace_back(EacsRunInfo::TITLE, std::string("test_title"));
        args.emplace_back(EacsRunInfo::DESCRIPTION,
                          std::string("test_description"));
        params.sendParameters(args);

        EQ(true, infoWaiter.wait([&info](DIMData::List &data) {
            // Check RunInfo is publishing the correct data
            return "test_title" ==
                data.at(EacsRunInfo::TITLE).getStringValue() &&
                "test_description" ==
                data.at(EacsRunInfo::DESCRIPTION).getStringValue();
        }));

        // Run is not going, so DB values are not updated
        EQ(false, dbUpdateWaiter.wait(3));
        RunInfo runInfo;
        db.getRunInfo(info.runNumber, runInfo);
        EQ(std::string("run title"), runInfo.title);
        EQ(std::string("sample run"), runInfo.description);

        // Try to modify a read-only param
        args.emplace_back(EacsRunInfo::EXPERIMENT, std::string("exp"));
        CPPUNIT_ASSERT_THROW(params.sendParameters(args), DIMException);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestServiceRunInfo);
