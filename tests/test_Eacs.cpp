/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-10T15:43:26+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "db/SQLiteDatabase.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacs : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacs);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(minimal);
    CPPUNIT_TEST(checkRunInfoUpdate);
    CPPUNIT_TEST(nextRunNumberFromRFM);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        Eacs::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void simple()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        {
            DIMData::List params;
            params.emplace_back(TITLE, std::string("my test title"));
            params.emplace_back(DESCRIPTION, std::string("my test desc"));
            params.emplace_back(EXPERIMENT, std::string("test"));
            params.emplace_back(DETECTORS_SETUP_ID, int64_t(1));
            params.emplace_back(MATERIALS_SETUP_ID, int64_t(1));
            EQ(true, cli.setRunConfig(params));
        }

        {
            DIMData::List params;
            params.emplace_back(DaqChannelConfig::IS_CHANNEL_ENABLED, true);
            EQ(true, cli.setDaqConfig("ntofdaq-mtest01.cern.ch", 0, 0, params));
        }

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));
        checkRunStatus(900001, RunInfo::Ongoing, RunInfo::DataUnknown);
    }

    void minimal()
    {
        const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
        // Change config with minimal
        ntof::eacs::Config::load((dataDir / "config_minimal.xml").string(),
                                 (dataDir / "configMisc.xml").string());

        simple();

        // Reset config
        ntof::eacs::Config::load((dataDir / "config.xml").string(),
                                 (dataDir / "configMisc.xml").string());
    }

    void checkRunInfoUpdate()
    {
        simple();

        // Prepare DB
        SQLiteDatabase &db = dynamic_cast<SQLiteDatabase &>(
            Database::instance());

        // Test initial DB values
        RunInfo runInfo;
        db.getRunInfo(900001, runInfo);
        EQ(std::string("my test title"), runInfo.title);
        EQ(std::string("my test desc"), runInfo.description);

        SignalWaiter dbUpdateWaiter;
        dbUpdateWaiter.listen(db.dbUpdatedSignal);

        // Send a RunInfo Parameter modification
        ntof::dim::DIMParamListClient params("EACS/RunInfo");
        std::vector<DIMData> args;
        args.emplace_back(EacsRunInfo::TITLE, std::string("test_title_mod"));
        args.emplace_back(EacsRunInfo::DESCRIPTION,
                          std::string("test_description_mod"));
        params.sendParameters(args);

        // Check new values
        EQ(true, dbUpdateWaiter.wait(1));
        db.getRunInfo(900001, runInfo);
        EQ(std::string("test_title_mod"), runInfo.title);
        EQ(std::string("test_description_mod"), runInfo.description);
    }

    void nextRunNumberFromRFM()
    {
        RunNumber rfmLastRunNumber = 910001;
        m_stub->rfm->setCurrentRunNumber(rfmLastRunNumber);

        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        {
            DIMData::List params;
            params.emplace_back(TITLE, std::string("my test title"));
            params.emplace_back(DESCRIPTION, std::string("my test desc"));
            params.emplace_back(EXPERIMENT, std::string("test"));
            params.emplace_back(DETECTORS_SETUP_ID, int64_t(1));
            params.emplace_back(MATERIALS_SETUP_ID, int64_t(1));
            EQ(true, cli.setRunConfig(params));
        }

        {
            DIMData::List params;
            params.emplace_back(DaqChannelConfig::IS_CHANNEL_ENABLED, true);
            EQ(true, cli.setDaqConfig("ntofdaq-mtest01.cern.ch", 0, 0, params));
        }

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        checkRunStatus(rfmLastRunNumber + 1, RunInfo::Ongoing,
                       RunInfo::DataUnknown);

        RunConfigWaiter configWaiter;
        // Check RunConfig is publishing the correct data
        // RunConfig is max(latest run number, rfmCurrentRunNumber) + 1
        EQ(true, configWaiter.wait([&rfmLastRunNumber](DIMData::List &data) {
            return (rfmLastRunNumber + 2) == // +2 because next run
                data.at(EacsRunInfo::RUN_NUMBER).getValue<uint32_t>();
        }));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacs);
