/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-10T09:42:12+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <DIMException.h>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "local-config.h"
#include "services/ServiceEvents.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;
using Params = ServiceEvents::Params;

class TestServiceEvents : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestServiceEvents);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(bufferSize);
    CPPUNIT_TEST(oldEvents);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override
    {
        Database::destroy();
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override { m_dim.reset(); }

    TimingEventDetails createTimingEvent(int64_t evtNumber,
                                         int64_t cycleStamp,
                                         int cycleNb)
    {
        TimingEventDetails event;
        event.evtNumber = evtNumber;
        event.evtType = "CALIBRATION";
        event.timeStamp = std::time(nullptr) * 1e9;
        event.cycleStamp = cycleStamp;
        event.cycleNb = cycleNb;
        return event;
    }

    EventValidator::ValidEvent createValidEvent(EventNumber eventNumber)
    {
        EventValidator::ValidEvent event;
        event.eventNumber = eventNumber;
        event.runNumber = 900000;
        return event;
    }

    void simple()
    {
        ServiceEvents events;
        EventsWaiter waiter;

        // Send Timing Event
        int64_t evtNumber = 100;
        TimingEventDetails e1 = createTimingEvent(evtNumber, 1, 1);
        events.onTimingEvent(e1);

        EQ(true,
           waiter.wait([](DIMData::List &data) { return data.size() == 1; }));

        DIMData::List event;
        CPPUNIT_ASSERT_NO_THROW(
            event = events.getDataAt(evtNumber).getNestedValue());
        EQ(uint32_t(evtNumber), event.at(Params::EVENT_NUMBER).getUIntValue());
        EQ(uint32_t(0), event.at(Params::VALIDATED_NUMBER).getUIntValue());
        EQ(std::string("CALIBRATION"),
           event.at(Params::BEAM_TYPE).getEnumValue().getName());
        DBL_EQ(double(0), (double) event.at(Params::PROTONS).getFloatValue(),
               0.1);
        EQ(int64_t(1), event.at(Params::CYCLESTAMP).getLongValue());
        EQ(int(1), event.at(Params::PERIOD_NB).getIntValue());

        // Send now BCT event
        float intensity = 1000.0;
        events.onBctEvent(e1, intensity);

        EQ(true, waiter.wait([&intensity](DIMData::List &data) {
            DIMData::List event = data.at(0).getNestedValue();
            for (const DIMData &d : event)
            {
                if (d.getIndex() == Params::PROTONS)
                {
                    return d.getValue<float>() == intensity;
                }
            }
            return false;
        }));

        // Send now Validated Event
        EventValidator::ValidEvent validEvent = createValidEvent(evtNumber);
        uint32_t extensionNumber = 2000;
        events.onValidatedEvent(validEvent, extensionNumber);

        EQ(true, waiter.wait([&extensionNumber](DIMData::List &data) {
            DIMData::List event = data.at(0).getNestedValue();
            for (const DIMData &d : event)
            {
                if (d.getIndex() == Params::VALIDATED_NUMBER)
                {
                    return d.getValue<uint32_t>() == extensionNumber;
                }
            }
            return false;
        }));

        // Send a new event
        evtNumber++;
        TimingEventDetails e2 = createTimingEvent(evtNumber, 2, 2);

        events.onTimingEvent(e2);

        EQ(true,
           waiter.wait([](DIMData::List &data) { return data.size() == 2; }));

        CPPUNIT_ASSERT_NO_THROW(
            event = events.getDataAt(evtNumber).getNestedValue());
        EQ(uint32_t(evtNumber), event.at(Params::EVENT_NUMBER).getUIntValue());
        EQ(int64_t(2), event.at(Params::CYCLESTAMP).getLongValue());
        EQ(int(2), event.at(Params::PERIOD_NB).getIntValue());
    }

    void bufferSize()
    {
        ServiceEvents events;
        EventsWaiter waiter;

        uint32_t bufferSize = Config::instance().getDimEventsCount();
        int64_t evtNumber = 100;
        for (uint32_t i = 0; i < bufferSize + 2; i++)
        {
            TimingEventDetails ev = createTimingEvent(evtNumber, 1, 1);
            events.onTimingEvent(ev);
            evtNumber++;
        }

        EQ(true, waiter.wait([&bufferSize](DIMData::List &data) {
            return data.size() == bufferSize;
        }));

        DIMData::List event;
        int64_t lastEventNumber = evtNumber - 1;
        CPPUNIT_ASSERT_NO_THROW(
            event = events.getDataAt(lastEventNumber).getNestedValue());
        EQ(uint32_t(lastEventNumber),
           event.at(Params::EVENT_NUMBER).getUIntValue());

        // Clear events
        events.clearEvents();

        EQ(true, waiter.wait([&bufferSize](DIMData::List &data) {
            return data.size() == 0;
        }));
    }

    /**
     * receiving updates on old events should be ignored
     */
    void oldEvents()
    {
        ServiceEvents events;
        uint32_t bufferSize = Config::instance().getDimEventsCount();
        const int64_t evtNumber = 100;

        { /* ensure that event "100" can be inserted */
            /* old event, should not crash */
            TimingEventDetails ev = createTimingEvent(evtNumber, 1, 1);
            events.onTimingEvent(ev);
            events.onBctEvent(ev, 1234);
            EQ(true, events.hasDataAt(evtNumber));

            EventValidator::ValidEvent validEvent = createValidEvent(evtNumber);
            events.onValidatedEvent(validEvent, 2000);
            EQ(true, events.hasDataAt(evtNumber));
        }

        for (uint32_t i = 0; i < bufferSize + 2; i++)
        {
            TimingEventDetails ev = createTimingEvent(evtNumber + i, 1, 1);
            events.onTimingEvent(ev);
        }

        EQ(int(bufferSize), events.getDataCount());

        { /* ensure that event "100" is being rejected (too old) */
            TimingEventDetails ev = createTimingEvent(evtNumber, 1, 1);
            events.onTimingEvent(ev);
            events.onBctEvent(ev, 1234);
            EQ(int(bufferSize), events.getDataCount());
            EQ(false, events.hasDataAt(evtNumber));

            EventValidator::ValidEvent validEvent = createValidEvent(evtNumber);
            events.onValidatedEvent(validEvent, 2000);
            EQ(int(bufferSize), events.getDataCount());
            EQ(false, events.hasDataAt(evtNumber));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestServiceEvents);
