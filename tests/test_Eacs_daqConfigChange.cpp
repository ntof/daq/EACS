/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-20T13:57:20+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "daq/DaqZSPConfig.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsDaqConfigChange : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsDaqConfigChange);
    CPPUNIT_TEST(daqConfig);
    CPPUNIT_TEST(zspConfig);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void daqConfig()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        {
            DIMData::List params;
            params.emplace_back(DaqChannelConfig::Params::IS_CHANNEL_ENABLED,
                                false);
            DIMParamListClient chanCli(
                "ntofdaq-mtest01.cern.ch/CARD0/CHANNEL0");
            chanCli.sendParameters(params);
        }

        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getEacsState() == EacsState(ERROR);
        }));
        // error action is asynchronuous
        waitFor([this]() {
            checkRunStatus(900001, RunInfo::Crashed, RunInfo::BadData);
            return true;
        });
    }

    void zspConfig()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        {
            DIMData::List params;
            params.emplace_back(DaqZSPConfig::MODE,
                                DIMEnum(ntof::eacs::ZSP::MULTIPLE_MASTER));
            params.emplace_back(DaqZSPConfig::CONFIGURATION, DIMData::List());
            DIMParamListClient chanCli(
                "ntofdaq-mtest01.cern.ch/ZeroSuppression");
            chanCli.sendParameters(params);
        }

        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getEacsState() == EacsState(ERROR);
        }));
        // error action is asynchronuous
        waitFor([this]() {
            checkRunStatus(900001, RunInfo::Crashed, RunInfo::BadData);
            return true;
        });
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsDaqConfigChange);
