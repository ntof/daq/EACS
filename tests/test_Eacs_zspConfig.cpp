/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-20T13:57:20+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <NTOFLogging.hpp>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "daq/DaqZSPConfig.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsDaqZSPConfig : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsDaqZSPConfig);
    CPPUNIT_TEST(configureZsp);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    DIMData::List getNode(const std::string &sn, uint32_t index)
    {
        return DIMData::List{
            DIMData(DaqZSPConfig::ChannelParams::SN, std::string(sn)),
            DIMData(DaqZSPConfig::ChannelParams::CHANNEL, uint32_t(index))};
    }

    void configureZsp()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        /* single master */
        {
            DIMData::List params;
            params.emplace_back(DaqZSPConfig::MODE,
                                DIMEnum(ntof::eacs::ZSP::SINGLE_MASTER));

            DIMData master(0, getNode("SPD-00001", 0));
            params.emplace_back(DaqZSPConfig::CONFIGURATION,
                                DIMData::List{master});
            EQ(true, cli.setZSPConfig("ntofdaq-mtest01.cern.ch", params));
        }

        /* multi-master */
        {
            DIMData::List params;
            params.emplace_back(DaqZSPConfig::MODE,
                                DIMEnum(ntof::eacs::ZSP::MULTIPLE_MASTER));

            DIMData master(0, getNode("SPD-00001", 0));
            master.addNestedData("slave", "", getNode("SPD-00001", 1));
            master.addNestedData("slave", "", getNode("SPD-00001", 2));
            params.emplace_back(DaqZSPConfig::CONFIGURATION,
                                DIMData::List{master});
            EQ(true, cli.setZSPConfig("ntofdaq-mtest01.cern.ch", params));
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsDaqZSPConfig);
