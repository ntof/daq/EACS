/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-10T15:43:26+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMDataSetClient.h>
#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "db/SQLiteDatabase.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsRecover : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsRecover);
    CPPUNIT_TEST(errorInIdle);
    CPPUNIT_TEST(errorInRunning);
    CPPUNIT_TEST(recoverPreviousRuns);
    CPPUNIT_TEST_SUITE_END();

    typedef std::unique_ptr<DaqStub> DaqStubPtr;
    typedef std::map<std::string, DaqStubPtr> DaqStubMap;

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void errorInIdle()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        m_stub->timing.reset();

        // bct + timing + lastError + firstError
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveErrors().size() == 4;
        }));
        EQ(EacsState(ERROR), cli.getEacsState());

        m_stub->timing.reset(new TimingStub());

        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveErrors().size() ==
                2; // last error + first error
        }));
        cli.cmdReset();
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
    }

    void errorInRunning()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        waitFor([this]() {
            checkRunStatus(900001, RunInfo::Ongoing, RunInfo::DataUnknown);
            return true;
        });

        m_stub->timing.reset();

        // bct + timing + lastError + firstError
        EQ(true, stateWaiter.wait([&cli]() {
            const auto &errors = cli.getStateClient()->getActiveErrors();
            return isErrorActive(errors,
                                 ntof::eacs::ErrorCode::ERR_BCT_NO_LINK) &&
                isErrorActive(errors,
                              ntof::eacs::ErrorCode::ERR_TIMING_NO_LINK) &&
                isErrorActive(errors, ntof::eacs::ErrorCode::ERR_LAST_ERROR) &&
                isErrorActive(errors, ntof::eacs::ErrorCode::ERR_FIRST_ERROR);
        }));
        EQ(EacsState(ERROR), cli.getEacsState());

        m_stub->timing.reset(new TimingStub());

        EQ(true, stateWaiter.wait([&cli]() {
            const auto &errors = cli.getStateClient()->getActiveErrors();
            return !isErrorActive(errors,
                                  ntof::eacs::ErrorCode::ERR_BCT_NO_LINK) &&
                !isErrorActive(errors,
                               ntof::eacs::ErrorCode::ERR_TIMING_NO_LINK);
        }));
        EQ(EacsState(ERROR), cli.getEacsState()); // stays in error

        cli.cmdReset();
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Crashed, RunInfo::BadData);
    }

    void genRunInfo(RunInfo &info)
    {
        info.runNumber = 901234;
        info.description = "sample run";
        info.title = "run title";
        info.experiment = "myExperiment";
        info.detectorsSetupId = 1111;
        info.materialsSetupId = 2222;
    }

    void recoverPreviousRuns()
    {
        // Prepare DB
        SQLiteDatabase &db = dynamic_cast<SQLiteDatabase &>(
            Database::instance());
        SignalWaiter dbUpdateWaiter;
        dbUpdateWaiter.listen(db.dbUpdatedSignal);

        // Create two fake crashed runs
        RunInfo info;
        genRunInfo(info);
        std::time_t start = std::time(nullptr);
        db.createRun(info, start);
        info.runNumber++;
        db.createRun(info, start);

        // Check runs are ongoing and data unknown
        RunInfo::Status status;
        RunInfo::DataStatus dataStatus;
        db.getRunStatus(901234, status, dataStatus);
        EQ(RunInfo::Status::Ongoing, status);
        EQ(RunInfo::DataStatus::DataUnknown, dataStatus);
        db.getRunStatus(901235, status, dataStatus);
        EQ(RunInfo::Status::Ongoing, status);
        EQ(RunInfo::DataStatus::DataUnknown, dataStatus);

        // On Init, EACS should recover those two crashed runs
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        // Check status is now crashed
        db.getRunStatus(901234, status, dataStatus);
        EQ(RunInfo::Status::Crashed, status);
        EQ(RunInfo::DataStatus::DataUnknown, dataStatus);
        db.getRunStatus(901235, status, dataStatus);
        EQ(RunInfo::Status::Crashed, status);
        EQ(RunInfo::DataStatus::DataUnknown, dataStatus);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsRecover);
