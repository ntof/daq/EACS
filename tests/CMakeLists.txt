
if(TESTS)

link_directories(
  ${CMAKE_BINARY_DIR}/src
  ${DIM_LIBRARIES}
  ${CPPUNIT_LIBRARY_DIRS})
include_directories(${CMAKE_BINARY_DIR} ${CMAKE_SOURCE_DIR}/src)
include_directories(SYSTEM
  ${CPPUNIT_INCLUDE_DIRS}
  ${DIM_INCLUDE_DIRS}
  ${NTOFUTILS_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS})

set(test_SRCS test_main.cpp test_init.cpp
  test_helpers.hpp test_helpers.cpp
  test_helpers_dim.hpp test_helpers_dim.cpp
  test_helpers_eacs.hpp test_helpers_eacs.cpp

  stubs/daq/DaqStubExceptions.hpp
  stubs/TimingStub.hpp stubs/TimingStub.cpp
  stubs/daq/DaqStub.hpp stubs/daq/DaqStub.cpp
  stubs/daq/ListDaqElementsStub.hpp stubs/daq/ListDaqElementsStub.cpp
  stubs/daq/ZeroSuppressionStub.hpp stubs/daq/ZeroSuppressionStub.cpp
  stubs/daq/ChannelConfigStub.hpp stubs/daq/ChannelConfigStub.cpp
  stubs/daq/AcqStateStub.hpp stubs/daq/AcqStateStub.cpp
  stubs/daq/DaqStateStub.hpp stubs/daq/DaqStateStub.cpp
  stubs/daq/WriterStateStub.hpp stubs/daq/WriterStateStub.cpp
  stubs/daq/CardStub.hpp stubs/daq/CardStub.cpp
  stubs/daq/RunExtensionNumberWroteStub.hpp stubs/daq/RunExtensionNumberWroteStub.cpp
  stubs/AddhStub.hpp stubs/AddhStub.cpp
  stubs/RFMStub.hpp stubs/RFMStub.cpp
  stubs/FilterStationStub.hpp stubs/FilterStationStub.cpp
  stubs/NTOFStub.hpp stubs/NTOFStub.cpp
  stubs/HVCardStub.hpp stubs/HVCardStub.cpp

  test_ClientAddhEvents.cpp
  test_Database.cpp
  test_ServiceDaqList.cpp
  test_ServiceEvents.cpp
  test_ServiceRunInfo.cpp
  test_ServiceRunConfig.cpp
  test_ServiceTiming.cpp
  test_Eacs.cpp
  test_Eacs_addhState.cpp
  test_Eacs_recover.cpp
  test_Eacs_stop.cpp
  test_Eacs_daqConfigChange.cpp
  test_Eacs_hvConfigChange.cpp
  test_Eacs_filterStation.cpp
  test_Eacs_zspConfig.cpp
  test_Eacs_run_file.cpp
  test_ClientFilterStation.cpp
  test_HVMonitor.cpp
  test_ServiceInfo.cpp
  test_EventValidator.cpp
  test_BeamMonitor.cpp
  )

add_executable(test_all ${test_SRCS})
target_link_libraries(test_all EACSLib EACSCliLib ntofutils
  ${CPPUNIT_LIBRARIES}
  ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${PUGI_LIBRARIES})

add_executable(test_integ test_main.cpp
  test_integ_init.cpp
  test_integ_ClientSampleExchanger.cpp
  test_helpers.cpp test_helpers.hpp
  test_helpers_dim.hpp test_helpers_dim.cpp
  test_helpers_eacs.hpp test_helpers_eacs.cpp)
target_link_libraries(test_integ EACSLib EACSCliLib ntofutils
  ${CPPUNIT_LIBRARIES}
  ${DIM_LIBRARIES} ${Boost_LIBRARIES} ${PUGI_LIBRARIES})


add_test(test_all ${CMAKE_CURRENT_BINARY_DIR}/test_all)

endif(TESTS)
