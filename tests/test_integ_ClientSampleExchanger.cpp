/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T10:33:12+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/
#include <chrono>
#include <memory>
#include <thread>

#include <Signals.hpp>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "clients/ClientSampleExchanger.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;
using namespace ntof::eacs;

class TestClientSampleExchanger : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestClientSampleExchanger);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override {}

    void tearDown() override {}

    void simple()
    {
        ClientSampleExchanger saex;
        saex.errorSignal.connect([](ErrorCode, const std::string &message) {
            LOG(ERROR) << "error: " << message;
        });
        saex.move(1).wait();
        EQ(int32_t(1), saex.getCurrentPosition());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestClientSampleExchanger);
