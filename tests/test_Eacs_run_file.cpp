/*
** Copyright (C) 2020 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-14T08:16:32+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

#include <cstdint>
#include <fstream>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <DaqTypes.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestRunFile : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestRunFile);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void simple()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        EQ(true, cli.cmdStop());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Finished, RunInfo::DataUnknown);

        EQ(true, bfs::exists("/tmp/eacs/run900001/run900001.run"));

        RunControlHeader rctr;
        std::ifstream ifs("/tmp/eacs/run900001/run900001.run");

        EQ(true, ifs.is_open());
        ifs >> rctr;
        EQ(true, bool(ifs));
        EQ(uint32_t(900001), rctr.runNumber);
        EQ(std::string("LAB"), rctr.getExperiment());
        EQ(uint32_t(1), rctr.totalNumberOfChassis);
        EQ(std::size_t(1), rctr.numberOfChannelsPerModule.size());
        EQ(uint32_t(1), rctr.numberOfChannelsPerModule[0]);

        ModuleHeader modh;
        ifs >> modh;
        EQ(true, bool(ifs));
        EQ(std::size_t(1), modh.channelsConfig.size());
        ChannelConfig &chan = modh.channelsConfig[0];
        EQ(std::string("BAF2"), chan.getDetectorType());
        EQ(uint32_t(1), chan.detectorId);
        EQ(std::string("S412"), chan.getModuleType());
        EQ(uint32_t(0x01010000), chan.str_mod_crate);
        DBL_EQ(float(1800), chan.sampleRate, 0.01);
        EQ(uint32_t(128000), chan.sampleSize);
        DBL_EQ(float(5050), chan.fullScale, 0.01);
        EQ(int32_t(0), chan.delayTime);
        EQ(int32_t(105), chan.threshold);
        EQ(int32_t(1), chan.thresholdSign);
        DBL_EQ(float(50), chan.offset, 0.01);
        EQ(uint32_t(512), chan.preSample);
        EQ(uint32_t(512), chan.postSample);
        EQ(uint32_t(0x43544E49), chan.clockState);
        EQ(uint32_t(0), chan.zeroSuppressionStart);
        EQ(std::string(), chan.getMasterDetectorType());
        EQ(uint32_t(0), chan.masterDetectorId);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRunFile);
