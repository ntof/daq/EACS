/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-20T10:15:27+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef TEST_HELPERS_EACS_HPP__
#define TEST_HELPERS_EACS_HPP__

#include <functional>
#include <list>
#include <map>
#include <mutex>
#include <string>

#include <DIMData.h>
#include <DIMState.h>
#include <DIMUtils.hpp>

#include "EACSTypes.hpp"
#include "cli/EACSCli.hpp"
#include "data/RunInfo.hpp"
#include "misc.h"

class ErrorWatcher
{
public:
    typedef std::map<ntof::eacs::WarningCode, std::string> WarningMap;
    typedef std::map<ntof::eacs::ErrorCode, std::string> ErrorMap;
    ErrorWatcher();

    template<typename Object>
    void connect(Object &o)
    {
        connect(&o.errorSignal, &o.warningSignal);
    }

    void connect(ntof::eacs::ErrorSignal *err,
                 ntof::eacs::WarningSignal *warn = nullptr);

    ErrorMap getErrors() const;
    WarningMap getWarnings() const;

    /**
     * @throws CppUnit::Exception
     */
    void check(std::size_t errorCount, std::size_t warningCount);

protected:
    std::list<ntof::dim::scoped_connection> m_conn;
    std::function<ntof::eacs::ErrorSignal::signature_type> m_errSlot;
    std::function<ntof::eacs::WarningSignal::signature_type> m_warnSlot;

    mutable std::mutex m_lock;
    WarningMap m_warnings;
    ErrorMap m_errors;
};

class RunInfoWaiter
{
public:
    bool wait(std::function<bool(ntof::dim::DIMData::List &)> check,
              unsigned int ms = 1000);
};

class RunConfigWaiter
{
public:
    bool wait(std::function<bool(ntof::dim::DIMData::List &)> check,
              unsigned int ms = 1000);
};

class EventsWaiter
{
public:
    bool wait(std::function<bool(ntof::dim::DIMData::List &)> check,
              unsigned int ms = 1000);
};

class DaqListWaiter
{
public:
    bool wait(std::function<bool(ntof::dim::DIMData::List &)> check,
              unsigned int ms = 1000);
};

void checkRunStatus(ntof::eacs::RunNumber runNumber,
                    ntof::eacs::RunInfo::Status status,
                    ntof::eacs::RunInfo::DataStatus dataStatus);

typedef std::vector<ntof::dim::DIMErrorWarning> ErrWarnVector;
bool isErrorActive(const ErrWarnVector &vec, int32_t code);

void runSetup(ntof::eacs::EACSCli &cli);

#endif
