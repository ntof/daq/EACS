/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-08-23T15:09:37+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsHvConfigChange : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsHvConfigChange);
    CPPUNIT_TEST(hvConfig);
    CPPUNIT_TEST(hvConfigSpecialSettings);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void hvConfig()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        {
            /* powering on an HV channel */
            m_stub->hvStubs.at(0)->setPower(0, true);
        }

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        EQ(int64_t(4), Database::instance().countHVChannelInfo());

        {
            /* modifying HV config */
            m_stub->hvStubs.at(0)->lockDataAt("chan0i0set")->setValue(float(42));
            m_stub->hvStubs.at(0)->updateData();
        }

        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getEacsState() == EacsState(ERROR);
        }));
        // error action is asynchronuous
        waitFor([this]() {
            checkRunStatus(900001, RunInfo::Crashed, RunInfo::BadData);
            return true;
        });

        /* error  can be cleared */
        EQ(true, cli.cmdReset());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
    }

    void hvConfigSpecialSettings()
    {
        Config::instance().setHVWarnOnly(true);
        Config::instance().setHVSkipUpdateDB(true);

        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        {
            /* powering on an HV channel */
            m_stub->hvStubs.at(0)->setPower(0, true);
        }

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        // Skip DB update for HV is active, so we expect 0
        EQ(int64_t(0), Database::instance().countHVChannelInfo());

        {
            /* modifying HV config */
            m_stub->hvStubs.at(0)->lockDataAt("chan0i0set")->setValue(float(42));
            m_stub->hvStubs.at(0)->updateData();
        }

        // ERR_HVCONF_CHANGED (800) became now a warning WARN_HVCONF_CHANGED
        // (1800)
        EQ(true, stateWaiter.wait([&cli]() {
            const auto &errors = cli.getStateClient()->getActiveErrors();
            const auto &warnings = cli.getStateClient()->getActiveWarnings();
            return errors.size() == 0 &&
                isErrorActive(warnings,
                              ntof::eacs::WarningCode::WARN_HVCONF_CHANGED);
        }));
        EQ(EacsState(RUNNING), cli.getEacsState());

        EQ(true, cli.cmdStop());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Finished, RunInfo::DataUnknown);

        Config::instance().setHVWarnOnly(false);
        Config::instance().setHVSkipUpdateDB(false);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsHvConfigChange);
