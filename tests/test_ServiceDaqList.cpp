/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-29T10:43:48+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <memory>

#include <DIMData.h>
#include <DIMException.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>
#include <services/ServiceDaqsList.hpp>

#include "Config.hpp"
#include "db/SQLiteDatabase.hpp"
#include "local-config.h"
#include "stubs/daq/DaqStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

#include <dis.hxx>

using namespace ntof::eacs;
using ntof::dim::DIMData;
using ntof::dim::DIMException;

class TestServiceDaqList : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestServiceDaqList);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(cardSerialInDatabase);
    CPPUNIT_TEST_SUITE_END();

    typedef std::unique_ptr<DaqStub> DaqStubPtr;
    typedef std::map<std::string, DaqStubPtr> DaqStubMap;

    std::unique_ptr<DimTestHelper> m_dim;
    DaqStubMap m_daqStubs;

public:
    void setUp() override
    {
        Database::destroy();
        m_dim.reset(new DimTestHelper());

        // Init daq stub
        const std::map<int32_t, std::string> &daqsList =
            Config::instance().getDaq();
        for (const auto &daq : daqsList)
        {
            int32_t chassisId = daq.first;
            const std::string &hostname = daq.second;

            DaqStubPtr daqStub(new DaqStub(hostname, chassisId));
            m_daqStubs.insert(std::pair<std::string, DaqStubPtr>(
                hostname, std::move(daqStub)));
        }
    }

    void tearDown() override
    {
        m_daqStubs.clear();
        m_dim.reset();
    }

    void simple()
    {
        ServiceDaqsList service;
        DaqListWaiter waiter;

        // Empty daqs
        EQ(true, waiter.wait([](DIMData::List &daqs) {
            return daqs.size() == 2 &&
                daqs.at(0).getNestedValue().size() == 1 &&
                daqs.at(1).getNestedValue().size() == 1;
        }));

        service.loadDaqConfigs();

        EQ(true, waiter.wait([](DIMData::List &daqs) {
            return daqs.size() == 2 &&
                daqs.at(0).getNestedValue().size() == 2 &&
                daqs.at(1).getNestedValue().size() ==
                2; // It means used is not there, otherwise 3
        }));

        // Enable a Channel and check if the live value updates (used)
        std::string hostname = service.getDaqs().begin()->first;
        DIMParamListClient cli(hostname + "/CARD0/CHANNEL0");
        std::vector<DIMData> data;
        data.emplace_back(DaqChannelConfig::Params::IS_CHANNEL_ENABLED, true);
        cli.sendParameters(data);

        EQ(true, waiter.wait([](DIMData::List &daqs) {
            return daqs.size() == 2 &&
                daqs.at(0).getNestedValue().size() == 3 &&
                daqs.at(0)
                    .getNestedValue()
                    .at(2) // Index of used in the vector
                    .getBoolValue();
        }));
    }

    void cardSerialInDatabase()
    {
        {
            // Prepare DB
            SQLiteDatabase &db = dynamic_cast<SQLiteDatabase &>(
                Database::instance());
            RunInfo info;
            info.runNumber = 901234;
            info.title = "run title";
            info.description = "sample run";
            info.experiment = "myExperiment";
            info.detectorsSetupId = 1111;
            info.materialsSetupId = 2222;
            db.createRun(info, std::time(nullptr));
        }

        ServiceDaqsList service;
        ntof::utils::SignalWaiter waiter;

        waiter.listen(service.daqLoadedSignal);

        service.loadDaqConfigs();
        EQ(true, waiter.wait(1));

        service.updateDatabase(901234);
        // same defaults for all channels (16) + 2*3 cards with serial
        EQ(int64_t((16) + 2 * 3), Database::instance().countDaqParams());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestServiceDaqList);
