/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-29T10:43:48+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <memory>
#include <thread>

#include <Signals.hpp>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "clients/ClientAddhEvents.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

#include <dis.hxx>

using namespace ntof::eacs;
using namespace ntof::utils;

class TestClientAddhEvents : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestClientAddhEvents);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override { m_dim.reset(new DimTestHelper()); }

    void tearDown() override { m_dim.reset(); }

    void simple()
    {
        ClientAddhEvents::Event event{};
        SignalWaiter waiter;
        ClientAddhEvents cli;

        ntof::utils::scoped_connection conn(cli.signal.connect(
            [&](const ClientAddhEvents::Event &evt) { event = evt; }));

        waiter.listen(cli.signal);

        DimService service("ADDH/Events",
                           "<addh>"
                           "<event runNumber=\"1234\" timingEvent=\"1111\""
                           " filePath=\"/tmp/pwet\" fileSize=\"42\">"
                           "</event></addh>");

        EQ(true, waiter.wait());
        EQ(1234, event.runNumber);
        EQ(1111, event.timingEvent);
        EQ(std::string("/tmp/pwet"), event.filePath);
        EQ(size_t(42), event.fileSize);
    }

    void errors()
    {
        SignalWaiter waiter;
        bool noLink;
        bool xmlInvalid;
        ClientAddhEvents cli;

        cli.errorSignal.connect([&](ErrorCode c, const std::string &m) {
            switch (c)
            {
            case ERR_ADDH_EVENTS_XML_INVALID: xmlInvalid = !m.empty(); break;
            case ERR_ADDH_EVENTS_NO_LINK: noLink = !m.empty(); break;
            default: break;
            }
        });
        waiter.listen(cli.errorSignal);

        {
            DimService service("ADDH/Events", "<addh></addh>");

            EQ(true, waiter.wait([&xmlInvalid]() { return xmlInvalid; }));
        }

        EQ(true, waiter.wait([&noLink]() { return noLink; }));

        {
            DimService service("ADDH/Events", "");

            service.updateService(
                "<addh>"
                "<event runNumber=\"1234\" timingEvent=\"1111\""
                " filePath=\"/tmp/pwet\" fileSize=\"42\">"
                "</event></addh>");

            EQ(true, waiter.wait([&noLink, &xmlInvalid]() {
                return !noLink && !xmlInvalid;
            })); /* errors are cleared */
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestClientAddhEvents);
