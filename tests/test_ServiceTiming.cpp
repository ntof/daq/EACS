/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-16T15:34:53+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <DIMParamListClient.h>
#include <NTOFLogging.hpp>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>

#include "services/ServiceTiming.hpp"
#include "stubs/TimingStub.hpp"
#include "test_helpers.hpp"

using namespace ntof::eacs;
using namespace ntof::utils;

class TestServiceTiming : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestServiceTiming);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<TimingStub> m_timingStub;

public:
    void setUp() override
    {
        m_dim.reset(new DimTestHelper());
        m_timingStub.reset(new TimingStub());
    }

    void tearDown() override
    {
        m_timingStub.reset();
        m_dim.reset();
    }

    void simple()
    {
        ServiceTiming timing;
        DIMParamListClient cli("EACS/Timing");

        SignalWaiter syncWaiter;
        syncWaiter.listen(timing.syncSignal);

        timing.load();
        EQ(true, syncWaiter.wait(1));

        EQ(false, timing.isSyncing());

        std::vector<DIMData> data;
        data.emplace_back(TimingStub::MODE, DIMEnum(TimingStub::CALIBRATION));
        data.emplace_back(TimingStub::CALIB_TRIGGER_PERIOD, int32_t(12));
        cli.sendParameters(data);

        DIMAck ack = timing.suspend();
        EQ(DIMAck::OK, ack.getStatus());

        EQ(int32_t(TimingStub::DISABLED),
           m_timingStub->getTimingService()
               ->getEnumValue(TimingStub::MODE)
               .getValue());

        ack = timing.start();
        EQ(DIMAck::OK, ack.getStatus());

        EQ(int32_t(TimingStub::CALIBRATION),
           m_timingStub->getTimingService()
               ->getEnumValue(TimingStub::MODE)
               .getValue());
        EQ(int32_t(12),
           m_timingStub->getTimingService()->getIntValue(
               TimingStub::CALIB_TRIGGER_PERIOD));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestServiceTiming);
