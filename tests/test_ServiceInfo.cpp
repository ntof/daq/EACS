/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-25T11:04:51+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <DIMData.h>
#include <DIMDataSetClient.h>
#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Eacs.hpp"
#include "local-config.h"
#include "misc.h"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

#include <dis.hxx>

using namespace ntof::eacs;
using namespace ntof::utils;
using ntof::dim::DIMData;
using ntof::dim::DIMException;

class TestServiceInfo : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestServiceInfo);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override
    {
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        Eacs::instance();
    }

    void tearDown() override
    {
        Eacs::destroy();
        m_dim.reset();
    }

    void simple()
    {
        Eacs &eacs(Eacs::instance());
        eacs.initServices();
        ntof::dim::DIMDataSetClient cli("EACS/Info");
        SignalWaiter waiter(cli.dataSignal);

        EQ(true, waiter.wait());
        DIMData::List data = cli.getLatestData();
        EQ(std::size_t(2), data.size());
        EQ(std::string("FilterStation"), data[0].getStringValue());

        const DIMData::List &hvCards = data[1].getNestedValue();
        EQ(std::size_t(2), hvCards.size());
        EQ(std::string("HV_0"), hvCards[0].getName());
        EQ(std::string("HV_3"), hvCards[1].getName());

        EQ(uint32_t(0), hvCards[1].getNestedValue().at(0).getValue<uint32_t>());
        EQ(uint32_t(3), hvCards[1].getNestedValue().at(1).getValue<uint32_t>());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestServiceInfo);
