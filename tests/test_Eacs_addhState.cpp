/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-09T08:29:37+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/
#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "db/SQLiteDatabase.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsAddh : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsAddh);
    CPPUNIT_TEST(warnings);
    CPPUNIT_TEST(noLink);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    static bool checkErrWarIsPresent(const ClientAddhState::ErrWarnVector &vec,
                                     int32_t code)
    {
        return std::any_of(vec.begin(), vec.end(),
                           [&code](const DIMErrorWarning &warn) {
                               return warn.getCode() == code;
                           });
    }

    void warnings()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        // Check there is no warning with code 801 (800 is addh offset)
        EQ(false,
           checkErrWarIsPresent(cli.getStateClient()->getActiveWarnings(),
                                WARN_ADDH_STATE + 1));

        eacs.clearErrors();
        eacs.clearWarnings();
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveWarnings().size() == 0;
        }));

        // Add warning for code 1 (it will be added to EACS/State as
        // WARN_ADDH_STATE + 1
        m_stub->addh->addWarning(1, "No Link for service: FAKE/OnChannels");
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveWarnings().size() == 1;
        }));
        EQ(true,
           checkErrWarIsPresent(cli.getStateClient()->getActiveWarnings(),
                                WARN_ADDH_STATE + 1));

        // Remove warning
        m_stub->addh->removeWarning(1);
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveWarnings().size() == 0;
        }));

        // Check there is no warning with code 801 again
        EQ(false,
           checkErrWarIsPresent(cli.getStateClient()->getActiveWarnings(),
                                WARN_ADDH_STATE + 1));
    }

    void noLink()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        // Check there is no ERR_ADDH_STATE_NO_LINK
        EQ(false,
           checkErrWarIsPresent(cli.getStateClient()->getActiveErrors(),
                                ERR_ADDH_STATE_NO_LINK));

        eacs.clearErrors();
        eacs.clearWarnings();
        EQ(true, stateWaiter.wait([&cli]() {
            return cli.getStateClient()->getActiveWarnings().size() == 0;
        }));

        // Shut down ADDH State
        m_stub->addh->resetState();
        EQ(true, stateWaiter.wait([&cli]() {
            return checkErrWarIsPresent(cli.getStateClient()->getActiveErrors(),
                                        ERR_ADDH_STATE_NO_LINK);
        }));

        // Restart ADDH and check that no-link is cleared when new data is
        // received
        size_t saveStateUpdateCount = stateWaiter.count();
        m_stub->addh->resetState(true);
        EQ(true, stateWaiter.wait(saveStateUpdateCount + 1)); // Until STARTED

        EQ(false,
           checkErrWarIsPresent(cli.getStateClient()->getActiveErrors(),
                                ERR_ADDH_STATE_NO_LINK));
    };
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsAddh);
