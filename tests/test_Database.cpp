/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-15T11:08:51+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <memory>
#include <thread>

#include <DaqTypes.h>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSException.hpp"
#include "db/DaqParametersDatabase.hpp"
#include "hv/HVBoardInfo.hpp"
#include "hv/HVChannelInfo.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

#include <dis.hxx>

using namespace ntof::eacs;

class TestDatabase : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestDatabase);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(errors);
    CPPUNIT_TEST(daqConfig);
    CPPUNIT_TEST(highVoltage);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override { Database::destroy(); }

    void genRunInfo(RunInfo &info)
    {
        info.runNumber = 901234;
        info.description = "sample run";
        info.title = "run title";
        info.experiment = "myExperiment";
        info.detectorsSetupId = 1111;
        info.materialsSetupId = 2222;
    }

    void simple()
    {
        Database &db = Database::instance();
        std::time_t now = std::time(nullptr);

        RunInfo info;
        genRunInfo(info);

        EQ(RunInfo::InvalidRunNumber, db.getLatestRun());
        db.createRun(info, now);

        time_t ts = time(nullptr);
        db.insertTrigger(info.runNumber, info.eventNumber,
                         BeamType::CALIBRATION, 42, ts * 1000);
        db.addProtons(info.runNumber, 42);

        {
            RunInfo ret;
            EQ(RunNumber(901234), db.getLatestRun());
            db.getRunInfo(901234, ret);

            EQ(info.title, ret.title);
            EQ(info.description, ret.description);
            EQ(info.experiment, ret.experiment);
            EQ(info.detectorsSetupId, ret.detectorsSetupId);
            EQ(info.materialsSetupId, ret.materialsSetupId);
            DBL_EQ(double(42), (double) ret.protons, 0.1);
        }
    }

    void errors()
    {
        Database &db = Database::instance();

        RunInfo info;
        genRunInfo(info);

        db.createRun(info);
        ASSERT_THROW(db.createRun(info), EACSException);
        time_t ts = time(nullptr);
        ASSERT_THROW(
            db.insertTrigger(1234, 1, BeamType::CALIBRATION, 42, ts * 1000),
            EACSException);
    }

    void daqConfig()
    {
        Database &db = Database::instance();

        RunInfo info;
        genRunInfo(info);
        db.createRun(info, std::time(nullptr));

        DaqParametersDatabase params;
        params.values()[DaqParametersDatabase::sampleRate] = "1234";
        params.values()[DaqParametersDatabase::sampleSize] = "1234";

        db.insertDaqConfig(info.runNumber,
                           ChannelConfig::makeLocation(1, 12, 2, 3), params);
        // revision is always there
        EQ(int64_t(3), Database::instance().countDaqParams());

        info.runNumber++;
        db.createRun(info, std::time(nullptr));
        params.values()[DaqParametersDatabase::sampleRate] = "1234.1";
        db.insertDaqConfig(info.runNumber,
                           ChannelConfig::makeLocation(1, 12, 2, 3), params);
        // only changed has been created
        EQ(int64_t(4), Database::instance().countDaqParams());
    }

    void highVoltage()
    {
        Database &db = Database::instance();

        RunInfo info;
        genRunInfo(info);
        db.createRun(info, std::time(nullptr));

        HVBoardInfo::ChannelSet channelSet = {0, 1, 2};
        HVBoardInfo board(0, channelSet);

        for (const HVBoardInfo::HVChannelInfoMap::value_type &it :
             board.channelInfo())
        {
            db.insertHVChannelInfo(info.runNumber, board, it.second);
        }

        EQ(int64_t(3), Database::instance().countHVChannelInfo());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDatabase);
