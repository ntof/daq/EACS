/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-10T15:43:26+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "cli/EACSCli.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEacsStop : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEacsStop);
    CPPUNIT_TEST(stop);
    CPPUNIT_TEST(stop_with_data_status);
    CPPUNIT_TEST(validEvents);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void stop()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        EQ(true, cli.cmdStop());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Finished, RunInfo::DataUnknown);
    }

    void stop_with_data_status()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        stateWaiter.listen(cli.eacsStateSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        EQ(true, cli.cmdStop(RunInfo::DataOk));
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Finished, RunInfo::DataOk);
    }

    void validEvents()
    {
        Eacs &eacs = Eacs::instance();
        eacs.initServices();
        eacs.start();

        EACSCli cli;
        cli.listen();
        SignalWaiter stateWaiter;
        SignalWaiter infoWaiter;
        stateWaiter.listen(cli.eacsStateSignal);
        infoWaiter.listen(cli.runInfoSignal);

        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));

        runSetup(cli);

        {
            DIMData::List params;
            params.emplace_back(ServiceTiming::MODE,
                                DIMEnum(ServiceTiming::CALIBRATION));
            params.emplace_back(ServiceTiming::CALIB_TRIGGER_PERIOD,
                                int32_t(100));
            EQ(true, cli.setTimingConfig(params));
        }

        EQ(true, cli.cmdStart());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == RUNNING; },
                            5000));

        EQ(true,
           infoWaiter.wait(
               [&cli]() {
                   DIMData::List info = cli.getRunInfo();
                   return info.at(EacsRunInfo::NUMBER_OF_EVENTS_VALID)
                              .getValue<uint32_t>() == 1;
               },
               5000));

        // wait X timing events
        EQ(true, cli.cmdStop());
        EQ(true,
           stateWaiter.wait([&cli]() { return cli.getEacsState() == IDLE; }));
        checkRunStatus(900001, RunInfo::Finished, RunInfo::DataUnknown);

        // ensure events have been recorded
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEacsStop);
