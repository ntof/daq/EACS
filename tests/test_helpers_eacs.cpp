/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-20T10:15:27+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "test_helpers_eacs.hpp"

#include <chrono>
#include <sstream>

#include <Signals.hpp>
#include <cppunit/TestAssert.h>
#include <easylogging++.h>

#include "Database.hpp"
#include "daq/DaqChannelConfig.hpp"
#include "misc.h"
#include "test_helpers.hpp"

using ntof::dim::DIMData;
using namespace ntof::eacs;
using namespace ntof::utils;
using namespace std::chrono;

bool RunInfoWaiter::wait(std::function<bool(DIMData::List &)> check,
                         unsigned int ms)
{
    EACSCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.runInfoSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getRunInfo();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

bool RunConfigWaiter::wait(std::function<bool(DIMData::List &)> check,
                           unsigned int ms)
{
    EACSCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.runConfigSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getRunConfig();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

bool EventsWaiter::wait(std::function<bool(DIMData::List &)> check,
                        unsigned int ms)
{
    EACSCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.eventSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getEvents();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

bool DaqListWaiter::wait(std::function<bool(DIMData::List &)> check,
                         unsigned int ms)
{
    EACSCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.daqListSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getDaqList();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

void checkRunStatus(ntof::eacs::RunNumber runNumber,
                    ntof::eacs::RunInfo::Status status,
                    ntof::eacs::RunInfo::DataStatus dataStatus)
{
    RunInfo::Status currentStatus;
    RunInfo::DataStatus currentDataStatus;
    Database::instance().getRunStatus(runNumber, currentStatus,
                                      currentDataStatus);
    EQ(status, currentStatus);
    EQ(dataStatus, currentDataStatus);
}

bool isErrorActive(const ErrWarnVector &vec, int32_t code)
{
    for (const auto &err : vec)
    {
        if (err.getCode() == code)
            return true;
    }
    return false;
}

void runSetup(ntof::eacs::EACSCli &cli)
{
    {
        DIMData::List params;
        params.emplace_back(TITLE, std::string("my test title"));
        params.emplace_back(EXPERIMENT, std::string("test"));
        params.emplace_back(DETECTORS_SETUP_ID, int64_t(1));
        params.emplace_back(MATERIALS_SETUP_ID, int64_t(1));
        EQ(true, cli.setRunConfig(params));
    }

    {
        DIMData::List params;
        params.emplace_back(DaqChannelConfig::IS_CHANNEL_ENABLED, true);
        EQ(true, cli.setDaqConfig("ntofdaq-mtest01.cern.ch", 0, 0, params));
    }
}

ErrorWatcher::ErrorWatcher()
{
    m_errSlot = [this](ErrorCode code, const std::string &message) {
        std::lock_guard<std::mutex> lock(m_lock);
        if (message.empty())
        {
            if (m_errors.erase(code))
                LOG(INFO) << "[ErrorWatcher]: error removed: " << code;
        }
        else
        {
            LOG(INFO) << "[ErrorWatcher]: error added: " << code
                      << " message: " << message;
            m_errors[code] = message;
        }
    };
    m_warnSlot = [this](WarningCode code, const std::string &message) {
        std::lock_guard<std::mutex> lock(m_lock);
        if (message.empty())
        {
            if (m_warnings.erase(code))
                LOG(INFO) << "[ErrorWatcher]: warning removed: " << code;
        }
        else
        {
            LOG(INFO) << "[ErrorWatcher]: warning added: " << code
                      << " message: " << message;
            m_warnings[code] = message;
        }
    };
}

void ErrorWatcher::connect(ErrorSignal *err, WarningSignal *warn)
{
    if (err)
        m_conn.emplace_back(err->connect(m_errSlot));
    if (warn)
        m_conn.emplace_back(warn->connect(m_warnSlot));
}

ErrorWatcher::ErrorMap ErrorWatcher::getErrors() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_errors;
}

ErrorWatcher::WarningMap ErrorWatcher::getWarnings() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_warnings;
}

void ErrorWatcher::check(std::size_t errorCount, std::size_t warningCount)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_errors.size() != errorCount)
    {
        std::ostringstream oss;
        oss << "Invalid error count: " << m_errors.size()
            << " != " << errorCount << "\nErrors: \n";
        for (const ErrorMap::value_type &it : m_errors)
        {
            oss << "code: " << it.first << " message: " << it.second;
        }
        CPPUNIT_FAIL(oss.str());
    }
    if (m_warnings.size() != warningCount)
    {
        std::ostringstream oss;
        oss << "Invalid warning count: " << m_warnings.size()
            << " != " << warningCount << "\nErrors: \n";
        for (const WarningMap::value_type &it : m_warnings)
        {
            oss << "code: " << it.first << " message: " << it.second << "\n";
        }
        CPPUNIT_FAIL(oss.str());
    }
}
