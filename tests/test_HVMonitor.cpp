/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-21T09:43:34+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <memory>

#include <boost/filesystem.hpp>

#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "Eacs.hpp"
#include "hv/HVChannelInfo.hpp"
#include "hv/HVMonitor.hpp"
#include "local-config.h"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_dim.hpp"
#include "test_helpers_eacs.hpp"

using namespace ntof::eacs;
using namespace ntof::utils;
using namespace stub;
namespace bfs = boost::filesystem;

class TestHVMonitor : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestHVMonitor);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(no_warn_pw_off);
    CPPUNIT_TEST(no_hv_declared);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
    }

    void tearDown() override
    {
        Eacs::destroy();
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        bfs::remove_all("/tmp/eacs");
    }

    void simple()
    {
        HVMonitor monitor;
        SignalWaiter waiter;
        ErrorWatcher errorWatch;

        waiter.listen(monitor.stateSignal);
        errorWatch.connect(monitor);

        monitor.start();
        EQ(true, waiter.wait([&monitor]() {
            return monitor.getState() == HVMonitor::State::ST_IDLE;
        }));
        errorWatch.check(0, 0);

        { /* test warnings */
            SignalWaiter warnWaiter(monitor.warningSignal);
            m_stub->hvStubs.at(0)->setPower(0, true);
            m_stub->hvStubs.at(0)->setStatusBitField(
                0, HVChannelInfo::SB_OVERVOLTAGE);
            EQ(true, warnWaiter.wait([&errorWatch]() {
                return errorWatch.getWarnings().size() == 1;
            }));
            errorWatch.check(0, 1);

            m_stub->hvStubs.at(0)->setPower(1, true);
            m_stub->hvStubs.at(0)->setStatusBitField(
                1, HVChannelInfo::SB_OVERVOLTAGE);
            m_stub->hvStubs.at(0)->setError(1, 1, "sample error");
            EQ(true, warnWaiter.wait([&errorWatch]() {
                return errorWatch.getWarnings().size() == 2;
            }));
            errorWatch.check(0, 2);

            m_stub->hvStubs.at(0)->setStatusBitField(0, 0);
            m_stub->hvStubs.at(0)->setStatusBitField(1, 0);
            m_stub->hvStubs.at(0)->setError(1, 0, std::string());
            EQ(true, warnWaiter.wait([&errorWatch]() {
                return errorWatch.getWarnings().size() == 0;
            }));
            errorWatch.check(0, 0);
        }
    }

    void no_warn_pw_off()
    {
        HVMonitor monitor;
        SignalWaiter waiter;
        ErrorWatcher errorWatch;

        waiter.listen(monitor.stateSignal);
        errorWatch.connect(monitor);

        monitor.start();
        EQ(true, waiter.wait([&monitor]() {
            return monitor.getState() == HVMonitor::State::ST_IDLE;
        }));
        errorWatch.check(0, 0);

        { /* warnings are not displayed for OFF channels */
            SignalWaiter warnWaiter(monitor.warningSignal);
            m_stub->hvStubs.at(0)->setPower(0, true);
            m_stub->hvStubs.at(0)->setStatusBitField(
                0, HVChannelInfo::SB_OVERVOLTAGE);
            EQ(true, warnWaiter.wait([&errorWatch]() {
                return errorWatch.getWarnings().size() == 1;
            }));

            /* turn channel OFF */
            m_stub->hvStubs.at(0)->setPower(0, false);
            EQ(true, warnWaiter.wait([&errorWatch]() {
                return errorWatch.getWarnings().size() == 0;
            }));
        }
    }

    void no_hv_declared()
    {
        const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
        // Change config with no HV card declared
        ntof::eacs::Config::load((dataDir / "config_minimal.xml").string(),
                                 (dataDir / "configMisc.xml").string());
        HVMonitor monitor;
        EQ(HVMonitor::State::ST_UNKNOWN, monitor.getState());
        EQ(false, monitor.hasBoards());

        // Reset config
        ntof::eacs::Config::load((dataDir / "config.xml").string(),
                                 (dataDir / "configMisc.xml").string());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestHVMonitor);
