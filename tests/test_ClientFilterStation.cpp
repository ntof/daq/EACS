/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T10:33:12+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/
#include <chrono>
#include <memory>
#include <thread>

#include <Signals.hpp>
#include <cppunit/TestFixture.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "EACSException.hpp"
#include "clients/ClientFilterStation.hpp"
#include "local-config.h"
#include "stubs/FilterStationStub.hpp"
#include "test_helpers.hpp"

using namespace ntof::utils;
using namespace ntof::eacs;

class BorkenFilterStationStub : public stub::FilterStationStub
{
protected:
    /**
     * @brief never ending movement
     */
    void endMove(const std::vector<FilterPosition> &) override {};
};

class TestClientFilterStation : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestClientFilterStation);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(brokenStation);
    CPPUNIT_TEST(noLink);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;

public:
    void setUp() override { m_dim.reset(new DimTestHelper()); }

    void tearDown() override { m_dim.reset(); }

    void simple()
    {
        ClientFilterStation cli;
        stub::FilterStationStub filterStation;
        cli.setTimeout(std::chrono::milliseconds(250)); // fasten TU

        SignalWaiter waiter;
        waiter.listen(cli.movedSignal);

        EQ(stub::FilterStationStub::PositionOut,
           filterStation.getFilterPosition(2));

        std::vector<ClientFilterStation::FilterPosition> positions(
            ClientFilterStation::FiltersCount, ClientFilterStation::PositionIn);
        std::shared_future<void> f = cli.move(positions);

        EQ(true, waiter.wait());
        EQ(std::future_status::ready, f.wait_for(std::chrono::seconds(3)));
        EQ(stub::FilterStationStub::PositionIn,
           filterStation.getFilterPosition(2));
    }

    void brokenStation()
    {
        ClientFilterStation cli;
        BorkenFilterStationStub filterStation;
        cli.setTimeout(std::chrono::milliseconds(250)); // fasten TU

        SignalWaiter waiter;
        waiter.listen(cli.movedSignal);
        SignalWaiter errorWaiter;
        errorWaiter.listen(cli.errorSignal);

        std::vector<ClientFilterStation::FilterPosition> positions(
            ClientFilterStation::FiltersCount, ClientFilterStation::PositionIn);
        std::shared_future<void> f = cli.move(positions);

        EQ(true, errorWaiter.wait());
        EQ(std::future_status::ready, f.wait_for(std::chrono::seconds(3)));
        ASSERT_THROW(f.get(), EACSException);
    }

    void noLink()
    {
        ClientFilterStation cli;
        cli.setTimeout(std::chrono::milliseconds(250)); // fasten TU
        cli.client().cmdClient().setTimeOut(std::chrono::milliseconds(2000));

        SignalWaiter waiter;
        waiter.listen(cli.movedSignal);
        SignalWaiter errorWaiter;
        errorWaiter.listen(cli.errorSignal);

        std::vector<ClientFilterStation::FilterPosition> positions(
            ClientFilterStation::FiltersCount, ClientFilterStation::PositionIn);
        std::shared_future<void> f = cli.move(positions);

        EQ(true, errorWaiter.wait());
        EQ(std::future_status::ready, f.wait_for(std::chrono::seconds(3)));
        ASSERT_THROW(f.get(), EACSException);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestClientFilterStation);
