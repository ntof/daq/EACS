/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-13T09:15:37+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cstdint>
#include <memory>

#include <boost/filesystem.hpp>

#include <DIMException.h>
#include <Signals.hpp>
#include <cppunit/TestFixture.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "EventValidator.hpp"
#include "clients/ClientAddhCmd.hpp"
#include "local-config.h"
#include "services/ServiceEventValidator.hpp"
#include "stubs/NTOFStub.hpp"
#include "test_helpers.hpp"
#include "test_helpers_dim.hpp"
#include "test_helpers_eacs.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;
using namespace stub;

class TestEventValidator : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestEventValidator);
    CPPUNIT_TEST(validEvents);
    CPPUNIT_TEST(validNoDaqs);
    CPPUNIT_TEST(dimSignal);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    std::unique_ptr<NTOFStub> m_stub;
    std::shared_ptr<ClientRFMerger> m_rfmCli;
    std::shared_ptr<ServiceDaqsList> m_daqListCli;
    std::mutex m_lock;
    std::condition_variable m_cond;

public:
    void setUp() override
    {
        bfs::create_directories("/tmp/eacs");
        Database::destroy();
        Eacs::destroy();
        m_dim.reset(new DimTestHelper());
        m_stub.reset(new NTOFStub());
        m_rfmCli.reset(new ClientRFMerger);
        m_daqListCli.reset(new ServiceDaqsList);
    }

    void tearDown() override
    {
        Database::destroy();
        m_stub.reset();
        m_dim.reset();
        m_rfmCli.reset();
        m_daqListCli.reset();
        bfs::remove_all("/tmp/eacs");
    }

    TimingEventDetails createTimingEvent(int64_t evtNumber,
                                         int64_t cycleStamp,
                                         int cycleNb)
    {
        TimingEventDetails event;
        event.evtNumber = evtNumber;
        event.evtType = "CALIBRATION";
        event.timeStamp = std::time(nullptr) * 1e9;
        event.cycleStamp = cycleStamp;
        event.cycleNb = cycleNb;
        return event;
    }

    /**
     * @details emulate EACS work, configuring DAQ if needed
     */
    void configureStubs(bool daqEnabled = false)
    {
        // EventValidator uses this
        Eacs::instance().setRunNumber(901234);

        {
            /* wait for DNS and MERGER to be ready */
            DimInfoWaiter waiter("MERGER/Ack");
            EQ(true, waiter.waitUpdate(1, 10000));
        }

        /* trigger addh write */
        m_rfmCli->sendRunStart(901234, "test", {1, 2});

        if (daqEnabled)
        {
            {
                SignalWaiter w(m_daqListCli->daqLoadedSignal);
                m_daqListCli->loadDaqConfigs();
                EQ(true, w.wait());
            }

            EACSCli cli;
            DIMData::List params;
            params.emplace_back(DaqChannelConfig::IS_CHANNEL_ENABLED, true);
            EQ(true, cli.setDaqConfig("ntofdaq-mtest01.cern.ch", 0, 0, params));

            {
                SignalWaiter w(m_daqListCli->daqReadySignal);
                m_daqListCli->configureDaqs(901234);
                EQ(true, w.wait());
            }
            m_daqListCli->startDaqs();
        }
    }

    void validEvents()
    {
        EventValidator validator(m_rfmCli, m_daqListCli);
        validator.start();

        configureStubs(true);

        EventValidator::ValidEvent event;
        EventNumber validatedNumber;

        validator.validatedSignal.connect(
            [this, &event,
             &validatedNumber](const EventValidator::ValidEvent &evt,
                               uint32_t valid, float, float) {
                std::lock_guard<std::mutex> lock(m_lock);
                event = evt;
                validatedNumber = valid;
                m_cond.notify_all();
            });

        {
            m_stub->timing->setEventNumber(1234);
            m_stub->timing->sendTimingEvent(1, 1);

            /* trigger addh write */
            ClientAddhCmd addhCmd;
            addhCmd.sendWrite(1234, 901234, "/tmp/eacs/out.raw", "TEST", "TEST");

            /** emulate BCT event */
            TimingEventDetails bctEvent(createTimingEvent(1234, 1, 1));
            validator.onBctEvent(bctEvent, 901234, 1000);
        }

        std::unique_lock<std::mutex> lock(m_lock);
        EQ(true, m_cond.wait_for(lock, std::chrono::seconds(3), [&event]() {
            return event.eventNumber == 1234;
        }));
        EQ(RunNumber(901234), event.runNumber);
        EQ(EventNumber(1), validatedNumber);
        // ADDH + EVEH/ACQC
        EQ(size_t(16 + 1024), event.fileSize);
    }

    void validNoDaqs()
    {
        EventValidator validator(m_rfmCli, m_daqListCli);
        validator.start();

        configureStubs(false);

        EventValidator::ValidEvent event;
        EventNumber validatedNumber;
        validator.validatedSignal.connect(
            [this, &event,
             &validatedNumber](const EventValidator::ValidEvent &evt,
                               uint32_t valid, float, float) {
                std::lock_guard<std::mutex> lock(m_lock);
                event = evt;
                validatedNumber = valid;
                m_cond.notify_all();
            });

        {
            /* no timing needed, no daqs on */
            /* trigger addh write */
            ClientAddhCmd addhCmd;
            addhCmd.sendWrite(1234, 901234, "/tmp/eacs/out.raw", "TEST", "TEST");

            /** emulate BCT event */
            TimingEventDetails bctEvent(createTimingEvent(1234, 1, 1));
            validator.onBctEvent(bctEvent, 901234, 1000);
        }

        std::unique_lock<std::mutex> lock(m_lock);
        EQ(true, m_cond.wait_for(lock, std::chrono::seconds(3), [&event]() {
            return event.eventNumber == 1234;
        }));
        EQ(RunNumber(901234), event.runNumber);
        EQ(EventNumber(1), validatedNumber);
        // ADDH
        EQ(size_t(16), event.fileSize);
    }

    void dimSignal()
    {
        EventValidator validator(m_rfmCli, m_daqListCli);
        validator.start();

        configureStubs(false);

        DIMDataSetClient cli("EACS/Validation");

        DIMData event;
        cli.dataSignal.connect([this, &event](DIMDataSetClient &client) {
            std::lock_guard<std::mutex> lock(m_lock);
            event = DIMData(0, client.getLatestData());
            m_cond.notify_all();
        });

        {
            /* no timing needed, no daqs on */
            /* trigger addh write */
            ClientAddhCmd addhCmd;
            addhCmd.sendWrite(1234, 901234, "/tmp/eacs/out.raw", "TEST", "TEST");

            /** emulate BCT event */
            TimingEventDetails bctEvent(createTimingEvent(1234, 1, 1));
            validator.onBctEvent(bctEvent, 901234, 1000);
        }

        std::unique_lock<std::mutex> lock(m_lock);
        EQ(true, m_cond.wait_for(lock, std::chrono::seconds(3), [&event]() {
            return getData("eventNumber", event).getValue<EventNumber>() == 1234;
        }));

        EQ(RunNumber(901234), getData("runNumber", event).getValue<RunNumber>());
        EQ(EventNumber(1),
           getData("validatedNumber", event).getValue<RunNumber>());
        EQ(size_t(16), getData("fileSize", event).getValue<uint64_t>());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestEventValidator);
