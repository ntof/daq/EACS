/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-15T09:05:25+02:00
**  Author: Gabriele De Blasi <gdeblase> <gabriele.deblasi@cern.ch>
**
*/

#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <string>
#include <thread>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>

#include "BeamMonitor.hpp"
#include "Config.hpp"
#include "EACSTypes.hpp"
#include "local-config.h"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::eacs;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

class TestBeamMonitor : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestBeamMonitor);
    CPPUNIT_TEST(lowBeamWarning);
    CPPUNIT_TEST(noBeamWarning);
    CPPUNIT_TEST_SUITE_END();

    std::mutex m_lock;
    std::condition_variable m_cond;

public:
    void setUp() override
    {
        const bfs::path dataDir = bfs::path(SRCDIR) / "tests" / "data";
        Config::load((dataDir / "config.xml").string(),
                     (dataDir / "configMisc.xml").string());
    }

    void tearDown() override {}

    TimingEventDetails createTimingEvent(int64_t evtNumber,
                                         std::string evtType,
                                         int64_t cycleStamp,
                                         int cycleNb)
    {
        TimingEventDetails event;
        event.evtNumber = evtNumber;
        event.evtType = evtType;
        event.timeStamp = std::time(nullptr) * 1e9;
        event.cycleStamp = cycleStamp;
        event.cycleNb = cycleNb;
        return event;
    }

    void lowBeamWarning()
    {
        BeamMonitor bm;

        const int32_t timeout = 500;
        const float threshold = 2000;
        bool received = false;

        EQ(timeout, Config::instance().lowBeamTimeout());
        EQ(threshold, Config::instance().lowBeamThreshold());

        bm.warningSignal.connect(
            [this, &received](WarningCode code, const std::string &msg) {
                if ((code != WARN_LOW_BEAM) ||
                    (msg.find("[BeamMonitor]: the beam intensity is low") ==
                     std::string::npos))
                    return;

                {
                    std::lock_guard<std::mutex> lock(m_lock);
                    received = true;
                }

                m_cond.notify_all();
            });

        bm.start();

        /* CALIBRATION timing event */
        received = false;
        {
            /** emulate BCT event */
            TimingEventDetails bctEvent(
                createTimingEvent(1234, "CALIBRATION", 1, 1));

            bm.onBctEvent(bctEvent, threshold - 100);
        }
        const steady_clock::time_point lastOk; // last good BCT event

        // no 'low beam' warnings in CALIBRATION mode
        {
            std::unique_lock<std::mutex> lock(m_lock);
            EQ(true,
               m_cond.wait_for(lock, milliseconds(timeout)) ==
                   std::cv_status::timeout);
            EQ(false, received);
        }

        /* PRIMARY timing event */
        received = false;
        {
            /** emulate BCT event */
            TimingEventDetails bctEvent(
                createTimingEvent(1234, "PRIMARY", 1, 1));
            bm.onBctEvent(bctEvent, threshold - 100);
        }

        {
            std::unique_lock<std::mutex> lock(m_lock);
            EQ(true,
               m_cond.wait_for(lock, milliseconds(timeout * 2),
                               [&received]() { return received; }));

            EQ(true,
               std::chrono::duration_cast<milliseconds>(
                   steady_clock::now() - lastOk) >= milliseconds(timeout));
        }

        /* PARASITIC timing event */
        received = false;
        {
            /** emulate BCT event */
            TimingEventDetails bctEvent(
                createTimingEvent(1234, "PARASITIC", 1, 1));

            bm.onBctEvent(bctEvent, threshold - 100);
        }

        {
            std::unique_lock<std::mutex> lock(m_lock);
            EQ(true,
               m_cond.wait_for(lock, milliseconds(timeout * 2),
                               [&received]() { return received; }));

            EQ(true,
               std::chrono::duration_cast<milliseconds>(
                   steady_clock::now() - lastOk) >= (milliseconds(timeout) * 2));
        }
    }

    void noBeamWarning()
    {
        BeamMonitor bm;

        bool received = false;
        const int32_t timeout = 1500;

        EQ(timeout, Config::instance().beamDetectionTimeout());

        bm.warningSignal.connect(
            [this, &received](WarningCode code, const std::string &msg) {
                if ((code != WARN_NO_BEAM) ||
                    (msg.find("[BeamMonitor]: no beam for a long "
                              "time") == std::string::npos))
                    return;

                {
                    std::lock_guard<std::mutex> lock(m_lock);
                    received = true;
                }

                m_cond.notify_all();
            });

        bm.start();

        /** emulate BCT event */
        TimingEventDetails bctEvent(
            createTimingEvent(1234, "CALIBRATION", 1, 1));

        bm.onBctEvent(bctEvent, 3000);
        const steady_clock::time_point lastOk = steady_clock::now();

        std::unique_lock<std::mutex> lock(m_lock);
        EQ(true,
           m_cond.wait_for(lock, milliseconds(timeout * 2),
                           [&received]() { return received; }));

        EQ(true,
           std::chrono::duration_cast<milliseconds>(
               steady_clock::now() - lastOk) >= milliseconds(timeout));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBeamMonitor);
