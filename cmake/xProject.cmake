
include(CMakeParseArguments)
include(ExternalProject)

include(ExtCache)

set_directory_properties(PROPERTIES
    EP_BASE "${CMAKE_BINARY_DIR}/external")

if(NOT GIT_EXE)
    find_package(Git)
    if(GIT_FOUND)
        set(GIT_EXE ${GIT_EXECUTABLE})
    endif(GIT_FOUND)
endif(NOT GIT_EXE)

function(xExternalProject_Add TARGET)
    cmake_parse_arguments(P "NOPREFETCH;AUTO_UPDATE;GIT_SUBMODULE" "GIT_REPOSITORY;GIT_TAG;LIBDIR" "INCLUDE_DIRS;LIBRARIES" ${ARGN})
    string(TOLOWER "${TARGET}" TARGET_LOW)

    if(${TARGET}_GIT_REPOSITORY)
        set(P_GIT_REPOSITORY ${${TARGET}_GIT_REPOSITORY})
        message(STATUS "Using ${TARGET} git repository: ${P_GIT_REPOSITORY}")
    endif(${TARGET}_GIT_REPOSITORY)
    if(${TARGET}_GIT_TAG)
        set(P_GIT_TAG ${${TARGET}_GIT_TAG})
        message(STATUS "Using ${TARGET} git tag: ${P_GIT_TAG}")
    endif(${TARGET}_GIT_TAG)

    if(NOT P_INCLUDE_DIRS)
        set(P_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${TARGET_LOW}/${CMAKE_INSTALL_INCLUDEDIR}")
    endif(NOT P_INCLUDE_DIRS)
    if(NOT P_LIBDIR)
        set(P_LIBDIR "${CMAKE_BINARY_DIR}/instroot/${TARGET_LOW}/${CMAKE_INSTALL_LIBDIR}")
    endif(NOT P_LIBDIR)

    if(NOT "${TARGET}_SRC_DIR")
        if(P_GIT_REPOSITORY)
                set(P_ARG
                    GIT_REPOSITORY "${P_GIT_REPOSITORY}"
                    GIT_TAG "${P_GIT_TAG}")
        endif()
        set(${TARGET}_SRC_DIR "${CMAKE_SOURCE_DIR}/deps/${TARGET_LOW}")
    else()
        message(STATUS "Using ${TARGET} source dir: ${${TARGET}_SRC_DIR}")
    endif()

    # Git submodule support
    if(P_GIT_SUBMODULE AND NOT P_NOPREFETCH)
        if(NOT EXISTS "${${TARGET}_SRC_DIR}/.git")
            message("Initializing git submodules")
            execute_process(COMMAND ${GIT_EXE} "submodule" "update" "--init"
                WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")
        endif()
    endif()

    ExternalProject_Add(${TARGET}
        ${P_ARG}
        SOURCE_DIR ${${TARGET}_SRC_DIR}
        ${P_UNPARSED_ARGUMENTS})

    if(CMAKE_VERSION VERSION_LESS "3.1")
        get_property(stamp TARGET ${TARGET} PROPERTY _EP_STAMP_DIR)
        macro(extra_stamp step before)
            ExternalProject_Add_Step(${TARGET} ${step}-stamp
                COMMAND touch "${stamp}/${TARGET}-${step}"
                COMMAND touch "${stamp}/${TARGET}-${step}-stamp"
                DEPENDEES "${step}"
                DEPENDERS "${before}")
        endmacro()
        # cmake -E touch Seems to have timestamps issues on CERN CC7
        extra_stamp(configure build)
        extra_stamp(build install)
    endif()

    if(NOT P_AUTO_UPDATE)
        get_property(stamp TARGET ${TARGET} PROPERTY _EP_STAMP_DIR)
        # Disable default update by creating stamp
        # Depends on same thing than update, but is ran after and does create the stamp
        # Workarround for CMake <= 2.8 on CC7
        ExternalProject_Add_Step(${TARGET} update-stamp
            COMMENT "Creating timestamp for update step"
            COMMAND touch "${stamp}/${TARGET}-update"
            COMMAND touch "${stamp}/${TARGET}-update-stamp"
            DEPENDEES update download
            DEPENDERS build)
    endif()
    ExternalProject_Add_Step(${TARGET} install-always
        COMMAND rm "${stamp}/${TARGET}-install"
        DEPENDEES install
        ALWAYS 1)

    set(${TARGET}_INCLUDE_DIRS "${P_INCLUDE_DIRS}" CACHE PATH "" FORCE)
    set(${TARGET}_LIBRARIES "${P_LIBRARIES}" CACHE PATH "" FORCE)
    set(${TARGET}_LIBDIR "${P_LIBDIR}" CACHE PATH "" FORCE)

    if(NOT P_NOPREFETCH AND NOT P_GIT_SUBMODULE)
        get_property(stamp TARGET ${TARGET} PROPERTY _EP_STAMP_DIR)
        get_property(command TARGET ${TARGET} PROPERTY _EP_download_COMMAND)
        message(STATUS "Prefetching ${TARGET}")

        # Handling multi-commands
        set(P_SUB "")
        foreach(PART ${command})
            if(PART STREQUAL "COMMAND")
                execute_process(COMMAND ${P_SUB})
                set(P_SUB "")
            else()
                list(APPEND P_SUB "${PART}")
            endif()
        endforeach()
        if(P_SUB)
            execute_process(COMMAND ${P_SUB})
        endif()
        file(WRITE "${stamp}/${TARGET}-download" "")
    endif()
    xProject_register(${TARGET})
endfunction()

function(xProject_register TARGET)
    if(NOT TARGET xProject-download)
        add_custom_target(xProject-download)
    endif(NOT TARGET xProject-download)
    get_property(command TARGET ${TARGET} PROPERTY _EP_download_COMMAND)
    if(command)
        add_custom_command(TARGET xProject-download
            COMMAND ${command})
    endif()

    if(NOT TARGET xProject-update)
        add_custom_target(xProject-update)
    endif(NOT TARGET xProject-update)
    get_property(command TARGET ${TARGET} PROPERTY _EP_update_COMMAND)
    add_custom_command(TARGET xProject-update
        COMMAND ${CMAKE_COMMAND} -E echo "-- Updating ${TARGET}"
        COMMAND ${command})

    if(NOT TARGET xProject-build)
        add_custom_target(xProject-build)
    endif(NOT TARGET xProject-build)
    get_property(command TARGET ${TARGET} PROPERTY _EP_build_COMMAND)
    add_custom_command(TARGET xProject-build
        COMMAND ${CMAKE_COMMAND} -E echo "-- Building ${TARGET}"
        COMMAND ${command})

    if(NOT TARGET xProject-cmake-copy)
        add_custom_target(xProject-cmake-copy)
    endif(NOT TARGET xProject-cmake-copy)
    if(NOT "${TARGET}" STREQUAL "Platform")
        ExternalProject_Get_Property(${TARGET} SOURCE_DIR)
        foreach(CMAKE_FILE "Project.cmake" "ExtCache.cmake")
            add_custom_command(TARGET xProject-cmake-copy
                COMMAND ${CMAKE_COMMAND} -E copy
                    "${CMAKE_SOURCE_DIR}/cmake/${CMAKE_FILE}"
                    "${SOURCE_DIR}/cmake/${CMAKE_FILE}"
                    COMMAND ${CMAKE_COMMAND} -E echo "-- Copying ${CMAKE_FILE} in ${TARGET}")
        endforeach(CMAKE_FILE)
    endif()
endfunction()

macro(xProject_Add TARGET)
    list(APPEND xProject_list ${TARGET})
    set(xProject_list "${xProject_list}" CACHE INTERNAL "" FORCE)

    if(EXT_CACHE)
        load_extern_cache(CACHE_DIR ${EXT_CACHE} VAR_LIST
            "${TARGET}_SRC_DIR"
            "${TARGET}_INCLUDE_DIRS"
            "${TARGET}_LIBRARIES"
            "${TARGET}_LIBDIR")
    endif(EXT_CACHE)

    if(EXTERNAL_DEPENDENCIES)
        if(NOT ${TARGET}_INCLUDE_DIRS)
            message(FATAL_ERROR "External dependency not set, ${TARGET}_INCLUDE_DIRS missing")
        endif(NOT ${TARGET}_INCLUDE_DIRS)
        add_custom_target(${TARGET})
    else(EXTERNAL_DEPENDENCIES)
        xExternalProject_Add(${TARGET} ${ARGN})
    endif(EXTERNAL_DEPENDENCIES)
endmacro()
