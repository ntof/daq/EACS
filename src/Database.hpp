/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-12T09:40:33+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef DATABASE_HPP__
#define DATABASE_HPP__

#include <cstdint>
#include <ctime>
#include <map>
#include <string>

#include <Singleton.hpp>

#include "EACSTypes.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace eacs {

class DaqParametersDatabase;
class HVBoardInfo;
class HVChannelInfo;

/**
 * @brief database abstraction class
 * @details this class throws EACSException on error
 */
class Database : public ntof::utils::Singleton<Database>
{
public:
    typedef std::map<std::size_t, bool> FiltersPositionMap;

    virtual ~Database() = default;

    /**
     * @brief create a new run
     * @throws EACSException
     */
    virtual void createRun(const RunInfo &runInfo,
                           std::time_t start = std::time_t(-1)) = 0;

    /**
     * @brief update information about a run
     * @param[in]  info  output runInfo object
     * @throws EACSException
     */
    virtual void updateRunTitleAndDescription(const RunInfo &info) = 0;

    /**
     * @brief update run status and data status
     * @param[in] runNumber  the run to update
     * @param[in] status     status to set
     * @param[in] dataStatus data status to set
     * @throws EACSException
     */
    virtual void updateRunStatus(RunNumber runNumber,
                                 RunInfo::Status status,
                                 RunInfo::DataStatus dataStatus) = 0;

    /**
     * @brief update all previous runs that have no stop date and set status as
     * bad
     * @throws EACSException
     */
    virtual void updatePreviousRuns() = 0;

    /**
     * @brief retrieve information about a run
     * @param[in]  runNumber the runNumber to retrieve
     * @param[out]  info  output runInfo object
     * @throws EACSException(ERR_DB_ENOENT) if run is not found
     */
    virtual void getRunInfo(RunNumber runNumber, RunInfo &info) = 0;

    /**
     * @brief retrieve status information about a run
     * @param[in]   runNumber  the run to retrieve
     * @param[out]  status     the run status
     * @param[out]  dataStatus the run data status
     * @throws EACSException(ERR_DB_ENOENT) if run is not found
     */
    virtual void getRunStatus(RunNumber runNumber,
                              RunInfo::Status &status,
                              RunInfo::DataStatus &dataStatus) = 0;

    /**
     * @brief get latest run number
     * @return latest RunNumber, or InvalidRunNumber if none found
     * @throws EACSException
     */
    virtual RunNumber getLatestRun() = 0;

    /**
     * @brief Update the value of the total amount of protons in a run
     * @throws EACSException
     */
    virtual void addProtons(RunNumber runNumber, float protons) = 0;

    /**
     * @brief Insert a new event and the protons related to this event
     * @throws EACSException
     */
    virtual void insertTrigger(RunNumber runNumber,
                               EventNumber eventNumber,
                               BeamType beamType,
                               float intensity,
                               int64_t cycleStamp) = 0;

    /**
     * @brief retrieve information about a materials setup
     * @param[in]  id the setup to retrieve
     * @return true if materials setup exist
     * @throws EACSException
     */
    virtual bool isMaterialsSetup(int64_t id) = 0;

    /**
     * @brief retrieve information about a detectors setup
     * @param[in]  id the setup to retrieve
     * @return true if detectors setup exist
     * @throws EACSException
     */
    virtual bool isDetectorsSetup(int64_t id) = 0;

    /**
     * @brief insert a channel configuration in database
     * @param[in] runNumber    the runNumber
     * @param[in] location     daq/card/channel location
     * @param[in] enabled      whereas the channel is enabled
     * @param[in] params       card configuration
     * @details location can be generated with ChannelConfig::makeLocation
     * (ntofutils)
     */
    virtual void insertDaqConfig(RunNumber runNumber,
                                 uint32_t location,
                                 const DaqParametersDatabase &params) = 0;

    /**
     * @brief retrieve filters position
     * @param[in] materialsSetupId database index of the materials setup
     * @param[out] filtersPosition filter index to position map
     */
    virtual void getFiltersPosition(int64_t materialsSetupId,
                                    FiltersPositionMap &filtersPosition) = 0;

    /**
     * @brief retrieve sample exchanger position
     * @param[in] materialsSetupId database index of the materials setup
     * @param[out] position current sample position
     */
    virtual void getSamplePosition(int64_t materialsSetupId,
                                   int32_t &position) = 0;

    /**
     * @brief return the total number of params rows
     * @details only used for testing purpose
     */
    virtual int64_t countDaqParams() = 0;

    /**
     * @brief insert an highVoltage channel configuration in database
     * @param[in] runNumber the runNumber
     * @param[in] board     the board the channel belongs to
     * @param[in] channel   the channel to insert
     */
    virtual void insertHVChannelInfo(RunNumber runNumber,
                                     const HVBoardInfo &board,
                                     const HVChannelInfo &channel) = 0;

    /**
     * @brief return the total number of hv rows
     * @details only used for testing purpose
     */
    virtual int64_t countHVChannelInfo() = 0;

    /**
     * @brief get the database name
     */
    virtual const std::string &name() const = 0;

protected:
    friend class ntof::utils::Singleton<Database>;
};

} // namespace eacs

namespace utils {
// Singleton specialization
template<>
ntof::eacs::Database &Singleton<ntof::eacs::Database>::instance();
} // namespace utils

} // namespace ntof

#endif
