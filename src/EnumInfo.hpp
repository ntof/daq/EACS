/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-14T16:33:48+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef ENUMINFO_HPP__
#define ENUMINFO_HPP__

#include <cstring>
#include <initializer_list>
#include <map>
#include <string>

class Strcmp
{
public:
    bool operator()(const char *str1, const char *str2) const
    {
        return std::strcmp(str1, str2) < 0;
    }
};

/**
 * @brief enumeration to/from string conversion classs
 */
template<typename T>
class EnumInfo
{
public:
    typedef std::map<T, const char *> Map;
    typedef std::map<const char *, T, Strcmp> RevMap;

    explicit EnumInfo(std::initializer_list<typename Map::value_type> l) :
        m_map{l}
    {
        for (const auto &p : m_map)
            m_revMap.emplace(p.second, p.first);
    }

    std::string toString(T t, T defaultValue) const
    {
        typename Map::const_iterator it = m_map.find(t);
        if (it == m_map.end())
            it = m_map.find(defaultValue);
        return (it == m_map.end()) ? std::string() : std::string(it->second);
    }

    std::string toString(T t, const std::string &defaultName) const
    {
        typename Map::const_iterator it = m_map.find(t);
        return (it == m_map.end()) ? defaultName : std::string(it->second);
    }

    T fromString(const std::string &name, T defaultValue) const
    {
        typename RevMap::const_iterator it = m_revMap.find(name.c_str());
        return (it == m_revMap.end()) ? defaultValue : it->second;
    }

protected:
    Map m_map;
    RevMap m_revMap;
};

#endif