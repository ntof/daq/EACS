/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: Sep 24, 2014
** Author: agiraud
**
** Heavily reworked by matteof and sfargier
*/

#ifndef EACS_H_
#define EACS_H_

#include <list>
#include <memory>
#include <mutex>

#include <DIMState.h>
#include <DIMSuperCommand.h>
#include <DaqTypes.h>
#include <Flock.hpp>
#include <Queue.h>
#include <Singleton.hpp>
#include <Synchro.h>
#include <Worker.hpp>
#include <easylogging++.h>

#include "BeamMonitor.hpp"
#include "Database.hpp"
#include "EACSTypes.hpp"
#include "WriterFile.hpp"
#include "clients/ClientAddhCmd.hpp"
#include "clients/ClientAddhState.hpp"
#include "clients/ClientFilterStation.hpp"
#include "clients/ClientRFMerger.hpp"
#include "clients/ClientSampleExchanger.hpp"
#include "hv/HVMonitor.hpp"
#include "misc.h"
#include "services/ServiceConfig.hpp"
#include "services/ServiceDaqsList.hpp"
#include "services/ServiceEvents.hpp"
#include "services/ServiceInfo.hpp"
#include "services/ServiceRunConfig.hpp"
#include "services/ServiceRunInfo.hpp"
#include "services/ServiceStateMachine.hpp"
#include "services/ServiceTiming.hpp"

namespace ntof {
namespace eacs {

// Forward declarations
class EventValidator;
class Config;
class ClientBct;

typedef std::unique_ptr<ServiceRunConfig> ServiceRunConfigPtr;
typedef std::unique_ptr<ServiceRunInfo> ServiceRunInfoPtr;
typedef std::unique_ptr<ServiceEvents> ServiceEventsPtr;
typedef std::unique_ptr<ServiceTiming> ServiceTimingPtr;
typedef std::unique_ptr<ServiceConfig> ServiceConfigPtr;
typedef std::unique_ptr<WriterFile> WriterFilePtr;
typedef std::unique_ptr<EventValidator> EventValidatorPtr;
typedef std::unique_ptr<BeamMonitor> BeamMonitorPtr;

typedef std::shared_ptr<ServiceDaqsList> ServiceDaqsListSharedPtr;
typedef std::shared_ptr<ClientRFMerger> ClientRFMergerSharedPtr;
typedef std::shared_ptr<ClientAddhCmd> ClientAddhCmdSharedPtr;

/**
 * @class Eacs
 * @brief Main class of the EACS system
 */
class Eacs :
    public ntof::dim::DIMSuperCommand,
    public ntof::utils::Singleton<Eacs>
{
public:
    /**
     * @brief Get the hostname of the machine
     * @return The hostname of the machine
     */
    static const std::string &getHostname();

    /**
     * @brief Destructor of the class EACS
     */
    virtual ~Eacs();

    /**
     * @brief Receive all the commands send to the DIM Super Command. This
     *method is executed each time a command is received
     */
    void commandReceived(ntof::dim::DIMCmd &cmdData) override;

    /**
     * @brief Init of EACS services
     */
    void initServices();

    /**
     * @brief Run EACS
     * @details starts the event validator
     */
    void run();

    /**
     * @brief Start EACS in a separate thread
     * @details starts the event validator
     */
    void start();

    /**
     * @brief stop EACS
     * @details will either exit "run" or stop the thread spawned
     */
    void stop();

    /**
     * @brief Getter for the run number
     * @return the actual run number
     */
    uint32_t getRunNumber() const;

    /**
     * @brief Setter for the run number
     * @param runNumber : the new run number
     */
    void setRunNumber(int32_t runNumber);

    void clearErrors();
    void clearWarnings();

    static void destroy();

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    friend class Singleton<Eacs>;

    /**
     * @brief Constructor of the class EACS
     */
    Eacs();

    /**
     * @flag the EACS as "started", acquisition ongoing
     * @details must be called from worker context
     */
    void setStarted(bool value);

    /**
     * @brief Allow to know if a run is on-going
     * @return Return true if the system is started
     * @details must be called from worker context
     */
    bool isStarted() const;

    /**
     * @brief post a task for the EACS
     */
    void post(const std::function<void()> &fun);

    /**
     * @brief Add a command to the end of the list
     * @param command : the command to add at the list
     * @param parent : the whole xml command node
     */
    void addCommand(const std::string &command, const pugi::xml_node &parent);

    /**
     * @brief Execute the action when a new command is received
     * @param command: the command to be executed
     * @param cmdDoc : the whole xml command
     */
    void doAction(const std::string &command, const pugi::xml_document &cmdDoc);

    /**
     * @brief Actions to perform when a command start is received
     */
    void actionStart();

    /**
     * @brief Actions to perform when a command stop is received
     */
    void actionStop();

    /**
     * @brief Actions to perform when a command reset is received
     */
    void actionReset();

    /**
     * @brief Actions to perform when a command clear is received
     */
    void actionClear();

    /**
     * @brief triggered by daqs when they're waiting for start signal
     * @details is used to exit DAQS_INIT state
     */
    void onDaqReady();

    /**
     * @brief triggered by FilterStation when filters are in the right position
     * @details is used to exit FILTERS_INIT state
     */
    void onFiltersMoved();

    /**
     * @brief triggered by SampleExchanger when samples are in the right position
     * @details is used to exit SAMPLES_INIT state
     */
    void onSamplesMoved();

    /**
     * @brief triggered by HVMonitor to notify a state change
     * @details is used to exit HIGHVOLTAGE_INIT state and LOADING state
     */
    void onHighVoltageState();

    /**
     * @brief triggered by daqs/timing
     * @details when they're synced with underlying middleware
     */
    void onLoaded();

    /**
     * @brief sent by event-validator when there's nothing else to process
     * @details used to know when STOPPING is finished
     */
    void onValidatorRunning(bool isRunning);

    /**
     * @brief sent by the state-machine on first/last error
     */
    void onError(bool isError);

    /* state change actions */
    void enterError();
    void enterInit();
    void enterLoading();
    void enterIdle();
    void enterFiltersInit();
    void enterSamplesInit();
    void enterCollimatorInit();
    void enterHighVoltageInit();
    void enterDaqsInit();
    void exitDaqsInit();
    void enterMergerInit();
    void enterStarting();
    void exitStopping();

    /**
     * @brief clear RunInfo and set RunConfig from DB
     */
    void resetRunInfo();

    /**
     * @brief fill MODH and RCTR
     * @param[out] rctr
     * @param[out] modh
     */
    void genHeaders(RunControlHeader &rctr, ModuleHeader &modh);

    static std::string m_hostname; //!< Hostname
    RunNumber m_runNumber;         //!< The number used to identified a run
    RunInfo::DataStatus m_runDataStatus; //!< The data status of the ongoing run
    std::time_t m_startTime;
    ntof::utils::Flock m_startedLock; //!< operation lock

    ntof::utils::Worker m_worker; //!< Worker used to execute tasks on EACS
    Database &m_db;               //!< DB access
    std::mutex m_mutex; //!< Mutex used to avoid the concurrent access to
                        //!< the data

    ClientRFMergerSharedPtr m_clientRFMerger; //!< Client used to send commands
                                              //!< to the merger
    std::unique_ptr<ClientFilterStation> m_filterStation;
    std::unique_ptr<ClientSampleExchanger> m_sampleExchanger;
    std::unique_ptr<HVMonitor> m_highVoltage;

    ClientAddhCmdSharedPtr m_clientADDH; //!< Client used to send commands to
                                         //!< the ADDH Writer

    DIMStateClientPtr m_bctState;                 //!< Client to read BCT state
    std::unique_ptr<ClientAddhState> m_addhState; //!< Client to read ADDH
                                                  //!< Writer state

    WriterFilePtr m_writer; //!< worker based class to create run and eveh file.
    EventValidatorPtr m_eventValidator; //!< threader based class that manage
                                        //!< event validation

    BeamMonitorPtr m_beamMonitor; //!< threader based class designed to monitor
                                  //!< the beam by raising related warnings

    // Services

    // EACS/State
    ServiceStateMachine m_state; //!< EACS State Machine

    // EACS/RunInfo
    ServiceRunInfoPtr m_runInfo; //!< RunInfo service

    // EACS/RunConfig
    ServiceRunConfigPtr m_runConfig; //!< RunConfig service

    // EACS/Events
    ServiceEventsPtr m_events; //!< Events service

    // EACS/DaqList
    ServiceDaqsListSharedPtr m_daqs; //!< Daqs List service

    // EACS/Timing
    ServiceTimingPtr m_timing; //!< Timing service

    // EACS/Config
    ServiceConfigPtr m_config; //!< Config service

    std::unique_ptr<ServiceInfo> m_info; // EACS/Info
};

} // namespace eacs
} // namespace ntof

#endif /* EACS_H_ */
