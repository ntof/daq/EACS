/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-24T10:21:22+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "ClientSampleExchanger.hpp"

#include <exception>
#include <thread>

#include <DIMData.h>
#include <DIMXMLCommand.h>
#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "EACSException.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;

const std::string ClientSampleExchanger::AcqPositionNumber = "positionNumber";
const std::chrono::milliseconds ClientSampleExchanger::CommandTimeout(30000);

ClientSampleExchanger::ClientSampleExchanger() :
    m_client(new DIMDataSetClient(
        Config::instance().getSampleExchangerServiceName())),
    m_timeout(5000),
    m_state(IDLE)
{
    warningSignal(WARN_SAEX_MOVED, std::string()); // clear warnings
    m_client->dataSignal.connect([this](DIMDataSetClient &) { monitor(); });
}

ClientSampleExchanger::~ClientSampleExchanger()
{
    if (m_task.valid())
        m_task.wait();
    m_client.reset();
}

std::shared_future<void> ClientSampleExchanger::move(std::int32_t position)
{
    if (m_task.valid() &&
        (m_task.wait_for(std::chrono::milliseconds(100)) ==
         std::future_status::timeout))
    {
        LOG(ERROR) << "[ClientSampleExchanger]: already moving";
        std::promise<void> p;
        p.set_exception(std::make_exception_ptr(
            EACSException("already moving", __FILE__, __LINE__, ERR_SAEX_CMD)));
        return p.get_future();
    }

    m_state.store(MOVING);
    m_task = std::async(std::launch::async, [position, this]() {
        SignalWaiter waiter(m_client->dataSignal);

        try
        {
            // index is not useful
            DIMData data(0, "positionNumber", "", position);

            pugi::xml_document doc;
            pugi::xml_node cmd = doc.append_child("command");
            // do not insert as a command, name is used by the proxy
            data.insertInto(cmd, false);

            const std::string &cmdService =
                Config::instance().getSampleExchangerCmdServiceName();
            if (cmdService.empty())
                EACS_THROW("no command service configured", ERR_SAEX_CMD);

            DIMXMLCommand::sendCommand(cmdService, doc);

            if (!waiter.wait(
                    [this, position]() {
                        return getCurrentPosition() == position;
                    },
                    CommandTimeout))
            {
                std::ostringstream oss;
                oss << "[" << m_client->getServiceName()
                    << "]: Failed to move the SampleExchanger: timeout";
                EACS_THROW(oss.str(), ERR_SAEX_CMD);
            }
            m_targetPosition = position;
            m_state.store(MONITORING);
            movedSignal();
        }
        catch (DIMException &ex)
        {
            std::ostringstream oss;
            oss << "[" << m_client->getServiceName()
                << "]: Failed to send command to the SampleExchanger: "
                << ex.what();
            m_state.store(ERROR);
            errorSignal(ERR_SAEX_CMD, oss.str());
            EACS_THROW(oss.str(), ERR_SAEX_CMD);
        }
        catch (EACSException &ex)
        {
            m_state.store(ERROR);
            errorSignal(ERR_SAEX_CMD, ex.getMessage());
            throw;
        }
    });
    return m_task;
}

void ClientSampleExchanger::reset()
{
    m_state.store(IDLE);
    warningSignal(WARN_SAEX_MOVED, std::string());
}

int32_t ClientSampleExchanger::getCurrentPosition() const
{
    try
    {
        for (const DIMData &data : m_client->getLatestData())
        {
            if (data.getName() == AcqPositionNumber)
            {
                const int32_t currentPosition = data.getValue<int32_t>();
                return currentPosition;
            }
        }
    }
    catch (DIMException &ex)
    {
        LOG(WARNING) << "[ClientSampleExchanger]: failed to retrieve postion: "
                     << ex.getMessage();
    }
    return -1;
}

void ClientSampleExchanger::monitor()
{
    if (m_state.load() == MONITORING)
    {
        if (getCurrentPosition() != m_targetPosition)
        {
            std::ostringstream oss;
            oss << "[" << m_client->getServiceName()
                << "]: SampleExchanger moved during acquisition: "
                << m_targetPosition << " != " << getCurrentPosition();
            LOG(WARNING) << "[ClientSampleExchanger]: " << oss.str();
            warningSignal(WARN_SAEX_MOVED, oss.str());
        }
    }
}
