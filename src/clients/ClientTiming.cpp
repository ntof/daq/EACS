/*
 * TimerClient.cpp
 *
 * Created on: Oct 29, 2014
 * Author: agiraud
 */

#include "ClientTiming.hpp"

#include "Config.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"

using namespace ntof::dim;

namespace ntof {
namespace eacs {

ClientTiming::ClientTiming() :
    m_triggers(Config::instance().getTriggersHistory()),
    m_eventReader(
        new EventReader(Config::instance().getTimeMachineName() + "/event"))
{
    m_eventReader->registerHandler(this);
}

ClientTiming::~ClientTiming()
{
    m_eventReader.reset();
}

void ClientTiming::eventReceived(const EventReader::Data &event)
{
    LOG(INFO)
        << "[ClientTiming] event received: eventNumber:" << event.evtNumber
        << " cycleStamp:" << event.cycleStamp;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_triggers.push_back(event);
    }
    timingSignal(event);
}

/**
 * Called when link is lost with timing machine
 */
void ClientTiming::noLink()
{
    // can't raise an error here, since we may not receive timing events if
    // machine is disabled
    LOG(ERROR) << "[ClientTiming] no link";
}

TimingEventDetails ClientTiming::getTriggerInfo(int64_t cycleStamp)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    TimingEventDetails res;
    bool found = false;

    for (TriggersBuffer::iterator triggerIterator = m_triggers.begin();
         triggerIterator != m_triggers.end(); ++triggerIterator)
    {
        if (triggerIterator->cycleStamp == cycleStamp)
        {
            res = (*triggerIterator);
            found = true;
            break;
        }
    }

    // Throw exception if not found
    if (!found)
    {
        std::ostringstream errorStream;
        errorStream << "No trigger found for the cycleStamp " << cycleStamp;
        LOG(ERROR) << errorStream.str();
        throw EACSException(errorStream.str(), __FILE__, __LINE__);
    }

    return res;
}

} // namespace eacs
} // namespace ntof
