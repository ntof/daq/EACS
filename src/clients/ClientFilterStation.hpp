/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T08:35:26+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef CLIENTFILTERSTATION_HPP__
#define CLIENTFILTERSTATION_HPP__

#include <atomic>
#include <chrono>
#include <future>
#include <memory>
#include <mutex>
#include <vector>

#include <DIMParamListClient.h>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

class ClientFilterStation
{
public:
    ClientFilterStation();
    ~ClientFilterStation();

    enum Params
    {
        AirPressure = 1,
        FilterFirst = 2,
        FilterLast = FilterFirst + 7
    };

    enum FilterPosition
    {
        PositionUnknown = 0,
        PositionIn = 1,
        PositionOut = 2,
        PositionMoving = 3,
        PositionInInterlocked = 4,
        PositionOutInterlocked = 5,
        PositionMovingInterlocked = 6
    };

    /**
     * @brief ignore "locked" state in comparison
     * @param[in]  filter1
     * @param[in]  filter2
     * @return true if position is roughly the same
     */
    static bool compare(FilterPosition filter1, FilterPosition filter2);

    enum State
    {
        IDLE,
        ERROR,
        MOVING,
        MONITORING
    };

    static const std::size_t FiltersCount;

    /**
     * @brief move the filters according to positions
     * @param[in]  positions an array with FiltersCount positions
     * @return a future that may throw EACSException
     * @details calling this method should also trigger errorSignal or
     * movedSignal
     * @details position will be monitored once filters are moved
     */
    std::shared_future<void> move(const std::vector<FilterPosition> &positions);

    /**
     * @brief stop monitoring
     */
    void reset();

    /**
     * @brief set timeout value for the filter station
     * @param[in] ms timeout
     */
    void setTimeout(std::chrono::milliseconds ms);

    inline ntof::dim::DIMParamListClient &client() { return *m_client; }

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    ntof::utils::signal<void()> movedSignal;

protected:
    bool checkPositions(const std::vector<FilterPosition> &positions);
    void monitor();

    std::unique_ptr<ntof::dim::DIMParamListClient> m_client;
    std::shared_future<void> m_task;
    std::chrono::milliseconds m_timeout;
    std::atomic<State> m_state;
    std::mutex m_lock;
    std::vector<FilterPosition> m_positions;
};

} // namespace eacs
} // namespace ntof

#endif
