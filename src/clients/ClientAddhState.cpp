/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-20T11:42:23+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ClientAddhState.hpp"

#include <easylogging++.h>

#include "DIMState.h"
#include "DIMUtils.hpp"

using namespace ntof::eacs;

ClientAddhState::ClientAddhState()
{
    dim::DIMLockGuard dimLock;
    m_addhState.reset(new ntof::dim::DIMStateClient("ADDH/State"));

    m_addhState->errorSignal.connect(
        [this](const std::string &message, dim::DIMStateClient & /*client*/) {
            if (message == dim::DIMStateClient::NoLinkError)
            {
                LOG(ERROR) << "[ADDH/State]: no link";
                errorSignal(static_cast<ErrorCode>(ERR_ADDH_STATE_NO_LINK),
                            "[ADDH/State]: no link");
            }
            else
            {
                LOG(ERROR) << "[ClientAddhState] error received: " << message;
            }
        });

    m_addhState->dataSignal.connect([this](dim::DIMStateClient &client) {
        // Remove no link error
        errorSignal(static_cast<ErrorCode>(ERR_ADDH_STATE_NO_LINK),
                    std::string());
        if (static_cast<GlobalState>(client.getActualState()) == ERROR)
        {
            std::ostringstream oss;
            oss << "[ADDH/State]: error";
            dim::DIMStateClient::ErrWarnVector errors = client.getActiveErrors();

            if (!errors.empty())
                oss << " : " << errors.back().getMessage();
            LOG(ERROR) << oss.str();
            errorSignal(ERR_ADDH_STATE_ERROR, oss.str());
        }
        else
        {
            errorSignal(ERR_ADDH_STATE_ERROR, std::string());
        }

        std::set<WarningCode> oldCodes;
        oldCodes.swap(m_lastActiveWarningCodes);

        for (const ntof::dim::DIMErrorWarning &warn : client.getActiveWarnings())
        {
            WarningCode code = static_cast<WarningCode>(WARN_ADDH_STATE +
                                                        warn.getCode());
            oldCodes.erase(code);
            m_lastActiveWarningCodes.insert(code);
            warningSignal(code, "[ADDH/State]: " + warn.getMessage());
        }

        // Clear old warnings
        for (WarningCode code : oldCodes)
        {
            warningSignal(code, std::string());
        }
    });
}
