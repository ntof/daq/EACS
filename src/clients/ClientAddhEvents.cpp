/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-29T09:48:14+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "ClientAddhEvents.hpp"

#include <easylogging++.h>

using namespace ntof::eacs;

ClientAddhEvents::ClientAddhEvents() : m_error(false)
{
    m_info.reset(new ntof::dim::DIMXMLInfo("ADDH/Events", this));
}

void ClientAddhEvents::errorReceived(std::string errMsg,
                                     const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[ADDH/Events]: parse error: " << errMsg;
    m_error = true;
    errorSignal(ERR_ADDH_EVENTS_XML_INVALID,
                "[ADDH/Events]: parse error: " + errMsg);
}

void ClientAddhEvents::dataReceived(pugi::xml_document &doc,
                                    const ntof::dim::DIMXMLInfo * /*info*/)
{
    pugi::xml_node node = doc.first_child().child("event");
    if (!node)
    {
        if (doc.document_element())
            errorReceived("invalid xml document", m_info.get());
        else
            LOG(WARNING) << "[ADDH/Events] empty document received";
        // at least we have a link
        errorSignal(ERR_ADDH_EVENTS_NO_LINK, std::string());
        return;
    }

    const Event event = {node.attribute("runNumber").as_int(-1),
                         node.attribute("timingEvent").as_int(-1),
                         node.attribute("filePath").as_string(),
                         node.attribute("fileSize").as_ullong(0)};
    LOG(INFO)
        << "[ADDH/Events]: event received"
        << " runNumber:" << event.runNumber
        << " eventNumber:" << event.timingEvent
        << " filePath:" << event.filePath << " fileSize:" << event.fileSize;

    clearErrors();
    signal(event);
}

void ClientAddhEvents::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    m_error = true;
    errorSignal(ERR_ADDH_EVENTS_NO_LINK, "[ADDH/Events]: no link");
}

void ClientAddhEvents::clearErrors()
{
    if (m_error)
    {
        m_error = false;
        errorSignal(ERR_ADDH_EVENTS_XML_INVALID, std::string());
        errorSignal(ERR_ADDH_EVENTS_NO_LINK, std::string());
    }
}
