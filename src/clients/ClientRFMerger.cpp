//
// Created by matteof on 7/22/20.
//

#include "ClientRFMerger.hpp"

#include <DIMException.h>
#include <easylogging++.h>

#include "EACSException.hpp"

namespace ntof {
namespace eacs {

ClientRFMerger::ClientRFMerger()
{
    m_clientCmd.reset(new dim::DIMSuperClient("MERGER"));
    m_clientCurrent.reset(new dim::DIMParamListClient("MERGER/Current"));
}

void ClientRFMerger::sendEvent(RunNumber runNumber,
                               int32_t timingEvent,
                               int32_t validatedEvent,
                               uint64_t fileSize)
{
    LOG(INFO) << "[ClientRFMerger] sending runNumber:" << runNumber
              << " eventNumber:" << timingEvent << " seqNum:" << validatedEvent
              << " fileSize:" << fileSize;

    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("command");
    root.append_attribute("name").set_value("event");
    root.append_attribute("runNumber").set_value(runNumber);
    root.append_attribute("timingEvent").set_value(timingEvent);
    root.append_attribute("validatedEvent").set_value(validatedEvent);
    root.append_attribute("fileSize")
        .set_value((unsigned long long int) fileSize);

    sendCommand(doc);
}
void ClientRFMerger::sendRunStart(RunNumber runNumber,
                                  const std::string &experiment,
                                  const std::vector<uint32_t> &activeDaqCrateId)
{
    LOG(INFO)
        << "[ClientRFMerger] sending runStart command runNumber:" << runNumber
        << " experiment:" << experiment;

    pugi::xml_document doc;
    pugi::xml_node cmd = doc.append_child("command");
    cmd.append_attribute("name").set_value("runStart");
    cmd.append_attribute("runNumber").set_value(runNumber);
    if (!experiment.empty())
    {
        cmd.append_attribute("experiment").set_value(experiment.c_str());
    }
    cmd.append_attribute("approved").set_value("1");
    cmd.append_attribute("standalone").set_value("0");

    for (uint32_t crateId : activeDaqCrateId)
    {
        pugi::xml_node daq = cmd.append_child("daq");
        daq.append_attribute("crateId").set_value(crateId);
    }
    sendCommand(doc);
}
void ClientRFMerger::sendRunStop(RunNumber runNumber, bool ignoreErrors)
{
    LOG(INFO) << "[ClientRFMerger] sending runStop command";

    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("command");
    root.append_attribute("name").set_value("runStop");
    root.append_attribute("runNumber").set_value(runNumber);

    sendCommand(doc, ignoreErrors);
}

void ClientRFMerger::sendCommand(pugi::xml_document &doc, bool ignoreErrors)
{
    try
    {
        ntof::dim::DIMAck ack = m_clientCmd->sendCommand(doc);
        if (ack.getErrorCode() < 0)
        {
            std::ostringstream oss;
            oss << "[RFMerger]: Acknowledge error from the merger: "
                << ack.getMessage();
            errorSignal(ERR_RFM_ACK, oss.str());
        }
    }
    catch (const dim::DIMException &e)
    {
        std::ostringstream oss;
        oss << "[RFMerger]: Failed to send command to the merger: " << e.what();
        if (ignoreErrors)
        {
            LOG(WARNING) << oss.str();
        }
        else
        {
            errorSignal(ERR_RFM_CMD, oss.str());
        }
    }
}

RunNumber ClientRFMerger::getCurrentRunNumber() const
{
    RunNumber runNumber = 0; // 0 if not found
    for (const dim::DIMData &param : m_clientCurrent->getParameters())
    {
        if (param.getName() == "runNumber")
        {
            runNumber = param.getValue<uint32_t>();
            break; // Exit the for loop
        }
    }
    return runNumber;
}

} // namespace eacs
} // namespace ntof
