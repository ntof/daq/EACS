/*
 * TimerClient.h
 *
 * Created on: Oct 29, 2014
 * Author: agiraud
 */

#ifndef CLIENTTIMING_H_
#define CLIENTTIMING_H_

#include <memory>
#include <mutex>

#include <boost/circular_buffer.hpp>

#include <DIMParamListClient.h>
#include <EventReader.h>
#include <Signals.hpp>
#include <Synchro.h>

#include "misc.h"

using namespace ntof::dim;

namespace ntof {
namespace eacs {

const int32_t EVENT_INDEX = 5; //!< ID of the event number in the XML
typedef boost::circular_buffer<TimingEventDetails> TriggersBuffer;

/**
 * @class ClientTiming
 * @brief class to read the DIM Service published by the timing machine. The
 * class receive the new trigger and send it to the EACS
 */
class ClientTiming : public ntof::dim::EventHandler
{
public:
    typedef ntof::utils::signal<void(const TimingEventDetails &event)>
        TimingSignal;

    ClientTiming();

    /**
     * @brief Destructor of the class TimerClient
     */
    ~ClientTiming() override;

    /**
     * @brief This function is called each time a new event is received
     * @param event information about the event
     */
    void eventReceived(const ntof::dim::EventReader::Data &event) override;

    /**
     * @brief Called when link is lost with timing machine
     */
    void noLink() override;

    /**
     * @brief Get the ...
     */
    // FIXME: replace with ntof::dim::EventReader::Data
    TimingEventDetails getTriggerInfo(int64_t cycleStamp);

    TimingSignal timingSignal;

protected:
    std::mutex m_mutex;        //!< Mutex to avoid concurrent access
    TriggersBuffer m_triggers; //!< Keep an history of the triggers
    std::unique_ptr<ntof::dim::EventReader>
        m_eventReader; //!< Object to parse the content of the
                       //!< service publish by the timing
                       //!< machine in order to get the event
                       //!< number
};

} // namespace eacs
} // namespace ntof

#endif /* CLIENTTIMING_H_ */
