//
// Created by matteof on 7/22/20.
//

#ifndef EACS_CLIENTRFMERGER_HPP
#define EACS_CLIENTRFMERGER_HPP

#include <boost/smart_ptr.hpp>

#include <DIMParamListClient.h>
#include <DIMSuperClient.h>

#include "misc.h"

namespace ntof {
namespace eacs {

class ClientRFMerger
{
public:
    typedef boost::scoped_ptr<ntof::dim::DIMSuperClient> DIMSuperClientPtr;
    typedef boost::scoped_ptr<ntof::dim::DIMParamListClient> DIMParamListClientPtr;

    ClientRFMerger();

    /**
     * @brief Send an event command to MERGER
     * @param runNumber: the run number
     * @param timingEvent: the id gave by the timing machine
     * @param validatedEvent: the amount of validated events since the beginning
     * of the run
     * @param fileSize: The size of the event (all the .acqc and the .event)
     */
    void sendEvent(RunNumber runNumber,
                   int32_t timingEvent,
                   int32_t validatedEvent,
                   uint64_t fileSize);

    /**
     * @brief Send runStart command to MERGER
     * @param runNumber the run number
     * @param experiment name of the experiment
     * @param activeDaqCrateId list of createId of active (used) daq
     */
    void sendRunStart(RunNumber runNumber,
                      const std::string &experiment,
                      const std::vector<uint32_t> &activeDaqCrateId);

    /**
     * @brief Send runStop command to MERGER
     * @param runNumber the run number
     * @param ignoreErrors silently discard errors
     */
    void sendRunStop(RunNumber runNumber, bool ignoreErrors = false);

    /**
     * @brief return the current RunNumber from Current service of RFMerger
     * @return the current RunNumber
     */
    RunNumber getCurrentRunNumber() const;

    /**
     * @brief signal used to notify errors
     */
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    void sendCommand(pugi::xml_document &doc, bool ignoreErrors = false);

    DIMSuperClientPtr m_clientCmd; //!< Client used to send commands to the
    //!< merger
    DIMParamListClientPtr m_clientCurrent; //!< Client used to get info about
                                           //!< current run
};

} // namespace eacs
} // namespace ntof
#endif // EACS_CLIENTRFMERGER_HPP
