/*
 * BeamClient.cpp
 *
 * Created on: Oct 20, 2014
 * Author: agiraud
 */

#include "ClientBct.hpp"

#include <sstream>

#include "Config.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"

namespace ntof {
namespace eacs {

ClientBct::ClientBct() : client_(Config::instance().getBctServiceName())
{
    client_.setHandler(this);
}

void ClientBct::dataChanged(std::vector<dim::DIMData> &dataSet,
                            const dim::DIMProxyClient & /*client*/)
{
    errorSignal(ERR_BCT_NO_LINK, std::string());
    if (dataSet.empty())
    {
        return;
    }

    BctEvent event;
    for (const dim::DIMData &d : dataSet)
    {
        if (d.getName() == "cycleStamp")
        {
            event.cycleStamp = d.getValue<int64_t>();
        }
        else if (d.getName() == "totalIntensitySingle")
        {
            event.totalIntensityPreferred = d.getValue<float>();
        }
    }

    if (event.cycleStamp != -1 && event.totalIntensityPreferred != -1)
    {
        bctSignal(event);
    }
    else
    {
        LOG(WARNING) << "[ClientBct] BctEvent received but data is incomplete.";
    }
}

void ClientBct::errorReceived(const std::string &errMsg,
                              const ntof::dim::DIMProxyClient & /*client*/)
{
    if (errMsg == DIMXMLInfo::NoLinkError)
    {
        std::ostringstream msg;
        msg << "[" << client_.getServiceName() << "]: no link";
        LOG(ERROR) << "[ClientBct] " << msg.str();
        errorSignal(ERR_BCT_NO_LINK, msg.str());
    }
    else
    {
        LOG(ERROR)
            << "[ClientBct] error received in the BCT Client: " << errMsg;
    }
}

} // namespace eacs
} // namespace ntof
