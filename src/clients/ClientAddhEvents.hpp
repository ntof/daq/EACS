/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-01-29T09:36:55+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef ADDHEVENTS_HPP__
#define ADDHEVENTS_HPP__

#include <cstdint>
#include <memory>
#include <string>

#include <DIMXMLInfo.h>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @brief client class for ADDH/Events service
 */
class ClientAddhEvents : public ntof::dim::DIMXMLInfoHandler
{
public:
    struct Event
    {
        int32_t runNumber;
        int32_t timingEvent;
        std::string filePath;
        size_t fileSize;
    };

    typedef ntof::utils::signal<void(const Event &event)> EventSignal;

    explicit ClientAddhEvents();
    virtual ~ClientAddhEvents() = default;

    void errorReceived(std::string errMsg,
                       const ntof::dim::DIMXMLInfo *info) override;
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info) override;
    void noLink(const ntof::dim::DIMXMLInfo *info) override;

    EventSignal signal;
    ErrorSignal errorSignal;

protected:
    /**
     * @brief send signals to clear errors
     */
    void clearErrors();

    bool m_error;
    std::unique_ptr<ntof::dim::DIMXMLInfo> m_info;
};

} // namespace eacs
} // namespace ntof

#endif
