/*
 * BeamClient.h
 *
 * Created on: Oct 20, 2014
 * Author: agiraud
 */

#ifndef CLIENTBCT_H_
#define CLIENTBCT_H_

#include <iostream>
#include <mutex>
#include <vector>

#include <DIMProxyClient.h>
#include <DaqTypes.h>
#include <Signals.hpp>
#include <Synchro.h>
#include <misc.h>

namespace ntof {
namespace eacs {
class Eacs;

struct BctEvent
{
    BctEvent() : cycleStamp(-1), totalIntensityPreferred(-1) {};

    int64_t cycleStamp;
    float totalIntensityPreferred;
};

/**
 * @class ClientBct
 * @brief Dim Client which is listening the DIM Service published by the Producer
 *
 * The class update automatically the BEAM parameters
 */
class ClientBct : public ntof::dim::DIMProxyClientHandler
{
public:
    typedef ntof::utils::signal<void(const BctEvent &event)> BctSignal;

    /**
     * @brief Constructor of the class BeamClient
     */
    ClientBct();

    /**
     * @brief Destructor of the class BeamClient
     */
    virtual ~ClientBct() = default;

    /**
     * @brief Function called each time the service change
     * @param dataSet: New list of data
     * @param client: Client responsible of this callback (can be shared)
     */
    void dataChanged(std::vector<ntof::dim::DIMData> &dataSet,
                     const ntof::dim::DIMProxyClient &client) override;

    /**
     * @brief Called when an error occured on client (NO-LINK, NOT READY...)
     * @param errMsg Error message
     * @param client Client responsible for this callback (can be shared)
     */
    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMProxyClient &client) override;

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    BctSignal bctSignal;

private:
    ntof::dim::DIMProxyClient client_; //!< The BCT Client
};
} // namespace eacs
} // namespace ntof
#endif /* CLIENTBCT_H_ */
