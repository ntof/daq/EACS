/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-20T09:36:55+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "ClientAddhCmd.hpp"

#include <DIMException.h>
#include <easylogging++.h>

#include "EACSException.hpp"

using namespace ntof::eacs;

ClientAddhCmd::ClientAddhCmd()
{
    m_cmd.reset(new ntof::dim::DIMSuperClient("ADDH/Command"));
}

void ClientAddhCmd::sendReset()
{
    LOG(INFO) << "[EACS] sending reset to ADDH";
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("command");
    root.append_attribute("name").set_value("reset");
    sendCommand(doc);
}

void ClientAddhCmd::sendClear()
{
    LOG(INFO) << "[EACS] sending clear to ADDH";
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("command");
    root.append_attribute("name").set_value("clear");
    sendCommand(doc);
}

void ClientAddhCmd::sendWrite(int32_t timingEvent,
                              int32_t runNumber,
                              const std::string &filePath,
                              const std::string &lsaCycle,
                              const std::string &user)
{
    LOG(INFO) << "[EACS] sending write to ADDH"
              << " runNumber:" << runNumber << " eventNumber:" << timingEvent
              << " lsaCycle:" << lsaCycle << " user:" << user
              << " filePath:" << filePath;

    // Build XML data to be sent
    pugi::xml_document doc;
    pugi::xml_node root = doc.append_child("command");
    root.append_attribute("name").set_value("write");
    root.append_attribute("timingEvent").set_value(timingEvent);
    root.append_attribute("filePath").set_value(filePath.c_str());
    root.append_attribute("runNumber").set_value(runNumber);
    root.append_attribute("lsaCycle").set_value(lsaCycle.c_str());
    root.append_attribute("user").set_value(user.c_str());
    sendCommand(doc);
}

void ClientAddhCmd::sendCommand(pugi::xml_document &doc)
{
    try
    {
        // Send the command
        ntof::dim::DIMAck ack = m_cmd->sendCommand(doc);
        if (ack.getErrorCode() < 0)
        {
            std::ostringstream oss;
            oss << "Acknowledge error from the ADDH Writer: "
                << ack.getMessage();
            throw EACSException(oss.str(), __FILE__, __LINE__);
        }
    }
    catch (const dim::DIMException &e)
    {
        std::ostringstream oss;
        oss << "[Addh] failed to send command: " << e.what();
        errorSignal(ERR_ADDH_EVENTS_CMD, oss.str());
    }
    catch (const EACSException &e)
    {
        errorSignal(ERR_ADDH_EVENTS_CMD, e.getMessage());
    }
}