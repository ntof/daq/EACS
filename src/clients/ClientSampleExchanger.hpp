/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-11-24T10:08:35+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef CLIENT_SAMPLE_EXCHANGER_HPP__
#define CLIENT_SAMPLE_EXCHANGER_HPP__

#include <atomic>
#include <chrono>
#include <cstdint>
#include <future>

#include <DIMDataSetClient.h>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

class ClientSampleExchanger
{
public:
    ClientSampleExchanger();
    ~ClientSampleExchanger();

    /**
     * @brief data service name in Acquisition,
     * better than index for proxied services
     */
    static const std::string AcqPositionNumber;

    enum State
    {
        IDLE,
        ERROR,
        MOVING,
        MONITORING
    };

    /**
     * @brief move device to this position
     * @details starts monitoring
     */
    std::shared_future<void> move(int32_t position);

    /**
     * @brief stop monitoring
     */
    void reset();

    inline ntof::dim::DIMDataSetClient &client() { return *m_client; }

    int32_t getCurrentPosition() const;

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

    /**
     * @brief signal sent when device reaches the right position
     */
    ntof::utils::signal<void()> movedSignal;

    /** @brief command timeout in milliseconds */
    static const std::chrono::milliseconds CommandTimeout;
    static constexpr int32_t DefaultPosition = 0;

protected:
    void monitor();

    std::unique_ptr<ntof::dim::DIMDataSetClient> m_client;
    std::shared_future<void> m_task;
    std::chrono::milliseconds m_timeout;
    int32_t m_targetPosition;
    std::atomic<State> m_state;
};

} // namespace eacs
} // namespace ntof

#endif
