/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-04-20T11:42:23+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef ADDHSTATE_HPP__
#define ADDHSTATE_HPP__

#include <cstdint>
#include <memory>
#include <set>
#include <string>

#include <DIMXMLInfo.h>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @brief client class for ADDH/Events service
 */
class ClientAddhState
{
public:
    typedef dim::DIMStateClient::ErrWarnVector ErrWarnVector;

    explicit ClientAddhState();
    virtual ~ClientAddhState() = default;

    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    std::set<WarningCode> m_lastActiveWarningCodes;
    std::unique_ptr<dim::DIMStateClient> m_addhState;
};

} // namespace eacs
} // namespace ntof

#endif
