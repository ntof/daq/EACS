/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-05-20T09:36:55+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef ADDHCMD_HPP__
#define ADDHCMD_HPP__

#include <cstdint>
#include <memory>
#include <string>

#include <DIMSuperClient.h>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @brief client class for ADDH/Command
 */
class ClientAddhCmd
{
public:
    explicit ClientAddhCmd();
    virtual ~ClientAddhCmd() = default;

    /**
     * @brief Send reset command to the ADDH Writer
     */
    void sendReset();

    /**
     * @brief Send clear command to the ADDH Writer
     */
    void sendClear();

    /**
     * @brief Send write command to the ADDH Writer
     * @param timingEvent: the id gave by the timing machine
     * @param runNumber: The run number
     * @param lsaCycle: The timing lsaCycle
     * @param user: The timing user
     * @param filePath: The full path of the file
     */
    void sendWrite(int32_t timingEvent,
                   int32_t runNumber,
                   const std::string &filePath,
                   const std::string &lsaCycle,
                   const std::string &user);

    ErrorSignal errorSignal;

protected:
    void sendCommand(pugi::xml_document &doc);

    DIMSuperClientPtr m_cmd; //!< Client used to send commands to the
                             //!< ADDH Writer
};

} // namespace eacs
} // namespace ntof

#endif
