/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-08T09:08:22+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "ClientFilterStation.hpp"

#include <condition_variable>

#include <DIMUtils.hpp>
#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "EACSException.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;
using namespace ntof::utils;

const std::size_t ClientFilterStation::FiltersCount = 8;

ClientFilterStation::ClientFilterStation() :
    m_client(new DIMParamListClient(
        Config::instance().getFilterStationServiceName())),
    m_timeout(5000),
    m_state(IDLE)
{
    warningSignal(WARN_FS_MOVED, std::string());
    m_client->dataSignal.connect([this](DIMParamListClient &) { monitor(); });
}

ClientFilterStation::~ClientFilterStation()
{
    if (m_task.valid())
        m_task.wait();
    m_client.reset();
}

bool ClientFilterStation::compare(FilterPosition filter1, FilterPosition filter2)
{
    switch (filter1)
    {
    case PositionIn:
    case PositionInInterlocked:
        return (filter2 == PositionIn) || (filter2 == PositionInInterlocked);
    case PositionOut:
    case PositionOutInterlocked:
        return (filter2 == PositionOut) || (filter2 == PositionOutInterlocked);
    default: return filter1 == filter2;
    }
}

std::shared_future<void> ClientFilterStation::move(
    const std::vector<FilterPosition> &positions)
{
    if (m_task.valid() &&
        (m_task.wait_for(std::chrono::milliseconds(100)) ==
         std::future_status::timeout))
    {
        LOG(ERROR) << "[ClientFilterStation]: already moving";
        std::promise<void> p;
        p.set_exception(std::make_exception_ptr(
            EACSException("already moving", __FILE__, __LINE__, ERR_FS_CMD)));
        return p.get_future();
    }

    m_state.store(MOVING);
    m_task = std::async(std::launch::async, [positions, this]() {
        SignalWaiter waiter(m_client->dataSignal);

        try
        {
            DIMData::List params;
            for (std::size_t i = 0; i < positions.size(); ++i)
            {
                params.push_back(
                    DIMData(i + FilterFirst, DIMEnum(positions[i])));
            }

            m_client->sendParameters(params);

            if (!waiter.wait(
                    [this, &positions]() { return checkPositions(positions); },
                    m_timeout))
            {
                std::ostringstream oss;
                oss << "[" << m_client->getServiceName()
                    << "]: Failed to move the FilterStation: timeout";
                EACS_THROW(oss.str(), ERR_FS_CMD);
            }
            LOG(INFO) << "[ClientFilterStation]: all filters moved";
            {
                std::lock_guard<std::mutex> lock(m_lock);
                m_positions = positions;
            }
            m_state.store(MONITORING);
            movedSignal();
        }
        catch (DIMException &ex)
        {
            std::ostringstream oss;
            oss << "[" << m_client->getServiceName()
                << "]: Failed to send command to the FilterStation: "
                << ex.what();
            m_state.store(ERROR);
            errorSignal(ERR_FS_CMD, oss.str());
            EACS_THROW(oss.str(), ERR_FS_CMD);
        }
        catch (EACSException &ex)
        {
            m_state.store(ERROR);
            errorSignal(ERR_FS_CMD, ex.getMessage());
            throw;
        }
    });
    return m_task;
}

void ClientFilterStation::reset()
{
    m_state.store(IDLE);
    warningSignal(WARN_FS_MOVED, std::string());
}

bool ClientFilterStation::checkPositions(
    const std::vector<FilterPosition> &positions)
{
    std::size_t count = 0;

    for (const DIMData &data : m_client->getParameters())
    {
        if (data.getIndex() >= FilterFirst && data.getIndex() <= FilterLast)
        {
            ++count;
            FilterPosition pos = FilterPosition(data.getEnumValue().getValue());
            std::size_t index = data.getIndex() - FilterFirst;
            if (positions.size() < index)
            {
                LOG(WARNING)
                    << "[ClientFilterStation]: unknown filter index:" << index;
            }
            else if (!compare(positions[index], pos))
            {
                LOG(INFO) << "[ClientFilterStation]: filter " << (index + 1)
                          << " is " << data.getEnumValue().getName();
                return false;
            }
        }
    }
    if (count != positions.size())
    {
        LOG(INFO) << "[ClientFilterStation]: filter information missing";
        return false;
    }
    return true;
}

void ClientFilterStation::monitor()
{
    if (m_state.load() == MONITORING)
    {
        bool moved;
        {
            std::lock_guard<std::mutex> lock(m_lock);
            moved = !checkPositions(m_positions);
        }
        if (moved)
        {
            std::ostringstream oss;
            oss << "[" << m_client->getServiceName()
                << "]: FilterStation moved during acquisition";
            LOG(WARNING) << "[ClientFilterStation]: " << oss.str();
            warningSignal(WARN_FS_MOVED, oss.str());
        }
    }
}

void ClientFilterStation::setTimeout(std::chrono::milliseconds ms)
{
    m_timeout = ms;
}
