/*
 * CheckFiles.h
 *
 * Created on: Nov 6, 2014
 * Author: agiraud
 */

#ifndef EVENTVALIDATOR_H_
#define EVENTVALIDATOR_H_

#include <chrono>
#include <condition_variable>
#include <list>
#include <mutex>

#include <Signals.hpp>
#include <Thread.hpp>

#include "EACSTypes.hpp"
#include "clients/ClientAddhEvents.hpp"
#include "clients/ClientRFMerger.hpp"
#include "misc.h"
#include "services/ServiceDaqsList.hpp"

namespace ntof {
namespace eacs {

class ServiceEventValidator;

/*
 * @class CheckFiles
 */
class EventValidator : public ntof::utils::Thread
{
public:
    typedef std::shared_ptr<ClientRFMerger> ClientRFMergerSharedPtr;
    typedef std::shared_ptr<ServiceDaqsList> ServiceDaqsListSharedPtr;

    struct ValidEvent
    {
        EventNumber eventNumber;
        RunNumber runNumber;
        size_t fileSize;
        std::chrono::steady_clock::time_point start;
    };

    struct EventData
    {
        EventNumber eventNumber; //!< Event number given by the timing machine
        int64_t cycleStamp;      //!< Event Cycle Stamp
        RunNumber runNumber;     //!< Run number
        BeamType beamTypes;      //!< Type of the beam as an enum
        float intensity;         //!< Intensity of the beam
        std::chrono::steady_clock::time_point start;
    };

    typedef ntof::utils::signal<void(const ValidEvent &event,
                                     uint32_t extensionNumber,
                                     float intensity,
                                     float totalIntensity)>
        ValidatedSignal;
    typedef ntof::utils::signal<void(bool isRunning)> RunningSignal;
    typedef std::map<EventNumber, ValidEvent> ValidEventMap;
    typedef std::map<EventNumber, EventData> EventDataMap;

    EventValidator(const ClientRFMergerSharedPtr &clientRfMerger,
                   const ServiceDaqsListSharedPtr &m_serviceDaq);
    ~EventValidator() override;

    /**
     * @brief On Bct event, create and commit an EventData in order to be
     * processed
     * @param event: Timing event details
     * @param runNumber: The run number
     * @param intensity: number of protons
     */
    void onBctEvent(const TimingEventDetails &event,
                    RunNumber runNumber,
                    float intensity);

    uint32_t getExtensionNumber() const;

    bool hasEvents() const;

    void reset();

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    ValidatedSignal validatedSignal;
    RunningSignal runningSignal;

protected:
    /**
     * @brief Launch the thread
     */
    void thread_func() override;

    // void deleteIndexFile(int32_t eventNumber, int32_t runNumber);

    /**
     * @brief Commit the data relative to an event number in database
     * @param event: the valid event
     */
    void commitDatabase(const EventData &event);

    /**
     * @brief Print the content of the object in order to be able to debug
     */
    void printEventNumberWrote();

    /**
     * @brief Take the events from the validated map (ADDH)
     * @param[in] eventNumber: the id given by the timing machine to identify
     * the event
     * @param[out] event: the event
     * @return true if event is found
     */
    bool takeValidEvent(EventNumber eventNumber, ValidEvent &event);

    /**
     * @brief Take event from the bct map
     * @param eventNumber: the id given by the timing machine to identify the
     * event
     * @param[out] event: the event
     * @return true if event is found
     */
    bool takeEvent(EventNumber eventNumber, EventData &event);

    /**
     * @brief AddhEvent slot
     * @details this method is called whenever ADDH services emits an ADDH/Events
     */
    void onAddhEvent(const ClientAddhEvents::Event &event);

    void processEvents();
    void processValidatedEvents();
    /**
     * @brief convenience function to increment missed event count
     */
    void missEvent();

    uint32_t m_extensionNumber; //!< The number of event validated since the
                                //!< beginning of the run
    double m_totalIntensity;    //!< validated events total intensity
    RunNumber m_runNumber; //!< Used to know when the extension number can be
                           //!< reset
    uint32_t m_timeout;    //!< Time to wait before defining an event as unvalid
    ValidEventMap m_validEvents; //!< Collection of event
                                 //!< validated by the EACS
    EventDataMap m_events;       //!< Collection of the data
                                 //!< related to the event
                                 //!< before being committed
                                 //!< into database

    int32_t m_missedEvents; //!< Number of consecutive missed events
    bool m_hasEvents;       //!< true if there are still events to be processed

    std::unique_ptr<ServiceEventValidator> m_srv;

    ClientAddhEvents m_addhEvents;

    // Dependencies
    ServiceDaqsListSharedPtr m_serviceDaq; //!< DaqList service
    ClientRFMergerSharedPtr m_clientRFMerger;
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* EVENTVALIDATOR_H_ */
