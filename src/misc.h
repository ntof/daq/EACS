/*
 * misc.h
 *
 *  Created on: Feb 10, 2017
 *      Author: agiraud
 */

#ifndef MISC_H_
#define MISC_H_

#include <DIMStateClient.h>
#include <DIMSuperClient.h>
#include <EventReader.h>
#include <Signals.hpp>

#include "EACSTypes.hpp"

namespace ntof {
namespace eacs {

/* common states to the machines */
enum GlobalState
{
    NO_LINK_STATE = -3,
    ERROR = -2,
    NOT_READY = -1,
};

/* ID of the data into the run info service */
enum EacsRunInfo
{
    RUN_NUMBER = 1,
    EVENT_NUMBER,
    NUMBER_OF_EVENTS_VALID,
    NUMBER_OF_PROTONS_VALID,
    TITLE,
    DESCRIPTION,
    AREA,
    EXPERIMENT,
    DETECTORS_SETUP_ID,
    MATERIALS_SETUP_ID
};

typedef ntof::utils::signal<void()> ExternalUpdateSignal;
typedef ntof::utils::signal<void(ErrorCode error, const std::string &message)>
    ErrorSignal;
typedef ntof::utils::signal<void(WarningCode error, const std::string &message)>
    WarningSignal;

typedef std::unique_ptr<ntof::dim::DIMSuperClient> DIMSuperClientPtr;
typedef std::unique_ptr<ntof::dim::DIMStateClient> DIMStateClientPtr;

typedef struct
{
    int32_t periodNumber;
    EventNumber eventNumber;
    uint64_t validatedNumber;
    int64_t cyclestamp;
    BeamType beamType;
    float bct;
} EventFileDetails;

typedef ntof::dim::EventReader::Data TimingEventDetails;

typedef std::string SerialNumber;

} // namespace eacs
} // namespace ntof
#endif /* MISC_H_ */
