/*
 * WriterFile.cpp
 *
 * Created on: Aug 1, 2014
 * Author: agiraud
 */

#include "WriterFile.hpp"

#include <cstring>
#include <iostream>

#include <sys/stat.h>
#include <sys/types.h>

#include "Config.hpp"
#include "DaqTypes.h"
#include "Eacs.hpp"
#include "EventValidator.hpp"
#include "Synchro.h"

namespace ntof {
namespace eacs {

WriterFile::WriterFile(Eacs &eacs, ClientAddhCmd &addhCmd) :
    m_eacs(eacs), m_addhCmd(addhCmd), m_worker("WriterFile", 1, 2048)
{}

WriterFile::~WriterFile()
{
    m_worker.stop();
}

void WriterFile::onBctEvent(const TimingEventDetails &event,
                            RunNumber runNumber,
                            float intensity)
{
    this->m_worker.post([this, event, runNumber, intensity]() {
        writeFileEvent(event, runNumber, intensity);
    });
}

void WriterFile::onInitRun(const RunControlHeader &rctr,
                           const ModuleHeader &modh)
{
    this->m_worker.post([this, rctr, modh]() { writeFileRun(rctr, modh); });
}

void WriterFile::createDirectory(const std::string &filename)
{
    // Creation of the folder if it doesn't exist
    std::size_t found = filename.find_last_of("/\\");
    std::string directory = filename.substr(0, found);
    ::mkdir(directory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

std::string WriterFile::generateFilenameRun()
{
    // Concat the elements to have the full path of the file
    std::ostringstream ossFilename;
    uint32_t runNumber = m_eacs.getRunNumber();
    ossFilename << Config::instance().getFolderDest() << "run" << runNumber
                << "/run" << runNumber << ".run";
    return ossFilename.str();
}

std::string WriterFile::generateFilenameEvent(int32_t triggerNumber)
{
    // Concat the elements to have the full path of the file
    std::ostringstream ossFilename;
    uint32_t runNumber = m_eacs.getRunNumber();
    ossFilename << Config::instance().getFolderDest() << "run" << runNumber
                << "/run" << runNumber << "_" << triggerNumber << ".event";

    return ossFilename.str();
}

void WriterFile::writeFileRun(const RunControlHeader &rctr,
                              const ModuleHeader &modh)
{
    // Create the file
    const std::string filename = generateFilenameRun();
    createDirectory(filename);

    std::ofstream out;
    out.open(filename.c_str());
    if (!out.good())
    {
        errorSignal(
            FILE_ERR,
            "[WriterFile]: Unable to open or create the file : " + filename);
        return;
    }

    out << rctr;
    if (!out.good())
    {
        std::ostringstream oss;
        oss << "[WriterFile]: failed to write RCTR: " << filename;
        errorSignal(WRITE_ERR, oss.str());
    }

    out << modh;
    if (!out.good())
    {
        std::ostringstream oss;
        oss << "[WriterFile]: failed to write MODH: " << filename;
        errorSignal(WRITE_ERR, oss.str());
    }

    out.close();
    LOG(INFO) << "[WriterFile]: run file written: " << filename;
}

void WriterFile::writeFileEvent(const TimingEventDetails &event,
                                const RunNumber &runNumber,
                                float intensity)
{
    //			LOG(INFO) << "Write the event file";
    // Create the file
    const std::string filename = generateFilenameEvent(event.evtNumber);
    createDirectory(filename);

    std::ofstream out;
    out.open(filename.c_str());
    if (!out.good())
    {
        errorSignal(FILE_ERR,
                    "[WriterFile]: failed to open or create file : " + filename);
        return;
    }

    // Create EventHeader
    EventHeader eveh;
    eveh.sizeOfEvent = 0;
    eveh.eventNumber = event.evtNumber;
    eveh.runNumber = runNumber;
    // Split the cyclestamp
    eveh.updateDateTime(static_cast<time_t>(event.cycleStamp / 1E9));
    eveh.setCompTS();
    // Get the beam info
    eveh.bctTS = event.cycleStamp;
    eveh.intensity = intensity;
    eveh.beamType = EventHeader::BeamType::CALIBRATION;
    if (event.evtType == "PRIMARY")
    {
        eveh.beamType = EventHeader::BeamType::PRIMARY;
    }
    else if (event.evtType == "PARASITIC")
    {
        eveh.beamType = EventHeader::BeamType::PARASITIC;
    }
    out << eveh;
    if (!out.good())
    {
        errorSignal(WRITE_ERR,
                    "[WriterFile]: failed to write EVEH: " + filename);
    }
    out.close();
    LOG(INFO) << "[WriterFile]: event file written: " << filename;

    // Notify THE ADDH Writer
    // Maybe eacs can listen to bctEvent and send the command ?
    m_addhCmd.sendWrite(eveh.eventNumber, eveh.runNumber, filename,
                        event.lsaCycle, event.user);
}

} // namespace eacs
} // namespace ntof
