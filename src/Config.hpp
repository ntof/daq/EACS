/*
 * Config.h
 *
 * Created on: Jan 29, 2015
 * Author: agiraud
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <ConfigMisc.h>
#include <Singleton.hpp>
#include <sys/types.h>

namespace ntof {
namespace eacs {

typedef struct
{
    std::string name;
    std::string serviceSource;
    std::string nameSource;
    std::string conditionalName;
    std::string conditionalServiceSource;
    int32_t conditionalValue;
} ADDH_DATA;

/**
 * @class Config
 * @brief Class used to read the config file
 */
class Config :
    public ntof::utils::ConfigMisc,
    public ntof::utils::Singleton<Config>
{
public:
    typedef std::map<uint32_t, std::set<uint32_t>> HighVoltageMap;

    static const std::string configFile;     //!< Path of the config file
    static const std::string configMiscFile; //!< Path of the global config file

    /**
     * @brief Destructor of the class
     */
    virtual ~Config() = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file, const std::string &miscFile);

    /**
     * @brief Get the name of the service of the timing machine
     * @return The name of the service of the timing machine
     */
    const std::string &getTimeMachineName() const;

    /**
     * @brief Get the value of the timeout for the CheckFiles object
     * @return The value of the timeout for the CheckFiles object
     */
    uint64_t getTimeoutCheckFiles() const;

    /**
     * @brief Get the path where the files has to written
     * @return The path where the files has to written
     */
    const std::string &getFolderDest() const;

    /**
     * @brief Get the name of the service of the BCT
     * @return The name of the service of the BCT
     */
    const std::string &getBctServiceName() const;

    /**
     * @brief Get the name of the FilterStation service
     * @details empty if the filterstation should not be used
     */
    const std::string &getFilterStationServiceName() const;

    /**
     * @brief Get the name of the SampleExchanger Acquisition service
     * @details empty if the sampleexchanger should not be used
     */
    inline const std::string &getSampleExchangerServiceName() const
    {
        return m_sampleExchangerServiceName;
    }

    /**
     * @brief Get the name of the SampleExchanger Cmd service
     * (PredefinedPosition)
     */
    inline const std::string &getSampleExchangerCmdServiceName() const
    {
        return m_sampleExchangerCmdServiceName;
    }

    /**
     * @brief Get the name of the server
     * @return The name of the server
     */
    const std::string &getServerName() const;

    /**
     * @brief Get the missing trigger error threshold
     * @return The missing trigger error threshold
     */
    int32_t getMissingTriggerError() const;

    /**
     * @brief Get the missing trigger warning threshold
     * @return The missing trigger warning threshold
     */
    int32_t getMissingTriggerWarning() const;

    /**
     * @brief Get the timeout of the initialization of the DAQs
     * @return Return the timeout of the initialization of the DAQs
     */
    uint64_t getTimeoutInitialization();

    /**
     * @brief Get the number of triggers to keep in the history
     * @return Return the number of triggers to keep in the history
     */
    uint64_t getTriggersHistory();

    /**
     * @brief get the activated database backend
     */
    inline const std::string &getDatabaseName() const { return m_databaseName; }

    /**
     * @brief get the database path (SQLiteDatabase backend)
     */
    inline const std::string &getDatabasePath() const { return m_databasePath; }

    /**
     * @brief get the config about mappintg HV errors into warnings
     */
    inline const bool isHVWarnOnly() const { return m_hvWarnOnly; }

    /**
     * @brief set the config about mappintg HV errors into warnings
     */
    inline const void setHVWarnOnly(bool warnOnly) { m_hvWarnOnly = warnOnly; }

    /**
     * @brief get the config about skipping HV db update
     */
    inline const bool isHVSkipUpdateDB() const { return m_hvSkipUpdateDB; }

    /**
     * @brief set the config about skipping HV db update
     */
    inline const void setHVSkipUpdateDB(bool skip) { m_hvSkipUpdateDB = skip; }

    const std::string &getDimPrefix() const;
    uint32_t getDimEventsCount() const;

    inline const HighVoltageMap &getHighVoltageConfig() const
    {
        return m_highVoltage;
    }

    inline float lowBeamThreshold() const { return m_lowBeamThreshold; }

    /**
     * @brief Get the timeout for low beam period
     * @return timeout value in milliseconds (disabled if negative)
     */
    inline int32_t lowBeamTimeout() const { return m_lowBeamTimeout; }

    /**
     * @brief Get the timeout for beam detection
     * @return timeout value in milliseconds (disabled if negative)
     */
    inline int32_t beamDetectionTimeout() const
    {
        return m_beamDetectionTimeout;
    }

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile,
                    const std::string &miscFile = configMiscFile);

    std::string timeMachineName_; //!< Name of the service of the timing machine
    uint64_t timeoutCheckFiles_;  //!< Timeout before unvalidating an event
    uint64_t timeoutInitialization_; //!< Timeout before unvalidating the
                                     //!< initialization of the DAQs
    uint64_t triggersHistory_;   //!< Number of triggers to keep in the history
    std::string folderDest_;     //!< Path to write the files
    std::string BCTServiceName_; //!< Name of the service of the BCT
    std::string m_filterStationServiceName;
    std::string m_sampleExchangerServiceName;
    std::string m_sampleExchangerCmdServiceName;

    std::string serverName_; //!< Name of the server

    std::string m_databaseName;
    std::string m_databasePath;

    int32_t missingTriggerWarning_; //!< Number of event missed before a warning
                                    //!< occurred
    int32_t missingTriggerError_;   //!< Number of event missed before an error
                                    //!< occurred

    std::string dimPrefix_;   //!< DIM service prefix
    uint32_t dimEventsCount_; //!< DIM Events service history count
    HighVoltageMap m_highVoltage;

    bool m_hvWarnOnly;     //!< Map Errors coming from HV Crates as warnings
    bool m_hvSkipUpdateDB; //!< Skip updateDatabase with HV values during HV_INIT

    float m_lowBeamThreshold;       //!< Threshold to determine the low beam
    int32_t m_lowBeamTimeout;       //!< Timeout before raising a 'Low beam'
                                    //!< warning (disabled if negative)
    int32_t m_beamDetectionTimeout; //!< Timeout before raising a 'No beam'
                                    //!< warning (disabled if negative)
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* CONFIG_H_ */
