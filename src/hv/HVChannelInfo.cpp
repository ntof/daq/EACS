/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-18T09:09:07+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "HVChannelInfo.hpp"

#include <cctype>
#include <cmath>
#include <sstream>

#include <NTOFLogging.hpp>

using namespace ntof::eacs;

const int32_t HVChannelInfo::StatusBitErrorMask =
    (0xFFFF &
     ~(HVChannelInfo::SB_ON | HVChannelInfo::SB_RUP | HVChannelInfo::SB_RDOWN));

HVChannelInfo::HVChannelInfo(HVChannelInfo::Index index) :
    m_index(index),
    m_name(),
    m_loaded(false),
    m_pw(HVChannelInfo::PW_UNKNOWN),
    m_v0set(NAN),
    m_i0set(NAN),
    m_trip(NAN),
    m_rampUp(NAN),
    m_rampDown(NAN),
    m_vMax(NAN),
    m_statusBitField(0),
    m_errorCode(0),
    m_errorMessage()
{}

bool HVChannelInfo::hasError() const
{
    return (m_statusBitField & StatusBitErrorMask) || m_errorCode;
}

void HVChannelInfo::clearError()
{
    m_statusBitField &= ~StatusBitErrorMask;
    m_errorCode = 0;
    m_errorMessage.clear();
}

void HVChannelInfo::reset()
{
    m_name.clear();
    m_errorMessage.clear();
    m_loaded = false;
    m_pw = HVChannelInfo::PW_UNKNOWN;
    m_v0set = NAN;
    m_i0set = NAN;
    m_trip = NAN;
    m_rampUp = NAN;
    m_rampDown = NAN;
    m_vMax = NAN;
    m_statusBitField = 0;
    m_errorCode = 0;
}

inline void checkStatus(bool test,
                        const char *text,
                        bool &first,
                        std::ostringstream &oss)
{
    if (test)
    {
        if (first)
            oss << text;
        else
            oss << "|" << text;
        first = false;
    }
}

std::string HVChannelInfo::getErrorMessage() const
{
    std::ostringstream ret;
    bool hasMsg = false;
    if (m_statusBitField & StatusBitErrorMask)
    {
        hasMsg = true;
        bool first = true;
        ret << "status: ";
        checkStatus(m_statusBitField & SB_OVERCURRENT, "OVERCURRENT", first,
                    ret);
        checkStatus(m_statusBitField & SB_OVERVOLTAGE, "OVERVOLTAGE", first,
                    ret);
        checkStatus(m_statusBitField & SB_UNDERVOLTAGE, "UNDERVOLTAGE", first,
                    ret);
        checkStatus(m_statusBitField & SB_EXTERNAL_TRIP, "EXTERNAL_TRIP", first,
                    ret);
        checkStatus(m_statusBitField & SB_MAX_V, "MAX_V", first, ret);
        checkStatus(m_statusBitField & SB_EXTERNAL_DISABLE, "EXTERNAL_DISABLE",
                    first, ret);
        checkStatus(m_statusBitField & SB_INTERNAL_TRIP, "INTERNAL_TRIP", first,
                    ret);
        checkStatus(m_statusBitField & SB_CALIB_ERROR, "CALIB_ERROR", first,
                    ret);
        checkStatus(m_statusBitField & SB_UNPLUGGED, "UNPLUGGED", first, ret);
        checkStatus(m_statusBitField & SB_OVERVOLTAGE_PROTECT,
                    "OVERVOLTAGE_PROTECT", first, ret);
        checkStatus(m_statusBitField & SB_POWER_FAIL, "POWER_FAIL", first, ret);
        checkStatus(m_statusBitField & SB_TEMPERATURE_ERROR,
                    "TEMPERATURE_ERROR", first, ret);
    }
    if (m_errorCode)
    {
        if (hasMsg)
            ret << " ";
        ret << "error(" << m_errorCode << "): " << m_errorMessage;
    }
    return ret.str();
}

bool HVChannelInfo::getPropInfo(const std::string &property,
                                HVChannelInfo::Index &chanIndex,
                                std::string &propName)
{
    if (property.compare(0, 4, "chan") != 0)
        return false;

    std::size_t idx;
    chanIndex = 0;

    for (idx = 4; std::isdigit(property[idx]); ++idx)
    {
        chanIndex = chanIndex * 10 + (property[idx] - '0');
    }
    propName = property.substr(idx);
    return true;
}

std::string HVChannelInfo::toString(PowerMode mode)
{
    switch (mode)
    {
    case PW_ON: return "on";
    case PW_OFF: return "off";
    default:
    case PW_UNKNOWN: return "unknown";
    }
}
