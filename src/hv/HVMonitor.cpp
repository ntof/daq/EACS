/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-18T17:00:41+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "HVMonitor.hpp"

#include <NTOFLogging.hpp>

#include "Config.hpp"

using namespace ntof::eacs;

HVMonitor::HVMonitor() :
    m_worker(new ntof::utils::Worker("HighVoltage", 1, 2048))
{
    const Config::HighVoltageMap &hv = Config::instance().getHighVoltageConfig();
    m_boardList.reserve(hv.size());

    if (hv.size() == 0)
        warningSignal(WarningCode::WARN_HV_STATUS,
                      "[HV]: No HighVoltage boards in configuration");

    for (const Config::HighVoltageMap::value_type &it : hv)
    {
        if (it.second.empty())
        {
            LOG(WARNING) << "[HVMonitor]: skipping card:" << it.first
                         << " : no channels registered";
        }
        else
        {
            m_boardList.emplace_back(
                new HVBoardMonitor(it.first, it.second, m_worker));
            BoardList::value_type &board = m_boardList.back();
            board->errorSignal.connect(errorSignal);
            board->warningSignal.connect(warningSignal);
            board->stateSignal.connect(
                [this](State state) { onStateSignal(state); });
        }
    }
}

HVMonitor::~HVMonitor()
{
    // first stop the worker before clearing the list
    m_worker->stop();
    m_boardList.clear();
    HVBoardMonitor::resetErrorIndex();
}

void HVMonitor::reset()
{
    for (BoardList::value_type &board : m_boardList)
        board->reset();
}

void HVMonitor::updateDatabase(RunNumber runNumber)
{
    for (BoardList::value_type &board : m_boardList)
        board->updateDatabase(runNumber);
}

HVMonitor::State HVMonitor::getState() const
{
    if (m_boardList.empty())
        return State::ST_UNKNOWN;

    State state = m_boardList.front()->getState();
    return std::all_of(m_boardList.begin(), m_boardList.end(),
                       [state](const BoardList::value_type &board) {
                           return board->getState() == state;
                       }) ?
        state :
        State::ST_UNKNOWN;
}

void HVMonitor::onStateSignal(State state)
{
    if (getState() == state)
    {
        LOG(INFO) << "[HVMonitor]: moving to state: "
                  << HVBoardMonitor::toString(state);
        stateSignal(state);
    }
}
