/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-18T10:53:37+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "HVBoardMonitor.hpp"

#include <sstream>

#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSException.hpp"

using namespace ntof::eacs;
using namespace ntof::utils;
using namespace ntof::dim;

static const std::map<HVBoardMonitor::State, std::string> s_stateMap = {
    {HVBoardMonitor::ST_UNKNOWN, "UNKNOWN"},
    {HVBoardMonitor::ST_INITIAL, "INITIAL"},
    {HVBoardMonitor::ST_LOADING, "LOADING"},
    {HVBoardMonitor::ST_IDLE, "IDLE"},
    {HVBoardMonitor::ST_MONITORING, "MONITORING"},
};
std::atomic<uint16_t> HVBoardMonitor::s_errIndex(0);

const std::string &HVBoardMonitor::toString(State state)
{
    std::map<HVBoardMonitor::State, std::string>::const_iterator it =
        s_stateMap.find(state);
    if (it == s_stateMap.end())
        return s_stateMap.at(HVBoardMonitor::ST_UNKNOWN);
    else
        return it->second;
}

HVBoardMonitor::HVBoardMonitor(HVBoardMonitor::Index index,
                               const HVBoardMonitor::ChannelSet &channels) :
    m_worker(new Worker("HighVoltage", 1, 512)),
    m_state(ST_INITIAL),
    m_info(index, channels)
{
    m_info.hvSignal.connect(
        [this](HVBoardInfo::Index, HVChannelInfo::Index channel,
               const std::string &error) { onHvStatus(channel, error); });
    m_info.confSignal.connect(
        [this](HVBoardInfo::Index, HVChannelInfo::Index channel,
               const std::string &error) { onHvConfig(channel, error); });
}

HVBoardMonitor::HVBoardMonitor(
    HVBoardMonitor::Index index,
    const HVBoardMonitor::ChannelSet &channels,
    const std::shared_ptr<ntof::utils::Worker> &worker) :
    m_worker(worker), m_state(ST_INITIAL), m_info(index, channels)
{
    if (!m_worker)
    {
        LOG(WARNING)
            << "[HVBoardMonitor]: invalid worker, running in detached mode";
        m_worker.reset(new Worker("HighVoltage", 1, 2048));
    }
    m_info.hvSignal.connect(
        [this](HVBoardInfo::Index, HVChannelInfo::Index channel,
               const std::string &error) { onHvStatus(channel, error); });
    m_info.confSignal.connect(
        [this](HVBoardInfo::Index, HVChannelInfo::Index channel,
               const std::string &error) { onHvConfig(channel, error); });
}

void HVBoardMonitor::resetErrorIndex()
{
    s_errIndex.store(0);
}

void HVBoardMonitor::dataChanged(ntof::dim::DIMData::List &dataSet,
                                 const ntof::dim::DIMDataSetClient &)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_cliData.clear();
        m_cliData.swap(dataSet);
    }
    errorSignal(ErrorCode(ERR_HV_NO_LINK + getErrorIndex(0)), std::string());
    post([this]() { processData(); });
}

void HVBoardMonitor::errorReceived(const std::string &errMsg,
                                   const ntof::dim::DIMDataSetClient &client)
{
    if (errMsg == DIMDataSetClient::NoLinkError)
    {
        std::ostringstream msg;
        msg << "[" << client.getServiceName() << "]: no link";
        LOG(ERROR) << "[HVBoardMonitor] " << msg.str();
        errorSignal(ErrorCode(ERR_HV_NO_LINK + getErrorIndex(0)), msg.str());
    }
}

void HVBoardMonitor::reset()
{
    post([this]() {
        std::vector<HVChannelInfo::Index> keys;

        {
            /* store error indexes, to clear all errors */
            std::lock_guard<std::mutex> lock(m_lock);
            m_state = ST_LOADING;
            keys.reserve(m_errIndexMap.size());
            for (const auto &errIt : m_errIndexMap)
                keys.push_back(errIt.first);
        }
        m_info.reset();

        for (HVChannelInfo::Index index : keys)
            warningSignal(WarningCode(WARN_HV_STATUS + getErrorIndex(index)),
                          std::string());

        m_cli.reset(new DIMDataSetClient(
            "HV_" + std::to_string(m_info.index()) + "/BoardInformation", this));
        stateSignal(ST_LOADING);
    });
}

HVBoardMonitor::State HVBoardMonitor::getState() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_state;
}

void HVBoardMonitor::updateDatabase(RunNumber runNumber)
{
    if (Config::instance().isHVSkipUpdateDB())
    {
        // Set status as Monitoring without updating db
        setState(ST_MONITORING);
        return;
    }

    post([runNumber, this]() {
        if (getState() != ST_IDLE)
        {
            std::ostringstream oss;
            oss << "[HVBoardMonitor] can't update database in "
                << toString(getState()) << " state";
            errorSignal(ERR_INTERNAL, oss.str());
            return;
        }

        try
        {
            Database &db = Database::instance();
            for (const HVBoardInfo::HVChannelInfoMap::value_type &it :
                 m_info.channelInfo())
            {
                LOG(INFO) << "[HVBoardMonitor] commit card:" << m_info.index()
                          << " channel:" << it.first;
                db.insertHVChannelInfo(runNumber, m_info, it.second);
            }
            LOG(INFO) << "[HVBoardMonitor] card:" << m_info.index()
                      << " database updated";
            setState(ST_MONITORING);
        }
        catch (EACSException &e)
        {
            std::ostringstream oss;
            oss << "[" << m_cli->getServiceName()
                << "]: failed to updateDatabase : " << e.what();
            LOG(ERROR) << oss.str();
            errorSignal(ErrorCode(e.getErrorCode()), oss.str());
        }
    });
}

void HVBoardMonitor::setState(State state)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_state = state;
    }
    stateSignal(state);
}

void HVBoardMonitor::processData()
{
    DIMData::List data;

    {
        std::lock_guard<std::mutex> lock(m_lock);
        m_cliData.swap(data);
    }

    if (data.empty())
    {
        LOG(WARNING) << "[HVBoardMonitor]: data already processed, thread "
                        "running too slowly, card:"
                     << m_info.index();
        return;
    }

    try
    {
        switch (getState())
        {
        case ST_LOADING:
            if (!m_info.isBoardInfoLoaded())
            {
                m_info.loadBoardInfo(data);
                m_cli.reset(new DIMDataSetClient(
                    "HV_" + std::to_string(m_info.index()) + "/Acquisition",
                    this));
            }
            else
            {
                m_info.loadAcquisition(data);
                setState(ST_IDLE);
            }
            break;
        case ST_IDLE:
        case ST_MONITORING: m_info.loadAcquisition(data); break;
        default: break;
        }
    }
    catch (DIMException &ex)
    {
        LOG(ERROR) << "[HVBoardMonitor]: failed to processData: " << ex.what();
    }
}

uint16_t HVBoardMonitor::getErrorIndex(HVChannelInfo::Index index)
{
    std::lock_guard<std::mutex> lock(m_lock);
    std::map<HVChannelInfo::Index, uint16_t>::const_iterator it =
        m_errIndexMap.find(index);
    if (it != m_errIndexMap.end())
        return it->second;

    uint16_t err = s_errIndex.fetch_add(1);
    m_errIndexMap[index] = err;
    return err;
}

void HVBoardMonitor::onHvStatus(HVChannelInfo::Index channel,
                                const std::string &error)
{
    if (!error.empty())
    {
        std::ostringstream oss;
        oss << "[HV_" + std::to_string(m_info.index()) << "]: " << error;
        warningSignal(WarningCode(WARN_HV_STATUS + getErrorIndex(channel)),
                      oss.str());
    }
    else
    {
        warningSignal(WarningCode(WARN_HV_STATUS + getErrorIndex(channel)),
                      error);
    }
}

void HVBoardMonitor::onHvConfig(HVChannelInfo::Index /*channel*/,
                                const std::string &error)
{
    if (getState() == ST_MONITORING)
    {
        std::ostringstream oss;
        oss << "[HV_" + std::to_string(m_info.index()) << "]: " << error;
        errorSignal(ERR_HVCONF_CHANGED, oss.str());
    }
}

void HVBoardMonitor::post(const std::function<void()> &fun)
{
    try
    {
        m_worker->post(fun);
    }
    catch (NTOFException &ex)
    {
        LOG(ERROR)
            << "[HVBoardMonitor] failed to queue task: " << ex.getMessage();
    }
}
