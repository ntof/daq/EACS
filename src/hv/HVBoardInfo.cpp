/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-18T09:10:24+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "HVBoardInfo.hpp"

#include <algorithm>
#include <cmath>
#include <iomanip>

#include <NTOFLogging.hpp>

using namespace ntof::eacs;

HVBoardInfo::HVBoardInfo(HVBoardInfo::Index index,
                         const HVBoardInfo::ChannelSet &channels) :
    m_index(index), m_loaded(false)
{
    for (HVChannelInfo::Index chanIdx : channels)
    {
        m_chanInfo.emplace(chanIdx, std::move(HVChannelInfo(chanIdx)));
    }
}

void HVBoardInfo::reset()
{
    m_loaded = false;
    m_model.clear();
    m_firmware.clear();
    m_serialNumber = 0;
    for (HVChannelInfoMap::value_type &it : m_chanInfo)
    {
        HVChannelInfo &info = it.second;
        if (info.hasError())
        {
            info.clearError();
            hvSignal(m_index, info.m_index, std::string());
        }
        info.reset();
    }
}

void HVBoardInfo::loadBoardInfo(const ntof::dim::DIMData::List &data)
{
    for (const ntof::dim::DIMData &d : data)
    {
        if (d.getName() == "model")
            m_model = d.getStringValue();
        else if (d.getName() == "firmware")
            m_firmware = d.getStringValue();
        else if (d.getName() == "serialNumber")
            m_serialNumber = d.getValue<int32_t>();
    }
    m_loaded = true;
}

bool HVBoardInfo::checkValue(const HVChannelInfo &chan,
                             const std::string &name,
                             float oldValue,
                             float newValue,
                             float delta,
                             std::string &out) const
{
    if (std::abs(oldValue - newValue) > delta)
    {
        std::ostringstream oss;
        oss << name << " change detected on " << chan.m_name << "(" << m_index
            << ":" << chan.m_index << "): "
            << std::setprecision(
                   std::min(3, int(std::abs(std::trunc(std::log10(delta))))))
            << oldValue << " -> " << newValue;
        out = oss.str();
        return true;
    }
    return false;
}

void HVBoardInfo::loadAcquisition(const ntof::dim::DIMData::List &data)
{
    std::set<HVChannelInfo::Index> errChange;
    std::map<HVChannelInfo::Index, std::string> configChange;
    for (const ntof::dim::DIMData &d : data)
    {
        HVChannelInfo::Index chanIdx;
        std::string propName;

        if (HVChannelInfo::getPropInfo(d.getName(), chanIdx, propName))
        {
            try
            {
                HVChannelInfo &chan = m_chanInfo.at(chanIdx);
                if (propName == "pw")
                {
                    const HVChannelInfo::PowerMode oldMode = chan.m_pw;
                    chan.m_pw = HVChannelInfo::PowerMode(d.getValue<int32_t>());
                    if (oldMode != chan.m_pw) // potential error change
                    {
                        configChange[chanIdx] = "power change detected: " +
                            HVChannelInfo::toString(oldMode) + " -> " +
                            HVChannelInfo::toString(chan.m_pw);
                        errChange.insert(chanIdx);
                    }
                }
                else if (propName == "v0set")
                {
                    const float old = chan.m_v0set;
                    chan.m_v0set = d.getValue<float>();

                    std::string msg;
                    if ((chan.m_pw != HVChannelInfo::PW_OFF) &&
                        checkValue(chan, "v0set", old, chan.m_v0set, 0.01, msg))
                        configChange[chanIdx] = msg;
                }
                else if (propName == "i0set")
                {
                    const float old = chan.m_i0set;
                    chan.m_i0set = d.getValue<float>();

                    std::string msg;
                    if ((chan.m_pw != HVChannelInfo::PW_OFF) &&
                        checkValue(chan, "i0set", old, chan.m_i0set, 0.01, msg))
                        configChange[chanIdx] = msg;
                }
                else if (propName == "trip")
                    chan.m_trip = d.getValue<float>();
                else if (propName == "rup")
                    chan.m_rampUp = d.getValue<float>();
                else if (propName == "rdwn")
                    chan.m_rampDown = d.getValue<float>();
                else if (propName == "svmax")
                    chan.m_vMax = d.getValue<float>();
                else if (propName == "caenErrorMessage")
                    chan.m_errorMessage = d.getStringValue();
                else if (propName == "caenErrorCode")
                {
                    int32_t oldError = chan.m_errorCode;
                    chan.m_errorCode = d.getValue<int32_t>();
                    if (oldError != chan.m_errorCode)
                        errChange.insert(chanIdx);
                }
                else if (propName == "channelStatusBitfield")
                {
                    int32_t oldError = chan.m_statusBitField &
                        HVChannelInfo::StatusBitErrorMask;
                    chan.m_statusBitField = d.getValue<int32_t>();
                    if (oldError !=
                        (chan.m_statusBitField &
                         HVChannelInfo::StatusBitErrorMask))
                        errChange.insert(chanIdx);
                }
                else if (propName == "channelName")
                    chan.m_name = d.getStringValue();

                chan.m_loaded = true;
            }
            catch (std::out_of_range &err)
            {
                LOG(DEBUG) << "[HVBoardInfo]: channel unknown " << m_index
                           << ":" << chanIdx;
            }
        }
    }

    for (const auto &val : configChange)
    {
        confSignal(m_index, val.first, val.second);
    }

    for (HVChannelInfo::Index chanIdx : errChange)
    {
        HVChannelInfo &chan = m_chanInfo.at(chanIdx);
        if (chan.hasError() && (chan.pw() != HVChannelInfo::PW_OFF))
        {
            std::ostringstream oss;
            oss << "error on " << chan.m_name << "(" << m_index << ":"
                << chan.m_index << "): " << chan.getErrorMessage();
            LOG(WARNING) << "[HVBoardInfo]: " << oss.str();
            hvSignal(m_index, chanIdx, oss.str());
        }
        else
            hvSignal(m_index, chanIdx, std::string());
    }
}

bool HVBoardInfo::isAcquisitionLoaded() const
{
    return std::all_of(m_chanInfo.begin(), m_chanInfo.end(),
                       [](const HVChannelInfoMap::value_type &it) {
                           return it.second.m_loaded;
                       });
}
