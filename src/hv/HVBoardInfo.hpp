/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-17T17:26:34+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef HVBOARDINFO_HPP__
#define HVBOARDINFO_HPP__

#include <cstdint>
#include <map>
#include <set>
#include <string>

#include <DIMData.h>
#include <Signals.hpp>

#include "HVChannelInfo.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

class HVBoardInfo
{
public:
    typedef uint32_t Index;
    typedef std::set<HVChannelInfo::Index> ChannelSet;
    typedef std::map<HVChannelInfo::Index, HVChannelInfo> HVChannelInfoMap;
    typedef ntof::utils::signal<
        void(Index, HVChannelInfo::Index, const std::string &error)>
        HVWarningSignal;

    HVBoardInfo(Index index, const ChannelSet &channels);

    /**
     * @brief load board information
     * @details load board information from a /BoardInformation service
     * @throws DIMException on parse error
     */
    void loadBoardInfo(const ntof::dim::DIMData::List &data);

    /**
     * @brief load channel information
     * @details load board information from a /Acquisition service
     * @throws DIMException on parse error
     */
    void loadAcquisition(const ntof::dim::DIMData::List &data);

    void reset();

    const Index index() const { return m_index; }
    int32_t serialNumber() const { return m_serialNumber; }

    const HVChannelInfoMap &channelInfo() const { return m_chanInfo; }
    HVChannelInfoMap &channelInfo() { return m_chanInfo; }

    inline const bool isBoardInfoLoaded() const { return m_loaded; }
    bool isAcquisitionLoaded() const;

    HVWarningSignal hvSignal; /*<! emitted on channel status changes (error) */
    HVWarningSignal confSignal; /*<! emitted on channel config changes */

protected:
    bool checkValue(const HVChannelInfo &chan,
                    const std::string &name,
                    float oldValue,
                    float newValue,
                    float delta,
                    std::string &out) const;

protected:
    const Index m_index;
    bool m_loaded;
    std::string m_model;
    std::string m_firmware;
    int32_t m_serialNumber;
    HVChannelInfoMap m_chanInfo;
};

} // namespace eacs
} // namespace ntof

#endif
