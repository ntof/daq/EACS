/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-17T18:18:55+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef HVBOARDMONITOR_HPP__
#define HVBOARDMONITOR_HPP__

#include <atomic>
#include <functional>
#include <memory>
#include <mutex>

#include <DIMData.h>
#include <DIMDataSetClient.h>
#include <Worker.hpp>

#include "HVBoardInfo.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

class HVBoardMonitor : public ntof::dim::DIMDataSetClientHandler
{
public:
    typedef HVBoardInfo::Index Index;
    typedef HVBoardInfo::ChannelSet ChannelSet;

    enum State
    {
        ST_UNKNOWN = -1,
        ST_INITIAL,
        ST_LOADING,
        ST_IDLE,
        ST_MONITORING
    };
    static const std::string &toString(State state);
    typedef ntof::utils::signal<void(State)> StateSignal;

    HVBoardMonitor(Index index, const ChannelSet &channels);
    HVBoardMonitor(Index index,
                   const ChannelSet &channels,
                   const std::shared_ptr<ntof::utils::Worker> &worker);

    /**
     * @brief clear reserved error codes
     * @details must ensure that all HVBoardMonitor are destroyed before
     * calling this
     */
    static void resetErrorIndex();

    void dataChanged(ntof::dim::DIMData::List &dataSet,
                     const ntof::dim::DIMDataSetClient &client) override;

    void errorReceived(const std::string &errMsg,
                       const ntof::dim::DIMDataSetClient &client) override;

    void updateDatabase(RunNumber runNumber);

    /**
     * @brief reset the internal state to ST_LOADING and schedule the service
     * load
     */
    void reset();
    inline void start() { reset(); }

    State getState() const;

    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    StateSignal stateSignal;

protected:
    void setState(State state);
    void processData();
    uint16_t getErrorIndex(HVChannelInfo::Index index);
    void onHvStatus(HVChannelInfo::Index index, const std::string &error);
    void onHvConfig(HVChannelInfo::Index index, const std::string &error);
    void post(const std::function<void()> &fun);

    static std::atomic<uint16_t> s_errIndex;
    std::shared_ptr<ntof::utils::Worker> m_worker;

    mutable std::mutex m_lock;
    ntof::dim::DIMData::List m_cliData;
    State m_state;
    std::map<HVChannelInfo::Index, uint16_t> m_errIndexMap;

    HVBoardInfo m_info;
    std::unique_ptr<ntof::dim::DIMDataSetClient> m_cli;
};

} // namespace eacs
} // namespace ntof

#endif
