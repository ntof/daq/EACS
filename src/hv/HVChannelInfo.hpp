/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-17T16:57:08+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef HVCHANNELINFO_HPP__
#define HVCHANNELINFO_HPP__

#include <cstdint>
#include <string>

namespace ntof {
namespace eacs {

class HVBoardInfo;

class HVChannelInfo
{
public:
    typedef uint32_t Index;

    enum PowerMode
    {
        PW_UNKNOWN = 0,
        PW_ON = 1,
        PW_OFF = 2
    };

    enum StatusBitField
    {
        SB_ON = 0x01,
        SB_RUP = 0x02,
        SB_RDOWN = 0x04,
        SB_OVERCURRENT = 0x08,
        SB_OVERVOLTAGE = 0x10,
        SB_UNDERVOLTAGE = 0x20,
        SB_EXTERNAL_TRIP = 0x40,
        SB_MAX_V = 0x80,
        SB_EXTERNAL_DISABLE = 0x100,
        SB_INTERNAL_TRIP = 0x200,
        SB_CALIB_ERROR = 0x400,
        SB_UNPLUGGED = 0x800,
        SB_OVERVOLTAGE_PROTECT = 0x2000,
        SB_POWER_FAIL = 0x4000,
        SB_TEMPERATURE_ERROR = 0x8000
    };

    explicit HVChannelInfo(Index index);

    inline Index index() const { return m_index; }
    inline PowerMode pw() const { return m_pw; }
    inline float v0Set() const { return m_v0set; }
    inline float i0Set() const { return m_i0set; }
    inline float trip() const { return m_trip; }
    inline float rampUp() const { return m_rampUp; }
    inline float rampDown() const { return m_rampDown; }
    inline float vMax() const { return m_vMax; }
    inline const std::string &name() const { return m_name; }

    static std::string toString(PowerMode mode);

    bool hasError() const;

    std::string getErrorMessage() const;

    void clearError();

    /**
     * @brief reset current values
     */
    void reset();

    /**
     * @brief extract property information from a DIM property name
     */
    static bool getPropInfo(const std::string &property,
                            Index &index,
                            std::string &propName);

protected:
    friend class HVBoardInfo; // eases loading

    static const int32_t StatusBitErrorMask;

    const Index m_index;
    std::string m_name;
    bool m_loaded;
    PowerMode m_pw;
    float m_v0set;
    float m_i0set;
    float m_trip;
    float m_rampUp;
    float m_rampDown;
    float m_vMax;
    int32_t m_statusBitField;
    int32_t m_errorCode;
    std::string m_errorMessage;
};

} // namespace eacs
} // namespace ntof

#endif
