/**
 * \file main.cpp
 * \brief Main system
 * \author A. GIRAUD
 * \version 1.0
 * \date Sep 24, 2014
 *
 * Start the system.
 *
 */

#include <easylogging++.h>

#include "Config.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"

/**
 * @brief Main function
 * @param argc: number of parameters
 * @param argv: list of parameters
 * @return Return 0
 */
int main(int argc, char **argv)
{
    int ret = 0;
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        const std::string configFile = (argc > 1) ?
            std::string(argv[1]) :
            ntof::eacs::Config::configFile;
        const std::string configMiscFile = (argc > 2) ?
            std::string(argv[2]) :
            ntof::eacs::Config::configMiscFile;
        const std::string logConfFile = (argc > 3) ?
            std::string(argv[3]) :
            std::string("/etc/ntof/eacslogger.conf");

        ntof::eacs::Config::load(configFile, configMiscFile);

        // Initialize logger
        el::Configurations confFromFile(
            logConfFile); // Load configuration from file
        el::Loggers::reconfigureAllLoggers(
            confFromFile); // Re-configures all the loggers to current
                           // configuration file

        // Initialized DIM parameters
        DimServer::setDnsNode(
            ntof::eacs::Config::instance().getDimDns().c_str());
        DimClient::setDnsNode(
            ntof::eacs::Config::instance().getDimDns().c_str());

        // Run EACS
        LOG(INFO) << "Eacs running";
        ntof::eacs::Eacs &eacs(ntof::eacs::Eacs::instance());
        eacs.initServices();
        eacs.run();
    }
    catch (const ntof::eacs::EACSException &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught EACS exception : " << ex.getMessage();
        ret = -1;
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught exception : " << ex.what();
        ret = -2;
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught EACS exception : ";
        ret = -3;
    }

    return ret;
}
