/*
 * DimRunInfo.cpp
 *
 * Created on: Feb 20, 2015
 * Author: agiraud
 */

#include "ServiceEvents.hpp"

#include "Config.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"

using namespace ntof::utils;

namespace ntof {
namespace eacs {

ServiceEvents::ServiceEvents() :
    dim::DIMDataSet(Config::instance().getDimPrefix() + "/Events"),
    m_bufferSize(Config::instance().getDimEventsCount())
{}

void ServiceEvents::onBctEvent(const TimingEventDetails &event, float intensity)
{
    try
    {
        LockRef<DIMData> data = findEvent(event.evtNumber);
        DIMData::List &dataset = data->getNestedValue();
        dataset[Params::PROTONS].setValue(intensity);
        dataset[Params::PROTONS].setHidden(intensity == 0);
    }
    catch (DIMException &ex)
    {
        LOG(WARNING)
            << "[ServiceEvents] failed to update ServiceEvents (event too old)";
    }
    updateData();
}

void ServiceEvents::onTimingEvent(const TimingEventDetails &event)
{
    try
    {
        LockRef<DIMData> data = findEvent(event.evtNumber);
        DIMData::List &dataset = data->getNestedValue();
        dataset[Params::BEAM_TYPE].setValue(
            dim::DIMEnum(fromString(event.evtType), event.evtType));
        dataset[Params::PERIOD_NB].setValue(event.cycleNb);
        dataset[Params::PERIOD_NB].setHidden(event.cycleNb == 0);
        dataset[Params::CYCLESTAMP].setValue(event.cycleStamp);
        dataset[Params::CYCLESTAMP].setHidden(event.cycleStamp == 0);
    }
    catch (DIMException &ex)
    {
        LOG(WARNING)
            << "[ServiceEvents] failed to update ServiceEvents (event too old)";
    }
    updateData();
}

void ServiceEvents::onValidatedEvent(const EventValidator::ValidEvent &event,
                                     uint32_t extensionNumber)
{
    try
    {
        LockRef<DIMData> data = findEvent(event.eventNumber);
        DIMData::List &dataset = data->getNestedValue();
        dataset[Params::VALIDATED_NUMBER].setValue(extensionNumber);
        dataset[Params::VALIDATED_NUMBER].setHidden(extensionNumber == 0);
    }
    catch (DIMException &ex)
    {
        LOG(WARNING)
            << "[ServiceEvents] failed to update ServiceEvents (event too old)";
    }
    updateData();
}

void ServiceEvents::clearEvents()
{
    dim::DIMDataSet::clearData();
}

LockRef<ntof::dim::DIMData> ServiceEvents::findEvent(EventNumber eventNumber)
{
    try
    {
        return lockDataAt(eventNumber);
    }
    catch (DIMException &ex)
    {
        dim::DIMData::List dataset;
        dataset.emplace_back(Params::EVENT_NUMBER, "eventNumber", "",
                             eventNumber);
        dataset.emplace_back(Params::VALIDATED_NUMBER, "validatedNumber", "",
                             uint32_t(0));
        dim::DIMEnum beamTypeEnum;
        beamTypeEnum.addItem(BeamType::CALIBRATION, "CALIBRATION");
        beamTypeEnum.addItem(BeamType::PRIMARY, "PRIMARY");
        beamTypeEnum.addItem(BeamType::PARASITIC, "PARASITIC");
        beamTypeEnum.setValue(BeamType::CALIBRATION);
        dataset.emplace_back(Params::BEAM_TYPE, "beamType", "", beamTypeEnum);
        dataset.emplace_back(Params::PROTONS, "protons", "", float(0.0));
        dataset.emplace_back(Params::CYCLESTAMP, "cyclestamp", "", int64_t(0));
        dataset.emplace_back(Params::PERIOD_NB, "periodNB", "", int32_t(0));
        // Set temporary values to hidden;
        dataset[Params::VALIDATED_NUMBER].setHidden(true);
        dataset[Params::PROTONS].setHidden(true);
        dataset[Params::CYCLESTAMP].setHidden(true);
        dataset[Params::PERIOD_NB].setHidden(true);
        // Create event but not update the service, it will be updated after
        // data changes
        addData(eventNumber, "event", "", dataset, AddMode::CREATE, false);

        // Check if events are more than buffer size and clean the map
        {
            std::lock_guard<std::mutex> lock(m_lock);
            if (dataList.size() > m_bufferSize)
            {
                // Remove the first (oldest) element in the map
                dataList.erase(dataList.begin());
            }
        }
    }
    return lockDataAt(eventNumber);
}

} /* namespace eacs */
} /* namespace ntof */
