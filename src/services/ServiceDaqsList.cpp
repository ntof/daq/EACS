/*
 * DimRunInfo.cpp
 *
 * Created on: Feb 20, 2015
 * Author: agiraud
 */

#include "ServiceDaqsList.hpp"

#include <pugixml.hpp>

#include "Config.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"

namespace ntof {
namespace eacs {

ServiceDaqsList::ServiceDaqsList() :
    DIMDataSet(Config::instance().getDimPrefix() + "/Daq/List")
{
    // Initialize the daqs list from config
    const std::map<int32_t, std::string> &daqsList = Config::instance().getDaq();
    uint daqIndex = 0;
    for (const auto &daq : daqsList)
    {
        int32_t chassisId = daq.first;
        const std::string &hostname = daq.second;

        DaqPtr daqPtr(new Daq(hostname, chassisId));

        // Forwards errors
        daqPtr->errorSignal.connect(this->errorSignal);
        daqPtr->warningSignal.connect(this->warningSignal);
        daqPtr->modeSignal.connect(
            [this](const Daq &, Daq::Mode) { this->onModeSignal(); });
        // Connect to cardUpdate signal in order to refresh the Daq/List service
        daqPtr->getConfig().cardUpdateSignal.connect(
            [this]() { this->updateDimService(); });
        daqPtr->getConfig().loadedSignal.connect(
            [this]() { this->onDaqLoaded(); });

        m_daqs.emplace(hostname, std::move(daqPtr));

        // publish empty daq on the dataset service
        createDaq(daqIndex++, hostname);
    }
    // Update list of empty daq
    updateData();
}

void ServiceDaqsList::loadDaqConfigs()
{
    for (const DaqMap::value_type &daqIt : m_daqs)
    {
        daqIt.second->getConfig().load();
    }
}

bool ServiceDaqsList::isDaqLoaded() const
{
    return std::all_of(m_daqs.begin(), m_daqs.end(),
                       [](const DaqMap::value_type &daqIt) {
                           return !daqIt.second->getConfig().isLoading();
                       });
}

bool ServiceDaqsList::configureDaqs(RunNumber runNumber)
{
    bool ret = true;
    for (const DaqMap::value_type &daq : m_daqs)
    {
        ret = daq.second->configure(runNumber) && ret;
    }
    return ret;
}

void ServiceDaqsList::updateDatabase(ntof::eacs::RunNumber runNumber)
{
    for (const DaqMap::value_type &daq : m_daqs)
    {
        daq.second->getConfig().updateDatabase(runNumber);
    }
}

void ServiceDaqsList::genHeaders(RunControlHeader &rctr,
                                 ModuleHeader &modh) const
{
    for (const DaqMap::value_type &daq : m_daqs)
    {
        daq.second->getConfig().genHeaders(rctr, modh);
    }
}

bool ServiceDaqsList::startDaqs()
{
    bool ret = true;
    bool daqFound = false;
    for (const DaqMap::value_type &daq : m_daqs)
    {
        if (daq.second->isUsed())
        {
            daqFound = true;
            ret = daq.second->startAcquisition() && ret;
        }
    }
    if (!daqFound)
    {
        errorSignal(ERR_NO_DAQ, "No DAQ found for this run");
    }
    return ret;
}

bool ServiceDaqsList::stopDaqs()
{
    bool ret = true;
    for (const DaqMap::value_type &daq : m_daqs)
    {
        if (daq.second->isUsed())
            ret = daq.second->stop() && ret;
        else
            daq.second->reset(); /* stop monitoring */
    }
    return ret;
}

bool ServiceDaqsList::resetDaqs()
{
    bool ret = true;
    for (const DaqMap::value_type &daq : m_daqs)
    {
        ret = daq.second->reset() && ret;
    }
    return ret;
}

void ServiceDaqsList::createDaq(uint32_t index, const std::string &hostname)
{
    if (!hasDataAt(index))
    {
        dim::DIMData::List dataset;
        dataset.emplace_back(DaqConfig::Params::NAME, "name", "",
                             std::string(hostname));
        dataset.emplace_back(DaqConfig::Params::CARDS, "cards", "",
                             dim::DIMData::List());
        dataset.back().setHidden(true);
        dataset.emplace_back(DaqConfig::Params::USED, "used", "", bool(false));
        dataset.back().setHidden(true);
        addData(index, "daq", "", dataset, AddMode::CREATE, false);
    }
}

void ServiceDaqsList::updateDimService()
{
    // Iterate over daqs published on dataset service
    for (int index = 0; index < getDataCount(); ++index)
    {
        utils::LockRef<DIMData> lockedDaq = lockDataAt(index);
        const std::string &hostname = lockedDaq->getNestedValue()
                                          .at(DaqConfig::Params::NAME)
                                          .getStringValue();
        DaqConfig &daqConfig = m_daqs.at(hostname)->getConfig();
        daqConfig.updateDimData(*lockedDaq);
    }
    updateData();
}

bool ServiceDaqsList::getEventSize(RunNumber runNumber,
                                   EventNumber event,
                                   uint64_t &eventSize) const
{
    for (const DaqMap::value_type &daqIt : m_daqs)
    {
        const DaqPtr &daq = daqIt.second;

        if (!daq->isUsed())
            continue;
        else if (!daq->isWritten(runNumber, event))
            return false;
        else
        {
            eventSize += daq->getSize(runNumber, event);
        }
    }
    return true;
}

void ServiceDaqsList::removeWritten(RunNumber runNumber, EventNumber event)
{
    for (const DaqMap::value_type &daqIt : m_daqs)
    {
        const DaqPtr &daq = daqIt.second;

        daq->removeWritten(runNumber, event);
    }
}

std::vector<uint32_t> ServiceDaqsList::getActiveCrateIds()
{
    std::vector<uint32_t> activeDaqsCrateId;
    for (const DaqMap::value_type &daqIt : m_daqs)
    {
        const DaqPtr &daqPtr = daqIt.second;
        const DaqConfig &daqConfig = daqPtr->getConfig();
        if (daqConfig.isUsed())
        {
            activeDaqsCrateId.push_back(daqConfig.getChassisId());
        }
    }
    return activeDaqsCrateId;
}

void ServiceDaqsList::onModeSignal()
{
    const bool monitoring = std::all_of(
        m_daqs.begin(), m_daqs.end(), [this](const DaqMap::value_type &it) {
            return it.second->getMode() == Daq::Mode::MONITOR;
        });
    if (monitoring)
    {
        LOG(INFO) << "[ServiceDaqsList]: daqReadySignal";
        daqReadySignal();
    }
}

void ServiceDaqsList::onDaqLoaded()
{
    if (isDaqLoaded())
    {
        LOG(INFO) << "[ServiceDaqsList]: daqLoadedSignal";
        daqLoadedSignal();
    }
}

} /* namespace eacs */
} /* namespace ntof */
