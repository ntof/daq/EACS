/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-09-24T14:13:35+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "ServiceInfo.hpp"

#include <cstdint>

#include <DIMData.h>
#include <NTOFLogging.hpp>

#include "Config.hpp"

using namespace ntof::dim;
using namespace ntof::eacs;

ServiceInfo::ServiceInfo() :
    DIMDataSet(Config::instance().getDimPrefix() + "/Info")
{
    Config &cfg = Config::instance();

    {
        const std::string &serviceName = cfg.getFilterStationServiceName();
        addData(FILTERSTATION, "filterStation", "", serviceName,
                AddMode::CREATE, false);
        if (serviceName.empty())
            lockDataAt(FILTERSTATION)->setHidden(true);
    }

    {
        DIMData::List hvData;
        for (const Config::HighVoltageMap::value_type &it :
             cfg.getHighVoltageConfig())
        {
            if (it.second.empty())
                continue;

            DIMData::List cardData;
            for (const auto &chan : it.second)
                cardData.emplace_back(DIMData::Index(chan), "channel", "",
                                      uint32_t(chan));

            hvData.emplace_back(DIMData::Index(it.first),
                                "HV_" + std::to_string(it.first), "", cardData);
        }
        addData(HIGHVOLTAGE, "highVoltage", "", hvData, AddMode::CREATE, false);
        if (hvData.empty())
            lockDataAt(HIGHVOLTAGE)->setHidden(true);
    }
    updateData();
}
