/*
 * DimRunInfo.h
 *
 * Created on: Feb 20, 2015
 * Author: agiraud
 */
#ifndef SERVICEEVENTS_H_
#define SERVICEEVENTS_H_

#include <mutex>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <EACSTypes.hpp>
#include <Synchro.h>

#include "EventValidator.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @class ServiceEvents
 * @brief Publish last events status
 */
class ServiceEvents : public dim::DIMDataSet
{
public:
    enum Params
    {
        EVENT_NUMBER = 0,
        VALIDATED_NUMBER,
        BEAM_TYPE,
        PROTONS,
        CYCLESTAMP,
        PERIOD_NB
    };

    ServiceEvents();

    /**
     * @brief actions to do when a btc event occurs
     * @param[in] event Timing details
     * @param[in] intensity number of protons
     */
    void onBctEvent(const TimingEventDetails &event, float intensity);

    /**
     * @brief actions to do when a timing event occurs
     * @param[in] event Timing details
     */
    void onTimingEvent(const TimingEventDetails &event);

    /**
     * @brief actions to do when a validated event occurs
     * @param[in] event Timing details
     * @param[in] extensionNumber number of event since start
     */
    void onValidatedEvent(const EventValidator::ValidEvent &event,
                          uint32_t extensionNumber);

    /**
     * @brief Remove all the events
     */
    void clearEvents();

protected:
    ntof::utils::LockRef<ntof::dim::DIMData> findEvent(EventNumber index);

    uint32_t m_bufferSize; //!< Max buffer size
    std::mutex m_mutex;    //!< Mutex to avoid concurrent access
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* SERVICEEVENTS_H_ */
