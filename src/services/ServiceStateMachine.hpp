/*
 * ServiceStateMachine.h
 *
 *  Created on: Feb 10, 2017
 *      Author: agiraud
 */

#ifndef SERVICESTATEMACHINE_H_
#define SERVICESTATEMACHINE_H_

#include <functional>
#include <list>

#include <DIMState.h>
#include <DIMUtils.hpp>
#include <Signals.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

class ServiceStateMachine
{
public:
    typedef ntof::utils::signal<void(bool)> IsErrorSignal;

    ServiceStateMachine();
    ServiceStateMachine(const ServiceStateMachine &other) = delete;
    ServiceStateMachine &operator=(const ServiceStateMachine &other) = delete;
    virtual ~ServiceStateMachine();

    /**
     * @brief Get the value of the state
     * @return The actual value of the state
     */
    EacsState getState() const;

    /**
     * @brief Change the value of the state machine
     * @param val: new value to set. This value is a enum EACSState
     * @return true if state was updated
     */
    bool setState(EacsState state);

    /**
     * @brief Remove all the errors from the state machine
     */
    void clearErrors();

    /**
     * @brief Remove all the warnings from the state machine
     */
    void clearWarnings();

    /**
     * @brief check if the system is in error
     * @return Return true if the system is in error
     */
    bool isInError();

    /**
     * Gets last error message
     * @return
     */
    std::string getErrorMessage();

    /**
     * Gets last error code
     * @return
     */
    int32_t getErrorCode();

    /**
     * @brief connect an error signal to this state machine
     */
    void connect(ErrorSignal *err, WarningSignal *warn = nullptr);

    IsErrorSignal isErrorSignal;

protected:
    bool check(EacsState state) const;

    /**
     * @brief set or clear an error
     * @param[in] code the error identifier
     * @param[in] message the error message, leave empty to clear the error
     */
    void setError(ErrorCode code, const std::string &message);

    /**
     * @brief set or clear a warning
     * @param[in] code the warning identifier
     * @param[in] message the warning message, leave empty to clear the warning
     */
    void setWarning(WarningCode code, const std::string &message);

    /**
     * @brief add now as ISO date prefix
     * @return message with iso date prefix
     */
    static std::string getMessageWithDatePrefix(const std::string &message);

    mutable std::mutex m_mutex;  //!< Mutex to avoid concurrent access
    ntof::dim::DIMState m_state; //!< State machine
    std::function<ErrorSignal::signature_type> m_errSlot;
    std::function<WarningSignal::signature_type> m_warnSlot;
    std::list<ntof::dim::scoped_connection> m_conn;
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* SERVICESTATEMACHINE_H_ */
