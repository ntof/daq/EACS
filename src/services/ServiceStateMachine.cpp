/*
 * ServiceStateMachine.cpp
 *
 *  Created on: Feb 10, 2017
 *      Author: agiraud
 */

#include "ServiceStateMachine.hpp"

#include <ctime>

#include <Signals.hpp>
#include <easylogging++.h>

#include "Config.hpp"
#include "Eacs.hpp"

namespace ntof {
namespace eacs {

ServiceStateMachine::ServiceStateMachine() :
    m_state(Config::instance().getDimPrefix() + "/State")
{
    m_state.setAutoRefresh(false);
    m_errSlot = [this](ErrorCode code, const std::string &message) {
        this->setError(code, message);
    };
    m_warnSlot = [this](WarningCode code, const std::string &message) {
        this->setWarning(code, message);
    };
    // create the DIM state
    m_state.addStateValue(LOADING, "LOADING");
    m_state.addStateValue(IDLE, "IDLE");
    m_state.addStateValue(FILTERS_INIT, "FILTERS INITIALISATION");
    m_state.addStateValue(SAMPLES_INIT, "SAMPLES INITIALISATION");
    m_state.addStateValue(COLLIMATOR_INIT, "COLLIMATOR INITIALISATION");
    m_state.addStateValue(HIGHVOLTAGE_INIT, "HIGHVOLTAGE INITIALISATION");
    m_state.addStateValue(DAQS_INIT, "DAQS INITIALISATION");
    m_state.addStateValue(MERGER_INIT, "MERGER INITIALISATION");
    m_state.addStateValue(STARTING, "STARTING");
    m_state.addStateValue(RUNNING, "RUNNING");
    m_state.addStateValue(STOPPING, "STOPPING");
    setState(EacsState(NOT_READY));
    m_state.refresh();
}

ServiceStateMachine::~ServiceStateMachine()
{
    m_conn.clear();
}

EacsState ServiceStateMachine::getState() const
{
    return EacsState(m_state.getValue());
}

bool ServiceStateMachine::setState(EacsState val)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (!check(val))
    {
        LOG(ERROR)
            << "[EACS/State] state change rejected: " << toString(getState())
            << " -> " << toString(val);
        return false;
    }
    LOG(INFO) << "[EACS/State] moving to state: " << toString(val);
    m_state.setValue(val);
    lock.unlock();
    m_state.refresh();
    return true;
}

bool ServiceStateMachine::check(EacsState val) const
{
    DIMState::State current = getState();
    switch (DIMState::State(val))
    {
    case LOADING:
        if (current == IDLE || current == NOT_READY)
            return true;
        else if (current == ERROR)
            return m_state.errorsCount() <= 2; // ERR_FIRST_ERROR ERR_LAST_ERROR
        return false;
    case IDLE:
        if (current != LOADING && current != STOPPING)
            return false;
        break;
    case FILTERS_INIT:
        if (current != IDLE)
            return false;
        break;
    case SAMPLES_INIT:
        if (current != FILTERS_INIT)
            return false;
        break;
    case COLLIMATOR_INIT:
        if (current != SAMPLES_INIT)
            return false;
        break;
    case HIGHVOLTAGE_INIT:
        if (current != COLLIMATOR_INIT)
            return false;
        break;
    case DAQS_INIT:
        if (current != HIGHVOLTAGE_INIT)
            return false;
        break;
    case MERGER_INIT:
        if (current != DAQS_INIT)
            return false;
        break;
    case STARTING:
        if (current != MERGER_INIT)
            return false;
        break;
    case RUNNING:
        if (current != STARTING)
            return false;
        break;
    case STOPPING:
        switch (current)
        {
        case FILTERS_INIT:
        case SAMPLES_INIT:
        case COLLIMATOR_INIT:
        case DAQS_INIT:
        case MERGER_INIT:
        case STARTING:
        case RUNNING: break;
        default: return false;
        };
        break;
    }
    return true;
}

void ServiceStateMachine::setError(ErrorCode code, const std::string &message)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (message.empty())
    {
        if (m_state.removeError(code))
        {
            LOG(ERROR) << "[EACS/State]: error cleared code=" << code;
            bool notify = (m_state.errorsCount() ==
                           2); // ERR_FIRST_ERROR and ERR_LAST_ERROR remains

            lock.unlock();
            m_state.refresh();
            if (notify)
                isErrorSignal(false);
        }
    }
    else
    {
        const std::string msgWithDate = getMessageWithDatePrefix(message);
        LOG(ERROR) << "[EACS/State]: error code=" << code
                   << " message=" << msgWithDate;
        bool firstError = (m_state.errorsCount() == 0);
        if (firstError)
            m_state.addError(ERR_FIRST_ERROR, "[FirstError]: " + msgWithDate);
        m_state.addError(code, msgWithDate);
        m_state.addError(ERR_LAST_ERROR, "[LastError]: " + msgWithDate);
        lock.unlock();
        m_state.refresh();
        if (firstError)
            isErrorSignal(true);
    }
}

void ServiceStateMachine::clearErrors()
{
    LOG(ERROR) << "[EACS/State]: clearing runtime errors";
    for (ErrorCode code : RuntimeErrors)
        setError(code, std::string());
}

void ServiceStateMachine::setWarning(WarningCode code,
                                     const std::string &message)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    if (message.empty())
    {
        if (m_state.removeWarning(code))
        {
            lock.unlock();
            LOG(WARNING) << "[EACS/State]: warning cleared code=" << code;
            m_state.refresh();
        }
    }
    else
    {
        const std::string msgWithDate = getMessageWithDatePrefix(message);
        LOG(WARNING) << "[EACS/State]: warning code=" << code
                     << " message=" << msgWithDate;
        m_state.addWarning(code, msgWithDate);
        lock.unlock();
        m_state.refresh();
    }
}

void ServiceStateMachine::clearWarnings()
{
    LOG(WARNING) << "[EACS/State]: clearing warnings";
    m_state.clearWarnings();
    m_state.refresh();
}

bool ServiceStateMachine::isInError()
{
    return (getState() == EacsState(ERROR));
}

void ServiceStateMachine::connect(ErrorSignal *err, WarningSignal *warn)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    if (err)
        m_conn.emplace_back(err->connect(m_errSlot));
    if (warn)
        m_conn.emplace_back(warn->connect(m_warnSlot));
}

std::string ServiceStateMachine::getErrorMessage()
{
    return m_state.getErrorMessage();
}

int32_t ServiceStateMachine::getErrorCode()
{
    return m_state.getErrorCode();
}

std::string ServiceStateMachine::getMessageWithDatePrefix(
    const std::string &message)
{
    const std::time_t t = std::time(nullptr);
    std::vector<char> buff(100);
    std::ostringstream oss;
    oss << "[";
    if (std::strftime(buff.data(), buff.size(), "%FT%T", std::localtime(&t)))
        oss << buff.data(); // it will be "[]" if fails to write on buffer
    oss << "] " << message;
    return oss.str();
}

} /* namespace eacs */
} /* namespace ntof */
