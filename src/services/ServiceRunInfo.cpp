//
// Created by matteof on 7/7/20.
//

#include "ServiceRunInfo.hpp"

#include "Config.hpp"
#include "data/RunInfo.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

ServiceRunInfo::ServiceRunInfo() :
    dim::DIMParamList(Config::instance().getDimPrefix() + "/RunInfo")
{
    setHandler(this);
}

void ServiceRunInfo::onValidatedEvent(
    const EventValidator::ValidEvent & /*event*/,
    uint32_t extensionNumber,
    float intensity)
{
    try
    {
        dim::AddMode mode = dim::AddMode::UPDATE;
        std::lock_guard<std::mutex> lock(m_atomic);
        addParameter<uint32_t>(NUMBER_OF_EVENTS_VALID, "validatedNumber", "",
                               extensionNumber, mode, false);
        addParameter<float>(NUMBER_OF_PROTONS_VALID, "validatedProtons", "",
                            intensity, mode, true);
    }
    catch (const dim::DIMException &e)
    {
        errorSignal(PARAM_CHANGE_ERR,
                    std::string("[RunInfo]: ") + e.getMessage());
    }
}

int ServiceRunInfo::parameterChanged(
    std::vector<ntof::dim::DIMData> &settingsChanged,
    const ntof::dim::DIMParamList & /*list*/,
    int &errCode,
    std::string &errMsg)
{
    // title and description can ALWAYS be modified
    int ret = 0;
    for (auto &dimData : settingsChanged)
    {
        switch (dimData.getIndex())
        {
        case RUN_NUMBER:
        case EVENT_NUMBER:
        case NUMBER_OF_EVENTS_VALID:
        case NUMBER_OF_PROTONS_VALID:
        case AREA:
        case EXPERIMENT:
        case DETECTORS_SETUP_ID:
        case MATERIALS_SETUP_ID:
            errCode = ErrorCode::PARAM_CHANGE_ERR;
            errMsg = "Trying to modify read-only parameter.";
            ret = -1;
            break;
        default: break;
        }
    }
    return ret;
}

void ServiceRunInfo::updateRunInfo(const RunInfo &info)
{
    try
    {
        dim::AddMode mode = dim::AddMode::UPDATE;
        const std::string &expArea = Config::instance().getExperimentalArea();

        std::lock_guard<std::mutex> lock(m_atomic);
        // Init RunInfo Service data
        addParameter<uint32_t>(RUN_NUMBER, "runNumber", "", info.runNumber,
                               mode, false);
        addParameter<uint32_t>(EVENT_NUMBER, "eventNumber", "",
                               info.eventNumber, mode, false);
        addParameter<uint32_t>(NUMBER_OF_EVENTS_VALID, "validatedNumber", "",
                               info.validatedNumber, mode, false);
        addParameter<float>(NUMBER_OF_PROTONS_VALID, "validatedProtons", "",
                            info.protons, mode, false);
        addParameter<const std::string &>(TITLE, "title", "", info.title, mode,
                                          false);
        addParameter<const std::string &>(DESCRIPTION, "description", "",
                                          info.description, mode, false);
        addParameter<const std::string &>(AREA, "area", "", expArea, mode,
                                          false);
        addParameter<const std::string &>(EXPERIMENT, "experiment", "",
                                          info.experiment, mode, false);
        addParameter<int64_t>(DETECTORS_SETUP_ID, "detectorsSetupId", "",
                              info.detectorsSetupId, mode, false);
        addParameter<int64_t>(MATERIALS_SETUP_ID, "materialsSetupId", "",
                              info.materialsSetupId, mode, true);
    }
    catch (const dim::DIMException &e)
    {
        errorSignal(PARAM_CHANGE_ERR,
                    std::string("[RunInfo]: ") + e.getMessage());
    }
}

void ServiceRunInfo::getRunInfo(RunInfo &info) const
{
    try
    {
        std::lock_guard<std::mutex> lock(m_atomic);
        info.runNumber = getParameterAt(RUN_NUMBER).getUIntValue();
        info.title = getParameterAt(TITLE).getStringValue();
        info.description = getParameterAt(DESCRIPTION).getStringValue();
        info.experiment = getParameterAt(EXPERIMENT).getStringValue();
        info.detectorsSetupId = getParameterAt(DETECTORS_SETUP_ID).getLongValue();
        info.materialsSetupId = getParameterAt(MATERIALS_SETUP_ID).getLongValue();
    }
    catch (const dim::DIMException &e)
    {
        errorSignal(PARAM_NOT_EXIST_ERR,
                    std::string("[RunInfo]: ") + e.getMessage());
    }
}

RunNumber ServiceRunInfo::getRunNumber() const
{
    return getParameterAt(RUN_NUMBER).getUIntValue();
}

void ServiceRunInfo::commandReceived(dim::DIMCmd &cmd)
{
    DIMParamList::commandReceived(cmd);
    if (m_ack->getStatus() == dim::DIMAck::OK)
    {
        externalUpdateSignal();
    }
}

} // namespace eacs
} // namespace ntof
