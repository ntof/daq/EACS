//
// Created by matteof on 7/7/20.
//

#ifndef EACS_SERVICERUNINFO_HPP
#define EACS_SERVICERUNINFO_HPP

#include <DIMData.h>
#include <DIMParamList.h>

#include "EventValidator.hpp"

namespace ntof {
namespace eacs {

class RunInfo;

class ServiceRunInfoHandler : public ntof::dim::DIMParamListHandler
{
public:
};

class ServiceRunInfo :
    public dim::DIMParamList,
    public ntof::dim::DIMParamListHandler
{
public:
    ServiceRunInfo();

    /**
     * @brief actions to do when a validated event occurs
     * @param[in] event Timing details
     * @param[in] extensionNumber number of event since start
     * @param[in] intensity number of valid protons
     */
    void onValidatedEvent(const EventValidator::ValidEvent &event,
                          uint32_t extensionNumber,
                          float intensity);

    /**
     * @brief publish RunInfo data on the service
     * @param info[in]: the run information
     */
    void updateRunInfo(const RunInfo &info);

    /**
     * @brief Extract Run information from service
     * @param[out] index: Index of the data to remove
     */
    void getRunInfo(RunInfo &info) const;

    RunNumber getRunNumber() const;

    virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const ntof::dim::DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) override;

    // Signals
    ExternalUpdateSignal externalUpdateSignal;
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    void commandReceived(dim::DIMCmd &cmd) override;

    mutable std::mutex m_atomic; //!< Mutex to avoid modification in the middle
                                 //!< of operations
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_SERVICERUNINFO_HPP
