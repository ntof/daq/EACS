//
// Created by matteof on 7/7/20.
//

#ifndef EACS_SERVICETIMING_HPP
#define EACS_SERVICETIMING_HPP

#include <DIMAck.h>
#include <DIMData.h>
#include <DIMParamList.h>
#include <Signals.hpp>
#include <proxy/DIMParamListProxy.hpp>

#include "clients/ClientBct.hpp"
#include "clients/ClientTiming.hpp"

namespace ntof {
namespace eacs {

struct BctPayload
{
    TimingEventDetails trigger;
    EventHeader eveh;
};

class ServiceTiming : protected ntof::dim::DIMParamListProxy
{
public:
    typedef ntof::utils::signal<void(const TimingEventDetails &event,
                                     float intensity)>
        BctSignal;

    enum Params
    {
        MODE = 1,
        CALIB_TRIGGER_REPEAT,
        CALIB_TRIGGER_PERIOD,
        CALIB_TRIGGER_PAUSE,
        EVENT_NUMBER,
        CALIB_OUT_ENABLED,
        PARASITIC_OUT_ENABLED,
        PRIMARY_OUT_ENABLED
    };

    enum Mode
    {
        DISABLED = 0,
        AUTOMATIC = 1,
        CALIBRATION = 2
    };

    ServiceTiming();
    ~ServiceTiming();

    /**
     * @brief reload service from remote
     */
    void load();

    /**
     * @brief suspend remote timing service
     * @throws DIMException, EACSException
     */
    ntof::dim::DIMAck suspend();

    /**
     * @brief start remote timing service
     * @throws DIMException, EACSException
     */
    ntof::dim::DIMAck start();

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

    BctSignal bctSignal;
    ClientTiming::TimingSignal timingSignal;

    using DIMParamListProxy::isSyncing;
    using DIMParamListProxy::syncSignal;

private:
    void onSync();
    void onBctEvent(const BctEvent &bctEvent);

    // Clients
    ClientBct m_bctClient; //!< The client to subscribe to the DIM service
                           //!< which is publishing the data to fill the
                           //!< structure

    ClientTiming m_timingClient;
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_SERVICETIMING_HPP
