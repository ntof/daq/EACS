//
// Created by matteof on 7/7/20.
//

#include "ServiceTiming.hpp"

#include <DIMException.h>
#include <NTOFLogging.hpp>
#include <math.h>

#include "Config.hpp"
#include "EACSException.hpp"
#include "EACSTypes.hpp"
#include "Eacs.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

ServiceTiming::ServiceTiming() :
    DIMParamListProxy(Config::instance().getDimPrefix() + "/Timing",
                      Config::instance().getTimeMachineName())
{
    /* let timing manage its outputs */
    setReadOnly(CALIB_OUT_ENABLED);
    setReadOnly(PARASITIC_OUT_ENABLED);
    setReadOnly(PRIMARY_OUT_ENABLED);
    setNoUpdatesOnSync(true);

    /* syncing is always singleShot */
    syncSignal.connect([this](DIMParamListProxy &) { this->onSync(); });

    // Wiring signals and do chaining
    m_bctClient.errorSignal.connect(errorSignal);
    m_bctClient.warningSignal.connect(warningSignal);

    // Forward timingSignals
    m_timingClient.timingSignal.connect(timingSignal);

    m_bctClient.bctSignal.connect(
        [this](const BctEvent &bctEvent) { onBctEvent(bctEvent); });

    m_client.reset(new dim::DIMParamListClient(m_remoteService));
    m_client->dataSignal.connect([this](dim::DIMParamListClient &) {
        errorSignal(ERR_TIMING_NO_LINK, std::string());
    });
    m_client->errorSignal.connect(
        [this](const std::string &err, dim::DIMParamListClient &) {
            if (err == DIMParamListClient::NoLinkError)
            {
                errorSignal(ERR_TIMING_NO_LINK, "[Timing]: no link");
            }
        });
}

ServiceTiming::~ServiceTiming()
{
    // since ServiceTiming connected its signals, it must die before us
    m_client.reset();
}

void ServiceTiming::load()
{
    warningSignal(WARN_TIMING_LOADING, "[Timing]: loading timing service");
    setSyncing(true);
}

DIMAck ServiceTiming::suspend()
{
    try
    {
        return m_client->setParameter(Params::MODE,
                                      DIMEnum(Mode::DISABLED, "mode"));
    }
    catch (const dim::DIMException &ex)
    {
        std::ostringstream oss;
        oss << "[Timing]: Failed to send command: " << ex.what();
        errorSignal(ERR_TIMING_CMD, oss.str());
        EACS_THROW(oss.str(), ERR_TIMING_CMD);
    }
}

DIMAck ServiceTiming::start()
{
    try
    {
        setValue<int64_t>(Params::EVENT_NUMBER, 0, false);
        return sendParameters();
    }
    catch (const dim::DIMException &ex)
    {
        std::ostringstream oss;
        oss << "[Timing]: Failed to send command: " << ex.what();
        errorSignal(ERR_TIMING_CMD, oss.str());
        EACS_THROW(oss.str(), ERR_TIMING_CMD);
    }
}

void ServiceTiming::onSync()
{
    if (getParameterCount() > 1)
    {
        LOG(INFO) << "[ServiceTiming] configuration synced";
        this->setSyncing(false);
        warningSignal(WARN_TIMING_LOADING, std::string());
    }
}

void ServiceTiming::onBctEvent(const BctEvent &bctEvent)
{
    try
    {
        TimingEventDetails tEvent;
        tEvent.evtType = "undefined";
        tEvent = m_timingClient.getTriggerInfo(bctEvent.cycleStamp);

        float intensity;
        if (tEvent.evtType == "CALIBRATION")
        {
            intensity = 0;
        }
        else
        {
            intensity = bctEvent.totalIntensityPreferred * pow(10, 10);
        }
        bctSignal(tEvent, intensity);
    }
    catch (const EACSException &e)
    {
        LOG(WARNING) << "[ServiceTiming] bct client error: " << e.getMessage();
    }
}

} // namespace eacs
} // namespace ntof
