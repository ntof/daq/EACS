/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-13T17:03:12+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "ServiceEventValidator.hpp"

#include "Config.hpp"

namespace ntof {
namespace eacs {

ServiceEventValidator::ServiceEventValidator() :
    dim::DIMDataSet(Config::instance().getDimPrefix() + "/Validation")
{
    addData(RUN_NUMBER, "runNumber", "", RunNumber(0));
    addData(EVENT_NUMBER, "eventNumber", "", EventNumber(0));
    addData(VALIDATED_NUMBER, "validatedNumber", "", EventNumber(0));
    addData(FILE_SIZE, "fileSize", "", uint64_t(0));
}

void ServiceEventValidator::update(EventValidator::ValidEvent &event,
                                   uint32_t extensionNumber)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    setValue(RUN_NUMBER, event.runNumber, false);
    setValue(EVENT_NUMBER, event.eventNumber, false);
    setValue(VALIDATED_NUMBER, extensionNumber, false);
    setValue(FILE_SIZE, event.fileSize, true);
}

} // namespace eacs
} // namespace ntof
