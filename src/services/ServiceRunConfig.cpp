//
// Created by matteof on 7/7/20.
//

#include "ServiceRunConfig.hpp"

#include <sstream>

#include "Config.hpp"
#include "data/RunInfo.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

ServiceRunConfig::ServiceRunConfig() :
    dim::DIMParamList(Config::instance().getDimPrefix() + "/RunConfig")
{
    setHandler(this);
}

int ServiceRunConfig::parameterChanged(
    std::vector<ntof::dim::DIMData> &settingsChanged,
    const ntof::dim::DIMParamList & /*list*/,
    int &errCode,
    std::string &errMsg)
{
    // title and description can ALWAYS be modified
    int ret = 0;
    for (auto &dimData : settingsChanged)
    {
        switch (dimData.getIndex())
        {
        case RUN_NUMBER:
            errCode = ErrorCode::PARAM_CHANGE_ERR;
            errMsg = "Trying to modify read-only parameter.";
            ret = -1;
            break;
        default: break;
        }
    }
    return ret;
}

void ServiceRunConfig::updateRunInfo(const RunInfo &info)
{
    try
    {
        dim::AddMode mode = dim::AddMode::UPDATE;
        std::lock_guard<std::mutex> lock(m_atomic);
        // Init RunConfig Service data
        addParameter<uint32_t>(RUN_NUMBER, "runNumber", "", info.runNumber,
                               mode, false);
        addParameter<const std::string &>(TITLE, "title", "", info.title, mode,
                                          false);
        addParameter<const std::string &>(DESCRIPTION, "description", "",
                                          info.description, mode, false);
        addParameter<const std::string &>(EXPERIMENT, "experiment", "",
                                          info.experiment, mode, false);
        addParameter<int64_t>(DETECTORS_SETUP_ID, "detectorsSetupId", "",
                              info.detectorsSetupId, mode, false);
        addParameter<int64_t>(MATERIALS_SETUP_ID, "materialsSetupId", "",
                              info.materialsSetupId, mode, true);
    }
    catch (const dim::DIMException &e)
    {
        errorSignal(PARAM_CHANGE_ERR,
                    std::string("[RunConfig]: ") + e.getMessage());
    }
}

void ServiceRunConfig::getRunInfo(RunInfo &info)
{
    try
    {
        std::lock_guard<std::mutex> lock(m_atomic);
        info.runNumber = getParameterAt(RUN_NUMBER).getUIntValue();
        info.title = getParameterAt(TITLE).getStringValue();
        info.description = getParameterAt(DESCRIPTION).getStringValue();
        info.experiment = getParameterAt(EXPERIMENT).getStringValue();
        info.detectorsSetupId = getParameterAt(DETECTORS_SETUP_ID).getLongValue();
        info.materialsSetupId = getParameterAt(MATERIALS_SETUP_ID).getLongValue();
    }
    catch (const dim::DIMException &e)
    {
        errorSignal(PARAM_NOT_EXIST_ERR,
                    std::string("[RunConfig]: ") + e.getMessage());
    }
}

void ServiceRunConfig::commandReceived(dim::DIMCmd &cmd)
{
    DIMParamList::commandReceived(cmd);
    if (m_ack->getStatus() == dim::DIMAck::OK)
    {
        externalUpdateSignal();
    }
}

} // namespace eacs
} // namespace ntof
