//
// Created by matteof on 7/7/20.
//

#include "ServiceConfig.hpp"

#include "Config.hpp"

namespace ntof {
namespace eacs {

ServiceConfig::ServiceConfig() :
    DIMXMLRpc(Config::instance().getDimPrefix() + "/Config")
{}

void ServiceConfig::rpcReceived(dim::DIMCmd & /*cmd*/)
{
    // TODO
}

} // namespace eacs
} // namespace ntof
