/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-13T16:59:46+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef SERVICE_EVENT_VALIDATOR_HPP__
#define SERVICE_EVENT_VALIDATOR_HPP__

#include <mutex>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <EACSTypes.hpp>

#include "EventValidator.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

class ServiceEventValidator : public dim::DIMDataSet
{
public:
    enum Params
    {
        RUN_NUMBER = 0,
        EVENT_NUMBER,
        VALIDATED_NUMBER,
        FILE_SIZE
    };

    ServiceEventValidator();

    void update(EventValidator::ValidEvent &event, uint32_t extensionNumber);

protected:
    std::mutex m_mutex; //!< Mutex to avoid concurrent access
};

} // namespace eacs
} // namespace ntof

#endif
