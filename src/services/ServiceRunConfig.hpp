//
// Created by matteof on 7/7/20.
//

#ifndef EACS_SERVICERUNCONFIG_HPP
#define EACS_SERVICERUNCONFIG_HPP

#include <DIMData.h>
#include <DIMParamList.h>

#include "misc.h"

namespace ntof {
namespace eacs {

class RunInfo;

class ServiceRunConfig :
    public dim::DIMParamList,
    public ntof::dim::DIMParamListHandler
{
public:
    ServiceRunConfig();
    /**
     * @brief publish RunInfo data on the service
     * @param info[in]: the run information
     */
    void updateRunInfo(const RunInfo &info);

    /**
     * @brief Extract Run information from service
     * @param[out] index: Index of the data to remove
     */
    void getRunInfo(RunInfo &info);

    virtual int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                                 const ntof::dim::DIMParamList &list,
                                 int &errCode,
                                 std::string &errMsg) override;

    // Signals
    ExternalUpdateSignal externalUpdateSignal;
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    void commandReceived(dim::DIMCmd &cmd) override;

    mutable std::mutex m_atomic; //!< Mutex to avoid modification in the middle
                                 //!< of operations
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_SERVICERUNCONFIG_HPP
