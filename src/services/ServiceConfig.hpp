//
// Created by matteof on 7/7/20.
//

#ifndef EACS_SERVICECONFIG_HPP
#define EACS_SERVICECONFIG_HPP

#include <DIMXMLRpc.h>

#include "misc.h"

namespace ntof {
namespace eacs {

class ServiceConfig : public ntof::dim::DIMXMLRpc
{
public:
    ServiceConfig();

    virtual void rpcReceived(ntof::dim::DIMCmd &cmd) override;

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_SERVICECONFIG_HPP
