/*
 * DimRunInfo.h
 *
 * Created on: Feb 20, 2015
 * Author: agiraud
 */
#ifndef SERVICEDAQSLIST_H_
#define SERVICEDAQSLIST_H_

#include <list>
#include <mutex>
#include <vector>

#include <DIMData.h>
#include <DIMDataSet.h>
#include <DaqTypes.h>
#include <Signals.hpp>
#include <Synchro.h>

#include "daq/Daq.hpp"
#include "daq/DaqConfig.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @class ServiceDaqsList
 * @brief Publish global information relative to the run on going
 */
class ServiceDaqsList : public ntof::dim::DIMDataSet
{
public:
    typedef std::string HostName;
    typedef std::unique_ptr<Daq> DaqPtr;

    typedef std::map<HostName, DaqPtr> DaqMap;

    ServiceDaqsList();
    /**
     * @brief Destructor of the class
     */
    ~ServiceDaqsList() override = default;

    /**
     * @brief configure all daqs
     */
    bool configureDaqs(ntof::eacs::RunNumber runNumber);

    /**
     * @brief start all enabled daqs
     */
    bool startDaqs();

    /**
     * @brief stop daqs
     */
    bool stopDaqs();

    /**
     * @brief reset all daqs
     */
    bool resetDaqs();

    /**
     * @brief Reload Card configurations
     */
    void loadDaqConfigs();

    /**
     * @brief check if all daqs have been loaded
     */
    bool isDaqLoaded() const;

    /**
     * @brief push daq configuration to database
     */
    void updateDatabase(ntof::eacs::RunNumber runNumber);

    /**
     * @brief generate MODH and RCTR for current configuration
     */
    void genHeaders(RunControlHeader &header, ModuleHeader &modh) const;

    /**
     * @brief get the recorded event size
     * @param[in]  runNumber the run number
     * @param[in]  event     the event
     * @param[out] eventSize the recorded event size
     * @return false if event has not yet been fully recorded
     */
    bool getEventSize(RunNumber runNumber,
                      EventNumber event,
                      uint64_t &eventSize) const;
    /**
     * @brief remove event from recorded events
     * @param[in] runNumber the run number
     * @param[in] event     the event number
     */
    void removeWritten(RunNumber runNumber, EventNumber event);

    /**
     * @brief Update the content of the DIM service
     */
    void updateDimService();

    inline const DaqMap &getDaqs() const { return m_daqs; }

    std::vector<uint32_t> getActiveCrateIds();

    /**
     * @brief signal used to notify errors
     */
    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    ntof::utils::signal<void()> daqReadySignal;
    ntof::utils::signal<void()> daqLoadedSignal;

protected:
    /**
     * @brief create a specific daq if not exist in the set
     * @param index: the index of the daq
     */
    void createDaq(uint32_t index, const std::string &hostname);
    void onModeSignal();
    void onDaqLoaded();

    DaqMap m_daqs; //!< Remote data related to a daq
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* SERVICEDAQSLIST_H_ */
