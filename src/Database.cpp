/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-12T10:05:55+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "Database.hpp"

#include <functional>
#include <map>
#include <string>

#include <easylogging++.h>

#include "Config.hpp"
#include "db/OracleDatabase.hpp"
#include "db/SQLiteDatabase.hpp"

#include <Singleton.hxx>

using namespace ntof::eacs;

template void ntof::utils::Singleton<Database>::destroy();

typedef std::map<std::string, std::function<Database *()>> DatabaseRegistry;
/*
 * since we use static libraries this method is safer than self-registering
 * objects, those would be dropped at link time
 */
static const DatabaseRegistry s_registry{
    {"OracleDatabase", [] { return new OracleDatabase(); }},
    {"SQLiteDatabase", [] { return new SQLiteDatabase(); }}};

template<>
Database &ntof::utils::Singleton<Database>::instance()
{
    if (!m_instance)
    {
        SingletonMutex lock;
        // cppcheck-suppress identicalInnerCondition ; MT protection
        if (!m_instance)
        {
            const std::string &dbName = Config::instance().getDatabaseName();
            DatabaseRegistry::const_iterator it = s_registry.find(dbName);
            if (it == s_registry.end())
            {
                LOG(WARNING) << "Database " << dbName << " not found, "
                             << " falling back on SQLiteDatabase";
                m_instance = new SQLiteDatabase();
            }
            else
            {
                m_instance = it->second();
            }
        }
    }
    return *m_instance;
}
