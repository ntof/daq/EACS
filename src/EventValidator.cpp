/*
 * CheckFiles.cpp
 *
 * Created on: Nov 6, 2014
 * Author: agiraud
 */

#include "EventValidator.hpp"

#include <chrono>

#include <boost/shared_ptr.hpp>

#include <easylogging++.h>

#include "Config.hpp"
#include "Database.hpp"
#include "EACSException.hpp"
#include "Eacs.hpp"
#include "daq/Daq.hpp"
#include "services/ServiceEventValidator.hpp"

using std::chrono::steady_clock;

namespace ntof {
namespace eacs {

using DaqMap = ServiceDaqsList::DaqMap;
using DaqPtr = ServiceDaqsList::DaqPtr;

EventValidator::EventValidator(const ClientRFMergerSharedPtr &clientRfMerger,
                               const ServiceDaqsListSharedPtr &serviceDaq) :
    Thread("EventValidator"),
    m_extensionNumber(0),
    m_runNumber(InvalidRunNumber),
    m_timeout((uint32_t) Config::instance().getTimeoutCheckFiles()),
    m_missedEvents(0),
    m_hasEvents(false),
    m_srv(new ServiceEventValidator),
    m_serviceDaq(serviceDaq),
    m_clientRFMerger(clientRfMerger)
{
    m_addhEvents.signal.connect(
        [this](const ntof::eacs::ClientAddhEvents::Event &evt) {
            this->onAddhEvent(evt);
        });
    m_addhEvents.errorSignal.connect(errorSignal);

    if (!m_clientRFMerger)
    {
        LOG(WARNING) << "[EventValidator] RFM Client is null. Creating my own.";
        m_clientRFMerger.reset(new ClientRFMerger());
    }
    if (!m_serviceDaq)
    {
        LOG(WARNING)
            << "[EventValidator] DAQ List Service is null. Creating my own.";
        m_serviceDaq.reset(new ServiceDaqsList());
    }
}

EventValidator::~EventValidator()
{
    /* can't rely on Thread::~Thread since EventValidator would already been
     * destroyed */
    stop();
}

void EventValidator::onBctEvent(const TimingEventDetails &event,
                                RunNumber runNumber,
                                float intensity)
{
    EventData commit;
    commit.eventNumber = event.evtNumber;
    commit.cycleStamp = event.cycleStamp;
    commit.runNumber = runNumber;
    commit.beamTypes = fromString(event.evtType);
    commit.intensity = intensity;
    commit.start = steady_clock::now();

    LOG(INFO) << "[EventValidator] adding event from BCT"
              << " runNumber:" << commit.runNumber
              << " eventNumber:" << commit.eventNumber;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_events[commit.eventNumber] = commit;
        m_waked = true;
    }
    // Wake up the algorithm of validation
    m_cond.notify_all();
}

void EventValidator::onAddhEvent(const ntof::eacs::ClientAddhEvents::Event &evt)
{
    LOG(INFO)
        << "[EventValidator] adding event from ADDH"
        << " runNumber:" << evt.runNumber << " eventNumber:" << evt.timingEvent;

    // Initialize the structure and add it to the collection
    ValidEvent event;
    event.eventNumber = evt.timingEvent;
    event.runNumber = evt.runNumber;
    event.fileSize = evt.fileSize;
    event.start = steady_clock::now();
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_validEvents[event.eventNumber] = event;
        m_waked = true;
    }
    // Wake up the algorithm of validation
    m_cond.notify_all();
}

void EventValidator::thread_func()
{
    while (m_started.load())
    {
        // Wait while the collection of validated events is empty
        std::unique_lock<std::mutex> lock(m_mutex);
        m_waked = false; // reset for later check(s)

        if (m_events.empty() && m_validEvents.empty() && m_hasEvents)
        {
            m_hasEvents = false;
            lock.unlock();
            runningSignal(false);
            lock.lock();
        }
        if (m_events.empty() && m_validEvents.empty())
        {
            LOG(INFO) << "[EventValidator] waiting for new events";
            m_cond.wait(lock, [this]() { return m_waked; });
        }
        else
        {
            m_cond.wait_for(lock, std::chrono::seconds(1),
                            [this]() { return m_waked; });
        }
        if (!m_started.load())
            break; // stop request
        LOG(WARNING) << "[EventValidator] running...";

        if ((!m_events.empty() || !m_validEvents.empty()) && !m_hasEvents)
        {
            m_hasEvents = true;
            lock.unlock();
            runningSignal(true);
            lock.lock();
        }
        lock.unlock();

        // if the run number changed the extensionNumber is reset to 0.
        const uint32_t runNumber = Eacs::instance().getRunNumber();
        if (m_runNumber != runNumber)
        {
            m_runNumber = runNumber;
            m_extensionNumber = 0;
            m_totalIntensity = 0;
        }

        processValidatedEvents();
        processEvents();
    }

    LOG(INFO) << "[EventValidator] stopped";
}

void EventValidator::processEvents()
{
    EventDataMap events;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        events.swap(m_events);
    }

    steady_clock::time_point now = steady_clock::now();
    for (EventDataMap::iterator eventIt = events.begin();
         eventIt != events.end(); ++eventIt)
    {
        EventNumber eventNumber = eventIt->first;
        const EventData &event = eventIt->second;

        std::chrono::milliseconds elapsed =
            std::chrono::duration_cast<std::chrono::milliseconds>(now -
                                                                  event.start);

        if (elapsed.count() <= m_timeout)
            continue;

        if (m_runNumber != event.runNumber)
        {
            LOG(WARNING) << "[EventValidator] missed (old-run) (bct) event "
                            "eventNumber:"
                         << eventNumber << " runNumber:" << event.runNumber;
        }
        else
        {
            LOG(WARNING) << "[EventValidator] missed (bct) event eventNumber:"
                         << eventNumber;
            missEvent();
        }

        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_validEvents.erase(eventNumber);
        }
        m_serviceDaq->removeWritten(event.runNumber, eventNumber);
        events.erase(eventIt--);

        if (events.empty())
            break;
    }

    {
        /* swap back valid events */
        std::lock_guard<std::mutex> lock(m_mutex);
        events.swap(m_events);
        m_events.insert(events.begin(), events.end());
    }
}

void EventValidator::processValidatedEvents()
{
    ValidEventMap validEvents;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        validEvents.swap(m_validEvents);
    }

    steady_clock::time_point now = steady_clock::now();
    // For all the events wrote by the EACS but not validated yet
    for (ValidEventMap::iterator validEventIt = validEvents.begin();
         validEventIt != validEvents.end(); ++validEventIt)
    {
        EventNumber eventNumber = validEventIt->first;
        ValidEvent &validEvent = validEventIt->second;

        uint64_t eventSize = 0;
        bool isValid = (m_runNumber == validEvent.runNumber) &&
            m_serviceDaq->getEventSize(validEvent.runNumber,
                                       validEvent.eventNumber, eventSize);
        eventSize += validEvent.fileSize; // ADDH / RCTR / MODH

        // Check if the event is valid or it timed out
        std::chrono::milliseconds elapsed =
            std::chrono::duration_cast<std::chrono::milliseconds>(
                now - validEvent.start);

        bool eventProcessed = false;
        EventData commit;
        if (isValid && takeEvent(eventNumber, commit))
        {
            eventProcessed = true;
            validEvent.fileSize = eventSize;
            LOG(DEBUG) << "[EventValidator] validating eventNumber:"
                       << validEvent.eventNumber << " " << eventSize;
            // Notify the merger
            m_clientRFMerger->sendEvent(validEvent.runNumber, eventNumber,
                                        ++m_extensionNumber, eventSize);
            m_srv->update(validEvent, m_extensionNumber);

            // Commit all the data into the database
            commitDatabase(commit);
            m_totalIntensity += commit.intensity;

            validatedSignal(validEvent, m_extensionNumber, commit.intensity,
                            m_totalIntensity);

            // Reset the number of consecutive missed events
            m_missedEvents = 0;
            /* no need to reset errors, everything stops on error */
            warningSignal(WARN_MISSED_TRIGG, std::string());

            LOG(INFO)
                << "[EventValidator] event validated eventNumber:"
                << validEvent.eventNumber << " seqNum:" << m_extensionNumber
                << " runNumber:" << validEvent.runNumber;
        }
        else if (elapsed.count() > m_timeout)
        {
            if (m_runNumber != validEvent.runNumber)
            {
                LOG(WARNING)
                    << "[EventValidator] missed (old-run) event "
                       "eventNumber:"
                    << eventNumber << " runNumber:" << validEvent.runNumber;
            }
            else
            {
                LOG(WARNING)
                    << "[EventValidator] missed (addh) event eventNumber:"
                    << eventNumber;
                missEvent();
            }

            eventProcessed = true;
            std::lock_guard<std::mutex> lock(m_mutex);
            m_events.erase(eventNumber);
        }

        if (eventProcessed)
        {
            m_serviceDaq->removeWritten(validEvent.runNumber, eventNumber);

            // clear Valid Event list
            validEvents.erase(validEventIt--);
            if (validEvents.empty())
                break;
        }
    }

    {
        /* swap back valid events */
        std::lock_guard<std::mutex> lock(m_mutex);
        validEvents.swap(m_validEvents);
        m_validEvents.insert(validEvents.begin(), validEvents.end());
    }
}

void EventValidator::missEvent()
{
    int32_t missed;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        // Increment the number of consecutive missed events
        missed = ++m_missedEvents;
    }

    // Publish an error or a warning if a threshold is reached
    if (missed >= Config::instance().getMissingTriggerError())
    {
        errorSignal(ERR_MISSED_TRIGG,
                    "[EventValidator]: the number of missing "
                    "events in a row is too high");
    }
    else if (missed >= Config::instance().getMissingTriggerWarning())
    {
        warningSignal(WARN_MISSED_TRIGG,
                      "[EventValidator]: the number of missing "
                      "events in a row is too high");
    }
}

void EventValidator::reset()
{
    LOG(INFO) << "[EventValidator]: reset";
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_events.clear();
        m_validEvents.clear();
        m_missedEvents = 0;
        m_extensionNumber = 0;
        m_totalIntensity = 0;
        m_runNumber = InvalidRunNumber;
    }
    errorSignal(ERR_MISSED_TRIGG, std::string());
    warningSignal(WARN_MISSED_TRIGG, std::string());
}

bool EventValidator::takeValidEvent(EventNumber eventNumber, ValidEvent &event)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    ValidEventMap::iterator it = m_validEvents.find(eventNumber);
    if (it != m_validEvents.end())
    {
        event = it->second;
        m_validEvents.erase(it);
        return true;
    }
    LOG(ERROR) << "[EventValidator] no such valid event: " << eventNumber;
    return false;
}

bool EventValidator::takeEvent(EventNumber eventNumber, EventData &event)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    EventDataMap::iterator it = m_events.find(eventNumber);
    if (it != m_events.end())
    {
        event = it->second;
        m_events.erase(it);
        return true;
    }
    LOG(ERROR) << "[EventValidator] no such event data: " << eventNumber;
    return false;
}

void EventValidator::commitDatabase(const EventData &event)
{
    // update db
    LOG(INFO) << "[EventValidator] committing to database eventNumber:"
              << event.eventNumber;
    try
    {
        Database &db(Database::instance());
        db.addProtons(event.runNumber, event.intensity);
        db.insertTrigger(event.runNumber, event.eventNumber, event.beamTypes,
                         event.intensity, event.cycleStamp);
    }
    catch (const EACSException &e)
    {
        // query is in error.
        LOG(ERROR)
            << "[EventValidator] ERROR for"
            << " runNumber:" << event.runNumber
            << " eventNumber: " << event.eventNumber << " : " << e.getMessage();
        errorSignal(ERR_DB, e.getMessage());
    }
}

void EventValidator::printEventNumberWrote()
{
    LOG(INFO) << "**************************************************";
    LOG(INFO) << "List Event Number Wrote Received";
    int i = 0;
    for (auto &event : m_validEvents)
    {
        LOG(INFO) << "[" << i << "]" << event.second.eventNumber;
        ++i;
    }
    LOG(INFO) << "**************************************************";
}

uint32_t EventValidator::getExtensionNumber() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_extensionNumber;
}

bool EventValidator::hasEvents() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_hasEvents;
}

} // namespace eacs
} // namespace ntof
