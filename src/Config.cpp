/*
 * Config.cpp
 *
 * Created on: Jan 29, 2015
 * Author: agiraud
 */

#include "Config.hpp"

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
#include <utility>

#include <easylogging++.h>
#include <pugixml.hpp>

#include "EACSException.hpp"

#include <Singleton.hxx>

template class ntof::utils::Singleton<ntof::eacs::Config>;

namespace ntof {
namespace eacs {

const std::string Config::configFile = "/etc/ntof/eacs.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";

int32_t Config::getMissingTriggerError() const
{
    return missingTriggerError_;
}

int32_t Config::getMissingTriggerWarning() const
{
    return missingTriggerWarning_;
}

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        throw EACSException("Unable to read the configuration file : " + file,
                            __FILE__, __LINE__);
    }
    else
    {
        // Parse the config file
        pugi::xml_node eacsNode = doc.child("eacs");
        timeMachineName_ = eacsNode.child("timeMachineName")
                               .attribute("value")
                               .as_string("Timing");
        timeoutCheckFiles_ = eacsNode.child("timeoutCheckFiles")
                                 .attribute("value")
                                 .as_int(30000000);
        timeoutInitialization_ = eacsNode.child("timeoutInitialization")
                                     .attribute("value")
                                     .as_int(30000000);
        triggersHistory_ =
            eacsNode.child("triggersHistory").attribute("value").as_int(10);
        folderDest_ = eacsNode.child("folderDest")
                          .attribute("value")
                          .as_string("/DAQ/data/");
        BCTServiceName_ = eacsNode.child("BCTServiceName")
                              .attribute("value")
                              .as_string("FTN.BCT468.TOF");
        m_filterStationServiceName = eacsNode.child("FilterStationServiceName")
                                         .attribute("value")
                                         .as_string();
        m_sampleExchangerServiceName = eacsNode
                                           .child("SampleExchangerServiceName")
                                           .attribute("value")
                                           .as_string();
        m_sampleExchangerCmdServiceName = eacsNode
                                              .child(
                                                  "SampleExchangerServiceName")
                                              .attribute("command")
                                              .as_string();
        if (!m_sampleExchangerServiceName.empty() &&
            m_sampleExchangerCmdServiceName.empty())
        {
            LOG(ERROR)
                << "[Config]: command service not set on sampleExchanger";
        }

        serverName_ =
            eacsNode.child("serverName").attribute("value").as_string("EACS");
        missingTriggerWarning_ = eacsNode.child("missingTriggerWarning")
                                     .attribute("value")
                                     .as_int(15);
        missingTriggerError_ =
            eacsNode.child("missingTriggerError").attribute("value").as_int(50);

        m_hvWarnOnly = eacsNode.child("hvSpecialSettings")
                           .attribute("warnOnly")
                           .as_bool(false);

        m_hvSkipUpdateDB = eacsNode.child("hvSpecialSettings")
                               .attribute("skipUpdateDB")
                               .as_bool(false);

        pugi::xml_node db = eacsNode.child("database");
        m_databaseName = db.attribute("name").as_string("SQLiteDatabase");
        m_databasePath = db.attribute("path").as_string("");

        // DIM service prefix and history count
        const pugi::xml_node &dimNode = eacsNode.child("DIM");
        dimPrefix_ = dimNode.attribute("prefix").as_string("EACS");
        dimEventsCount_ = dimNode.attribute("history-events").as_int(20);

        for (pugi::xml_node hvCard = eacsNode.child("highVoltage").first_child();
             hvCard; hvCard = hvCard.next_sibling())
        {
            HighVoltageMap::mapped_type &hvChan =
                m_highVoltage[hvCard.attribute("index").as_uint()];
            for (pugi::xml_node channel = hvCard.first_child(); channel;
                 channel = channel.next_sibling())
            {
                hvChan.insert(channel.attribute("index").as_uint());
            }
        }

        m_lowBeamThreshold =
            eacsNode.child("lowBeam").attribute("threshold").as_float(0);
        m_lowBeamTimeout = eacsNode.child("lowBeam").attribute("timeout").as_int(
            -1);

        m_beamDetectionTimeout =
            eacsNode.child("beamDetection").attribute("timeout").as_int(-1);
    }
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
        delete m_instance;

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getFolderDest() const
{
    return folderDest_;
}

const std::string &Config::getTimeMachineName() const
{
    return timeMachineName_;
}

uint64_t Config::getTimeoutCheckFiles() const
{
    return timeoutCheckFiles_;
}

const std::string &Config::getBctServiceName() const
{
    return BCTServiceName_;
}

const std::string &Config::getFilterStationServiceName() const
{
    return m_filterStationServiceName;
}

const std::string &Config::getServerName() const
{
    return serverName_;
}

uint64_t Config::getTimeoutInitialization()
{
    return timeoutInitialization_;
}

uint64_t Config::getTriggersHistory()
{
    return triggersHistory_;
}

const std::string &Config::getDimPrefix() const
{
    return dimPrefix_;
}

uint32_t Config::getDimEventsCount() const
{
    return dimEventsCount_;
}

} /* namespace eacs */
} /* namespace ntof */
