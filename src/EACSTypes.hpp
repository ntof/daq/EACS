/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-15T09:56:48+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef EACSTYPES_HPP__
#define EACSTYPES_HPP__

#include <cstdint>
#include <string>
#include <vector>

namespace ntof {
namespace eacs {

typedef uint32_t RunNumber;
typedef uint32_t EventNumber;

extern const RunNumber InvalidRunNumber;
extern const EventNumber InvalidEventNumber;

enum BeamType
{
    CALIBRATION = 1,
    PRIMARY = 2,
    PARASITIC = 3
};

enum EnableType
{
    DISABLED,
    ENABLED
};

const std::string &toString(BeamType beamType);
BeamType fromString(const std::string &beamType);

/* states of the EACS */
enum EacsState
{
    LOADING = 1,
    IDLE,
    FILTERS_INIT,
    SAMPLES_INIT,
    COLLIMATOR_INIT,
    HIGHVOLTAGE_INIT,
    DAQS_INIT,
    MERGER_INIT,
    STARTING,
    RUNNING,
    STOPPING
};
const std::string &toString(EacsState state);

/* Errors and warnings codes */
enum ErrorCode
{
    ERR_LAST_ERROR = 1,  /* last error reported, makes error state permanent */
    ERR_INTERNAL = 2,    /* should not happen */
    ERR_FIRST_ERROR = 3, /* first error reported, makes error state permanent */

    ERR_DAQ_STATE = 10,

    ERR_NO_DAQ = 12,

    ERR_DB = 20,
    ERR_DB_EILSEQ = 21,
    ERR_DB_ENOENT = 22,

    ERR_CMD = 33,

    FILE_ERR = 50,
    WRITE_ERR = 51,
    LSEEK_ERR = 52,

    ERR_MISSED_TRIGG = 60,

    PARAM_CHANGE_ERR = 70,
    PARAM_NOT_EXIST_ERR = 71,

    ERR_ADDH_EVENTS_CMD = 110,
    ERR_ADDH_EVENTS_NO_LINK = 111,
    ERR_ADDH_EVENTS_XML_INVALID = 112,
    ERR_ADDH_STATE_NO_LINK = 113,
    ERR_ADDH_STATE_ERROR = 114,

    ERR_BCT_NO_LINK = 120,

    ERR_TIMING_CMD = 125,
    ERR_TIMING_NO_LINK = 126,

    ERR_RFM_CMD = 130,
    ERR_RFM_ACK = 131,

    ERR_FS_CMD = 135,
    ERR_SAEX_CMD = 136,

    ERR_TIMING_NO_SUCH_EVENT = 150,

    ERR_DAQCONF_CMD = 160,
    ERR_DAQCONF_CHANGED = 161,

    ERR_DAQ_NO_LINK = 200,
    // Save 99 slots for all daqs
    ERR_DAQ_NO_LINK_LAST = 299,

    ERR_HV_NO_LINK = 300,
    // Save 500 slots for all HV channels
    ERR_HV_NO_LINK_LAST = 799,
    /* only one slot for HV config change, error is sticky anyway */
    ERR_HVCONF_CHANGED = 800
};

/**
 * @brief list of errors that can be cleared
 */
extern const std::vector<ErrorCode> RuntimeErrors;

enum WarningCode
{
    WARN_TIMING_LOADING = 1,
    WARN_DAQ_ERR_DAQ_STATE = 2,

    /* db */
    WARN_NO_SUCH_MATERIAL_SETUP = 21,
    WARN_NO_SUCH_DETECTOR_SETUP = 22,
    WARN_NO_DAQS = 23,

    WARN_MISSED_TRIGG = 60,
    WARN_LOW_BEAM = 61,
    WARN_NO_BEAM = 62,

    WARN_FS_MOVED = 63,
    WARN_SAEX_MOVED = 64,

    WARN_DAQCONF_LOADING = 200,
    // Save 99 slots for all daqs
    WARN_DAQCONF_LOADING_LAST = 299,

    WARN_HV_STATUS = 300,
    // Save 500 slots for all HV channels
    WARN_HV_STATUS_LAST = 799,

    WARN_ADDH_STATE = 800,
    // Save 200 slots for ADDH no link warnings
    WARN_ADDH_STATE_LAST = 999,

    // This is used when Config::skipHVErrors is true.
    // It means all the errors are mapped as warnings
    // formula: warning index = error index + 1000
    WARN_HV_NO_LINK = 1300,
    // Save 500 slots for all HV channels nolink
    WARN_HV_NO_LINK_LAST = 1799,
    WARN_HVCONF_CHANGED = 1800
};

enum class MaterialType
{
    FILTER,
    SAMPLE,
    SOURCE
};

const std::string &toString(MaterialType state);

} // namespace eacs
} // namespace ntof

#endif
