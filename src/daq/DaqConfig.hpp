//
// Created by matteof on 7/22/20.
//

#ifndef EACS_DAQCONFIG_HPP
#define EACS_DAQCONFIG_HPP

#include <map>
#include <memory>
#include <mutex>

#include <DIMXMLInfo.h>
#include <DaqTypes.h>
#include <Signals.hpp>

#include "DaqCard.hpp"
#include "DaqChannelConfig.hpp"
#include "DaqZSPConfig.hpp"

namespace ntof {
namespace eacs {

class DaqConfig
{
public:
    typedef ntof::utils::signal<void()> CardUpdateSignal;
    typedef ntof::utils::signal<void()> LoadedSignal;
    typedef ntof::utils::signal<void(const DaqConfig &)> ConfigChangedSignal;
    typedef uint32_t CardNumber;
    typedef std::map<CardNumber, DaqCard::Shared> DaqCardMap;

    enum Params
    {
        NAME = 0,
        CARDS,
        USED
    };

    DaqConfig(const std::string &hostname, int32_t mChassisId);
    ~DaqConfig();

    /**
     * @brief (re)load configuration
     */
    void load();

    /**
     * @brief return true if some pieces are still loading
     */
    bool isLoading() const;

    int32_t getChassisId() const;

    /**
     * @brief return whereas the daq is used or not
     * @details this reflect the live value
     */
    bool isUsed() const;

    void updateDimData(dim::DIMData &daq);

    /**
     * @brief Apply configuration to Daqs
     */
    void sendParameters();

    /**
     * @brief update database info with current config
     */
    void updateDatabase(RunNumber runNumber);

    /**
     * @brief update rctr and modh
     * @param[out] rctr RCTR to update
     * @param[out] modh MODH to append to
     */
    void genHeaders(RunControlHeader &rctr, ModuleHeader &modh) const;

    // Signals
    CardUpdateSignal cardUpdateSignal;
    ConfigChangedSignal configChangedSignal;
    LoadedSignal loadedSignal;
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

private:
    void processListDaqElements(pugi::xml_document &document);
    void onLoadWarning(WarningCode error, const std::string &message);
    void onChannelEnabledChange(const DaqChannelConfig &config, bool enabled);

    DaqZSPConfig::Shared m_zspConfig;

    uint8_t m_loadingState;

    mutable std::mutex m_mutex; //!< The mutex the avoid concurrency

    DaqCardMap m_cards; //!< Cards info of this Daq
    bool m_isUsed;

    std::unique_ptr<dim::DIMXMLInfo> m_listDaqClient;

    std::string m_hostname; //!< Hostname
    int32_t m_chassisId;    //!< Crate id
};

} /* namespace eacs */
} /* namespace ntof */
#endif // EACS_DAQCONFIG_HPP
