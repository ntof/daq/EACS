//
// Created by matteof on 7/22/20.
//

#ifndef EACS_DAQCHANNELCONFIG_HPP
#define EACS_DAQCHANNELCONFIG_HPP

#include <DIMDataSetClient.h>
#include <DaqTypes.h>
#include <Signals.hpp>
#include <proxy/DIMParamListProxy.hpp>

#include "DaqUsedWarning.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

class DaqChannelConfig :
    protected dim::DIMParamListProxy,
    public dim::DIMParamListHandler
{
public:
    typedef ntof::utils::signal<void(const DaqChannelConfig &, bool)>
        ChannelEnabledSignal;
    typedef ntof::utils::signal<void(const DaqChannelConfig &,
                                     dim::DIMParamListClient &)>
        ConfigChangedSignal;

    enum Params
    {
        IS_CHANNEL_ENABLED = 1,
        DETECTOR_TYPE,
        DETECTOR_ID,
        MODULE_TYPE,
        CHANNEL_ID,
        MODULE_ID,
        CHASSIS_ID,
        STREAM_ID,
        SAMPLE_RATE,
        SAMPLE_SIZE,
        FULL_SCALE,
        DELAY_TIME,
        THRESHOLD,
        THRESHOLD_SIGN,
        ZSP_START,
        OFFSET,
        PRE_SAMPLES,
        POST_SAMPLES,
        CLOCK_STATE,
        INPUT_IMPEDANCE
    };

    enum CalibrationParams
    {
        CALIBRATED_FULL_SCALE = 0,
        CALIBRATED_OFFSET,
        CALIBRATED_THRESHOLD
    };

    DaqChannelConfig(const std::string &hostname,
                     const std::string &cardSn,
                     uint32_t card,
                     uint32_t channel);
    ~DaqChannelConfig();

    void load();

    void checkEnabled(dim::DIMParamListClient &client);

    /**
     * @brief return enabled state on the real service
     */
    inline bool isCurrentEnabled() const { return m_isCurrentEnabled; }

    inline const std::string &getCardSn() const { return m_cardSn; }
    inline const uint32_t getCardNumber() const { return m_cardNumber; }
    inline const uint32_t getChannelNumber() const { return m_channelNumber; }

    inline ntof::dim::DIMParamListClient *client() const
    {
        return m_client.get();
    };

    using dim::DIMParamListProxy::sendParameters;

    int parameterChanged(dim::DIMData::List &settingsChanged,
                         const DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;

    void getChannelConfig(ChannelConfig &config) const;

    // Signals
    WarningSignal warningSignal;
    ChannelEnabledSignal currentEnabledSignal; //<! connected on live service
    ConfigChangedSignal configChangedSignal;   //<! connected on live service

private:
    void onSync();
    void setNextUsed(bool enabled);

    const std::string m_hostname;
    const std::string m_cardSn;
    const uint32_t m_cardNumber;
    const uint32_t m_channelNumber;

    bool m_isCurrentEnabled;
    std::unique_ptr<ntof::dim::DIMDataSetClient> m_calibration;
    std::unique_ptr<DaqUsedWarning> m_nextUsed;
};

} /* namespace eacs */
} /* namespace ntof */
#endif // EACS_DAQCHANNELCONFIG_HPP
