//
// Created by matteof on 8/4/20.
//

#ifndef EACS_DAQCARD_HPP
#define EACS_DAQCARD_HPP

#include <memory>

#include "DaqChannelConfig.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

typedef std::unique_ptr<DaqChannelConfig> DaqChannelConfigPtr;
typedef std::list<DaqChannelConfigPtr> DaqChannelConfigList;

struct DaqCard
{
    typedef std::shared_ptr<DaqCard> Shared;

    enum Params
    {
        CHANNELS_COUNT = 0,
        TYPE,
        SERIAL_NUMBER,
        USED
    };

    DaqCard();

    /**
     * @brief send all channel configuration parameters to daqs
     * @throw DIMException if send command fails
     */
    void sendParameters();

    uint32_t nbChannel;
    uint32_t cardNumber;
    std::string type;
    SerialNumber serialNumber;
    bool isUsed;
    DaqChannelConfigList channels; //!< List of proxies for channels
    //!< config (all cards)
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_DAQCARD_HPP
