/*
 * RemoteDaq.h
 *
 * Created on: Aug 4, 2015
 * Author: agiraud
 */

#ifndef DAQINFO_H_
#define DAQINFO_H_

#include <functional>
#include <memory>

#include <DIMStateClient.h>
#include <DIMSuperClient.h>
#include <DIMXMLInfo.h>
#include <Signals.hpp>
#include <Worker.hpp>

#include "EACSTypes.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

class ClientDaqWriter;
class DaqConfig;

/**
 * @class DaqInfo
 * @brief Allow to keep an object to the all live time of the EACS object
 */

class Daq
{
public:
    enum class Mode
    {
        IDLE,
        CONFIG,
        MONITOR
    };
    static std::string toString(Mode state);

    /* states of the DAQs */
    enum DaqState
    {
        NOT_READY = GlobalState::NOT_READY,
        ERROR = GlobalState::ERROR,
        DETECTING_HARDWARE = 0,
        NOT_CONFIGURED = 1,
        CALIBRATING_CARDS = 2,
        WAITING_FOR_CMD = 3,
        PROCESSING_CMD = 4,
        STOPPING_ACQ = 8,
        STARTING_ACQ = 9,
        RESETING_DAQ = 10,
        INITIALIZING_ACQ = 11,
        WAITING_FOR_START_CMD = 12,
        SUPER_USER = 13
    };
    static std::string toString(DaqState state);

    typedef ntof::utils::signal<void(const Daq &, Mode)> ModeSignal;

    /**
     * @brief Constructor of the class
     * @param hostname: name of the machine
     * @param chassisId: ID of the crate
     */
    Daq(const std::string &hostname, int32_t chassisId);
    Daq(const Daq &other) = delete;
    Daq &operator=(const Daq &other) = delete;

    /**
     * @brief Destructor of the class
     */
    virtual ~Daq();

    /**
     * @brief start the daq initialization sequence
     * @details moves to MONITOR mode at the end of the initialization sequence
     * (async)
     */
    bool configure(ntof::eacs::RunNumber runNumber);

    /**
     * @brief start the acquisition
     * @return true if start command was properly sent to daq
     */
    bool startAcquisition();

    /**
     * @brief reset the Daq and this state machine
     * @details moves to IDLE Mode
     */
    bool reset();

    /**
     * @brief send a stop command to daq
     * @param[in] canReset whereas reset command is allowed
     * @details should be called in MONITOR Mode, moves to IDLE
     * @details reset command is sent if canReset=true and daq not in
     * WAITING_FOR_CMD
     */
    bool stop(bool canReset = true);

    /**
     * @brief Get the id of the crate
     * @return Return the id of the crate
     */
    inline int32_t getChassisId() const { return m_chassisId; }

    /**
     * @brief Get the hostname of the machine
     * @return Return the hostname
     */
    inline const std::string &getHostname() { return m_hostname; }

    /**
     * @brief Check if the DAQ is being used in the actual run
     * @return true if the daq is being used or false if it's not
     * @details this is updated after daq configuration
     */
    bool isUsed() const;

    /**
     * @brief Get the error message of the DAQ
     * @return Return the error message
     * @throw EACSException in case of error
     */
    std::string getErrors();

    /**
     * @brief Get the state of the DAQ
     * @return Return the state of the DAQ
     * @throw EACSException in case of error
     */
    std::string getStates();

    /**
     * @brief Check if an event number has been wrote by the DAQ
     * @param eventNumber: The event number to check
     * @param runNumber: The run number relative
     * @return Return true if the event has been wrote by the DAQ or false if it
     * is not
     */
    bool isWritten(RunNumber runNumber, EventNumber eventNumber);

    /**
     * @brief Remove an event form the collection of event wrote
     * @param eventNumber: The event number to remove
     * @param runNumber: The run number relative
     */
    void removeWritten(RunNumber runNumber, EventNumber eventNumber);

    /**
     * @brief Get the size of the file wrote by the DAQ
     * @param eventNumber: The event number to check
     * @param runNumber: The run number relative
     * @return Return the size of the file wrote by the DAQ
     */
    uint64_t getSize(RunNumber runNumber, EventNumber eventNumber);

    Mode getMode() const;

    inline DaqConfig &getConfig() { return *m_config; }
    inline const DaqConfig &getConfig() const { return *m_config; }

    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    ModeSignal modeSignal;

protected:
    /**
     * @brief Send a command to the DAQ
     * @param command: Command to send to the DAQ
     * @throw EACSException in case of error
     */
    bool sendCommand(const std::string &command);
    void onDaqStateChanged(DaqState state);
    void onConfigChanged();
    void clear();
    void setMode(Mode mode);
    void post(const std::function<void()> &fun);

    mutable std::mutex m_mutex;   //!< The mutex the avoid the concurrency
    ntof::utils::Worker m_worker; //!< Worker used to execute tasks on a Daq

    const int32_t m_chassisId;    //!< Crate id
    const std::string m_hostname; //!< Hostname

    std::unique_ptr<dim::DIMStateClient> m_daqState; //!< Client to the service
                                                     //!< of the DAQ state
    std::unique_ptr<dim::DIMStateClient> m_aqnState; //!< Client to the service
                                                     //!< of the AQN state
    std::unique_ptr<dim::DIMStateClient> m_writerState; //!< Client to the
                                                        //!< service of the
                                                        //!< writer state
    std::unique_ptr<ClientDaqWriter> m_wroteNumber; //!< Client to the service
                                                    //!< of the wrote number
    ntof::dim::DIMSuperClient m_command; //!< Client to send the commands start
    std::unique_ptr<DaqConfig> m_config; //!< Config Manager class (proxies of
                                         //!< channels and zsp)
    RunNumber m_runNumber;

    Mode m_mode;
};
} /* namespace eacs */
} /* namespace ntof */

#endif /* DAQINFO_H_ */
