//
// Created by matteof on 7/22/20.
//

#include "DaqConfig.hpp"

#include <sstream>
#include <utility>

#include <DIMXMLInfo.h>
#include <DaqTypes.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "DaqUsedWarning.hpp"
#include "Database.hpp"
#include "EACSException.hpp"
#include "db/DaqParametersDatabase.hpp"

namespace ntof {
namespace eacs {

DaqConfig::DaqConfig(const std::string &hostname, int32_t mChassisId) :
    m_loadingState(0),
    m_isUsed(false),
    m_hostname(hostname),
    m_chassisId(mChassisId)
{
    m_zspConfig.reset(new DaqZSPConfig(m_hostname));
    m_zspConfig->warningSignal.connect(
        [this](WarningCode error, const std::string &message) {
            this->onLoadWarning(error, message);
        });
    m_zspConfig->errorSignal.connect(errorSignal);
    m_zspConfig->configChangedSignal.connect(
        [this](const DaqZSPConfig &, dim::DIMParamListClient &) {
            configChangedSignal(*this);
        });
}

DaqConfig::~DaqConfig()
{
    DISABLE_AST; // lock dim
    m_cards.clear();
    m_listDaqClient.reset();
    m_zspConfig.reset();
    ENABLE_AST;
}

void DaqConfig::load()
{
    DaqUsedWarning::init(warningSignal);
    DaqCardMap cardMap;
    std::unique_lock<std::mutex> lock(m_mutex);
    m_cards.swap(cardMap); // destroy services
    m_loadingState = 0;

    lock.unlock();
    /*
    lock must be released when destroying cards
    it requests the DIM lock and may deadlock otherwise
     */
    cardMap.clear();
    onLoadWarning(WARN_DAQCONF_LOADING,
                  "Loading"); /* zsp may resolve before list daqs */
    m_zspConfig->load();
    lock.lock();

    // Load info from ListDaqElements service
    m_listDaqClient.reset(new dim::DIMXMLInfo());
    m_listDaqClient->dataSignal.connect(
        [this](pugi::xml_document &doc, dim::DIMXMLInfo & /*info*/) {
            this->processListDaqElements(doc);
        });

    // Error signal called in case of timeout (to be checked)
    m_listDaqClient->errorSignal.connect(
        [this](const std::string &message, dim::DIMXMLInfo & /*client*/) {
            std::ostringstream oss;
            oss << "[" << m_hostname
                << "]: timeout retrieving ListDaqElements : " << message;
            LOG(ERROR) << "[DaqConfig] " << oss.str();
            errorSignal(ERR_NO_DAQ, oss.str());
        });

    // Make the call now!
    // -5 is used as single shot request with timeout of 5 seconds
    m_listDaqClient->subscribe(m_hostname + "/ListDaqElements", -5);
}

bool DaqConfig::isLoading() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_loadingState > 0;
}

void DaqConfig::processListDaqElements(pugi::xml_document &doc)
{
    pugi::xml_node node = doc.child("listDaqElements");

    DaqCardMap cardMap;
    uint32_t cardNumber = 0;
    for (pugi::xml_node card = node.first_child(); card;
         card = card.next_sibling())
    {
        std::string nodeName(card.name());
        std::transform(nodeName.begin(), nodeName.end(), nodeName.begin(),
                       ::toupper);
        if (nodeName.compare(0, 4, "CARD") == 0)
        {
            DaqCard *cardInfo = new DaqCard();
            cardInfo->cardNumber = cardNumber++;
            cardInfo->nbChannel = card.attribute("nbChannel").as_int(0);
            cardInfo->type = card.attribute("type").as_string("");
            cardInfo->serialNumber = card.attribute("serialNumber").as_string("");

            // Init channels
            for (uint32_t channel = 0; channel < cardInfo->nbChannel; channel++)
            {
                DaqChannelConfigPtr chanConfig(
                    new DaqChannelConfig(m_hostname, cardInfo->serialNumber,
                                         cardInfo->cardNumber, channel));
                chanConfig->warningSignal.connect(
                    [this](WarningCode error, const std::string &message) {
                        this->onLoadWarning(error, message);
                    });

                // Listen on ChannelConfig Live value in order to monitor
                // enabled channels
                chanConfig->currentEnabledSignal.connect(
                    [this](const DaqChannelConfig &channelConfig,
                           bool isEnabled) {
                        this->onChannelEnabledChange(channelConfig, isEnabled);
                    });
                chanConfig->configChangedSignal.connect(
                    [this](const DaqChannelConfig &, dim::DIMParamListClient &) {
                        configChangedSignal(*this);
                    });

                chanConfig->load();
                cardInfo->channels.push_back(std::move(chanConfig));
            }
            cardMap.insert(std::make_pair(cardInfo->cardNumber,
                                          DaqCard::Shared(cardInfo)));
        }
    }
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_cards.swap(cardMap);
    }
    // remove general warning
    onLoadWarning(WARN_DAQCONF_LOADING, std::string());
    cardUpdateSignal();
}

void DaqConfig::onLoadWarning(WarningCode error, const std::string &message)
{
    if (error != WARN_DAQCONF_LOADING)
    {
        warningSignal(error, message);
        return;
    }

    uint8_t loadingState;
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        loadingState = (message.empty()) ? --m_loadingState : ++m_loadingState;
    }
    error = static_cast<WarningCode>(WARN_DAQCONF_LOADING + m_chassisId);
    if (!message.empty())
    {
        if (loadingState == 1)
        {
            std::ostringstream oss;
            oss << "[" << m_hostname << "]: loading DAQ configuration";
            warningSignal(error, oss.str());
        }
    }
    else if (loadingState == 0)
    {
        LOG(INFO) << "[DaqConfig] [" << m_hostname << "]: configuration synced";
        warningSignal(error, std::string());
        loadedSignal();
    }
}

void DaqConfig::onChannelEnabledChange(const DaqChannelConfig &config,
                                       bool enabled)
{
    // we want to update Daq/List every time there is a
    // change on enabled live values
    bool modified = false;
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        DaqCard::Shared card(m_cards.at(config.getCardNumber()));

        if (enabled && card->isUsed != enabled)
        {
            card->isUsed = enabled;
            modified = true;
            m_isUsed = true;
        }
        else
        {
            bool used = std::any_of(card->channels.begin(), card->channels.end(),
                                    [](const DaqChannelConfigPtr &c) {
                                        return c->isCurrentEnabled();
                                    });

            if (used != card->isUsed)
            {
                card->isUsed = used;
                modified = true;

                m_isUsed = used ||
                    std::any_of(m_cards.begin(), m_cards.end(),
                                [](const DaqCardMap::value_type &it) {
                                    return it.second->isUsed;
                                });
            }
        }
    }
    if (modified)
    {
        cardUpdateSignal();
    }
}

int32_t DaqConfig::getChassisId() const
{
    return m_chassisId;
}

bool DaqConfig::isUsed() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_isUsed;
}

void DaqConfig::updateDimData(dim::DIMData &daq)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    // Update Cards
    dim::DIMData::List &cards =
        daq.getNestedValue().at(DaqConfig::Params::CARDS).getNestedValue();
    cards.clear();
    bool anyCardUsed = false;
    for (const auto &daqCardPair : m_cards)
    {
        const CardNumber cardNumber = daqCardPair.first;
        const DaqCard::Shared &daqCard = daqCardPair.second;

        dim::DIMData::List card;
        card.emplace_back(DaqCard::Params::CHANNELS_COUNT, "channelsCount", "",
                          daqCard->nbChannel);
        card.emplace_back(DaqCard::Params::TYPE, "type", "", daqCard->type);
        card.emplace_back(DaqCard::Params::SERIAL_NUMBER, "serialNumber", "",
                          daqCard->serialNumber);
        card.emplace_back(DaqCard::Params::USED, "used", "", daqCard->isUsed);
        card[DaqCard::Params::USED].setHidden(!daqCard->isUsed);
        anyCardUsed |= daqCard->isUsed;
        cards.emplace_back(cardNumber, "card", "", card);
    }
    daq.getNestedValue().at(DaqConfig::Params::CARDS).setHidden(false);

    // Update Used
    daq.getNestedValue().at(DaqConfig::Params::USED).setValue(anyCardUsed);
    daq.getNestedValue().at(DaqConfig::Params::USED).setHidden(!anyCardUsed);
}

void DaqConfig::sendParameters()
{
    try
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        /* must not hold a lock whilst sending dim messages */
        DaqZSPConfig::Shared zsp(m_zspConfig);
        DaqCardMap cards(m_cards);
        lock.unlock();

        // Send ZSP parameters
        dim::DIMAck ack = zsp->sendParameters();
        if (ack.getStatus() != dim::DIMAck::OK)
        {
            std::ostringstream ss;
            ss << "ZSP sendParameters cmd refused ( " << ack.getErrorCode()
               << " ): " << ack.getMessage();
            throw dim::DIMException(ss.str(), __LINE__, -1);
        }

        // Send All channels parameters
        for (DaqCardMap::value_type &card : cards)
        {
            card.second->sendParameters();
        }
    }
    catch (dim::DIMException &e)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: failed to sendParameters: " << e.what();
        LOG(ERROR) << oss.str();
        errorSignal(ERR_DAQCONF_CMD, oss.str());
    }
}

void DaqConfig::updateDatabase(RunNumber runNumber)
{
    try
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        /* must not hold a lock whilst sending dim messages */
        DaqZSPConfig::Shared zsp(m_zspConfig);
        DaqCardMap cards(m_cards);
        lock.unlock();

        LOG(INFO)
            << "[DaqConfig] [" << m_hostname << "]: preparing database update";
        std::map<std::string, DaqParametersDatabase> params;
        for (DaqCardMap::value_type &cardIt : cards)
        {
            const DaqCard &card = *(cardIt.second);
            for (const DaqChannelConfigPtr &chan : card.channels)
            {
                const std::string chanId = chan->getCardSn() + ":" +
                    std::to_string(chan->getChannelNumber());
                params[chanId].load(*chan);
            }
        }

        // build zsp relations
        for (const ZSP::Master &master : zsp->GetMasterList())
        {
            const std::string masterId = master.masterSn + ":" +
                std::to_string(master.channel);
            for (const ZSP::Slave &slave : master.slaveList)
            {
                const std::string slaveId = slave.sn + ":" +
                    std::to_string(slave.index);
                DaqParametersDatabase &slaveParams = params[slaveId];
                DaqParametersDatabase &masterParams = params[masterId];
                slaveParams.values()[DaqParametersDatabase::masterDetectorId] =
                    masterParams.values()[DaqParametersDatabase::detectorId];
                slaveParams.values()[DaqParametersDatabase::masterDetectorType] =
                    masterParams.values()[DaqParametersDatabase::detectorType];
            }
        }

        Database &db = Database::instance();
        for (DaqCardMap::value_type &cardIt : cards)
        {
            const DaqCard &card = *(cardIt.second);
            for (const DaqChannelConfigPtr &chan : card.channels)
            {
                const std::string chanId = chan->getCardSn() + ":" +
                    std::to_string(chan->getChannelNumber());
                LOG(INFO) << "[DaqConfig] [" << m_hostname
                          << "]: commit chanId:" << chanId;
                params[chanId].values()[DaqParametersDatabase::cardSerialNumber] =
                    card.serialNumber;
                db.insertDaqConfig(runNumber,
                                   ChannelConfig::makeLocation(
                                       1, m_chassisId, chan->getCardNumber(),
                                       chan->getChannelNumber()),
                                   params[chanId]);
            }
        }
        LOG(INFO) << "[DaqConfig] [" << m_hostname << "]: database updated";
    }
    catch (EACSException &e)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: failed to updateDatabase: " << e.what();
        LOG(ERROR) << "[DaqConfig] " << oss.str();
        errorSignal(ErrorCode(e.getErrorCode()), oss.str());
    }
}

void DaqConfig::genHeaders(RunControlHeader &rctr, ModuleHeader &modh) const
{
    if (!isUsed())
        return;

    try
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        /* must not hold a lock whilst sending dim messages */
        DaqZSPConfig::Shared zsp(m_zspConfig);
        DaqCardMap cards(m_cards);
        lock.unlock();

        std::map<std::string, ChannelConfig> configs;
        for (DaqCardMap::value_type &cardIt : cards)
        {
            const DaqCard &card = *(cardIt.second);

            if (!card.isUsed)
                continue;

            uint32_t chanUsed = 0;
            ++rctr.totalNumberOfModules;
            for (const DaqChannelConfigPtr &chan : card.channels)
            {
                if (!chan->isCurrentEnabled())
                    continue;

                ++chanUsed;
                ChannelConfig &config =
                    configs[chan->getCardSn() + ":" +
                            std::to_string(chan->getChannelNumber())];

                chan->getChannelConfig(config);
                config.setStream(1);
                config.setLocation(ChannelConfig::makeLocation(
                    1, m_chassisId, chan->getCardNumber(),
                    chan->getChannelNumber()));
            }
            rctr.numberOfChannelsPerModule.push_back(chanUsed);
        }

        for (const ZSP::Master &master : zsp->GetMasterList())
        {
            const std::string masterId = master.masterSn + ":" +
                std::to_string(master.channel);
            if (configs.count(masterId) == 0)
                continue; // ignore offline masters

            for (const ZSP::Slave &slave : master.slaveList)
            {
                const std::string slaveId = slave.sn + ":" +
                    std::to_string(slave.index);
                if (configs.count(slaveId) == 0)
                    continue; // ignore offline slaves

                ChannelConfig &config = configs[slaveId];
                config.masterDetectorId = configs[masterId].detectorId;
                config.masterDetectorType = configs[masterId].detectorType;
            }
        }

        ++rctr.totalNumberOfChassis;
        for (auto &it : configs)
        {
            modh.channelsConfig.push_back(it.second);
        }
    }
    catch (EACSException &e)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: failed to generate MODH: " << e.what();
        LOG(ERROR) << "[DaqConfig] " << oss.str();
        errorSignal(ErrorCode(e.getErrorCode()), oss.str());
    }
}

} /* namespace eacs */
} /* namespace ntof */
