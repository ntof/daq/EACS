/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-14T11:09:33+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef DAQUSEDWARNING_HPP__
#define DAQUSEDWARNING_HPP__

#include <atomic>

#include "misc.h"

namespace ntof {
namespace eacs {

/**
 * @brief convenience RAII class to raise a warning when no daqs are used
 */
class DaqUsedWarning
{
public:
    explicit DaqUsedWarning(const WarningSignal &signal);
    ~DaqUsedWarning();

    static void init(const WarningSignal &signal);

protected:
    const WarningSignal &m_signal;
    static std::atomic_int s_counter;
};

} // namespace eacs
} // namespace ntof

#endif
