//
// Created by matteof on 8/5/20.
//

#ifndef EACS_DAQZSPCONFIG_HPP
#define EACS_DAQZSPCONFIG_HPP

#include <memory>

#include <Signals.hpp>
#include <proxy/DIMParamListProxy.hpp>

#include "misc.h"

namespace ntof {
namespace eacs {

namespace ZSP {

struct Slave
{
    std::string sn;
    int cardIndex;
    int index;
};

struct Master
{
    int id;
    std::string masterSn;
    int cardIndex;
    int channel;
    std::vector<Slave> slaveList;
};

enum Mode : int
{
    INDEPENDENT = 0,
    SINGLE_MASTER = 1,
    MULTIPLE_MASTER = 2
};

} // namespace ZSP

class DaqZSPConfig :
    protected dim::DIMParamListProxy,
    protected dim::DIMParamListHandler
{
public:
    typedef std::shared_ptr<DaqZSPConfig> Shared;
    typedef std::vector<ZSP::Master> MasterList;
    typedef ntof::utils::signal<void(const DaqZSPConfig &,
                                     dim::DIMParamListClient &)>
        ConfigChangedSignal;

    enum Params
    {
        MODE = 1,
        CONFIGURATION
    };

    enum ChannelParams
    {
        SN = 0,
        CHANNEL
    };

    explicit DaqZSPConfig(const std::string &hostname);
    ~DaqZSPConfig();

    int parameterChanged(dim::DIMData::List &settingsChanged,
                         const dim::DIMParamList &list,
                         int &errCode,
                         std::string &errMsg) override;

    void load();

    ZSP::Mode GetMode() const;
    MasterList GetMasterList() const;

    using dim::DIMParamListProxy::sendParameters;

    ErrorSignal errorSignal;
    WarningSignal warningSignal;
    ConfigChangedSignal configChangedSignal;

protected:
    ZSP::Master getMaster(dim::DIMData &master) const;
    void onSync();

    std::string m_hostname; //!< Hostname
    mutable std::mutex m_mutex;
    ZSP::Mode m_zsMode;
    MasterList m_masterList;
};

} /* namespace eacs */
} /* namespace ntof */

#endif // EACS_DAQZSPCONFIG_HPP
