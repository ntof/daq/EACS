//
// Created by matteof on 8/6/20.
//

#include "DaqCard.hpp"

namespace ntof {
namespace eacs {

DaqCard::DaqCard() : nbChannel(0), cardNumber(0), isUsed(false) {}

void DaqCard::sendParameters()
{
    for (DaqChannelConfigPtr &channel : channels)
    {
        try
        {
            dim::DIMAck ack = channel->sendParameters();
            if (ack.getStatus() != dim::DIMAck::OK)
            {
                std::ostringstream ss;
                ss << "Channel sendParameters cmd refused ( "
                   << ack.getErrorCode() << " ): " << ack.getMessage();
                throw dim::DIMException(ss.str(), __LINE__, -1);
            }
        }
        catch (dim::DIMException &e)
        {
            std::ostringstream ss;
            ss << "[DaqCard] CardNumber ( " << cardNumber << " ) "
               << "ChannelNumber ( " << channel->getChannelNumber()
               << " ): " << e.getMessage();
            throw dim::DIMException(ss.str(), __LINE__, e.getErrorCode());
        }
    }
}

} /* namespace eacs */
} /* namespace ntof */