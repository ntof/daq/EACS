/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-10-14T11:11:09+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "DaqUsedWarning.hpp"

namespace ntof {
namespace eacs {

std::atomic_int DaqUsedWarning::s_counter(0);

DaqUsedWarning::DaqUsedWarning(const WarningSignal &signal) : m_signal(signal)
{
    ++s_counter;
    m_signal(WARN_NO_DAQS, std::string());
}

DaqUsedWarning::~DaqUsedWarning()
{
    if (--s_counter == 0)
        m_signal(WARN_NO_DAQS, "[Daq]: No DAQs configured");
}

void DaqUsedWarning::init(const WarningSignal &signal)
{
    if (s_counter.load() == 0)
        signal(WARN_NO_DAQS, "[Daq]: No DAQs configured");
}

} // namespace eacs
} // namespace ntof