//
// Created by matteof on 7/22/20.
//

#include "DaqChannelConfig.hpp"

#include <string>

#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "EACSException.hpp"
#include "misc.h"

namespace ntof {
namespace eacs {

DaqChannelConfig::DaqChannelConfig(const std::string &hostname,
                                   const std::string &cardSn,
                                   uint32_t card,
                                   uint32_t channel) :
    DIMParamListProxy(Config::instance().getDimPrefix() + "/Daq/" + hostname +
                          "/CARD" + std::to_string(card) + "/CHANNEL" +
                          std::to_string(channel),
                      hostname + "/CARD" + std::to_string(card) + "/CHANNEL" +
                          std::to_string(channel)),
    m_hostname(hostname),
    m_cardSn(cardSn),
    m_cardNumber(card),
    m_channelNumber(channel),
    m_isCurrentEnabled(false)
{
    setReadOnly(Params::MODULE_TYPE);
    setReadOnly(Params::CHANNEL_ID);
    setReadOnly(Params::MODULE_ID);
    setReadOnly(Params::CHASSIS_ID);
    setReadOnly(Params::STREAM_ID);

    setNoUpdatesOnSync(true);

    /* syncing is always singleShot */
    syncSignal.connect([this](dim::DIMParamListProxy &) { this->onSync(); });

    m_client.reset(new dim::DIMParamListClient(m_remoteService));
    /* live data from the client in order to track if card is enabled */
    m_client->dataSignal.connect([this](dim::DIMParamListClient &client) {
        this->checkEnabled(client);
        this->configChangedSignal(*this, client);
    });
    setHandler(this);

    m_calibration.reset(
        new dim::DIMDataSetClient(m_remoteService + "/Calibration"));
}

DaqChannelConfig::~DaqChannelConfig()
{
    DISABLE_AST; // disable dim
    m_client.reset();
    m_calibration.reset();
    ENABLE_AST;
}

void DaqChannelConfig::load()
{
    warningSignal(WARN_DAQCONF_LOADING, "Loading Cards Channels configuration");
    setSyncing(true);
}

void DaqChannelConfig::checkEnabled(dim::DIMParamListClient &client)
{
    // TODO implement template functions to access parameter on ntofutils
    std::vector<dim::DIMData> params = client.getParameters();
    try
    {
        bool isEnabled = params.at(DaqChannelConfig::Params::IS_CHANNEL_ENABLED)
                             .getValue<bool>();
        if (isEnabled != m_isCurrentEnabled)
        {
            m_isCurrentEnabled = isEnabled;
            currentEnabledSignal(*this, isEnabled);
        }
    }
    catch (const std::out_of_range &e)
    {
        // Maybe temp or not complete. Ignore this live signal.
        LOG(WARNING) << "[DaqChannelConfig] Received not complete data "
                        "Signal for hostname: "
                     << m_hostname << " card: " << m_cardNumber
                     << " channel: " << m_channelNumber;
    }
}

void DaqChannelConfig::onSync()
{
    if (getParameterCount() > 1)
    {
        this->setSyncing(false);
        warningSignal(WARN_DAQCONF_LOADING, std::string());
        setNextUsed(
            lockParameterAt(Params::IS_CHANNEL_ENABLED)->getValue<bool>());
    }
}

void DaqChannelConfig::setNextUsed(bool enabled)
{
    if (!enabled)
        m_nextUsed.reset();
    else if (!m_nextUsed)
        m_nextUsed.reset(new DaqUsedWarning(warningSignal));
}

int DaqChannelConfig::parameterChanged(dim::DIMData::List &settingsChanged,
                                       const DIMParamList &,
                                       int &,
                                       std::string &)
{
    for (const dim::DIMData &data : settingsChanged)
    {
        if (data.getIndex() == Params::IS_CHANNEL_ENABLED)
            setNextUsed(data.getValue<bool>());
    }
    return 0;
}

void DaqChannelConfig::getChannelConfig(ChannelConfig &config) const
{
    dim::DIMParamListClient *cli = client();
    if (!cli)
        EACS_THROW("Failed to get client from DaqChannelConfig", ERR_INTERNAL);

    // FIXME: calibrated full-scale +offset and threshold
    const dim::DIMData::List dataList = cli->getParameters();
    for (const dim::DIMData &data : dataList)
    {
        switch (Params(data.getIndex()))
        {
        case DaqChannelConfig::DETECTOR_TYPE:
            config.setDetectorType(data.getStringValue());
            break;
        case DaqChannelConfig::DETECTOR_ID:
            config.detectorId = data.getValue<uint32_t>();
            break;
        case DaqChannelConfig::MODULE_TYPE:
            config.setModuleType(data.getStringValue());
            break;
        case DaqChannelConfig::INPUT_IMPEDANCE:
        case DaqChannelConfig::IS_CHANNEL_ENABLED:
        case DaqChannelConfig::CHANNEL_ID:
        case DaqChannelConfig::MODULE_ID:
        case DaqChannelConfig::CHASSIS_ID:
        case DaqChannelConfig::STREAM_ID: break;
        case DaqChannelConfig::SAMPLE_RATE:
            config.sampleRate = data.getValue<float>();
            break;
        case DaqChannelConfig::SAMPLE_SIZE:
            config.sampleSize = data.getValue<uint32_t>();
            break;
        case DaqChannelConfig::FULL_SCALE:
            // just in case calibrated values are not available
            config.fullScale = data.getValue<float>();
            break;
        case DaqChannelConfig::THRESHOLD:
            // just in case calibrated values are not available
            config.threshold = data.getValue<float>();
            break;
        case DaqChannelConfig::OFFSET:
            // just in case calibrated values are not available
            config.offset = data.getValue<float>();
            break;
        case DaqChannelConfig::DELAY_TIME:
            config.delayTime = data.getValue<int32_t>();
            break;
        case DaqChannelConfig::THRESHOLD_SIGN:
            config.thresholdSign = data.getValue<int32_t>();
            break;
        case DaqChannelConfig::ZSP_START:
            config.zeroSuppressionStart = data.getValue<uint32_t>();
            break;
        case DaqChannelConfig::PRE_SAMPLES:
            config.preSample = data.getValue<uint32_t>();
            break;
        case DaqChannelConfig::POST_SAMPLES:
            config.postSample = data.getValue<uint32_t>();
            break;
        case DaqChannelConfig::CLOCK_STATE:
            config.setClockState(data.getStringValue());
            break;
        }
    }

    if (!m_calibration)
        EACS_THROW("Failed to get calibration from DaqChannelConfig",
                   ERR_INTERNAL);

    const dim::DIMData::List calibDataList = m_calibration->getLatestData();
    for (const dim::DIMData &data : calibDataList)
    {
        switch (CalibrationParams(data.getIndex()))
        {
        case CalibrationParams::CALIBRATED_FULL_SCALE:
            config.fullScale = data.getValue<float>();
            break;
        case CalibrationParams::CALIBRATED_OFFSET:
            config.offset = data.getValue<float>();
            break;
        case CalibrationParams::CALIBRATED_THRESHOLD:
            config.threshold = data.getValue<float>();
            break;
        }
    }
}

} /* namespace eacs */
} /* namespace ntof */