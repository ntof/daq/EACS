/*
 * WroteNumberClient.cpp
 *
 * Created on: Nov 20, 2014
 * Author: agiraud
 */

#include "ClientDaqWriter.hpp"

#include <iostream>

#include <easylogging++.h>

namespace ntof {
namespace eacs {

ClientDaqWriter::ClientDaqWriter(const std::string &hostname) :
    m_info(hostname + "/WRITER/RunExtensionNumber", this),
    m_hostname(hostname),
    m_firstCall(true)
{}

ClientDaqWriter::~ClientDaqWriter()
{
    m_info.unsubscribe();
}

/**
 * @FIXME: can't turn errMsg into a const ref, overloaded function must match
 * the one from ntofutils
 */
void ClientDaqWriter::errorReceived(std::string errMsg,
                                    const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[ClientDaqWriter] error on " << m_hostname
               << "/WRITER/RunExtensionNumber: " << errMsg;
}

void ClientDaqWriter::dataReceived(pugi::xml_document &doc,
                                   const ntof::dim::DIMXMLInfo * /*info*/)
{
    // Parse the results
    int32_t extensionNumber =
        doc.child("RunExtensionNumberWrote").attribute("event").as_int(-1);
    int32_t runNumber =
        doc.child("RunExtensionNumberWrote").attribute("run").as_int(-1);
    uint64_t filesize =
        doc.child("RunExtensionNumberWrote").attribute("size").as_ullong(0);

    std::lock_guard<std::mutex> lock(m_lock);
    // The first initialisation is ignored
    if (m_firstCall || extensionNumber == -1 || runNumber == -1)
    {
        m_firstCall = false;
        return;
    }

    // Check if the data can be accepted or not
    if (getLastExtensionNumber(runNumber) < extensionNumber)
    {
        WroteNumber wroteNumber = {extensionNumber, runNumber, filesize};
        m_events[runNumber][extensionNumber] = std::move(wroteNumber);

        LOG(INFO)
            << "[ClientDaqWriter] event from " << m_hostname << " :"
            << " runNumber:" << runNumber << " eventNumber:" << extensionNumber;
    }
    else
    {
        LOG(ERROR)
            << "[ClientDaqWriter] rejecting event from " << m_hostname << " :"
            << " runNumber:" << runNumber << " eventNumber:" << extensionNumber;
    }
}

int32_t ClientDaqWriter::getLastExtensionNumber(RunNumber runNumber)
{
    if (m_events.count(runNumber) == 0)
        return -1;
    EventsMap::const_reverse_iterator it = m_events[runNumber].rbegin();
    if (it != m_events[runNumber].rend())
        return it->second.extensionNumber;
    else
        return -1;
}

void ClientDaqWriter::clearEvents()
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_events.clear();
}

void ClientDaqWriter::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    LOG(ERROR) << "[ClientDaqWriter] no link on: " << m_hostname
               << "/WRITER/RunExtensionNumber";
}

bool ClientDaqWriter::isWritten(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_lock);
    RunEventsMap::const_iterator reIt = m_events.find(runNumber);
    if (reIt != m_events.end())
    {
        return reIt->second.count(eventNumber) > 0;
    }
    return false;
}

uint64_t ClientDaqWriter::getSize(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_lock);
    RunEventsMap::const_iterator reIt = m_events.find(runNumber);
    if (reIt != m_events.end())
    {
        EventsMap::const_iterator eIt = reIt->second.find(eventNumber);
        return (eIt != reIt->second.end()) ? eIt->second.fileSize : 0;
    }
    return 0;
}

void ClientDaqWriter::removeWritten(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_lock);
    RunEventsMap::iterator reIt = m_events.find(runNumber);
    if (reIt != m_events.end())
    {
        reIt->second.erase(eventNumber);
    }
}

} /* namespace eacs */
} /* namespace ntof */
