/*
 * RemoteDaq.cpp
 *
 * Created on: Aug 4, 2015
 * Author: agiraud
 */

#include "Daq.hpp"

#include <algorithm>
#include <string>

#include <DIMException.h>
#include <DIMSuperClient.h>
#include <easylogging++.h>

#include "ClientDaqWriter.hpp"
#include "DaqConfig.hpp"
#include "EACSException.hpp"

namespace ntof {
namespace eacs {

static const std::map<Daq::DaqState, std::string> s_daqStateMap{
    {Daq::NOT_READY, "NOT_READY"},
    {Daq::ERROR, "ERROR"},
    {Daq::DETECTING_HARDWARE, "DETECTING_HARDWARE"},
    {Daq::NOT_CONFIGURED, "NOT_CONFIGURED"},
    {Daq::CALIBRATING_CARDS, "CALIBRATING_CARDS"},
    {Daq::WAITING_FOR_CMD, "WAITING_FOR_CMD"},
    {Daq::PROCESSING_CMD, "PROCESSING_CMD"},
    {Daq::STOPPING_ACQ, "STOPPING_ACQ"},
    {Daq::STARTING_ACQ, "STARTING_ACQ"},
    {Daq::RESETING_DAQ, "RESETING_DAQ"},
    {Daq::INITIALIZING_ACQ, "INITIALIZING_ACQ"},
    {Daq::WAITING_FOR_START_CMD, "WAITING_FOR_START_CMD"},
    {Daq::SUPER_USER, "SUPER_USER"}};
static const std::map<Daq::Mode, std::string> s_daqModeMap{
    {Daq::Mode::IDLE, "IDLE"},
    {Daq::Mode::CONFIG, "CONFIG"},
    {Daq::Mode::MONITOR, "MONITOR"}};

Daq::Daq(const std::string &hostname, int32_t chassisId) :
    m_worker(hostname, 1, 128),
    m_chassisId(chassisId),
    m_hostname(hostname),
    m_daqState(new dim::DIMStateClient(hostname + "/DaqState")),
    m_aqnState(new dim::DIMStateClient(hostname + "/AcquisitionState")),
    m_writerState(new dim::DIMStateClient(hostname + "/WriterState")),
    m_wroteNumber(new ClientDaqWriter(hostname)),
    m_command(m_hostname + "/Daq/Command"),
    m_config(new DaqConfig(hostname, chassisId)),
    m_runNumber(-1),
    m_mode(Mode::IDLE)
{
    m_config->errorSignal.connect(errorSignal);
    m_config->warningSignal.connect(warningSignal);
    m_config->configChangedSignal.connect(
        [this](const DaqConfig &) { post([this]() { onConfigChanged(); }); });

    m_daqState->errorSignal.connect([this](const std::string &message,
                                           dim::DIMStateClient & /*client*/) {
        if (message == dim::DIMStateClient::NoLinkError)
        {
            std::ostringstream oss;
            oss << "[" << m_hostname << "]: no link";
            LOG(ERROR) << "[Daq]: " << oss.str();
            errorSignal(static_cast<ErrorCode>(ERR_DAQ_NO_LINK + m_chassisId),
                        oss.str());
        }
    });
    // TODO Listen on data signals and drive updates
    m_daqState->dataSignal.connect([this](dim::DIMStateClient &) {
        errorSignal(static_cast<ErrorCode>(ERR_DAQ_NO_LINK + m_chassisId),
                    std::string());
        post([this]() {
            onDaqStateChanged(
                static_cast<DaqState>(m_daqState->getActualState()));
        });
    });
}

std::string Daq::toString(DaqState state)
{
    std::map<DaqState, std::string>::const_iterator it = s_daqStateMap.find(
        state);

    if (it == s_daqStateMap.end())
        return "UNKNOWN(" + std::to_string(state) + ")";
    else
        return it->second;
}

std::string Daq::toString(Mode state)
{
    std::map<Mode, std::string>::const_iterator it = s_daqModeMap.find(state);

    if (it == s_daqModeMap.end())
        return "UNKNOWN(" + std::to_string(int(state)) + ")";
    else
        return it->second;
}

Daq::~Daq()
{
    m_worker.stop();
    DISABLE_AST; // disable dim
    m_daqState.reset();
    m_aqnState.reset();
    m_writerState.reset();
    m_wroteNumber.reset();
    m_config.reset();
    ENABLE_AST;
}

void Daq::clear()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_wroteNumber->clearEvents();
}

void Daq::setMode(Mode mode)
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_mode = mode;
    }
    LOG(DEBUG)
        << "[Daq] " << m_hostname << " entering mode: " << toString(mode);
    modeSignal(*this, mode);
}

Daq::Mode Daq::getMode() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_mode;
}

bool Daq::isUsed() const
{
    return m_config->isUsed();
}

std::string Daq::getErrors()
{
    std::ostringstream res("");
    // Get the error message for each state in error
    if (m_daqState->getActualState() == GlobalState::ERROR)
    {
        res << m_hostname << " : DAQ STATE is in error" << std::endl;
    }
    if (m_aqnState->getActualState() == GlobalState::ERROR)
    {
        res << m_hostname << " : AQN STATE is in error" << std::endl;
    }
    if (m_writerState->getActualState() == GlobalState::ERROR)
    {
        res << m_hostname << " : WRITER STATE is in error" << std::endl;
    }
    return res.str();
}

std::string Daq::getStates()
{
    std::ostringstream res("");
    // Get each status as string
    res << m_hostname << " : " << std::endl;
    res << "DAQ STATE is " << m_daqState->getActualStateAsString() << std::endl;
    res << "AQN STATE is " << m_aqnState->getActualStateAsString() << std::endl;
    res << "WRITER STATE is " << m_writerState->getActualStateAsString()
        << std::endl;
    return res.str();
}

bool Daq::sendCommand(const std::string &command)
{
    // Initiate the client
    LOG(INFO) << "[Daq] sending cmd '" << command << "' to " << m_hostname;

    try
    {
        // Build XML data to be sent
        pugi::xml_document doc;
        pugi::xml_node root = doc.append_child("command");
        pugi::xml_node data = root.append_child("command");
        data.append_child(pugi::node_pcdata).set_value(command.c_str());

        // Send the command
        m_command.setTimeOut(std::chrono::milliseconds(60000));
        ntof::dim::DIMAck ack = m_command.sendCommand(doc);
        if (ack.getErrorCode() < 0)
        {
            std::ostringstream oss;
            oss << "[" << m_hostname << "]: Failed to send command '" << command
                << "' to daq " << m_hostname << ": " << ack.getMessage();
            LOG(ERROR) << "[Daq] " << oss.str();
            errorSignal(ERR_DAQCONF_CMD, oss.str());
            return false;
        }
        return true;
    }
    catch (const dim::DIMException &e)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: Failed to send command '" << command
            << "' to daq " << m_hostname << ": " << e.getMessage();
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQCONF_CMD, oss.str());
        return false;
    }
}

bool Daq::isWritten(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_wroteNumber->isWritten(runNumber, eventNumber);
}

uint64_t Daq::getSize(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_wroteNumber->getSize(runNumber, eventNumber);
}

void Daq::removeWritten(RunNumber runNumber, EventNumber eventNumber)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_wroteNumber->removeWritten(runNumber, eventNumber);
}

bool Daq::configure(ntof::eacs::RunNumber runNumber)
{
    if (getMode() != Mode::IDLE)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: not ready to be configured";
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQCONF_CMD, oss.str());
        return false;
    }

    setMode(Mode::CONFIG);
    m_runNumber = runNumber;

    post([this]() {
        DaqState state = static_cast<DaqState>(m_daqState->getActualState());

        if (state == DaqState::DETECTING_HARDWARE)
        {
            // just wait
        }
        else if (state != DaqState::NOT_CONFIGURED)
        {
            sendCommand("reset");
        }
        else
        {
            m_config->sendParameters();
        }
    });
    return true;
}

bool Daq::startAcquisition()
{
    DaqState state = static_cast<DaqState>(m_daqState->getActualState());

    clear();
    if (state != DaqState::WAITING_FOR_START_CMD)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname
            << "]: not ready to start (state: " << toString(state) << ")";
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQCONF_CMD, oss.str());
        return false;
    }
    return sendCommand("start");
}

bool Daq::reset()
{
    setMode(Mode::IDLE);

    DaqState state = static_cast<DaqState>(m_daqState->getActualState());
    if (state != DaqState::NOT_CONFIGURED &&
        state != DaqState::DETECTING_HARDWARE)
    {
        return sendCommand("reset");
    }
    return true;
}

bool Daq::stop(bool canReset)
{
    DaqState state = static_cast<DaqState>(m_daqState->getActualState());
    bool isReset = canReset ? (state != DaqState::WAITING_FOR_CMD) : false;

    if (!isReset && state != DaqState::WAITING_FOR_CMD)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname
            << "]: not ready to stop (state: " << toString(state) << ")";
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQCONF_CMD, oss.str());
        return false;
    }

    setMode(Mode::IDLE);
    if (isReset)
        return sendCommand("reset");
    else
        return sendCommand("stop");
}

void Daq::post(const std::function<void()> &fun)
{
    try
    {
        m_worker.post(fun);
    }
    catch (NTOFException &ex)
    {
        LOG(ERROR) << "[Daq] failed to queue task: " << ex.getMessage();
    }
}

void Daq::onDaqStateChanged(DaqState state)
{
    if (state == ERROR)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname << "]: error";
        dim::DIMStateClient::ErrWarnVector errors = m_daqState->getActiveErrors();

        if (!errors.empty())
            oss << " : " << errors.back().getMessage();
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQ_STATE, oss.str());
    }
    if (getMode() == Mode::CONFIG)
    {
        switch (state)
        {
        case DaqState::NOT_CONFIGURED: m_config->sendParameters(); break;
        case DaqState::WAITING_FOR_CMD:
            sendCommand("initialization?runnumber=" +
                        std::to_string(m_runNumber));
            break;
        case DaqState::WAITING_FOR_START_CMD: {
            setMode(Mode::MONITOR);
            break;
        }
        default: break;
        }
    }
}

void Daq::onConfigChanged()
{
    if (getMode() == Mode::MONITOR)
    {
        std::ostringstream oss;
        oss << "[" << m_hostname
            << "]: configuration change detected on daq: " << m_hostname;
        LOG(ERROR) << "[Daq] " << oss.str();
        errorSignal(ERR_DAQCONF_CHANGED, oss.str());
    }
}

} /* namespace eacs */
} /* namespace ntof */
