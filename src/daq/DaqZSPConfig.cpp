//
// Created by matteof on 8/5/20.
//

#include "DaqZSPConfig.hpp"

#include <DIMException.h>

#include "Config.hpp"

namespace ntof {
namespace eacs {

/* incoming values can be partial, this is used to rebuild it */
static const dim::DIMData masterInfo(0, "master", "", 0);
static const dim::DIMData snInfo(0, "sn", "", 0);
static const dim::DIMData channelInfo(1, "channel", "", 0);
static const dim::DIMData slaveInfo(0, "slave", "", 0);

DaqZSPConfig::DaqZSPConfig(const std::string &hostname) :
    DIMParamListProxy(Config::instance().getDimPrefix() + "/Daq/" + hostname +
                          "/ZeroSuppression",
                      hostname + "/ZeroSuppression"),
    m_hostname(hostname),
    m_zsMode(ZSP::Mode::INDEPENDENT)
{
    setNoUpdatesOnSync(true);

    /* syncing is always singleShot */
    syncSignal.connect([this](DIMParamListProxy &) { this->onSync(); });

    m_client.reset(new dim::DIMParamListClient(m_remoteService));
    m_client->dataSignal.connect([this](dim::DIMParamListClient &client) {
        this->configChangedSignal(*this, client);
    });
    setHandler(this);
}

DaqZSPConfig::~DaqZSPConfig()
{
    DISABLE_AST; // disable dim
    m_client.reset();
    ENABLE_AST;
}

void DaqZSPConfig::load()
{
    warningSignal(WARN_DAQCONF_LOADING, "Loading Zero Suppression config");
    setSyncing(true);
}

void DaqZSPConfig::onSync()
{
    if (getParameterCount() > 1)
    {
        this->setSyncing(false);
        warningSignal(WARN_DAQCONF_LOADING, std::string());
    }
}

int DaqZSPConfig::parameterChanged(dim::DIMData::List &settingsChanged,
                                   const dim::DIMParamList & /*list*/,
                                   int & /*errCode*/,
                                   std::string & /*errMsg*/)
{
    ZSP::Mode mode = GetMode();
    MasterList masterList;
    dim::DIMData::List configData;

    for (dim::DIMData &data : settingsChanged)
    {
        if (data.getIndex() == Params::MODE)
        {
            mode = static_cast<ZSP::Mode>(
                data.getValue<dim::DIMEnum>().getValue());
        }
        else if (data.getIndex() == Params::CONFIGURATION)
        {
            configData = data.getNestedValue();
            for (dim::DIMData &masterData : configData)
                masterList.push_back(std::move(getMaster(masterData)));
        }
    }

    switch (mode)
    {
    case ZSP::Mode::INDEPENDENT:
        if (!masterList.empty())
            throw dim::DIMException("can't set masters in independent mode",
                                    __LINE__);
        break;
    case ZSP::Mode::SINGLE_MASTER:
        if (masterList.size() != 1)
            throw dim::DIMException(
                "a single master must be set in singlemaster mode", __LINE__);
        else if (!masterList[0].slaveList.empty())
            throw dim::DIMException(
                "slave list must be empty in singlemaster mode", __LINE__);
        break;
    case ZSP::Mode::MULTIPLE_MASTER: break;
    default:
        throw dim::DIMException("invalid mode: " + std::to_string(mode),
                                __LINE__);
    }

    /* directly pre-replace content */
    setValue(Params::CONFIGURATION, std::move(configData), false);

    std::lock_guard<std::mutex> lock(m_mutex);
    m_masterList.swap(masterList);
    m_zsMode = mode;

    return 0;
}

ZSP::Master DaqZSPConfig::getMaster(dim::DIMData &master) const
{
    ZSP::Master ret;
    ret.channel = -1;

    for (dim::DIMData &data : master.getNestedValue())
    {
        if (data.getIndex() == ChannelParams::SN)
        {
            ret.masterSn = data.getValue<std::string>();
            data.copyParameterInfo(snInfo);
        }
        else if (data.getIndex() == ChannelParams::CHANNEL)
        {
            ret.channel = data.getValue<uint32_t>();
            data.copyParameterInfo(channelInfo);
        }
        else
        {
            ZSP::Slave slave;
            slave.index = -1;
            data.copyParameterInfo(slaveInfo);

            for (dim::DIMData &slaveData : data.getNestedValue())
            {
                if (slaveData.getIndex() == 0)
                {
                    slave.sn = slaveData.getValue<std::string>();
                    slaveData.copyParameterInfo(snInfo);
                }
                else if (slaveData.getIndex() == 1)
                {
                    slave.index = slaveData.getValue<uint32_t>();
                    slaveData.copyParameterInfo(channelInfo);
                }
            }

            if (slave.sn.empty() || (slave.index < 0))
                throw dim::DIMException(
                    "invalid channel on master: " + ret.masterSn +
                        std::to_string(slave.index),
                    __LINE__);
            else if (slave.index == ret.channel && slave.sn == ret.masterSn)
                throw dim::DIMException("channel " + ret.masterSn + ":" +
                                            std::to_string(ret.channel) +
                                            " alread set as master",
                                        __LINE__);

            ret.slaveList.push_back(std::move(slave));
        }
    }
    if (ret.masterSn.empty() || ret.channel < 0)
        throw dim::DIMException("invalid master channel: " + ret.masterSn +
                                    ":" + std::to_string(ret.channel),

                                __LINE__);

    master.copyParameterInfo(masterInfo);
    return ret;
}

ZSP::Mode DaqZSPConfig::GetMode() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_zsMode;
}

DaqZSPConfig::MasterList DaqZSPConfig::GetMasterList() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_masterList;
}

} /* namespace eacs */
} /* namespace ntof */
