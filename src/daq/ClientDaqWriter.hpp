/*
 * WroteNumberClient.h
 *
 * Created on: Nov 20, 2014
 * Author: agiraud
 */

#ifndef CLIENTDAQWRITER_H_
#define CLIENTDAQWRITER_H_

#include <list>
#include <mutex>

#include <DIMXMLInfo.h>

#include "EACSTypes.hpp"

namespace ntof {
namespace eacs {
class Eacs;

typedef struct
{
    int32_t extensionNumber; //!< Segment number
    int32_t runNumber;       //!< Run number
    uint64_t fileSize;       //!< Size of the file in bytes
} WroteNumber;

class ClientDaqWriter : public ntof::dim::DIMXMLInfoHandler
{
public:
    typedef std::map<EventNumber, WroteNumber> EventsMap;
    typedef std::map<RunNumber, EventsMap> RunEventsMap;
    /**
     * @brief Constructor of the class
     * @param hostname: name of the machine to listen
     */
    explicit ClientDaqWriter(const std::string &hostname);

    /**
     * @brief Destructor of the class
     */
    virtual ~ClientDaqWriter();

    /**
     * @brief Get the hostname
     * @return Return the hostname
     */
    inline const std::string &getHostname() const { return m_hostname; }

    /**
     * @brief Callback when an error is made by XML parser
     * @param errMsg: Error message in string
     * @param info: DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg,
                       const ntof::dim::DIMXMLInfo *info) override;

    /**
     * @brief Callback when a new DIMXMLInfo is received
     * @param doc: XML document containing new data
     * @param info: DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info) override;

    /**
     * @brief Callback when a not link is present on DIMXMLInfo
     * @param info: DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info) override;

    /**
     * @brief Check if an event number has been wrote by the DAQ
     * param eventNumber: The event number to check
     * @param runNumber: The run number relative
     * @return Return true if the event has been wrote by the DAQ or false if it
     * is not
     */
    bool isWritten(RunNumber RunNumber, EventNumber EventNumber);

    /**
     * @brief Remove an event form the collection of event wrote
     * @param eventNumber: The event number to remove
     * @param runNumber: The run number relative
     */
    void removeWritten(RunNumber runNumber, EventNumber eventNumber);

    /**
     * @brief Get the size of the file wrote by the DAQ
     * @param eventNumber: The event number to check
     * @param runNumber: The run number relative
     * @return Return the size of the file wrote by the DAQ
     */
    uint64_t getSize(RunNumber runNumber, EventNumber eventNumber);

    /**
     * @brief Clear the list of the couples event number/run number received
     */
    void clearEvents();

protected:
    int32_t getLastExtensionNumber(RunNumber runNumber);

    mutable std::mutex m_lock;
    ntof::dim::DIMXMLInfo m_info; //!< Client used to read the service content
    RunEventsMap m_events;        //!< Collection of the couples event
                                  //!< number/run number received
    std::string m_hostname;       //!< Hostname
    bool m_firstCall; //!< Allow to know if the system has been initialized
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* CLIENTDAQWRITER_H_ */
