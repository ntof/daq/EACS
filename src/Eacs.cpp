/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created a long time ago in a galaxy far far away
**
*/

#include "Eacs.hpp"

#include <chrono>
#include <cstdint>
#include <future>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <DIMStateClient.h>
#include <DIMSuperClient.h>
#include <DIMUtils.hpp>
#include <DaqTypes.h>
#include <easylogging++.h>

#include "BeamMonitor.hpp"
#include "Config.hpp"
#include "DIMException.h"
#include "EACSException.hpp"
#include "EventValidator.hpp"

#include <Singleton.hxx> // instantiates templates

using std::chrono::steady_clock;
using namespace ntof::eacs;

void signalHandler(int32_t signum)
{
    LOG(INFO) << "Received signal #" << signum << " from ORACLE";
    signal(SIGTERM, SIG_DFL);
    signal(SIGINT, SIG_DFL);
    exit(0);
}

template class ntof::utils::Singleton<ntof::eacs::Eacs>;

namespace ntof {
namespace eacs {

std::string Eacs::m_hostname = "";

Eacs::Eacs() :
    DIMSuperCommand(Config::instance().getDimPrefix()),
    m_runNumber(0),
    m_startedLock(utils::Flock::Shared, Config::instance().lockFile()),
    m_worker("Eacs", 1, 2048),
    m_db(Database::instance())
{
    // Install signal handler back (because Oracle steal them!)
    LOG(INFO) << "[EACS] installing signal handlers";
    signal(SIGTERM, signalHandler);
    signal(SIGINT, signalHandler);

    DimServer::start(Config::instance().getServerName().c_str());
    m_state.connect(&errorSignal, &warningSignal);
    m_state.isErrorSignal.connect([this](bool isError) { onError(isError); });
}

Eacs::~Eacs()
{
    /* stop the worker before the db closes */
    m_worker.stop();
    stop();

    ntof::dim::lock_guard lock;
    m_clientRFMerger.reset();
    m_clientADDH.reset();
    m_bctState.reset();
    m_addhState.reset();
    m_writer.reset();
    m_beamMonitor.reset();
    m_eventValidator.reset();
    m_runInfo.reset();
    m_runConfig.reset();
    m_events.reset();
    m_daqs.reset();
    m_timing.reset();
    m_config.reset();
}

void Eacs::destroy()
{
    if (m_instance)
        Eacs::instance().stop();
    Singleton<Eacs>::destroy();
}

void Eacs::post(const std::function<void()> &fun)
{
    try
    {
        m_worker.post(fun);
    }
    catch (NTOFException &ex)
    {
        LOG(ERROR) << "[EACS] failed to queue task: " << ex.getMessage();
    }
}

void Eacs::initServices()
{
    std::promise<void> prom;

    // we need to be thread safe when playing with properties, moving to
    // dedicated thread
    post([this, &prom]() {
        this->enterInit();
        prom.set_value();
    });
    if (prom.get_future().wait_for(std::chrono::seconds(30)) !=
        std::future_status::ready)
        LOG(INFO) << "[EACS] initialization failed";
}

void Eacs::enterInit()
{
    m_state.setState(LOADING);
    // clear services first
    m_filterStation.reset();
    m_sampleExchanger.reset();
    m_runInfo.reset();
    m_runConfig.reset();
    m_events.reset();
    m_daqs.reset();
    m_timing.reset();
    m_config.reset();
    m_clientRFMerger.reset();
    m_bctState.reset();
    m_addhState.reset();
    m_clientADDH.reset();
    m_highVoltage.reset();
    m_writer.reset();
    m_beamMonitor.reset();
    m_eventValidator.reset();
    m_info.reset();

    // Update previous runs with no stop date
    m_db.updatePreviousRuns();

    // EACS/RunInfo
    m_runInfo.reset(new ServiceRunInfo());
    m_state.connect(&m_runInfo->errorSignal, &m_runInfo->warningSignal);
    m_runInfo->externalUpdateSignal.connect([this]() {
        // Update only if a run is ongoing, otherwise the run won't be in the db
        if (!isStarted())
            return;
        this->post([this]() {
            RunInfo runInfo;
            m_runInfo->getRunInfo(runInfo);
            LOG(INFO) << "[RunInfoHandler] updating run in db "
                      << "runNumber: " << runInfo.runNumber;
            this->m_db.updateRunTitleAndDescription(runInfo);
            // Update Run Config
            runInfo.runNumber++;
            m_runConfig->updateRunInfo(runInfo);
        });
    });

    // EACS/RunConfig
    m_runConfig.reset(new ServiceRunConfig());
    m_state.connect(&m_runConfig->errorSignal, &m_runConfig->warningSignal);
    m_runConfig->externalUpdateSignal.connect([this]() {
        this->post([this]() {
            const RunNumber &runNumber =
                m_runConfig->getParameterAt(RUN_NUMBER).getUIntValue();
            const int64_t &detectorsSetupId =
                m_runConfig->getParameterAt(DETECTORS_SETUP_ID).getLongValue();
            const int64_t &materialsSetupId =
                m_runConfig->getParameterAt(MATERIALS_SETUP_ID).getLongValue();
            try
            {
                if (!this->m_db.isDetectorsSetup(detectorsSetupId))
                    warningSignal(WARN_NO_SUCH_DETECTOR_SETUP,
                                  "[RunConfig]: no such detectorsSetupId: " +
                                      std::to_string(detectorsSetupId));
                else
                    warningSignal(WARN_NO_SUCH_DETECTOR_SETUP, std::string());

                if (!this->m_db.isMaterialsSetup(materialsSetupId))
                    warningSignal(WARN_NO_SUCH_MATERIAL_SETUP,
                                  "[RunConfig]: no such materialsSetupId: " +
                                      std::to_string(materialsSetupId));
                else
                    warningSignal(WARN_NO_SUCH_MATERIAL_SETUP, std::string());
            }
            catch (const EACSException &e)
            {
                // query is in error.
                LOG(ERROR) << "[RunConfigHandler] ERROR for runNumber "
                           << runNumber << ": " << e.getMessage();
                errorSignal(ERR_DB, e.getMessage());
                return;
            }
        });
    });

    // EACS/Events
    m_events.reset(new ServiceEvents());

    // EACS/Daq/List
    m_daqs.reset(new ServiceDaqsList());
    m_state.connect(&(m_daqs->errorSignal), &(m_daqs->warningSignal));
    m_daqs->daqReadySignal.connect([this]() { this->onDaqReady(); });
    m_daqs->daqLoadedSignal.connect([this]() { this->onLoaded(); });
    m_daqs->loadDaqConfigs();

    // EACS/Timing
    m_timing.reset(new ServiceTiming());
    m_state.connect(&(m_timing->errorSignal), &(m_timing->warningSignal));
    m_timing->bctSignal.connect(
        [this](const TimingEventDetails &event, float intensity) {
            // Ignore Bct Signals If EACS is not running
            if (m_state.getState() != RUNNING)
                return;

            // Update RunInfo
            m_runInfo->setValue<uint32_t>(EVENT_NUMBER, event.evtNumber, false);
            // Update Events
            m_events->onBctEvent(event, intensity);
            // Update EventValidator
            m_eventValidator->onBctEvent(event, getRunNumber(), intensity);
            // Trigger EventHeader file writer
            m_writer->onBctEvent(event, getRunNumber(), intensity);
            // Check Beam intensity
            m_beamMonitor->onBctEvent(event, intensity);
        });
    m_timing->timingSignal.connect([this](const TimingEventDetails &event) {
        // Ignore Bct Signals If EACS is not running
        if (m_state.getState() != RUNNING)
            return;

        m_events->onTimingEvent(event);
    });
    m_timing->syncSignal.connect(
        [this](DIMParamListProxy &) { this->onLoaded(); });
    m_timing->load();

    // EACS/Config
    m_config.reset(new ServiceConfig());
    m_state.connect(&(m_config->errorSignal), &(m_config->warningSignal));

    // Client: RawFileMerger CMD and Current
    m_clientRFMerger.reset(new ClientRFMerger());
    m_state.connect(&m_clientRFMerger->errorSignal,
                    &m_clientRFMerger->warningSignal);

    // Client: BCTReader State
    m_bctState.reset(new dim::DIMStateClient(
        Config::instance().getBctServiceName() + "/State"));

    // Client: ADDH State
    m_addhState.reset(new ClientAddhState());
    m_state.connect(&m_addhState->errorSignal, &m_addhState->warningSignal);

    // Client: ADDH CMD
    m_clientADDH.reset(new ClientAddhCmd());

    // Client: filterStation
    if (!Config::instance().getFilterStationServiceName().empty())
    {
        m_filterStation.reset(new ClientFilterStation());
        m_state.connect(&m_filterStation->errorSignal,
                        &m_filterStation->warningSignal);
        m_filterStation->movedSignal.connect([this]() { onFiltersMoved(); });
    }

    if (!Config::instance().getSampleExchangerServiceName().empty())
    {
        m_sampleExchanger.reset(new ClientSampleExchanger());
        m_state.connect(&m_sampleExchanger->errorSignal,
                        &m_sampleExchanger->warningSignal);
        m_sampleExchanger->movedSignal.connect([this]() { onSamplesMoved(); });
    }

    resetRunInfo();

    m_highVoltage.reset(new HVMonitor());
    if (Config::instance().isHVWarnOnly())
    {
        // Connect HV Errors as Warnings
        m_state.connect(nullptr, &m_highVoltage->warningSignal);
        m_highVoltage->errorSignal.connect(
            [this](ErrorCode error, const std::string &message) {
                WarningCode newCode = WarningCode(error + 1000);
                this->m_highVoltage->warningSignal(newCode, message);
            });
    }
    else
    {
        m_state.connect(&m_highVoltage->errorSignal,
                        &m_highVoltage->warningSignal);
    }

    m_highVoltage->stateSignal.connect(
        [this](HVMonitor::State) { this->onHighVoltageState(); });
    m_highVoltage->start();

    // WriterFile
    m_writer.reset(new WriterFile(*this, *m_clientADDH));
    m_state.connect(&m_writer->errorSignal, &m_writer->warningSignal);

    // Beam Monitor
    m_beamMonitor.reset(new BeamMonitor());
    m_state.connect(nullptr, &(m_beamMonitor->warningSignal));

    // EventValidator
    m_eventValidator.reset(new EventValidator(m_clientRFMerger, m_daqs));
    m_state.connect(&m_eventValidator->errorSignal,
                    &m_eventValidator->warningSignal);

    // When EventValidator fire validated signal, update Events and RunInfo
    m_eventValidator->validatedSignal.connect(
        [this](const EventValidator::ValidEvent &event, uint32_t extensionNumber,
               float /* intensity */, float totalIntensity) {
            // Update events
            m_events->onValidatedEvent(event, extensionNumber);
            // Update RunInfo
            m_runInfo->onValidatedEvent(event, extensionNumber, totalIntensity);
        });

    m_eventValidator->runningSignal.connect(
        [this](bool isRunning) { this->onValidatorRunning(isRunning); });

    m_info.reset(new ServiceInfo());
}

void Eacs::run()
{
    if (!m_eventValidator)
        initServices();
    m_eventValidator->run();
}

void Eacs::start()
{
    if (!m_eventValidator)
        initServices();
    m_eventValidator->start();
}

void Eacs::stop()
{
    if (m_eventValidator)
        m_eventValidator->stop();
    if (m_beamMonitor)
        m_beamMonitor->stop();
}

void Eacs::commandReceived(dim::DIMCmd &cmdData)
{
    // If the client sends non-valid data, the DIMSuperCommand sends
    // automatically a error to the client
    pugi::xml_node parent = cmdData.getData();
    LOG(INFO) << "[EACS] command received";

    if (parent)
    {
        std::string command = parent.attribute("name").as_string();
        std::transform(command.begin(), command.end(), command.begin(),
                       ::tolower);

        // Check if the command can be accepted
        if (command != "start" && command != "stop" && command != "reset" &&
            command != "config")
        {
            DIMSuperCommand::setError(cmdData.getKey(), -1, "Command unknown.");
        }
        else
        {
            if (command == "reset" &&
                (m_state.getState() != IDLE &&
                 m_state.getState() != EacsState(ERROR)))
            {
                DIMSuperCommand::setError(
                    cmdData.getKey(), -2, "The system is running. Can't reset.");
            }
            else
            {
                addCommand(command, parent);
                DIMSuperCommand::setOk(cmdData.getKey(), std::string("Done!"));
            }
        }
    }
    else
    {
        LOG(INFO) << "The parent node of the command received is empty";
        DIMSuperCommand::setError(cmdData.getKey(), -3, "Parent node is empty");
    }
}

void Eacs::actionStart()
{
    if (m_state.getState() != IDLE)
    {
        LOG(ERROR) << "[EACS] can't start an eacs that is not IDLE state:"
                   << m_state.getState();
        return;
    }

    try
    {
        setStarted(true);

        // Set Run information from RunInfo and RunConfig
        // Migrate from runConfig to runInfo and create a new run
        RunInfo newRun;
        m_runConfig->getRunInfo(newRun);
        // Check runNumber from RFM
        RunNumber lastRunNumberRFM = m_clientRFMerger->getCurrentRunNumber();
        newRun.runNumber = std::max(newRun.runNumber, lastRunNumberRFM + 1);

        // Checks
        if (newRun.title.empty() || newRun.experiment.empty())
        {
            errorSignal(
                ERR_CMD,
                "[EACS]: failed to start: title or experiment are empty");
            return;
        }
        m_runNumber = newRun.runNumber;
        m_runDataStatus = RunInfo::DataStatus::DataUnknown;
        m_startTime = std::time(nullptr);

        m_db.createRun(newRun, m_startTime);
        // Update RunInfo
        m_runInfo->clearParameters(false);
        m_runInfo->updateRunInfo(newRun);

        // Update RunConfig for the next run
        m_runConfig->clearParameters(false);
        newRun.runNumber++;
        m_runConfig->updateRunInfo(newRun);

        enterFiltersInit();
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::actionStop()
{
    if (!m_state.setState(STOPPING))
        return;

    m_daqs->stopDaqs();
    m_beamMonitor->stop();
    if (!m_eventValidator->hasEvents())
    {
        onValidatorRunning(false);
    }
}

void Eacs::onValidatorRunning(bool isRunning)
{
    if (!isRunning && m_state.getState() == STOPPING)
    {
        post([this]() {
            exitStopping();
            enterIdle();
        });
    }
}

void Eacs::actionReset()
{
    EacsState state = m_state.getState();
    if (state != IDLE && state != EacsState(ERROR))
    {
        LOG(ERROR) << "[EACS] can't reset an eacs that is not IDLE or ERROR:"
                   << m_state.getState();
        return;
    }

    // Send the command to the ADDH Writer
    m_clientADDH->sendReset();

    // Clear everything we can
    m_state.clearErrors();
    m_state.clearWarnings();
    resetRunInfo();
    setStarted(false);
    m_eventValidator->reset();
    enterLoading(); // at least try
}

void Eacs::resetRunInfo()
{
    try
    {
        // Get Latest Run
        m_runNumber = m_db.getLatestRun();
        if (m_runNumber == RunInfo::InvalidRunNumber)
        {
            m_runInfo->updateRunInfo(RunInfo());
            // IF is invalid, publish only RunConfig with area.min + 1
            const ExperimentArea &area = ExperimentArea::findArea(
                Config::instance().getExperimentalArea());
            RunInfo nextRun;
            // m_runNumber = nextRun.runNumber = area.min + 1;
            m_runNumber = nextRun.runNumber =
                std::max(area.min, m_clientRFMerger->getCurrentRunNumber()) + 1;
            m_runConfig->updateRunInfo(nextRun);
        }
        else
        {
            RunInfo latestRunInfo;
            m_db.getRunInfo(m_runNumber, latestRunInfo);
            m_runInfo->updateRunInfo(latestRunInfo);
            // latestRunInfo.runNumber++;
            latestRunInfo.runNumber =
                std::max(latestRunInfo.runNumber,
                         m_clientRFMerger->getCurrentRunNumber()) +
                1;
            m_runConfig->updateRunInfo(latestRunInfo);
        }
        m_runDataStatus = RunInfo::DataStatus::DataUnknown;
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::actionClear()
{
    // TODO should we keep it ?
    // Send the command to the ADDH Writer
    m_clientADDH->sendClear();

    // Clear the warnings
    m_state.clearWarnings();
}

void Eacs::doAction(const std::string &command, const pugi::xml_document &cmdDoc)
{
    LOG(INFO) << "[EACS] running command: " << command;
    if (command == "start")
        actionStart();
    else if (command == "stop")
    {
        std::string dStr =
            cmdDoc.child("command").attribute("dataStatus").as_string("unknown");
        LOG(INFO) << "[EACS] command stop with dataStatus: " << dStr;
        m_runDataStatus = RunInfo::fromString<RunInfo::DataStatus>(dStr);
        actionStop();
    }
    else if (command == "clear")
        actionClear();
    else if (command == "reset")
        actionReset();
    else if (command == "forceStop")
        actionStop();
}

void Eacs::addCommand(const std::string &command, const pugi::xml_node &parent)
{
    // xml_node copy-constructor is private se we use shared_ptr.
    // We need to copy the xml doc otherwise is destroyed and we get segfault
    std::shared_ptr<pugi::xml_document> doc(
        std::make_shared<pugi::xml_document>());
    doc->append_copy(parent);

    post([this, command, doc]() { doAction(command, *doc); });
}

void Eacs::setRunNumber(int32_t runNumber)
{
    this->m_runNumber = runNumber;
}

bool Eacs::isStarted() const
{
    return m_startedLock.isLocked();
}

void Eacs::setStarted(bool value)
{
    if (value)
        m_startedLock.lock();
    else
        m_startedLock.unlock();
}

uint32_t Eacs::getRunNumber() const
{
    return m_runNumber;
}

const std::string &Eacs::getHostname()
{
    // Get the hostname of the hostname
    if (m_hostname.empty())
    {
        char hostname[128] = {};
        if (gethostname(hostname, 128) != 0)
        {
            throw EACSException("Can't get the host name.", __FILE__, __LINE__);
        }
        m_hostname = std::string(hostname);
    }
    return m_hostname;
}

void Eacs::genHeaders(RunControlHeader &rctr, ModuleHeader &modh)
{
    rctr.runNumber = m_runNumber;
    rctr.segmentNumber = 0;
    rctr.streamNumber = 1;
    rctr.setExperiment(Config::instance().getExperimentalArea());
    rctr.updateDateTime(m_startTime);
    rctr.totalNumberOfStreams = 2;
    m_daqs->genHeaders(rctr, modh);
}

void Eacs::clearErrors()
{
    m_state.clearErrors();
}

void Eacs::clearWarnings()
{
    m_state.clearWarnings();
}

void Eacs::onLoaded()
{
    EacsState state = m_state.getState();
    if (state != LOADING && state != STOPPING)
    {
        LOG(ERROR) << "[EACS] invalid state to receive loaded signal state:"
                   << toString(m_state.getState());
        return;
    }
    else
    {
        post([this]() { this->enterIdle(); });
    }
}

void Eacs::onError(bool isError)
{
    post([this, isError]() {
        if (m_state.getState() != EacsState(ERROR)) // unlikely
        {
            LOG(ERROR) << "[EACS] inconsistent state, not in ERROR anymore";
            return;
        }

        if (isError)
            enterError();
    });
}

void Eacs::enterError()
{
    if (m_state.getState() != EacsState(ERROR))
        return;

    try
    {
        if (!isStarted())
            return;

        m_clientRFMerger->sendRunStop(m_runNumber, true);
        m_daqs->resetDaqs();
        m_db.updateRunStatus(m_runNumber, RunInfo::Crashed, RunInfo::BadData);
        m_beamMonitor->stop();
        // TODO: we could add a comment in database here
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::enterLoading()
{
    if (!m_state.setState(LOADING))
        return;

    m_daqs->loadDaqConfigs();
    m_timing->load();
    m_highVoltage->reset();
    if (m_sampleExchanger)
        m_sampleExchanger->reset();
    if (m_filterStation)
        m_filterStation->reset();
}

void Eacs::enterIdle()
{
    if (m_daqs->isDaqLoaded() && !m_timing->isSyncing() &&
        (!m_highVoltage->hasBoards() ||
         m_highVoltage->getState() == HVMonitor::State::ST_IDLE))
    {
        m_state.setState(IDLE);
    }
}

void Eacs::enterFiltersInit()
{
    if (!m_state.setState(FILTERS_INIT))
        return;
    else if (!m_filterStation)
    {
        // filter station disabled
        post([this]() { this->enterSamplesInit(); });
        return;
    }

    try
    {
        Database::FiltersPositionMap filtersPosition;
        m_db.getFiltersPosition(
            m_runInfo->getParameterAt(MATERIALS_SETUP_ID).getLongValue(),
            filtersPosition);

        std::vector<ClientFilterStation::FilterPosition> pos;
        pos.reserve(ClientFilterStation::FiltersCount);
        for (std::size_t i = 0; i < ClientFilterStation::FiltersCount; ++i)
        {
            Database::FiltersPositionMap::const_iterator it =
                filtersPosition.find(i + 1);
            if (it != filtersPosition.end())
                pos.push_back((it->second) ? ClientFilterStation::PositionIn :
                                             ClientFilterStation::PositionOut);
            else
                pos.push_back(ClientFilterStation::PositionOut);
        }

        m_filterStation->move(pos);
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::onFiltersMoved()
{
    if (m_state.getState() != FILTERS_INIT)
    {
        LOG(ERROR)
            << "[EACS] invalid state to receive FiltersMoved signal state:"
            << toString(m_state.getState());
        return;
    }

    post([this]() { this->enterSamplesInit(); });
}

void Eacs::enterSamplesInit()
{
    if (!m_state.setState(SAMPLES_INIT))
        return;
    else if (!m_sampleExchanger)
    {
        post([this]() { this->enterCollimatorInit(); });
        return;
    }

    try
    {
        int32_t position;
        m_db.getSamplePosition(
            m_runInfo->getParameterAt(MATERIALS_SETUP_ID).getLongValue(),
            position);

        /* database position are not shifted like on filter-station,
           position 0 is reserved as a secure position */
        m_sampleExchanger->move((position >= 0) ?
                                    position :
                                    ClientSampleExchanger::DefaultPosition);
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::onSamplesMoved()
{
    if (m_state.getState() != SAMPLES_INIT)
    {
        LOG(ERROR)
            << "[EACS] invalid state to receive SamplesMoved signal state:"
            << toString(m_state.getState());
        return;
    }

    post([this]() { this->enterCollimatorInit(); });
}

void Eacs::enterCollimatorInit()
{
    if (!m_state.setState(COLLIMATOR_INIT))
        return;

    post([this]() { this->enterHighVoltageInit(); });
}

void Eacs::enterHighVoltageInit()
{
    if (!m_state.setState(HIGHVOLTAGE_INIT))
        return;

    if (m_highVoltage->getState() == HVMonitor::State::ST_IDLE)
        m_highVoltage->updateDatabase(m_runNumber);

    // If there are no HV declared, call directly onHighVoltageState().
    if (!m_highVoltage->hasBoards() &&
        m_highVoltage->getState() == HVMonitor::State::ST_UNKNOWN)
        this->onHighVoltageState();
}

void Eacs::onHighVoltageState()
{
    EacsState state = m_state.getState();
    if (state == LOADING || state == STOPPING)
        post([this]() { enterIdle(); });
    else if (state == HIGHVOLTAGE_INIT)
    {
        switch (m_highVoltage->getState())
        {
        case HVMonitor::State::ST_UNKNOWN:
            // If there are no boards, skip HV
            if (!m_highVoltage->hasBoards())
                post([this]() { enterDaqsInit(); });
            break;
        case HVMonitor::State::ST_MONITORING:
            post([this]() { enterDaqsInit(); });
            break;
        case HVMonitor::State::ST_IDLE:
            /* was not ready when we entered INIT */
            m_highVoltage->updateDatabase(m_runNumber);
            break;
        case HVMonitor::State::ST_INITIAL:
        case HVMonitor::State::ST_LOADING: break;
        }
    }
}

void Eacs::enterDaqsInit()
{
    if (!m_state.setState(DAQS_INIT))
        return;

    m_events->clearEvents();
    m_daqs->configureDaqs(m_runNumber);
}

void Eacs::onDaqReady()
{
    if (m_state.getState() != DAQS_INIT)
    {
        LOG(ERROR) << "[EACS] invalid state to receive DaqReady signal state:"
                   << toString(m_state.getState());
        return;
    }

    post([this]() {
        exitDaqsInit();
        enterMergerInit();
    });
}

void Eacs::exitDaqsInit()
{
    if (m_state.getState() != DAQS_INIT)
    {
        return;
    }
    m_daqs->updateDatabase(m_runNumber);
}

void Eacs::enterMergerInit()
{
    if (!m_state.setState(MERGER_INIT))
        return;

    m_clientRFMerger->sendRunStart(
        m_runNumber, m_runInfo->lockParameterAt(EXPERIMENT)->getStringValue(),
        m_daqs->getActiveCrateIds());

    post([this]() { this->enterStarting(); });
}

void Eacs::enterStarting()
{
    if (!m_state.setState(STARTING))
        return;

    try
    {
        m_timing->suspend();
        steady_clock::time_point time_point = steady_clock::now();
        m_events->clearEvents();

        // Write the .run file
        RunControlHeader rctr;
        ModuleHeader modh;
        genHeaders(rctr, modh);
        m_writer->onInitRun(rctr, modh);

        // This corresponds to the DaqManager EventReader event margin (+5)
        std::this_thread::sleep_until(time_point +
                                      std::chrono::milliseconds(250 + 5));
        m_daqs->startDaqs();
        time_point = steady_clock::now();

        m_db.updateRunStatus(m_runInfo->getRunNumber(), RunInfo::Ongoing,
                             RunInfo::DataUnknown);
        LOG(INFO) << "[EACS]: run:" << m_runNumber << " ongoing";

        // wait for daqs to be really ready
        std::this_thread::sleep_until(time_point +
                                      std::chrono::milliseconds(100));

        m_beamMonitor->reset();
        m_beamMonitor->start();

        // Now let's start recording
        m_timing->start();
        m_state.setState(RUNNING);
    }
    catch (EACSException &ex)
    {
        errorSignal(ErrorCode(ex.getErrorCode()), ex.getMessage());
    }
}

void Eacs::exitStopping()
{
    if (m_state.getState() != STOPPING)
    {
        return;
    }
    // Always ignore stop errors on RFMerger:
    //   - run will be stopped on next start in worst case
    //   - this may send an error if called during init sequence (Unknown run)
    m_clientRFMerger->sendRunStop(m_runNumber, true);
    m_highVoltage->reset(); /* stop monitoring HV */
    m_db.updateRunStatus(m_runNumber, RunInfo::Finished, m_runDataStatus);
    setStarted(false);
}

} // namespace eacs
} // namespace ntof
