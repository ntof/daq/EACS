/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-15T13:09:00+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef RUNINFO_HPP__
#define RUNINFO_HPP__

#include <ctime>
#include <vector>

#include <DIMData.h>

#include "EACSTypes.hpp"

namespace ntof {
namespace eacs {

class RunInfo
{
public:
    typedef ntof::eacs::RunNumber RunNumber;

    static const RunNumber InvalidRunNumber;
    static const EventNumber InvalidEventNumber;

    template<typename T>
    static T fromString(const std::string &str);

    enum Status
    {
        Unknown = -2,
        Crashed = -1,
        Finished = 0,
        Ongoing = 1
    };
    static std::string toString(Status status);

    enum DataStatus
    {
        DataUnknown = -2,
        DataStopped = -1,
        BadData = 0,
        DataOk = 1
    };
    static std::string toString(DataStatus status);

    RunInfo();
    RunInfo(const RunInfo &) = delete;
    RunInfo &operator=(const RunInfo &) = delete;
    RunInfo(RunInfo &&) = default;

    /**
     * @brief construct RunInfo from data
     * @throws DIMException on conversion error
     */
    static RunInfo fromData(const std::vector<dim::DIMData> &data);

    RunNumber runNumber;
    EventNumber eventNumber;
    EventNumber validatedNumber;
    float protons;
    std::string title;
    std::string description;
    std::string experiment;
    int64_t detectorsSetupId;
    int64_t materialsSetupId;
};

template<>
RunInfo::Status RunInfo::fromString<RunInfo::Status>(const std::string &str);

template<>
RunInfo::DataStatus RunInfo::fromString<RunInfo::DataStatus>(
    const std::string &str);

} // namespace eacs
} // namespace ntof

#endif
