/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-15T13:15:45+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "data/RunInfo.hpp"

#include <limits>

#include "EnumInfo.hpp"
#include "misc.h"

using namespace ntof::eacs;
using namespace ntof::dim;

const RunNumber RunInfo::InvalidRunNumber = std::numeric_limits<RunNumber>::max();
const EventNumber RunInfo::InvalidEventNumber =
    std::numeric_limits<EventNumber>::max();

static const EnumInfo<RunInfo::Status> s_statusMap{
    {RunInfo::Ongoing, "ongoing"},
    {RunInfo::Finished, "finished"},
    {RunInfo::Crashed, "crashed"},
    {RunInfo::Unknown, "unknown"}};

static const EnumInfo<RunInfo::DataStatus> s_dataStatusMap{
    {RunInfo::DataStopped, "stopped"},
    {RunInfo::BadData, "bad"},
    {RunInfo::DataOk, "ok"},
    {RunInfo::DataUnknown, "unknown"}};

RunInfo::RunInfo() :
    runNumber(InvalidRunNumber),
    eventNumber(InvalidEventNumber),
    validatedNumber(InvalidEventNumber),
    protons(0),
    detectorsSetupId(-1),
    materialsSetupId(-1)
{}

std::string RunInfo::toString(RunInfo::DataStatus status)
{
    return s_dataStatusMap.toString(status, RunInfo::DataUnknown);
}

std::string RunInfo::toString(RunInfo::Status status)
{
    return s_statusMap.toString(status, RunInfo::Unknown);
}

template<>
RunInfo::Status RunInfo::fromString<RunInfo::Status>(const std::string &str)
{
    return s_statusMap.fromString(str, RunInfo::Unknown);
}

template<>
RunInfo::DataStatus RunInfo::fromString<RunInfo::DataStatus>(
    const std::string &str)
{
    return s_dataStatusMap.fromString(str, RunInfo::DataUnknown);
}

RunInfo RunInfo::fromData(const std::vector<DIMData> &data)
{
    RunInfo info;
    for (const DIMData &d : data)
    {
        switch (d.getIndex())
        {
        case RUN_NUMBER: info.runNumber = d.getUIntValue(); break;
        case TITLE: info.title = d.getStringValue(); break;
        case DESCRIPTION: info.description = d.getStringValue(); break;
        case EXPERIMENT: info.experiment = d.getStringValue(); break;
        case DETECTORS_SETUP_ID:
            info.detectorsSetupId = d.getLongValue();
            break;
        case MATERIALS_SETUP_ID:
            info.materialsSetupId = d.getLongValue();
            break;
        }
    }
    return std::move(info);
}
