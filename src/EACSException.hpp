/*
 * EACSException.hpp
 *
 * Created on: May 8, 2015
 * Author: mdonze
 */

#ifndef EACSEXCEPTION_HPP__
#define EACSEXCEPTION_HPP__

#include <NTOFException.h>

namespace ntof {
namespace eacs {

#define EACS_THROW(msg, code) throw EACSException(msg, __FILE__, __LINE__, code)

/**
 * @class EACSException
 * @brief Exception throw by the EACS
 */
class EACSException : public NTOFException
{
public:
    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     */
    EACSException(const std::string &Msg,
                  const std::string &file,
                  int Line,
                  int error = 0);

    /**
     * @brief Constructor of the class
     * @param Msg Error message
     * @param file Name of the file where the exception is thrown (__FILE__)
     * @param Line Line of code doing the error (__LINE__)
     * @param error Error code
     */
    EACSException(const char *Msg,
                  const std::string &file,
                  int Line,
                  int error = 0);

    /**
     * @brief Empty constructor
     */
    EACSException() = default;

    /**
     * @brief Destructor of the class
     */
    ~EACSException() override = default;

    /**
     * @brief Overload of std::exception
     * \return Error message
     */
    virtual const char *what() const noexcept override;
};

} /* namespace eacs */
} /* namespace ntof */

#endif
