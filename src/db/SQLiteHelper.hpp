/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-02T15:13:30+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef SQLITEHELPER_HPP__
#define SQLITEHELPER_HPP__

#include <cstdint>
#include <mutex>
#include <string>

#include <sqlite3.h>

/**
 * lightweight sqlite wrapper
 */
namespace sqlite {

class StmtGuard;

class SharedStmt
{
public:
    explicit SharedStmt(sqlite3_stmt *stmt = nullptr);
    explicit SharedStmt(const SharedStmt &other) = delete;
    SharedStmt &operator=(const SharedStmt &other) = delete;
    ~SharedStmt();

    inline operator bool() const { return stmt != nullptr; }

    inline void lock() { m_lock.lock(); }
    void unlock() { m_lock.unlock(); }

    sqlite3_stmt *stmt;

protected:
    friend class StmtGuard;

    std::mutex m_lock;
};

class StmtGuard
{
public:
    explicit StmtGuard(SharedStmt &stmt);
    ~StmtGuard();

    void acquire();

    int step();

    void reset();

    int bind(int64_t value, int idx = -1);
    int bind(const std::string &value, int idx = -1);
    int bindNull(int idx = -1);

    template<typename T>
    T get(int idx = -1);

    inline operator bool() const { return bool(stmt); }

    int autoIndex;
    bool m_locked;
    SharedStmt &stmt;
};

} // namespace sqlite

#endif
