/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-12T11:08:10+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef SQLITEDATABASE_HPP__
#define SQLITEDATABASE_HPP__

#include <map>
#include <mutex>
#include <string>

#include <Signals.hpp>
#include <sqlite3.h>

#include "Database.hpp"
#include "SQLiteHelper.hpp"
#include "data/SetupObject.hpp"

namespace ntof {
namespace eacs {

class SQLiteDatabase : public Database
{
public:
    typedef ntof::utils::signal<void()> DbUpdatedSignal;

    SQLiteDatabase();
    virtual ~SQLiteDatabase();

    virtual void createRun(const RunInfo &info, std::time_t start) override;

    virtual void updateRunTitleAndDescription(const RunInfo &info) override;

    virtual void updateRunStatus(RunNumber runNumber,
                                 RunInfo::Status status,
                                 RunInfo::DataStatus dataStatus) override;

    virtual void updatePreviousRuns() override;

    virtual void getRunInfo(RunNumber runNumber, RunInfo &info) override;

    virtual void getRunStatus(RunNumber runNumber,
                              RunInfo::Status &status,
                              RunInfo::DataStatus &dataStatus) override;

    virtual RunNumber getLatestRun() override;

    virtual void addProtons(RunNumber runNumber, float protons) override;

    virtual void insertTrigger(RunNumber runNumber,
                               EventNumber eventNumber,
                               BeamType beamType,
                               float intensity,
                               int64_t cycleStamp) override;

    virtual void insertDaqConfig(RunNumber runNumber,
                                 uint32_t location,
                                 const DaqParametersDatabase &params) override;
    virtual int64_t countDaqParams() override;

    virtual bool isMaterialsSetup(int64_t id) override;
    virtual bool isDetectorsSetup(int64_t id) override;

    virtual void getFiltersPosition(
        int64_t materialsSetupId,
        std::map<std::size_t, bool> &filtersPosition) override;

    virtual void getSamplePosition(int64_t materialsSetupId,
                                   int32_t &position) override;

    virtual void insertHVChannelInfo(RunNumber runNumber,
                                     const HVBoardInfo &board,
                                     const HVChannelInfo &channel) override;
    virtual int64_t countHVChannelInfo() override;

    /**
     * @details mostly used for testing
     */
    void createMaterialsSetup(const SetupObject &info);

    /**
     * @details mostly used for testing
     */
    void createDetectorsSetup(const SetupObject &info);

    /**
     * @details mostly used for testing
     * @param[in] materialsSetupId materials setups id
     * @param[in] filter           filter index
     * @param[in] position         position IN(true)/OUT(false)
     */
    void insertFilterPosition(int64_t materialsSetupId,
                              std::size_t filter,
                              bool position);

    /**
     * @brief find or create the given daq instrument
     * @return the DaqInstrument id
     */
    int64_t getDaqInstrument(uint32_t location);

    /**
     * @brief create an entry for a given daq instrument
     * @return the Daq Config id
     */
    int64_t createDaqConfig(RunNumber runNumber, int64_t daqiId, bool enabled);
    int64_t getDaqConfig(RunNumber runNumber, int64_t daqiId);

    void insertParams(int64_t configId, const DaqParametersDatabase &db);

    virtual const std::string &name() const override;

    DbUpdatedSignal dbUpdatedSignal;

protected:
    enum StmtId
    {
        CreateRun,
        UpdateRun,
        UpdateRunStatus,
        UpdateRunStatusStop,
        UpdatePreviousRuns,
        GetRunInfo,
        GetRunStatus,
        LatestRun,
        UpdateProtons,
        InsertTrigger,
        IsMaterialsSetup,
        CreateMaterialsSetup,
        IsDetectorsSetup,
        CreateDetectorsSetup,
        GetDaqInstrument,
        InsertDaqInstrument,
        CreateDaqConfig,
        GetDaqConfig,
        InsertParam,
        InsertRelConfParam,
        CountParams,
        GetFiltersPosition,
        GetSamplePosition,
        InsertFilterPosition,
        InsertHVChannelInfo,
        CountHVChannelInfo
    };
    typedef std::map<StmtId, sqlite::SharedStmt> StmtMap;

    void createDb();
    bool check(int rc, const char *msg, bool nothrow = false);
    sqlite::SharedStmt &getStatement(StmtId id, const char *sql);

    std::string m_dbName;
    std::mutex m_lock;
    sqlite3 *m_db;
    StmtMap m_stmts;
};

} // namespace eacs
} // namespace ntof

#endif
