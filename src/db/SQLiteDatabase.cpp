/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-12T13:19:28+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "SQLiteDatabase.hpp"

#include <cerrno>

#include <DaqTypes.h>
#include <easylogging++.h>

#include "Config.hpp"
#include "DaqParametersDatabase.hpp"
#include "EACSException.hpp"
#include "hv/HVBoardInfo.hpp"
#include "hv/HVChannelInfo.hpp"

using namespace ntof::eacs;
using namespace sqlite;

static const std::string s_name = "SQLiteDatabase";

SQLiteDatabase::SQLiteDatabase() :
    m_dbName(Config::instance().getDatabasePath())
{
    LOG(DEBUG) << "Opening " << m_dbName << " database";
    int rc = sqlite3_open_v2(m_dbName.c_str(), &m_db,
                             SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE |
                                 SQLITE_OPEN_FULLMUTEX |
                                 SQLITE_OPEN_PRIVATECACHE,
                             nullptr);
    if (!check(rc, "failed to open SQLiteDatabase"))
    {
        sqlite3_close(m_db);
        m_db = nullptr;
    }
    createDb();
}

SQLiteDatabase::~SQLiteDatabase()
{
    if (m_db)
    {
        m_stmts.clear();
        sqlite3_close(m_db);
    }
}

const std::string &SQLiteDatabase::name() const
{
    return s_name;
}

void SQLiteDatabase::createDb()
{
    char *msg;
    int rc;

    if (!m_db)
        return;
    rc = sqlite3_exec(m_db, "PRAGMA foreign_keys = ON;", nullptr, nullptr, &msg);
    check(rc, "failed to enabled foreign keys", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS materials_setups("
                      "id INTEGER PRIMARY KEY,"
                      "name TEXT,"
                      "description TEXT,"
                      "earNumber REAL DEFAULT 0,"
                      "operYear REAL DEFAULT 0);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create materials_setups db", msg);

    // FIXME no materials table, type is in relation
    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS rel_materials_setups("
                      "materialId INTEGER,"
                      "materialsSetupId INTEGER,"
                      "position NUMBER,"
                      "status INTEGER,"
                      "type INTEGER DEFAULT 0);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create rel_materials_setups db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS detectors_setups("
                      "id INTEGER PRIMARY KEY,"
                      "name TEXT,"
                      "description TEXT,"
                      "earNumber REAL DEFAULT 0,"
                      "operYear REAL DEFAULT 0);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create detectors_setups db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS runs("
                      "runNumber INTEGER PRIMARY KEY,"
                      "start INTEGER, stop INTEGER DEFAULT NULL,"
                      "title TEXT, description TEXT, experiment TEXT,"
                      "status INTEGER, dataStatus INTEGER,"
                      "protons REAL,"
                      "detectorsSetupId INTEGER,"
                      "materialsSetupId INTEGER);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create runs db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS triggers("
                      "runNumber INTEGER NOT NULL,"
                      "cycleStamp INTEGER DEFAULT NULL,"
                      "eventNumber INTEGER NOT NULL,"
                      "beamType INTEGER,"
                      "intensity REAL DEFAULT 0,"
                      "UNIQUE (runNumber, eventNumber),"
                      "FOREIGN KEY(runNumber) REFERENCES "
                      "runs(runNumber) ON DELETE CASCADE);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create triggers db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS daqInstruments("
                      "id INTEGER PRIMARY KEY,"
                      "channel INTEGER NOT NULL,"
                      "module INTEGER NOT NULL,"
                      "crate INTEGER NOT NULL,"
                      "stream INTEGER NOT NULL);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create daqInstruments db", msg);

    rc = sqlite3_exec(
        m_db,
        "CREATE TABLE IF NOT EXISTS configurations("
        "id INTEGER PRIMARY KEY,"
        "runNumber INTEGER NOT NULL,"
        "status INTEGER NOT NULL,"
        "daqInstrument INTEGER NOT NULL,"
        "UNIQUE (runNumber, daqInstrument),"
        "FOREIGN KEY(runNumber) REFERENCES runs(runNumber) ON DELETE CASCADE,"
        "FOREIGN KEY(daqInstrument) REFERENCES daqInstruments(id) ON DELETE "
        "CASCADE"
        ");",
        nullptr, nullptr, &msg);
    check(rc, "failed to create configurations db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS daqParameters("
                      "id INTEGER PRIMARY KEY,"
                      "idx INTEGER NOT NULL,"
                      "name TEXT,"
                      "value TEXT,"
                      "UNIQUE (idx, name, value));",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create daqParameters db", msg);

    rc = sqlite3_exec(
        m_db,
        "CREATE TABLE IF NOT EXISTS relConfParam("
        "paramId INTEGER NOT NULL,"
        "confId INTEGER NOT NULL,"
        "FOREIGN KEY(paramId) REFERENCES daqParameters(id) ON DELETE CASCADE,"
        "FOREIGN KEY(confId) REFERENCES configurations(id) ON DELETE CASCADE"
        ");",
        nullptr, nullptr, &msg);
    check(rc, "failed to create daqParameters db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS hv("
                      "runNumber INTEGER NOT NULL,"
                      "slotId INTEGER NOT NULL,"
                      "channelId INTEGER NOT NULL,"
                      "serialNumber INTEGER,"
                      "name TEXT, v0set REAL, i0set REAL, trip REAL,"
                      "rup REAL, rdown REAL, vMax INTEGER,"
                      "UNIQUE (runNumber, slotId, channelId),"
                      "FOREIGN KEY(runNumber) REFERENCES "
                      "runs(runNumber) ON DELETE CASCADE);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create hv db", msg);
}

SharedStmt &SQLiteDatabase::getStatement(SQLiteDatabase::StmtId id,
                                         const char *sql)
{
    std::lock_guard<std::mutex> lock(m_lock);
    SharedStmt &shared = m_stmts[id];
    if (!shared)
    {
        int rc = sqlite3_prepare_v2(m_db, sql, -1, &shared.stmt, nullptr);
        check(rc, "failed to prepare statement");
    }
    return shared;
}

void SQLiteDatabase::createRun(const RunInfo &info, std::time_t start)
{
    int rc;
    StmtGuard stmt(getStatement(CreateRun,
                                "INSERT INTO runs "
                                "(runNumber, start, title, description, "
                                "experiment, status, dataStatus, protons, "
                                "detectorsSetupId, materialsSetupId) VALUES "
                                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    if (start == std::time_t(-1))
    {
        start = std::time(nullptr);
    }

    stmt.bind(info.runNumber);
    stmt.bind(start * 1000);
    stmt.bind(info.title);
    stmt.bind(info.description);
    stmt.bind(info.experiment);
    stmt.bind(RunInfo::Ongoing);
    stmt.bind(RunInfo::DataUnknown);
    stmt.bind(0);

    if (info.detectorsSetupId < 0)
        stmt.bindNull();
    else
        stmt.bind(info.detectorsSetupId);

    if (info.materialsSetupId < 0)
        stmt.bindNull();
    else
        stmt.bind(info.materialsSetupId);

    rc = stmt.step();
    check(rc, "failed to insert runs");
    dbUpdatedSignal();
}

void SQLiteDatabase::getRunInfo(RunNumber runNumber, RunInfo &info)
{
    StmtGuard stmt(
        getStatement(GetRunInfo,
                     "SELECT title, description, experiment, protons, "
                     "detectorsSetupId, materialsSetupId FROM runs "
                     "WHERE runNumber = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    int rc = stmt.step();
    if (rc == SQLITE_DONE)
    {
        // no rows to process, it means no results
        EACS_THROW(
            "run " + std::to_string(runNumber) + " not found in database!",
            ERR_DB_ENOENT);
    }
    else if (rc == SQLITE_ROW)
    {
        info.runNumber = runNumber;
        info.title = stmt.get<std::string>();
        info.description = stmt.get<std::string>();
        info.experiment = stmt.get<std::string>();
        info.protons = stmt.get<double>();
        info.detectorsSetupId = stmt.get<int64_t>();
        if (info.detectorsSetupId <= 0)
            info.detectorsSetupId = -1;
        info.materialsSetupId = stmt.get<int64_t>();
        if (info.materialsSetupId <= 0)
            info.materialsSetupId = -1;
    }
    else
    {
        check(rc, "failed to select run");
    }
}

void SQLiteDatabase::getRunStatus(RunNumber runNumber,
                                  RunInfo::Status &status,
                                  RunInfo::DataStatus &dataStatus)
{
    StmtGuard stmt(getStatement(GetRunStatus,
                                "SELECT status, dataStatus FROM runs "
                                "WHERE runNumber = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    int rc = stmt.step();
    if (rc == SQLITE_DONE)
    {
        // no rows to process, it means no results
        EACS_THROW(
            "run " + std::to_string(runNumber) + " not found in database!",
            ERR_DB_ENOENT);
    }
    else if (rc == SQLITE_ROW)
    {
        status = static_cast<RunInfo::Status>(stmt.get<int64_t>());
        dataStatus = static_cast<RunInfo::DataStatus>(stmt.get<int64_t>());
    }
    else
    {
        check(rc, "failed to select run");
    }
}

void SQLiteDatabase::updateRunTitleAndDescription(const RunInfo &info)
{
    StmtGuard stmt(getStatement(UpdateRun,
                                "UPDATE runs SET title = ?, "
                                "description = ? "
                                "WHERE runNumber = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(info.title);
    stmt.bind(info.description);
    stmt.bind(info.runNumber);

    int rc = stmt.step();
    check(rc, "failed to update run");
    dbUpdatedSignal();
}

void SQLiteDatabase::updateRunStatus(RunNumber runNumber,
                                     RunInfo::Status status,
                                     RunInfo::DataStatus dataStatus)
{
    {
        StmtGuard stmt(getStatement(UpdateRunStatus,
                                    "UPDATE runs SET status = ?, "
                                    "dataStatus = ? "
                                    "WHERE runNumber = ?"));

        if (!stmt)
            EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

        stmt.bind(status);
        stmt.bind(dataStatus);
        stmt.bind(runNumber);

        int rc = stmt.step();
        check(rc, "failed to update run status");
    }

    if (status != RunInfo::Status::Ongoing)
    {
        const std::time_t stop = time(nullptr);
        StmtGuard stmt(getStatement(UpdateRunStatusStop,
                                    "UPDATE runs SET stop = ? "
                                    "WHERE runNumber = ? and stop IS NULL"));

        if (!stmt)
            EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

        stmt.bind(stop * 1000);
        stmt.bind(runNumber);

        int rc = stmt.step();
        check(rc, "failed to update run stop");
    }
    dbUpdatedSignal();
}

void SQLiteDatabase::updatePreviousRuns()
{
    StmtGuard stmt(getStatement(UpdatePreviousRuns,
                                "UPDATE runs "
                                "SET status = ?, "
                                "  dataStatus = ?, "
                                "  stop = (COALESCE( "
                                "    (SELECT MAX(cycleStamp) FROM triggers "
                                "WHERE triggers.runNumber = runs.runNumber), "
                                "    start"
                                "  )) "
                                "WHERE stop IS NULL "
                                "AND runNumber >= ? AND runNumber <= ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(RunInfo::Status::Crashed);
    stmt.bind(RunInfo::DataStatus::DataUnknown);
    const ExperimentArea &area = ExperimentArea::findArea(
        Config::instance().getExperimentalArea());
    stmt.bind(area.min);
    stmt.bind(area.max);

    int rc = stmt.step();
    check(rc, "failed to update run stop");
    dbUpdatedSignal();
}

RunNumber SQLiteDatabase::getLatestRun()
{
    StmtGuard stmt(getStatement(LatestRun,
                                "SELECT runNumber FROM runs WHERE "
                                "runNumber >= ? AND runNumber <= ? "
                                "ORDER BY runNumber DESC LIMIT 1"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    const ExperimentArea &area = ExperimentArea::findArea(
        Config::instance().getExperimentalArea());
    stmt.bind(area.min);
    stmt.bind(area.max);
    int rc = stmt.step();
    if (rc == SQLITE_ROW)
    {
        return stmt.get<RunNumber>();
    }
    else
    {
        check(rc, "failed to get latest run");
    }
    return RunInfo::InvalidRunNumber;
}

void SQLiteDatabase::addProtons(RunNumber runNumber, float protons)
{
    StmtGuard stmt(getStatement(UpdateProtons,
                                "UPDATE runs SET protons = protons + ? "
                                "WHERE runNumber = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(protons);
    stmt.bind(runNumber);

    int rc = stmt.step();
    check(rc, "failed to add protons");
    dbUpdatedSignal();
}

void SQLiteDatabase::insertTrigger(RunNumber runNumber,
                                   EventNumber eventNumber,
                                   BeamType beamType,
                                   float intensity,
                                   int64_t cycleStamp)
{
    StmtGuard stmt(getStatement(
        InsertTrigger,
        "INSERT INTO triggers "
        "(runNumber,cycleStamp,eventNumber,beamType,intensity) VALUES "
        "(?,?,?,?,?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    /* cycleStamp is in nanos, let's move to millis */
    stmt.bind(cycleStamp / 1000000);
    stmt.bind(eventNumber);
    stmt.bind(beamType);
    stmt.bind(intensity);

    int rc = stmt.step();
    check(rc, "failed to insert trigger");
    dbUpdatedSignal();
}

void SQLiteDatabase::insertDaqConfig(RunNumber runNumber,
                                     uint32_t location,
                                     const DaqParametersDatabase &params)
{
    int64_t daqiId = getDaqInstrument(location);
    int64_t configId = createDaqConfig(runNumber, daqiId, params.isEnabled());
    insertParams(configId, params);
}

bool SQLiteDatabase::isMaterialsSetup(int64_t id)
{
    StmtGuard stmt(getStatement(
        IsMaterialsSetup,
        "SELECT EXISTS(SELECT 1 FROM materials_setups WHERE id = ? LIMIT 1)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(id);
    int rc = stmt.step();
    if (rc == SQLITE_ROW)
    {
        return stmt.get<int64_t>() == 1;
    }
    else
    {
        check(rc, "failed to get materials setup");
        return false;
    }
}

void SQLiteDatabase::createMaterialsSetup(const SetupObject &info)
{
    int rc;
    StmtGuard stmt(getStatement(CreateMaterialsSetup,
                                "INSERT INTO materials_setups "
                                "(id, name, description, earNumber, operYear) "
                                "VALUES (?, ?, ?, ?, ?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(info.id);
    stmt.bind(info.name);
    stmt.bind(info.description);
    stmt.bind(info.ear_number);
    stmt.bind(info.operational_year);

    rc = stmt.step();
    check(rc, "failed to insert material setups");
    dbUpdatedSignal();
}

void SQLiteDatabase::getFiltersPosition(
    int64_t materialsSetupId,
    std::map<std::size_t, bool> &filtersPosition)
{
    StmtGuard stmt(
        getStatement(GetFiltersPosition,
                     "SELECT position, status FROM rel_materials_setups WHERE "
                     "materialsSetupId = ? AND type = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(materialsSetupId);
    stmt.bind(int64_t(MaterialType::FILTER));

    filtersPosition.clear();
    int rc;
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        std::size_t position = std::size_t(stmt.get<int64_t>());
        filtersPosition[position] = stmt.get<bool>();
    }
    check(rc, "failed to get filters position");
}

void SQLiteDatabase::getSamplePosition(int64_t materialsSetupId,
                                       int32_t &position)
{
    StmtGuard stmt(
        getStatement(GetSamplePosition,
                     "SELECT position FROM rel_materials_setups WHERE "
                     "materialsSetupId = ? AND type = ? AND status = 1"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(materialsSetupId);
    stmt.bind(int64_t(MaterialType::SAMPLE));

    int rc = stmt.step();
    if (rc == SQLITE_ROW)
    {
        position = stmt.get<int64_t>();
    }
    else
    {
        position = -1;
    }
    check(rc, "failed to get sample position");
}

void SQLiteDatabase::insertFilterPosition(int64_t materialsSetupId,
                                          std::size_t filter,
                                          bool position)
{
    StmtGuard stmt(
        getStatement(InsertFilterPosition,
                     "INSERT INTO rel_materials_setups "
                     "(materialId,materialsSetupId,position,status) VALUES "
                     "(?,?,?,?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(1);
    stmt.bind(materialsSetupId);
    stmt.bind(filter + 1);
    stmt.bind(position ? 1 : 0);

    int rc = stmt.step();
    check(rc, "failed to insert filter position");
    dbUpdatedSignal();
}

void SQLiteDatabase::insertHVChannelInfo(RunNumber runNumber,
                                         const HVBoardInfo &board,
                                         const HVChannelInfo &channel)
{
    StmtGuard stmt(
        getStatement(InsertHVChannelInfo,
                     "INSERT INTO hv "
                     "(runNumber,slotId,channelId,serialNumber,name,v0set,"
                     "i0set,trip,rup,rdown) VALUES (?,?,?,?,?,?,?,?,?,?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    stmt.bind(board.index());
    stmt.bind(channel.index());
    stmt.bind(board.serialNumber());
    stmt.bind(channel.name());
    stmt.bind(channel.v0Set());
    stmt.bind(channel.i0Set());
    stmt.bind(channel.trip());
    stmt.bind(channel.rampUp());
    stmt.bind(channel.rampDown());

    int rc = stmt.step();
    check(rc, "failed to insert hv channel info");
    dbUpdatedSignal();
}

bool SQLiteDatabase::isDetectorsSetup(int64_t id)
{
    StmtGuard stmt(getStatement(
        IsDetectorsSetup,
        "SELECT EXISTS(SELECT 1 FROM detectors_setups WHERE id = ? LIMIT 1)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(id);
    int rc = stmt.step();
    if (rc == SQLITE_ROW)
    {
        return stmt.get<int64_t>() == 1;
    }
    else
    {
        check(rc, "failed to get detectors setup");
        return false;
    }
}

void SQLiteDatabase::createDetectorsSetup(const SetupObject &info)
{
    int rc;
    StmtGuard stmt(getStatement(CreateDetectorsSetup,
                                "INSERT INTO detectors_setups "
                                "(id, name, description, earNumber, operYear) "
                                "VALUES (?, ?, ?, ?, ?)"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(info.id);
    stmt.bind(info.name);
    stmt.bind(info.description);
    stmt.bind(info.ear_number);
    stmt.bind(info.operational_year);

    rc = stmt.step();
    check(rc, "failed to insert detector setups");
    dbUpdatedSignal();
}

int64_t SQLiteDatabase::getDaqInstrument(uint32_t location)
{
    uint8_t crate = ChannelConfig::locationGetChassis(location);
    uint8_t card = ChannelConfig::locationGetModule(location);
    uint8_t channel = ChannelConfig::locationGetChannel(location);

    int rc;
    StmtGuard stmt(getStatement(GetDaqInstrument,
                                "SELECT id FROM daqInstruments WHERE "
                                "crate = ? AND module = ? AND channel = ? "
                                "LIMIT 1"));
    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(crate);
    stmt.bind(card);
    stmt.bind(channel);
    rc = stmt.step();
    if (rc == SQLITE_ROW)
        return stmt.get<int64_t>();

    {
        StmtGuard insertStmt(getStatement(InsertDaqInstrument,
                                          "INSERT INTO daqInstruments "
                                          "(crate, module, channel, stream) "
                                          "VALUES (?, ?, ?, ?)"));
        if (!insertStmt)
            EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);
        insertStmt.bind(crate);
        insertStmt.bind(card);
        insertStmt.bind(channel);
        insertStmt.bind(1);
        rc = insertStmt.step();
        check(rc, "failed to insert daqInstrument");
    }

    stmt.reset();
    stmt.bind(crate);
    stmt.bind(card);
    stmt.bind(channel);
    rc = stmt.step();
    if (rc == SQLITE_ROW)
        return stmt.get<int64_t>();
    else
    {
        check(rc, "failed to get daqInstrument");
        EACS_THROW("failed to get daqInstrument", ERR_DB);
    }
}

int64_t SQLiteDatabase::createDaqConfig(RunNumber runNumber,
                                        int64_t daqId,
                                        bool enabled)
{
    StmtGuard stmt(getStatement(CreateDaqConfig,
                                "INSERT INTO configurations "
                                "(runNumber, status, daqInstrument) "
                                "VALUES (?, ?, ?)"));
    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    stmt.bind(enabled ? 1 : 0);
    stmt.bind(daqId);
    int rc = stmt.step();
    check(rc, "failed to create configuration");
    return getDaqConfig(runNumber, daqId);
}

int64_t SQLiteDatabase::getDaqConfig(RunNumber runNumber, int64_t daqId)
{
    StmtGuard stmt(getStatement(GetDaqConfig,
                                "SELECT id FROM configurations "
                                "WHERE runNumber = ? AND daqInstrument = ?"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    stmt.bind(runNumber);
    stmt.bind(daqId);
    int rc = stmt.step();
    if (rc == SQLITE_ROW)
        return stmt.get<int64_t>();
    else
        EACS_THROW("failed to get configuration", ERR_DB);
}

void SQLiteDatabase::insertParams(int64_t configId,
                                  const DaqParametersDatabase &db)
{
    StmtGuard paramStmt(getStatement(InsertParam,
                                     "INSERT OR IGNORE INTO daqParameters "
                                     "(idx, name, value) VALUES (?, ?, ?)"));
    StmtGuard relStmt(
        getStatement(InsertRelConfParam,
                     "INSERT INTO relConfParam (paramId, confId) "
                     "VALUES ((SELECT id FROM daqParameters WHERE "
                     "idx = ? AND name = ? AND value = ?), ?)"));

    if (!paramStmt || !relStmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    for (const DaqParametersDatabase::ConfigMap::value_type &it : db.values())
    {
        const std::string name = DaqParametersDatabase::toString(it.first);
        paramStmt.bind(it.first);
        paramStmt.bind(name);
        paramStmt.bind(it.second);
        int rc = paramStmt.step();
        check(rc, "failed to insert daqParameter");
        paramStmt.reset();

        relStmt.bind(it.first);
        relStmt.bind(name);
        relStmt.bind(it.second);
        relStmt.bind(configId);
        rc = relStmt.step();
        check(rc, "failed to insert relConfParam");
        relStmt.reset();
    }
}

int64_t SQLiteDatabase::countDaqParams()
{
    StmtGuard stmt(
        getStatement(CountParams, "SELECT count(*) FROM daqParameters"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    int rc = stmt.step();
    if (rc == SQLITE_ROW)
        return stmt.get<int64_t>();
    else
        EACS_THROW("failed to count daqParameters", ERR_DB);
}

int64_t SQLiteDatabase::countHVChannelInfo()
{
    StmtGuard stmt(getStatement(CountParams, "SELECT count(*) FROM hv"));

    if (!stmt)
        EACS_THROW("failed to prepare statement", ERR_DB_EILSEQ);

    int rc = stmt.step();
    if (rc == SQLITE_ROW)
        return stmt.get<int64_t>();
    else
        EACS_THROW("failed to count hv channel info", ERR_DB);
}

bool SQLiteDatabase::check(int rc, const char *msg, bool nothrow)
{
    switch (rc)
    {
    case SQLITE_OK:
    case SQLITE_DONE:
    case SQLITE_ROW: return true;
    default: {
        const std::string errorMsg = msg + std::string(": ") +
            sqlite3_errmsg(m_db);
        LOG(ERROR) << "[SQLiteDatabase] " << errorMsg;

        if (!nothrow)
            EACS_THROW(errorMsg, ERR_DB);
        return false;
    }
    }
}
