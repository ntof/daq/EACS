/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-18T09:34:03+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#ifndef DAQPARAMETERSDATABASE_HPP__
#define DAQPARAMETERSDATABASE_HPP__

#include <map>
#include <string>

namespace ntof {
namespace eacs {

class DaqChannelConfig;

class DaqParametersDatabase
{
public:
    DaqParametersDatabase();
    /**
     * @brief construct DaqParametersDatabase from live values
     * @throws EACSException on error
     */
    void load(const DaqChannelConfig &config);

    enum Index
    {
        detectorType = 1,
        detectorId = 2,
        sampleRate = 3,
        sampleSize = 4,
        preSample = 5,
        postSample = 6,
        delayTime = 7,
        fullScale = 8,
        offset = 9,
        threshold = 10,
        moduleType = 11,
        clockState = 12,
        thresholdSign = 13,
        revision = 14,
        impedance = 15,
        zsp_start = 16,
        // timeWin = 17, // can be computed, nothing to do in DB
        // lowerLimit = 18, // can be computed, nothing to do in DB
        masterDetectorType = 19,
        masterDetectorId = 20,
        cardSerialNumber = 21
    };
    static const std::string &toString(Index index);

    struct Entry
    {
        Index index;
        std::string name;
        std::string value;
    };
    typedef std::map<Index, std::string> ConfigMap;

    inline const ConfigMap &values() const { return m_values; }
    inline ConfigMap &values() { return m_values; }

    inline bool isEnabled() const { return m_enabled; }

protected:
    ConfigMap m_values;
    bool m_enabled;
};

} // namespace eacs
} // namespace ntof

#endif
