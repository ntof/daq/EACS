/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-08-19T08:30:58+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "DaqParametersDatabase.hpp"

#include <cstdio>

#include <DaqTypes.h>

#include "EACSException.hpp"
#include "EACSTypes.hpp"
#include "daq/DaqChannelConfig.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;

static const std::string s_empty;
static const std::map<DaqParametersDatabase::Index, std::string> s_indexNameMap{
    {DaqParametersDatabase::detectorType, "detectorType"},
    {DaqParametersDatabase::detectorId, "detectorId"},
    {DaqParametersDatabase::sampleRate, "sampleRate"},
    {DaqParametersDatabase::sampleSize, "sampleSize"},
    {DaqParametersDatabase::preSample, "preSample"},
    {DaqParametersDatabase::postSample, "postSample"},
    {DaqParametersDatabase::delayTime, "delayTime"},
    {DaqParametersDatabase::fullScale, "fullScale"},
    {DaqParametersDatabase::offset, "offset"},
    {DaqParametersDatabase::threshold, "threshold"},
    {DaqParametersDatabase::moduleType, "moduleType"},
    {DaqParametersDatabase::clockState, "clockState"},
    {DaqParametersDatabase::thresholdSign, "thresholdSign"},
    {DaqParametersDatabase::revision, "revision"},
    {DaqParametersDatabase::impedance, "impedance"},
    {DaqParametersDatabase::zsp_start, "Zero suppression start"},
    {DaqParametersDatabase::masterDetectorType, "masterDetectorType"},
    {DaqParametersDatabase::masterDetectorId, "masterDetectorId"},
    {DaqParametersDatabase::cardSerialNumber, "cardSerialNumber"}};

static std::string floatToString(float f)
{
    char buf[40];
    std::snprintf(buf, 40, "%G", f);
    return std::string(buf);
}

DaqParametersDatabase::DaqParametersDatabase() : m_enabled(false)
{
    m_values[revision] = std::to_string(ModuleHeader::REVISION);
}

void DaqParametersDatabase::load(const DaqChannelConfig &config)
{
    DIMParamListClient *client = config.client();
    if (!client)
        EACS_THROW("Failed to get client from DaqChannelConfig", ERR_INTERNAL);

    const DIMData::List dataList = client->getParameters();
    for (const DIMData &data : dataList)
    {
        switch (DaqChannelConfig::Params(data.getIndex()))
        {
        case DaqChannelConfig::IS_CHANNEL_ENABLED:
            m_enabled = data.getBoolValue();
            break;
        case DaqChannelConfig::DETECTOR_TYPE:
            m_values[detectorType] = data.getStringValue();
            break;
        case DaqChannelConfig::DETECTOR_ID:
            m_values[detectorId] = std::to_string(data.getValue<uint32_t>());
            break;
        case DaqChannelConfig::MODULE_TYPE:
            m_values[moduleType] = data.getStringValue();
            break;
        case DaqChannelConfig::CHANNEL_ID:
        case DaqChannelConfig::MODULE_ID:
        case DaqChannelConfig::CHASSIS_ID:
        case DaqChannelConfig::STREAM_ID: break;
        case DaqChannelConfig::SAMPLE_RATE:
            m_values[sampleRate] = floatToString(data.getValue<float>());
            break;
        case DaqChannelConfig::SAMPLE_SIZE:
            m_values[sampleSize] = std::to_string(data.getValue<uint32_t>());
            break;
        case DaqChannelConfig::FULL_SCALE:
            m_values[fullScale] = floatToString(data.getValue<float>());
            break;
        case DaqChannelConfig::DELAY_TIME:
            m_values[delayTime] = std::to_string(data.getValue<int32_t>());
            break;
        case DaqChannelConfig::THRESHOLD:
            m_values[threshold] = floatToString(data.getValue<float>());
            break;
        case DaqChannelConfig::THRESHOLD_SIGN:
            m_values[thresholdSign] = std::to_string(data.getValue<int32_t>());
            break;
        case DaqChannelConfig::ZSP_START:
            m_values[zsp_start] = std::to_string(data.getValue<uint32_t>());
            break;
        case DaqChannelConfig::OFFSET:
            m_values[offset] = floatToString(data.getValue<float>());
            break;
        case DaqChannelConfig::PRE_SAMPLES:
            m_values[preSample] = std::to_string(data.getValue<uint32_t>());
            break;
        case DaqChannelConfig::POST_SAMPLES:
            m_values[postSample] = std::to_string(data.getValue<uint32_t>());
            break;
        case DaqChannelConfig::CLOCK_STATE:
            m_values[clockState] = data.getStringValue();
            break;
        case DaqChannelConfig::INPUT_IMPEDANCE:
            m_values[impedance] = floatToString(data.getValue<float>());
            break;
        }
    }
}

const std::string &DaqParametersDatabase::toString(
    DaqParametersDatabase::Index index)
{
    std::map<DaqParametersDatabase::Index, std::string>::const_iterator it =
        s_indexNameMap.find(index);

    if (it == s_indexNameMap.end())
        return s_empty;
    else
        return it->second;
}
