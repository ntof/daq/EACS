/*
 * EACSDatabase.hpp
 *
 * Created on: Oct 26, 2015
 * Author: mdonze
 */

#ifndef Database_H_
#define Database_H_

#include <map>
#include <set>

#include <easylogging++.h>
#include <occi.h>
#include <pugixml.hpp>

#include "Database.hpp"

namespace ntof {
namespace eacs {

/**
 * CASTOR database to store/retrieve files association
 */
class OracleDatabase : public Database
{
public:
    typedef std::shared_ptr<oracle::occi::Environment> DBEnvPtr;
    typedef std::shared_ptr<oracle::occi::StatelessConnectionPool> DBConnPoolPtr;

    OracleDatabase();
    virtual ~OracleDatabase();

    virtual const std::string &name() const override;

    virtual void createRun(const RunInfo &info, std::time_t start) override;

    virtual void updateRunTitleAndDescription(const RunInfo &info) override;

    virtual void updateRunStatus(RunNumber runNumber,
                                 RunInfo::Status status,
                                 RunInfo::DataStatus dataStatus) override;

    virtual void updatePreviousRuns() override;

    virtual void getRunInfo(RunNumber runNumber, RunInfo &info) override;

    virtual void getRunStatus(RunNumber runNumber,
                              RunInfo::Status &status,
                              RunInfo::DataStatus &dataStatus) override;

    virtual RunNumber getLatestRun() override;

    virtual void addProtons(RunNumber runNumber, float totalProtons) override;

    virtual void insertTrigger(RunNumber runNumber,
                               EventNumber eventNumber,
                               BeamType beamType,
                               float intensity,
                               int64_t cycleStamp) override;

    virtual void insertDaqConfig(RunNumber runNumber,
                                 uint32_t location,
                                 const DaqParametersDatabase &params) override;
    virtual int64_t countDaqParams() override;

    virtual bool isMaterialsSetup(int64_t id) override;
    virtual bool isDetectorsSetup(int64_t id) override;

    virtual void getFiltersPosition(
        int64_t materialsSetupId,
        std::map<std::size_t, bool> &filtersPosition) override;

    virtual void getSamplePosition(int64_t materialsSetupId,
                                   int32_t &position) override;

    virtual void insertHVChannelInfo(RunNumber runNumber,
                                     const HVBoardInfo &board,
                                     const HVChannelInfo &channel) override;

    virtual int64_t countHVChannelInfo() override;

    /**
     * @brief find or create the given daq instrument
     * @return the DaqInstrument id
     */
    int64_t getDaqInstrument(uint32_t location);

    /**
     * @brief create an entry for a given daq instrument
     * @return the Daq Config id
     */
    int64_t createDaqConfig(RunNumber runNumber, int64_t daqiId, bool enabled);

    void insertParams(int64_t configId, const DaqParametersDatabase &db);

protected:
    /**
     * Helper to create a self-destructable statement
     */
    class Statement
    {
    public:
        explicit Statement(const DBConnPoolPtr &dbConPool);
        Statement(const DBConnPoolPtr &dbConPool, const std::string &sql);
        virtual ~Statement();

        inline oracle::occi::Statement *get() { return m_stmt; }
        inline oracle::occi::Statement *operator->() const { return m_stmt; };

    protected:
        const DBConnPoolPtr &m_dbConPool;
        oracle::occi::Connection *m_dbCon;
        oracle::occi::Statement *m_stmt;
    };

    /**
     * Initialize DB connection
     * @param dbServer
     * @param dbUser
     * @param dbPass
     */
    void initialize(const std::string &dbServer,
                    const std::string &dbUser,
                    const std::string &dbPass);

    /**
     * Destroy all Oracle connections
     */
    void destroyConnection();

    /**
     * @brief It reconnects to oracle
     * @param dbConPool pointer reference to connection pool
     */
    void reconnect(DBConnPoolPtr &dbConPool);

    template<typename TReturn>
    TReturn executeRetry(DBConnPoolPtr &dbConPool,
                         size_t retry,
                         std::function<TReturn()> lambda);

    DBEnvPtr m_dbEnv;
    DBConnPoolPtr m_dbConPool;
    std::mutex m_poolLock;

    std::string m_userName;
    std::string m_pass;
    std::string m_dbName;
};

} // namespace eacs
} /* namespace ntof */

#endif /* Database_H_ */
