/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-02T15:47:13+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "SQLiteHelper.hpp"

#include <ctime>

namespace sqlite {

StmtGuard::StmtGuard(SharedStmt &stmt) :
    autoIndex(0), m_locked(false), stmt(stmt)
{
    stmt.lock();
}

StmtGuard::~StmtGuard()
{
    sqlite3_reset(stmt.stmt);
    stmt.unlock();
}

int StmtGuard::step()
{
    autoIndex = 0;
    return sqlite3_step(stmt.stmt);
}

void StmtGuard::reset()
{
    autoIndex = 0;
    sqlite3_reset(stmt.stmt);
}

int StmtGuard::bind(int64_t value, int idx)
{
    return sqlite3_bind_int64(stmt.stmt, (idx < 0) ? ++autoIndex : idx, value);
}

int StmtGuard::bind(const std::string &value, int idx)
{
    return sqlite3_bind_text(stmt.stmt, (idx < 0) ? ++autoIndex : idx,
                             value.c_str(), value.length(), nullptr);
}

int StmtGuard::bindNull(int idx)
{
    return sqlite3_bind_null(stmt.stmt, (idx < 0) ? ++autoIndex : idx);
}

template<>
int32_t StmtGuard::get<>(int idx)
{
    return sqlite3_column_int(stmt.stmt, (idx < 0) ? autoIndex++ : idx);
}

template<>
uint32_t StmtGuard::get<>(int idx)
{
    return static_cast<uint32_t>(
        sqlite3_column_int64(stmt.stmt, (idx < 0) ? autoIndex++ : idx));
}

template<>
int64_t StmtGuard::get<>(int idx)
{
    return sqlite3_column_int64(stmt.stmt, (idx < 0) ? autoIndex++ : idx);
}

template<>
bool StmtGuard::get<>(int idx)
{
    return sqlite3_column_int(stmt.stmt, (idx < 0) ? autoIndex++ : idx) != 0;
}

template<>
double StmtGuard::get<>(int idx)
{
    return sqlite3_column_double(stmt.stmt, (idx < 0) ? autoIndex++ : idx);
}

template<>
std::string StmtGuard::get<>(int idx)
{
    const char *str = reinterpret_cast<const char *>(
        sqlite3_column_text(stmt.stmt, (idx < 0) ? autoIndex++ : idx));
    return (str == nullptr) ? std::string() : std::string(str);
}

SharedStmt::SharedStmt(sqlite3_stmt *stmt) : stmt(stmt) {}

SharedStmt::~SharedStmt()
{
    if (stmt)
    {
        sqlite3_finalize(stmt);
    }
}

} // namespace sqlite
