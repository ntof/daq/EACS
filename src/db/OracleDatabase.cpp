/*
 * EACSDataBase.cpp
 *
 * Created on: Oct 26, 2015
 * Author: mdonze
 */

#include "OracleDatabase.hpp"

#include <cmath>
#include <sstream>

#include <DaqTypes.h>

#include "Config.hpp"
#include "DaqParametersDatabase.hpp"
#include "EACSException.hpp"
#include "hv/HVBoardInfo.hpp"
#include "hv/HVChannelInfo.hpp"

#define MAX_RETRY 3

using namespace ntof::eacs;

static const std::string s_name = "OracleDatabase";

/*
 * Fail-over callback for Oracle
 */
int taf_callback(oracle::occi::Environment *,
                 oracle::occi::Connection *,
                 void *,
                 oracle::occi::Connection::FailOverType foType,
                 oracle::occi::Connection::FailOverEventType foEvent)
{
    LOG(INFO)
        << "[OracleDatabase] got Oracle fail-over callback type:" << foType
        << " event:" << foEvent;
    switch (foEvent)
    {
    case oracle::occi::Connection::FO_ERROR: return OCI_FO_RETRY;
    case oracle::occi::Connection::FO_END:
    default: return 0;
    }
}

OracleDatabase::OracleDatabase() : Database()
{
    try
    {
        initialize(Config::instance().getOracleServer(),
                   Config::instance().getOracleUser(),
                   Config::instance().getOraclePass());
    }
    catch (EACSException &e)
    {
        LOG(ERROR) << "[OracleDatabase] init failed: " << e.what();
    }
}

OracleDatabase::~OracleDatabase()
{
    destroyConnection();
}

const std::string &OracleDatabase::name() const
{
    return s_name;
}

void OracleDatabase::destroyConnection()
{
    // Close connection if possible
    std::lock_guard<std::mutex> lock(m_poolLock);
    try
    {
        m_dbConPool.reset();
    }
    catch (...)
    {}

    // Destroy environment if possible
    try
    {
        m_dbEnv.reset();
    }
    catch (...)
    {}
}

void OracleDatabase::reconnect(DBConnPoolPtr &dbConPool)
{
    std::lock_guard<std::mutex> lock(m_poolLock);
    if (dbConPool == m_dbConPool)
    { // check if another thread already did the job
        LOG(INFO) << "[OracleDatabase] re-connecting to Oracle database: "
                  << m_dbName;
        m_dbConPool.reset(
            m_dbEnv->createStatelessConnectionPool(m_userName, m_pass, m_dbName,
                                                   10),
            [this](oracle::occi::StatelessConnectionPool *poolPtr) {
                m_dbEnv->terminateStatelessConnectionPool(poolPtr);
            });
    }
    dbConPool = m_dbConPool;
}

void OracleDatabase::initialize(const std::string &dbServer,
                                const std::string &dbUser,
                                const std::string &dbPass)
{
    std::lock_guard<std::mutex> lock(m_poolLock);
    m_userName = dbUser;
    m_pass = dbPass;
    m_dbName = dbServer;

    try
    {
        if (!m_dbEnv)
        {
            LOG(TRACE) << "[OracleDatabase] creating Oracle environment";
            m_dbEnv.reset(
                oracle::occi::Environment::createEnvironment(
                    oracle::occi::Environment::THREADED_MUTEXED),
                [this](oracle::occi::Environment *envPtr) {
                    oracle::occi::Environment::terminateEnvironment(envPtr);
                });
        }
        if (!m_dbConPool)
        {
            LOG(INFO) << "[OracleDatabase] connecting to Oracle database: "
                      << m_dbName;
            m_dbConPool.reset(
                m_dbEnv->createStatelessConnectionPool(m_userName, m_pass,
                                                       m_dbName, 10),
                [this](oracle::occi::StatelessConnectionPool *poolPtr) {
                    m_dbEnv->terminateStatelessConnectionPool(poolPtr);
                });
        }
    }
    catch (oracle::occi::SQLException &e)
    {
        EACS_THROW(e.getMessage(), ERR_DB);
    }
}

void OracleDatabase::createRun(const RunInfo &info, std::time_t start)
{
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this, &info, &start]() {
        LOG(DEBUG) << "[OracleDatabase] creating run " << info.runNumber;

        if (start == std::time_t(-1))
        {
            start = std::time(nullptr);
        }

        Statement stmt(m_dbConPool);
        stmt->setSQL(
            "INSERT INTO RUNS "
            "(RUN_NUMBER, RUN_START, RUN_STOP, RUN_TITLE, "
            "RUN_TITLE_ORIGINAL, "
            "RUN_DESCRIPTION, RUN_DESCRIPTION_ORIGINAL, RUN_CASTOR_FOLDER, "
            "RUN_RUN_STATUS, RUN_DATA_STATUS, RUN_TOTAL_PROTONS, "
            "RUN_DET_SETUP_ID, "
            "RUN_MAT_SETUP_ID) VALUES (:c1, :c2, null, :c3, :c4, :c5, :c6, "
            ":c7, "
            ":c8, "
            ":c9, 0, :c10, :c11)");

        stmt->setNumber(1, info.runNumber);
        stmt->setNumber(2, start * 1000);
        stmt->setString(3, info.title);
        stmt->setString(4, info.title);
        stmt->setString(5, info.description);
        stmt->setString(6, info.description);
        stmt->setString(7, info.experiment);
        stmt->setString(8, RunInfo::toString(RunInfo::Ongoing));
        stmt->setString(9, RunInfo::toString(RunInfo::DataUnknown));

        if (info.detectorsSetupId < 0)
            stmt->setNull(10, oracle::occi::Type::OCCINUMBER);
        else
            stmt->setNumber(10, info.detectorsSetupId);

        if (info.materialsSetupId < 0)
            stmt->setNull(11, oracle::occi::Type::OCCINUMBER);
        else
            stmt->setNumber(11, info.materialsSetupId);

        stmt->executeUpdate();
    });
}

void OracleDatabase::updateRunTitleAndDescription(const RunInfo &info)
{
    // Only Title and Description are allowed to be updated
    // All the other parameters are settled on create only.
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this, &info]() {
        LOG(DEBUG)
            << "[OracleDatabase] Updating runInfo for: " << info.runNumber;

        Statement stmt(m_dbConPool);
        stmt->setSQL("UPDATE RUNS "
                     "set RUN_TITLE = :c1, "
                     "RUN_DESCRIPTION = :c2 "
                     "WHERE RUN_NUMBER = :c3");
        stmt->setString(1, info.title);
        stmt->setString(2, info.description);
        stmt->setNumber(3, info.runNumber);
        stmt->executeUpdate();
    });
}

void OracleDatabase::updateRunStatus(RunNumber runNumber,
                                     RunInfo::Status status,
                                     RunInfo::DataStatus dataStatus)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY, [this, &runNumber, &status, &dataStatus]() {
            LOG(DEBUG)
                << "[OracleDatabase] Updating runStatus for: " << runNumber;

            Statement stmt(m_dbConPool);
            stmt->setSQL("UPDATE RUNS "
                         "set RUN_RUN_STATUS = :c1, "
                         "RUN_DATA_STATUS = :c2 "
                         "WHERE RUN_NUMBER = :c3");
            stmt->setString(1, RunInfo::toString(status));
            stmt->setString(2, RunInfo::toString(dataStatus));
            stmt->setNumber(3, runNumber);
            stmt->executeUpdate();
        });
    if (status != RunInfo::Status::Ongoing)
    {
        executeRetry<void>(
            m_dbConPool, MAX_RETRY, [this, &runNumber, &status, &dataStatus]() {
                const std::time_t stop = time(nullptr);
                LOG(DEBUG)
                    << "[OracleDatabase] Updating stop date for: " << runNumber;

                Statement stmt(m_dbConPool);
                stmt->setSQL("UPDATE RUNS "
                             "set RUN_STOP = :c1 "
                             "WHERE RUN_NUMBER = :c2 AND RUN_STOP IS NULL");
                stmt->setNumber(1, stop * 1000);
                stmt->setNumber(2, runNumber);
                stmt->executeUpdate();
            });
    }
}

void OracleDatabase::updatePreviousRuns()
{
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this]() {
        LOG(DEBUG) << "[OracleDatabase] Updating previous runs.";

        Statement stmt(m_dbConPool);
        stmt->setSQL("UPDATE RUNS "
                     "SET RUN_RUN_STATUS = :c1, "
                     "  RUN_DATA_STATUS = :c2, "
                     "  RUN_STOP = (COALESCE( "
                     "    (SELECT MAX(TRIG_CYCLESTAMP) FROM TRIGGERS WHERE "
                     "TRIG_RUN_NUMBER = RUN_NUMBER), "
                     "    RUN_START"
                     "  )) "
                     "WHERE RUN_STOP IS NULL "
                     "  AND RUN_NUMBER >= :c3 AND RUN_NUMBER <= :c4");
        stmt->setString(1, RunInfo::toString(RunInfo::Status::Crashed));
        stmt->setString(2, RunInfo::toString(RunInfo::DataStatus::DataUnknown));
        const ExperimentArea &area = ExperimentArea::findArea(
            Config::instance().getExperimentalArea());
        stmt->setNumber(3, area.min);
        stmt->setNumber(4, area.max);
        stmt->executeUpdate();
    });
}

void OracleDatabase::getRunInfo(RunNumber runNumber, RunInfo &info)
{
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this, &runNumber, &info]() {
        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT RUN_TITLE, RUN_DESCRIPTION, RUN_CASTOR_FOLDER, "
                     "RUN_TOTAL_PROTONS, RUN_DET_SETUP_ID, "
                     "RUN_MAT_SETUP_ID FROM RUNS WHERE RUN_NUMBER = :c1");

        stmt->setInt(1, runNumber);
        oracle::occi::ResultSet *rs = stmt->executeQuery();
        if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
        {
            info.runNumber = runNumber;
            info.title = rs->getString(1);
            info.description = rs->getString(2);
            info.experiment = rs->getString(3);
            info.protons = rs->getNumber(4);

            if (rs->isNull(5))
                info.detectorsSetupId = -1;
            else
                info.detectorsSetupId = rs->getNumber(5);

            if (rs->isNull(6))
                info.materialsSetupId = -1;
            else
                info.materialsSetupId = rs->getNumber(6);
        }
        else
        {
            stmt->closeResultSet(rs);
            EACS_THROW(
                "run " + std::to_string(runNumber) + " not found in database!",
                ERR_DB_ENOENT);
        }

        stmt->closeResultSet(rs);
    });
}

void OracleDatabase::getRunStatus(RunNumber runNumber,
                                  RunInfo::Status &status,
                                  RunInfo::DataStatus &dataStatus)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY, [this, &runNumber, &status, &dataStatus]() {
            Statement stmt(m_dbConPool);
            stmt->setSQL("SELECT RUN_RUN_STATUS, RUN_DATA_STATUS "
                         "FROM RUNS WHERE RUN_NUMBER = :c1");

            stmt->setInt(1, runNumber);
            oracle::occi::ResultSet *rs = stmt->executeQuery();
            if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
            {
                status = RunInfo::fromString<RunInfo::Status>(rs->getString(1));
                dataStatus = RunInfo::fromString<RunInfo::DataStatus>(
                    rs->getString(2));
            }
            else
            {
                stmt->closeResultSet(rs);
                EACS_THROW("run " + std::to_string(runNumber) +
                               " not found in database!",
                           ERR_DB_ENOENT);
            }

            stmt->closeResultSet(rs);
        });
}

RunNumber OracleDatabase::getLatestRun()
{
    return executeRetry<RunNumber>(m_dbConPool, MAX_RETRY, [this]() {
        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT RUN_NUMBER FROM RUNS WHERE "
                     "RUN_NUMBER >= :c1 AND RUN_NUMBER <= :c2 AND ROWNUM <= 1 "
                     "ORDER BY RUN_NUMBER DESC");

        RunNumber ret = RunInfo::InvalidRunNumber;
        const ExperimentArea &area = ExperimentArea::findArea(
            Config::instance().getExperimentalArea());
        stmt->setNumber(1, area.min);
        stmt->setNumber(2, area.max);
        oracle::occi::ResultSet *rs = stmt->executeQuery();
        if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
        {
            ret = rs->getNumber(1);
        }
        stmt->closeResultSet(rs);
        return ret;
    });
}

void OracleDatabase::addProtons(RunNumber runNumber, float protons)
{
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this, &runNumber, &protons]() {
        LOG(DEBUG) << "[OracleDatabase] Updating the total number of protons "
                      "of the run "
                   << runNumber;

        Statement stmt(m_dbConPool);
        stmt->setSQL("UPDATE RUNS set RUN_TOTAL_PROTONS = "
                     "NVL(RUN_TOTAL_PROTONS, 0) + :c1 "
                     "WHERE RUN_NUMBER = :c2");

        stmt->setFloat(1, protons);
        stmt->setNumber(2, runNumber);
        stmt->executeUpdate();
    });
}

void OracleDatabase::insertTrigger(RunNumber runNumber,
                                   EventNumber eventNumber,
                                   BeamType beamType,
                                   float intensity,
                                   int64_t cycleStamp)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY,
        [this, &runNumber, &eventNumber, &beamType, &intensity, &cycleStamp]() {
            Statement stmt(m_dbConPool);
            stmt->setSQL(
                "INSERT INTO TRIGGERS (TRIG_SOURCE, TRIG_CYCLESTAMP, "
                "TRIG_TRIGGER_NUMBER, TRIG_INTENSITY, TRIG_RUN_NUMBER) "
                "VALUES (:c1, :c2, :c3, :c4, :c5)");
            stmt->setString(1, toString(beamType));
            /* cycleStamp is in nanos, let's move to millis */
            stmt->setNumber(2, cycleStamp / 1000000);
            stmt->setNumber(3, eventNumber);
            stmt->setFloat(4, intensity);
            stmt->setNumber(5, runNumber);

            stmt->executeUpdate();
        });
}

void OracleDatabase::insertDaqConfig(RunNumber runNumber,
                                     uint32_t location,
                                     const DaqParametersDatabase &params)
{
    int64_t daqiId = getDaqInstrument(location);
    int64_t configId = createDaqConfig(runNumber, daqiId, params.isEnabled());
    insertParams(configId, params);
}

int64_t OracleDatabase::countDaqParams()
{
    return executeRetry<int64_t>(m_dbConPool, MAX_RETRY, [this]() {
        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT COUNT(*) FROM DAQ_PARAMETERS");

        oracle::occi::ResultSet *rs = stmt->executeQuery();
        int64_t ret = 0;
        if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
        {
            ret = int64_t(rs->getNumber(1));
        }
        else
        {
            stmt->closeResultSet(rs);
            EACS_THROW("failed to count DAQ_PARAMETERS", ERR_DB_ENOENT);
        }
        stmt->closeResultSet(rs);
        return ret;
    });
}

bool OracleDatabase::isMaterialsSetup(int64_t id)
{
    return executeRetry<bool>(m_dbConPool, MAX_RETRY, [this, &id]() {
        LOG(DEBUG) << "[OracleDatabase] retrieving materials setup " << id;

        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT COUNT(*) "
                     "FROM MATERIALS_SETUPS "
                     "WHERE MAT_SETUP_ID = :c1 AND ROWNUM <= 1");

        stmt->setInt(1, id);
        oracle::occi::ResultSet *rs = stmt->executeQuery();
        bool found = false;
        while (rs->next())
        {
            found = rs->getInt(1) == 1;
        }
        stmt->closeResultSet(rs);
        return found;
    });
}

bool OracleDatabase::isDetectorsSetup(int64_t id)
{
    return executeRetry<bool>(m_dbConPool, MAX_RETRY, [this, &id]() {
        LOG(DEBUG) << "[OracleDatabase] retrieving detectors setup " << id;

        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT COUNT(*) "
                     "FROM DETECTORS_SETUPS "
                     "WHERE DET_SETUP_ID = :c1 AND ROWNUM <= 1");

        stmt->setInt(1, id);
        oracle::occi::ResultSet *rs = stmt->executeQuery();
        bool found = false;
        while (rs->next())
        {
            found = rs->getInt(1) == 1;
        }
        stmt->closeResultSet(rs);
        return found;
    });
}

void OracleDatabase::getFiltersPosition(
    int64_t materialsSetupId,
    std::map<std::size_t, bool> &filtersPosition)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY, [this, &materialsSetupId, &filtersPosition]() {
            LOG(DEBUG)
                << "[OracleDatabase] retrieving filters for materials setup "
                << materialsSetupId;

            Statement stmt(m_dbConPool);
            stmt->setSQL("SELECT MAT_POSITION, REL_MATERIALS_SETUPS.MAT_STATUS "
                         "FROM REL_MATERIALS_SETUPS "
                         " INNER JOIN MATERIALS ON "
                         " MATERIALS.MAT_ID = REL_MATERIALS_SETUPS.MAT_ID "
                         "WHERE MAT_SETUP_ID = :c1 AND MAT_TYPE = :c2");

            stmt->setInt(1, materialsSetupId);
            stmt->setString(2, toString(MaterialType::FILTER));
            oracle::occi::ResultSet *rs = stmt->executeQuery();
            filtersPosition.clear();
            while (rs->next())
            {
                filtersPosition[rs->getInt(1)] = (rs->getInt(2) > 0);
            }
            stmt->closeResultSet(rs);
        });
}

void OracleDatabase::getSamplePosition(int64_t materialsSetupId,
                                       int32_t &position)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY, [this, &materialsSetupId, &position]() {
            LOG(DEBUG)
                << "[OracleDatabase] retrieving sample for materials setup "
                << materialsSetupId;

            Statement stmt(m_dbConPool);
            stmt->setSQL("SELECT MAT_POSITION "
                         "FROM REL_MATERIALS_SETUPS "
                         " INNER JOIN MATERIALS ON "
                         " MATERIALS.MAT_ID = REL_MATERIALS_SETUPS.MAT_ID "
                         "WHERE MAT_SETUP_ID = :c1 AND MAT_TYPE = :c2 AND "
                         "REL_MATERIALS_SETUPS.MAT_STATUS = 1");

            stmt->setInt(1, materialsSetupId);
            stmt->setString(2, toString(MaterialType::SAMPLE));
            oracle::occi::ResultSet *rs = stmt->executeQuery();
            if (rs->next())
                position = rs->getInt(1);
            else
                position = -1;
            stmt->closeResultSet(rs);
        });
}

void OracleDatabase::insertHVChannelInfo(RunNumber runNumber,
                                         const HVBoardInfo &board,
                                         const HVChannelInfo &channel)
{
    executeRetry<void>(
        m_dbConPool, MAX_RETRY, [this, &runNumber, &board, &channel]() {
            Statement stmt(m_dbConPool);
            stmt->setSQL(
                "INSERT INTO HV_PARAMETERS(HV_SLOT_ID,HV_SERIAL_NUMBER,"
                "HV_CHANNEL_ID,HV_NAME,HV_VO_SET_VOLT,HV_IO_SET_AMP,HV_TRIP,"
                "HV_RAMP_UP,HV_RAMP_DOWN,HV_V_MAX,HV_RUN_NUMBER) "
                "VALUES (:c1,:c2,:c3,:c4,:c5,:c6,:c7,:c8,:c9,:c10,:c11)");
            stmt->setNumber(1, board.index());
            stmt->setNumber(2, board.serialNumber());
            stmt->setNumber(3, channel.index());
            stmt->setString(4, channel.name());

            if (isnanf(channel.v0Set()))
                stmt->setNull(5, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setFloat(5, channel.v0Set());

            if (isnanf(channel.i0Set()))
                stmt->setNull(6, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setFloat(6, channel.i0Set());

            if (isnanf(channel.trip()))
                stmt->setNull(7, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setFloat(7, channel.trip());

            if (isnanf(channel.rampUp()))
                stmt->setNull(8, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setFloat(8, channel.rampUp());

            if (isnanf(channel.rampDown()))
                stmt->setNull(9, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setFloat(9, channel.rampDown());

            if (isnanf(channel.vMax()))
                stmt->setNull(10, oracle::occi::Type::OCCINUMBER);
            else
                stmt->setNumber(10, channel.vMax());

            stmt->setNumber(11, runNumber);

            stmt->executeUpdate();
        });
}

int64_t OracleDatabase::countHVChannelInfo()
{
    return executeRetry<int64_t>(m_dbConPool, MAX_RETRY, [this]() {
        Statement stmt(m_dbConPool);
        stmt->setSQL("SELECT COUNT(*) FROM HV_PARAMETERS");

        oracle::occi::ResultSet *rs = stmt->executeQuery();
        int64_t ret = 0;
        if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
        {
            ret = int64_t(rs->getNumber(1));
        }
        else
        {
            stmt->closeResultSet(rs);
            EACS_THROW("failed to count HV_PARAMETERS", ERR_DB_ENOENT);
        }
        stmt->closeResultSet(rs);
        return ret;
    });
}

int64_t OracleDatabase::getDaqInstrument(uint32_t location)
{
    uint8_t crate = ChannelConfig::locationGetChassis(location);
    uint8_t card = ChannelConfig::locationGetModule(location);
    uint8_t channel = ChannelConfig::locationGetChannel(location);

    return executeRetry<int64_t>(
        m_dbConPool, MAX_RETRY, [this, &crate, &card, &channel]() {
            Statement stmt(m_dbConPool);
            stmt->setSQL("SELECT DAQI_ID FROM DAQ_INSTRUMENTS "
                         "WHERE DAQI_CRATE = :c1 AND DAQI_MODULE = :c2 "
                         "AND DAQI_CHANNEL = :c3");

            stmt->setInt(1, crate);
            stmt->setInt(2, card);
            stmt->setInt(3, channel);
            oracle::occi::ResultSet *rs = stmt->executeQuery();
            int64_t ret;
            if (rs->next() == oracle::occi::ResultSet::DATA_AVAILABLE)
            {
                ret = int64_t(rs->getNumber(1));
                stmt->closeResultSet(rs);
            }
            else
            {
                stmt->closeResultSet(rs);
                stmt->setSQL(
                    "INSERT INTO DAQ_INSTRUMENTS "
                    "(DAQI_CRATE, DAQI_MODULE, DAQI_CHANNEL, DAQI_STREAM) "
                    "VALUES (:c1, :c2, :c3, 1) "
                    "RETURNING DAQI_ID INTO :c4");
                stmt->setInt(1, crate);
                stmt->setInt(2, card);
                stmt->setInt(3, channel);
                stmt->registerOutParam(4, oracle::occi::OCCINUMBER);
                stmt->execute();
                ret = int64_t(stmt->getNumber(4));
            }
            return ret;
        });
}

int64_t OracleDatabase::createDaqConfig(RunNumber runNumber,
                                        int64_t daqiId,
                                        bool enabled)
{
    return executeRetry<int64_t>(
        m_dbConPool, MAX_RETRY, [this, &runNumber, &daqiId, &enabled]() {
            Statement stmt(m_dbConPool);
            stmt->setSQL("INSERT INTO CONFIGURATIONS "
                         "(CONF_STATUS, CONF_RUN_NUMBER, CONF_DAQI_ID) "
                         "VALUES (:c1, :c2, :c3) "
                         "RETURNING CONF_ID INTO :c4");
            stmt->setInt(1, enabled ? 1 : 0);
            stmt->setNumber(2, runNumber);
            stmt->setNumber(3, daqiId);
            stmt->registerOutParam(4, oracle::occi::OCCINUMBER);
            stmt->execute();
            return int64_t(stmt->getNumber(4));
        });
}

void OracleDatabase::insertParams(int64_t configId,
                                  const DaqParametersDatabase &db)
{
    executeRetry<void>(m_dbConPool, MAX_RETRY, [this, &configId, &db]() {
        for (const DaqParametersDatabase::ConfigMap::value_type &it :
             db.values())
        {
            Statement stmt(m_dbConPool);
            stmt->setSQL("BEGIN "
                         "INSERT INTO DAQ_PARAMETERS "
                         "(PARAM_INDEX, PARAM_NAME, PARAM_VALUE) "
                         "VALUES (:c1, :c2, :c3); "
                         "EXCEPTION WHEN dup_val_on_index then null; "
                         "END;");
            const std::string name = DaqParametersDatabase::toString(it.first);
            stmt->setInt(1, it.first);
            stmt->setString(2, name);
            stmt->setString(3, it.second);
            stmt->execute();

            Statement relStmt(m_dbConPool);
            relStmt->setSQL(
                "INSERT INTO REL_CONF_PARAMS (PARAM_ID, CONF_ID) "
                "VALUES ((SELECT PARAM_ID FROM DAQ_PARAMETERS WHERE "
                "PARAM_INDEX = :c1 AND PARAM_NAME = :c2 AND PARAM_VALUE = :c3),"
                " :c4)");
            relStmt->setInt(1, it.first);
            relStmt->setString(2, name);
            relStmt->setString(3, it.second);
            relStmt->setNumber(4, configId);
            relStmt->execute();
        }
    });
}

template<typename TReturn>
TReturn OracleDatabase::executeRetry(DBConnPoolPtr &dbConPool,
                                     size_t retry,
                                     std::function<TReturn()> lambda)
{
    try
    {
        return lambda();
    }
    catch (oracle::occi::SQLException &e)
    {
        LOG(ERROR)
            << "[OracleDatabase] retry:" << retry << " exception: " << e.what();
        if (retry <= 0)
        {
            EACS_THROW(e.getMessage(), ERR_DB);
        }
        else
        {
            reconnect(dbConPool);
            return executeRetry<TReturn>(dbConPool, --retry, lambda);
        }
    }
}

/**
 * Helper to create a self-destructable statement
 */
OracleDatabase::Statement::Statement(const DBConnPoolPtr &dbConPool) :
    m_dbConPool(dbConPool)
{
    m_dbCon = m_dbConPool->getAnyTaggedConnection();
    m_dbCon->setTAFNotify(taf_callback, NULL);
    m_stmt = m_dbCon->createStatement();
    m_stmt->setAutoCommit(true);
}

OracleDatabase::Statement::Statement(const DBConnPoolPtr &dbConPool,
                                     const std::string &sql) :
    m_dbConPool(dbConPool)
{
    m_dbCon = m_dbConPool->getAnyTaggedConnection();
    m_dbCon->setTAFNotify(taf_callback, NULL);
    m_stmt = m_dbCon->createStatement(sql);
    m_stmt->setAutoCommit(true);
}

OracleDatabase::Statement::~Statement()
{
    if (m_dbCon)
    {
        if (m_stmt)
        {
            m_dbCon->terminateStatement(m_stmt);
        }
        m_dbConPool->releaseConnection(m_dbCon);
    }
}
