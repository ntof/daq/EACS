/**
 *  occipool - Demontrating the Connection Pool interface of OCCI.
 *
 *  DESCRIPTION :
 *    This program demonstates the creating and using of connection pool in the
 *    database and fetching records of a table.
 *
 */

// #define _GLIBCXX_USE_CXX11_ABI 0
#include <iostream>

#include <occi.h>
using namespace oracle::occi;
using namespace std;

class occipool
{
private:
    Environment *env;
    Connection *con;
    Statement *stmt;

public:
    /**
     * Constructor for the occipool test case.
     */
    occipool()
    {
        env = Environment::createEnvironment(Environment::DEFAULT);
    } // end of constructor occipool ()

    /**
     * Destructor for the occipool test case.
     */
    ~occipool()
    {
        Environment::terminateEnvironment(env);
    } // end of ~occipool ()

    /**
     * The testing logic of the test case.
     */
    dvoid select()
    {
        cout << "occipool - Selecting records using ConnectionPool interface"
             << endl;
        const string poolUserName = "ntof";
        const string poolPassword = "aD3+JKf5A-XGh5d9U*6FD31*";
        const string connectString = "pdbr";
        const string username = "ntof";
        const string passWord = "aD3+JKf5A-XGh5d9U*6FD31*";
        unsigned int maxConn = 5;
        unsigned int minConn = 3;
        unsigned int incrConn = 2;
        StatelessConnectionPool *connPool = env->createStatelessConnectionPool(
            poolUserName, poolPassword, connectString, maxConn, minConn,
            incrConn);
        try
        {
            if (connPool)
                cout << "SUCCESS - createConnectionPool" << endl;
            else
                cout << "FAILURE - createConnectionPool" << endl;
            con = connPool->getConnection(username, passWord);
            if (con)
                cout << "SUCCESS - createConnection" << endl;
            else
                cout << "FAILURE - createConnection" << endl;
        }
        catch (SQLException ex)
        {
            cout << "Exception thrown for createConnectionPool" << endl;
            cout << "Error number: " << ex.getErrorCode() << endl;
            cout << ex.getMessage() << endl;
        }

        cout << "retrieving the data" << endl;
        stmt = con->createStatement(
            "SELECT RUN_TITLE FROM RUNS WHERE RUN_NUMBER = 7345");
        ResultSet *rset = stmt->executeQuery();
        while (rset->next())
        {
            cout << "author_name:" << rset->getString(1) << endl;
        }
        stmt->closeResultSet(rset);
        con->terminateStatement(stmt);
        connPool->terminateConnection(con);
        env->terminateStatelessConnectionPool(connPool);

        cout << "occipool - done" << endl;
    } // end of test (Connection *)

}; // end of class occipool

int main(void)
{
    occipool *demo = new occipool();

    demo->select();
    delete demo;

} // end of main ()
