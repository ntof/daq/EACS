/*
 * WriterFile.h
 *
 *Created on: Jan 29, 2015
 *Author: agiraud
 */

#ifndef WRITERFILE_H_
#define WRITERFILE_H_

#include <DaqTypes.h>
#include <Worker.hpp>

#include "misc.h"
#include "services/ServiceTiming.hpp"

#define MAX_SIZE 2000000000;

namespace ntof {
namespace eacs {
class Eacs;
class ClientAddhCmd;

/**
 * \class WriterFile
 * @brief class to write the .run and .event files. The class get all the
 * structures and organize them to write the index file
 */
class WriterFile
{
public:
    explicit WriterFile(Eacs &eacs, ClientAddhCmd &addhCmd);
    virtual ~WriterFile();

    /**
     * @brief actions to do when a bct event occurs
     * @param[in] event Timing details
     * @param[in] runNumber: The run number
     * @param[in] intensity number of protons
     */
    void onBctEvent(const TimingEventDetails &event,
                    RunNumber runNumber,
                    float intensity);

    /**
     * @brief actions to do when a new run starts
     */
    void onInitRun(const RunControlHeader &rctr, const ModuleHeader &modh);

    // Signals
    ErrorSignal errorSignal;
    WarningSignal warningSignal;

protected:
    /**
     * @brief Write the .run file
     */
    void writeFileRun(const RunControlHeader &rctr, const ModuleHeader &modh);

    /**
     * @brief Write the .event file
     */
    void writeFileEvent(const TimingEventDetails &event,
                        const RunNumber &runNumber,
                        float intensity);

    /**
     * @brief ensure the directory exists for the given filename
     */
    void createDirectory(const std::string &filename);

    /**
     * @brief Generate the full path for the .run file
     * @return Return the path
     */
    std::string generateFilenameRun();

    /**
     * @brief Generate the full path for the .event file
     * @return Return the path
     */
    std::string generateFilenameEvent(int32_t triggerNumber);

    Eacs &m_eacs;
    ClientAddhCmd &m_addhCmd;
    ntof::utils::Worker m_worker; //!< Worker used to execute tasks on EACS
};
} // namespace eacs
} // namespace ntof

#endif /* WRITERFILE_H_ */
