/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-12T15:21:00+02:00
**     Author: Gabriele De Blasi <gdeblasi> <gabriele.deblasi@cern.ch>
**
*/

#ifndef BEAMMONITOR_H_
#define BEAMMONITOR_H_

#include <chrono>
#include <list>
#include <mutex>
#include <string>

#include <Signals.hpp>
#include <Thread.hpp>

#include "misc.h"
#include "services/ServiceTiming.hpp"

namespace ntof {
namespace eacs {

/**
 * @class BeamMonitor
 * @brief Class meant to monitor the beam
 */
class BeamMonitor : public ntof::utils::Thread
{
public:
    typedef std::chrono::steady_clock::time_point time_point;

    BeamMonitor();
    ~BeamMonitor() override;

    class Tester
    {
    public:
        Tester(WarningCode code, const std::chrono::milliseconds &timeout);
        virtual ~Tester() = default;

        /**
         * @brief timeout check
         * @param now: time point against which to check
         */
        virtual bool check(const time_point &now) = 0;

        /**
         * @brief actions the tester has to perform upon a BCT event
         * @param event: Timing details
         * @param intensity: number of protons
         * @param now: time point when the BCT event occurs
         */
        virtual void onBctEvent(const TimingEventDetails &event,
                                float intensity,
                                const time_point &now) = 0;

        /**
         * @brief get the final instant of the warning timeout
         * @return final time point of warning timeout
         * @details must return time_point::max() when there's no deadline
         */
        virtual time_point deadline() const = 0;

        /**
         * @brief get the current warning message. An empty one if there is no
         * warning
         * @return warning message
         */
        std::string message() const
        {
            std::lock_guard<std::mutex> lock(m_lock);
            return m_message;
        }

        /**
         * @brief reset the tester
         */
        virtual void reset();

        const WarningCode code;
        const std::chrono::milliseconds timeout;

    protected:
        mutable std::mutex m_lock;
        std::string m_message; // warning message
        time_point m_lastOk;   // time point of the last successful BCT event or
                               // the last expired timeout
    };

    void reset();

    /**
     * @brief Actions to perform on a BTC event
     * @param event: Timing event details
     * @param intensity: number of protons
     */
    void onBctEvent(const TimingEventDetails &event, float intensity);

    // Signals
    WarningSignal warningSignal;

protected:
    /**
     * @brief Main thread function that performs checks on the beam
     */
    void thread_func() override;

    std::list<std::shared_ptr<Tester>> m_testers;
};

class LowBeamTester : public BeamMonitor::Tester
{
public:
    explicit LowBeamTester(const std::chrono::milliseconds &timeout);

    bool check(const BeamMonitor::time_point &now) override;
    void onBctEvent(const TimingEventDetails &event,
                    float intensity,
                    const BeamMonitor::time_point &now) override;
    BeamMonitor::time_point deadline() const override;
    void reset() override;

protected:
    float m_lastIntensity;
    BeamMonitor::time_point m_firstLow; /**< first low beam event */
};

class NoBeamTester : public BeamMonitor::Tester
{
public:
    explicit NoBeamTester(const std::chrono::milliseconds &timeout);

    bool check(const BeamMonitor::time_point &now) override;
    void onBctEvent(const TimingEventDetails &event,
                    float intensity,
                    const BeamMonitor::time_point &now) override;
    BeamMonitor::time_point deadline() const override;
};

} /* namespace eacs */
} /* namespace ntof */

#endif /* BEAMMONITOR_H_ */
