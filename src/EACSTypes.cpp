/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-06-15T11:14:01+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "EACSTypes.hpp"

#include <limits>
#include <map>

#include "misc.h" // For NOT_READY and ERROR states

using namespace ntof::eacs;

const RunNumber ntof::eacs::InvalidRunNumber =
    std::numeric_limits<RunNumber>::max();
const EventNumber ntof::eacs::InvalidEventNumber =
    std::numeric_limits<EventNumber>::max();
const std::vector<ErrorCode> ntof::eacs::RuntimeErrors{
    ErrorCode(-1) /* just in case */,
    ERR_INTERNAL,
    ERR_NO_DAQ,
    ERR_DB,
    ERR_DB_EILSEQ,
    ERR_DB_ENOENT,
    ERR_CMD,
    ERR_DAQ_STATE,
    ERR_RFM_ACK,
    ERR_RFM_CMD,
    ERR_FS_CMD,
    ERR_SAEX_CMD,
    ERR_TIMING_CMD,
    ERR_TIMING_NO_SUCH_EVENT,
    ERR_DAQCONF_CHANGED,
    ERR_DAQCONF_CMD,
    ERR_HVCONF_CHANGED};

static const std::string s_empty;
static const std::map<BeamType, std::string> s_beamTypeMap{
    {CALIBRATION, "CALIBRATION"},
    {PRIMARY, "PRIMARY"},
    {PARASITIC, "PARASITIC"}};

static const std::map<EacsState, std::string> s_eacsStateMap{
    {static_cast<EacsState>(NOT_READY), "NOT READY"},
    {static_cast<EacsState>(ERROR), "ERROR"},
    {LOADING, "LOADING"},
    {IDLE, "IDLE"},
    {FILTERS_INIT, "FILTERS_INIT"},
    {SAMPLES_INIT, "SAMPLES_INIT"},
    {COLLIMATOR_INIT, "COLLIMATOR_INIT"},
    {HIGHVOLTAGE_INIT, "HIGHVOLTAGE_INIT"},
    {DAQS_INIT, "DAQS_INIT"},
    {MERGER_INIT, "MERGER_INIT"},
    {STARTING, "STARTING"},
    {RUNNING, "RUNNING"},
    {STOPPING, "STOPPING"}};

/**
 * @details this must match db enum
 */
static const std::map<MaterialType, std::string> s_materialTypeMap{
    {MaterialType::FILTER, "filter"},
    {MaterialType::SAMPLE, "sample"},
    {MaterialType::SOURCE, "source"}};

const std::string &ntof::eacs::toString(BeamType beamType)
{
    std::map<BeamType, std::string>::const_iterator it = s_beamTypeMap.find(
        beamType);

    return (it != s_beamTypeMap.end()) ? it->second : s_empty;
}

BeamType ntof::eacs::fromString(const std::string &beamType)
{
    BeamType bt = BeamType::CALIBRATION;
    if (beamType == "PARASITIC")
    {
        bt = BeamType::PARASITIC;
    }
    else if (beamType == "PRIMARY")
    {
        bt = BeamType::PRIMARY;
    }
    return bt;
}

const std::string &ntof::eacs::toString(EacsState state)
{
    std::map<EacsState, std::string>::const_iterator it = s_eacsStateMap.find(
        state);

    return (it != s_eacsStateMap.end()) ? it->second : s_empty;
}

const std::string &ntof::eacs::toString(MaterialType type)
{
    std::map<MaterialType, std::string>::const_iterator it =
        s_materialTypeMap.find(type);

    return (it != s_materialTypeMap.end()) ? it->second : s_empty;
}
