/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-10-12T15:21:00+02:00
** Author: Gabriele De Blasi <gdeblasi> <gabriele.deblasi@cern.ch>
**
*/

#include "BeamMonitor.hpp"

#include <algorithm>
#include <condition_variable>
#include <limits>
#include <mutex>

#include <NTOFLogging.hpp>

#include "Config.hpp"

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

namespace ntof {
namespace eacs {

BeamMonitor::Tester::Tester(WarningCode code, const milliseconds &timeout) :
    code(code), timeout(timeout), m_lastOk(steady_clock::now())
{}

void BeamMonitor::Tester::reset()
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_message.clear();
    m_lastOk = steady_clock::now();
}

/* --- LOW BEAM Tester --- */
LowBeamTester::LowBeamTester(const milliseconds &timeout) :
    BeamMonitor::Tester(WARN_LOW_BEAM, timeout),
    m_lastIntensity(std::numeric_limits<decltype(m_lastIntensity)>::max()),
    m_firstLow(BeamMonitor::time_point::min())
{}

void LowBeamTester::reset()
{
    // There's already a NoBeam monitor, let's start with a valid beam
    std::lock_guard<std::mutex> lock(m_lock);
    m_lastIntensity = std::numeric_limits<decltype(m_lastIntensity)>::max();
    m_lastOk = steady_clock::now();
    m_firstLow = BeamMonitor::time_point::min();
    m_message.clear();
}

bool LowBeamTester::check(const BeamMonitor::time_point &now)
{
    std::lock_guard<std::mutex> lock(m_lock);
    const milliseconds elapsed = duration_cast<milliseconds>(now - m_firstLow);

    if ((elapsed > timeout) &&
        (m_lastIntensity < Config::instance().lowBeamThreshold()))
    {
        m_message = "[BeamMonitor]: the beam intensity is low: " +
            std::to_string(m_lastIntensity);
        return true;
    }
    else
    {
        m_message.clear();
        return false;
    }
}

BeamMonitor::time_point LowBeamTester::deadline() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    // we are in timeout mode and error has not yet been reported -> we have a
    // deadline that may be in the past
    if (m_lastIntensity < Config::instance().lowBeamThreshold() &&
        m_message.empty())
        return m_firstLow + timeout + std::chrono::milliseconds(1);
    else
        return BeamMonitor::time_point::max();
}

void LowBeamTester::onBctEvent(const TimingEventDetails &event,
                               float intensity,
                               const BeamMonitor::time_point &now)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (event.evtType == "CALIBRATION")
    {
        m_lastIntensity = Config::instance().lowBeamThreshold();
        m_lastOk = now;
    }
    else
    {
        if (intensity >= Config::instance().lowBeamThreshold())
            m_lastOk = now;
        else if (m_lastOk > m_firstLow) // first low-beam event since lastOk
            m_firstLow = now;
        m_lastIntensity = intensity;
    }
}

/* --- No Beam Tester --- */
NoBeamTester::NoBeamTester(const milliseconds &timeout) :
    BeamMonitor::Tester(WARN_NO_BEAM, timeout)
{}

bool NoBeamTester::check(const BeamMonitor::time_point &now)
{
    std::lock_guard<std::mutex> lock(m_lock);
    const milliseconds elapsed = duration_cast<milliseconds>(now - m_lastOk);

    if (elapsed > timeout)
    {
        m_message = "[BeamMonitor]: no beam for a long time: " +
            std::to_string(elapsed.count()) + " ms";
        return true;
    }
    else
    {
        m_message.clear();
        return false;
    }
}

BeamMonitor::time_point NoBeamTester::deadline() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    // already expired and checked, no timeout
    if (!m_message.empty())
        return BeamMonitor::time_point::max();
    return m_lastOk + timeout + std::chrono::milliseconds(1);
}

void NoBeamTester::onBctEvent(const TimingEventDetails &,
                              float,
                              const BeamMonitor::time_point &now)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_lastOk = now;
}

/* --- Beam Monitor --- */
BeamMonitor::BeamMonitor() : Thread("BeamMonitor")
{
    int32_t timeout = Config::instance().beamDetectionTimeout();
    if (timeout >= 0)
    {
        std::shared_ptr<NoBeamTester> tester(
            std::make_shared<NoBeamTester>(milliseconds(timeout)));
        m_testers.push_back(tester);
    }

    timeout = Config::instance().lowBeamTimeout();
    if (timeout >= 0)
    {
        std::shared_ptr<LowBeamTester> tester(
            std::make_shared<LowBeamTester>(milliseconds(timeout)));
        m_testers.push_back(tester);
    }
}

BeamMonitor::~BeamMonitor()
{
    stop();
}

void BeamMonitor::reset()
{
    LOG(INFO) << "[BeamMonitor]: reset";
    {
        for (std::shared_ptr<Tester> &t : m_testers)
        {
            t->reset();
            warningSignal(t->code, std::string());
        }
    }
}

void BeamMonitor::onBctEvent(const TimingEventDetails &event, float intensity)
{
    const time_point now = steady_clock::now();
    for (std::shared_ptr<Tester> &t : m_testers)
    {
        t->onBctEvent(event, intensity, now);
    }

    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_waked = true;
    }
    // wake up thread for beam checks
    m_cond.notify_all();
}

void BeamMonitor::thread_func()
{
    if (m_testers.empty())
    {
        m_started.store(false);
        LOG(WARNING) << "[BeamMonitor]: stopped (beam checks disabled)";
        return;
    }

    LOG(INFO) << "[BeamMonitor]: started";

    while (m_started.load())
    {
        // we could sleep forever (time_point::max()), this is just a security
        time_point deadline = steady_clock::now() + std::chrono::seconds(60);

        std::unique_lock<std::mutex> lock(m_mutex);
        m_waked = false; // reset for later check(s)

        for (std::shared_ptr<Tester> &t : m_testers)
        {
            time_point tp = t->deadline();
            if (tp < deadline)
                deadline = tp;
        }

        // wait until a BCT event occurs or the deadline is reached
        m_cond.wait_until(lock, deadline, [this]() { return m_waked; });

        const steady_clock::time_point now = steady_clock::now();

        for (std::shared_ptr<Tester> &t : m_testers)
        {
            t->check(now);

            lock.unlock();
            warningSignal(t->code, t->message());
            lock.lock();
        }
    }

    LOG(INFO) << "[BeamMonitor]: stopped";
}

} // namespace eacs
} // namespace ntof
