/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T09:03:43+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#ifndef EACSCLI_HPP__
#define EACSCLI_HPP__

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <DIMData.h>
#include <DIMDataSetClient.h>
#include <DIMParamListClient.h>
#include <DIMStateClient.h>
#include <DIMSuperClient.h>

#include "EACSTypes.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace eacs {

class EACSCli
{
public:
    typedef ntof::dim::DIMParamListClient::DataSignal ParamListSignal;
    typedef ntof::dim::DIMDataSetClient::DataSignal DataSetSignal;
    typedef ntof::dim::DIMStateClient::DataSignal StateSignal;

    EACSCli();

    bool cmdStart();
    bool cmdStop(
        RunInfo::DataStatus dataStatus = RunInfo::DataStatus::DataUnknown);
    bool cmdReset();
    bool cmdConfig();

    void listen();

    ntof::dim::DIMData::List getRunInfo() const;
    ntof::dim::DIMData::List getRunConfig() const;
    ntof::dim::DIMData::List getEvents() const;
    ntof::dim::DIMData::List getDaqList() const;
    EacsState getEacsState() const;

    bool setRunConfig(const ntof::dim::DIMData::List &params);
    bool setRunInfo(const ntof::dim::DIMData::List &params);
    bool setDaqConfig(const std::string &hostname,
                      int card,
                      int channel,
                      const ntof::dim::DIMData::List &params);
    bool setZSPConfig(const std::string &hostname,
                      const ntof::dim::DIMData::List &params);
    bool setTimingConfig(const ntof::dim::DIMData::List &params);

    inline ntof::dim::DIMStateClient *getStateClient() const
    {
        return m_eacsState.get();
    };

    ParamListSignal runInfoSignal;
    ParamListSignal runConfigSignal;
    DataSetSignal eventSignal;
    DataSetSignal daqListSignal;
    StateSignal eacsStateSignal;

protected:
    bool sendCommand(const std::string &cmd);
    bool sendRawCommand(pugi::xml_document &doc);

    ntof::dim::DIMSuperClient m_cmd;
    std::unique_ptr<ntof::dim::DIMParamListClient> m_runInfoCli;
    std::unique_ptr<ntof::dim::DIMParamListClient> m_runConfigCli;
    std::unique_ptr<ntof::dim::DIMDataSetClient> m_eventsCli;
    std::unique_ptr<ntof::dim::DIMDataSetClient> m_daqListCli;
    std::unique_ptr<ntof::dim::DIMStateClient> m_eacsState;
};

} // namespace eacs
} // namespace ntof

#endif
