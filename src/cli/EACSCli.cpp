/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T09:03:43+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include "EACSCli.hpp"

#include <DIMException.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include "Config.hpp"
#include "EACSTypes.hpp"

using namespace ntof::eacs;
using namespace ntof::dim;

EACSCli::EACSCli() : m_cmd(Config::instance().getDimPrefix()) {}

bool EACSCli::cmdStart()
{
    return sendCommand("start");
}

bool EACSCli::cmdStop(RunInfo::DataStatus dataStatus)
{
    // send with no dataStatus for backward compatibility
    if (dataStatus == RunInfo::DataStatus::DataUnknown)
        return sendCommand("stop");

    pugi::xml_document doc;
    pugi::xml_node commandNode = doc.append_child("command");
    commandNode.append_attribute("name").set_value("stop");
    std::string dStr = RunInfo::toString(dataStatus);
    commandNode.append_attribute("dataStatus").set_value(dStr.c_str());
    return sendRawCommand(doc);
}

bool EACSCli::cmdReset()
{
    return sendCommand("reset");
}

bool EACSCli::cmdConfig()
{
    // TODO
    return false;
}

bool EACSCli::sendCommand(const std::string &cmd)
{
    pugi::xml_document doc;
    doc.append_child("command").append_attribute("name").set_value(cmd.c_str());
    return sendRawCommand(doc);
}

bool EACSCli::sendRawCommand(pugi::xml_document &doc)
{
    try
    {
        std::ostringstream oss;
        doc.save(oss);
        LOG(DEBUG) << "Sending Command with document:\n" << oss.str();
        m_cmd.sendCommand(doc);
        return true;
    }
    catch (DIMException &ex)
    {
        std::string cmdName = doc.attribute("name").as_string("??");
        LOG(INFO) << "Command " << cmdName << " failed: " << ex.what();
        return false;
    }
}

void EACSCli::listen()
{
    if (m_runInfoCli)
        return;

    m_runInfoCli.reset(
        new DIMParamListClient(Config::instance().getDimPrefix() + "/RunInfo"));
    m_runInfoCli->dataSignal.connect(runInfoSignal);

    m_runConfigCli.reset(new DIMParamListClient(
        Config::instance().getDimPrefix() + "/RunConfig"));
    m_runConfigCli->dataSignal.connect(runConfigSignal);

    m_eventsCli.reset(
        new DIMDataSetClient(Config::instance().getDimPrefix() + "/Events"));
    m_eventsCli->dataSignal.connect(eventSignal);

    m_daqListCli.reset(
        new DIMDataSetClient(Config::instance().getDimPrefix() + "/Daq/List"));
    m_daqListCli->dataSignal.connect(daqListSignal);

    m_eacsState.reset(
        new DIMStateClient(Config::instance().getDimPrefix() + "/State"));
    m_eacsState->dataSignal.connect(eacsStateSignal);
}

std::vector<DIMData> EACSCli::getRunInfo() const
{
    if (m_runInfoCli)
        return m_runInfoCli->getParameters();
    return DIMData::List();
}

bool EACSCli::setRunInfo(const DIMData::List &data)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/RunInfo");
    try
    {
        DIMAck ack = cli.sendParameters(data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

std::vector<DIMData> EACSCli::getRunConfig() const
{
    if (m_runConfigCli)
        return m_runConfigCli->getParameters();
    return DIMData::List();
}

bool EACSCli::setRunConfig(const DIMData::List &data)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/RunConfig");
    try
    {
        DIMAck ack = cli.sendParameters(data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool EACSCli::setDaqConfig(const std::string &hostname,
                           int card,
                           int channel,
                           const DIMData::List &data)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Daq/" +
                           hostname + "/CARD" + std::to_string(card) +
                           "/CHANNEL" + std::to_string(channel));
    try
    {
        DIMAck ack = cli.sendParameters(data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool EACSCli::setZSPConfig(const std::string &hostname,
                           const DIMData::List &data)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Daq/" +
                           hostname + "/ZeroSuppression");
    try
    {
        DIMAck ack = cli.sendParameters(data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

bool EACSCli::setTimingConfig(const DIMData::List &data)
{
    DIMParamListClient cli(Config::instance().getDimPrefix() + "/Timing");
    try
    {
        DIMAck ack = cli.sendParameters(data);
        if (ack.getStatus() != DIMAck::OK)
            throw DIMException(ack.getMessage(), __LINE__, ack.getErrorCode());
        return true;
    }
    catch (DIMException &ex)
    {
        LOG(INFO) << "Command failed: " << ex.what();
        return false;
    }
}

std::vector<DIMData> EACSCli::getEvents() const
{
    if (m_eventsCli)
        return m_eventsCli->getLatestData();
    return DIMData::List();
}

std::vector<DIMData> EACSCli::getDaqList() const
{
    if (m_daqListCli)
        return m_daqListCli->getLatestData();
    return DIMData::List();
}

EacsState EACSCli::getEacsState() const
{
    if (m_eacsState)
        return static_cast<EacsState>(m_eacsState->getActualState());
    return static_cast<EacsState>(-3); // doesn't exist
}
