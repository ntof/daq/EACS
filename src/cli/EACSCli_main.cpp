/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-07-15T09:03:43+01:00
**     Author: Matteo Ferrari <matteof> <matteo.ferrari.1@cern.ch>
**
*/

#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <boost/program_options.hpp>

#include <NTOFLogging.hpp>

#include "Config.hpp"
#include "EACSCli.hpp"
#include "EACSException.hpp"
#include "misc.h"

#include <dic.hxx>

namespace po = boost::program_options;
using namespace ntof::eacs;
using namespace ntof::log;
using namespace ntof::dim;

typedef std::vector<std::string> VArgs;

int startCmd(const std::string &name, VArgs &vargs)
{
    po::options_description desc("Start Options");
    // clang-format off
    desc.add_options()
        ("help", "produce this help message");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " start \n\n" << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    EACSCli cli;
    if (!cli.cmdStart())
        EACS_THROW("Failed to start", 0);
    std::cout << "Run started" << std::endl;
    return 0;
}

int stopCmd(const std::string &name, VArgs &vargs)
{
    po::options_description desc("Stop Options");
    // clang-format off
    desc.add_options()
        ("help", "produce this help message");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " stop \n\n" << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    EACSCli cli;
    if (!cli.cmdStop())
        EACS_THROW("Failed to stop", 0);
    std::cout << "Run stopped" << std::endl;
    return 0;
}

int resetCmd(const std::string &name, VArgs &vargs)
{
    po::options_description desc("Reset Options");
    // clang-format off
    desc.add_options()
        ("help", "produce this help message");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " reset \n\n" << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    EACSCli cli;
    if (!cli.cmdReset())
        EACS_THROW("Failed to reset run", 0);
    std::cout << "Eacs reset executed" << std::endl;
    return 0;
}

int listen(const std::string &name, VArgs &vargs)
{
    po::options_description desc("ListenMode Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runInfo", "Listen for runInfo events")
    ("runConfig", "Listen for runConfig run events")
    ("events", "Listen for timing events")
    ("state", "Listen for EACS State changes");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " listen [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    bool all = !vm.count("runInfo") && !vm.count("runConfig") &&
        !vm.count("events") && !vm.count("state");
    EACSCli cli;
    cli.listen();
    if (all || vm.count("runInfo"))
    {
        cli.runInfoSignal.connect([](DIMParamListClient &cli) {
            CLOG(INFO, "out")
                << ntof::log::debug() << "/RunInfo : " << cli.getParameters();
        });
    }
    if (all || vm.count("runConfig"))
    {
        cli.runConfigSignal.connect([](DIMParamListClient &cli) {
            CLOG(INFO, "out")
                << ntof::log::debug() << "/RunConfig : " << cli.getParameters();
        });
    }
    if (all || vm.count("events"))
    {
        cli.eventSignal.connect([](DIMDataSetClient &cli) {
            CLOG(INFO, "out")
                << ntof::log::debug() << "/Events : " << cli.getLatestData();
        });
    }
    if (all || vm.count("state"))
    {
        cli.eacsStateSignal.connect([](DIMStateClient &cli) {
            CLOG(INFO, "out") << "/State is now [ " << cli.getActualState()
                              << " ] " << cli.getActualStateAsString();

            std::ostringstream oss;
            oss << "\tError and Warnings\n";
            for (const auto &error : cli.getActiveErrors())
                oss << "\t\tError [ " << error.getCode()
                    << " ]: " << error.getMessage() << "\n";
            for (const auto &warning : cli.getActiveWarnings())
                oss << "\t\tWarning [ " << warning.getCode()
                    << " ]: " << warning.getMessage() << "\n";
            CLOG(INFO, "out") << oss.str() << "\n";
        });
    }

    std::cout << "Listening..." << std::endl;
    while (true)
        std::this_thread::sleep_for(std::chrono::seconds(1));
}

int setRunConfig(const std::string &name, VArgs &vargs)
{
    std::string title, description, experiment;
    int64_t detectorsId, materialsId;

    po::options_description desc("ListenMode Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("title", po::value(&title), "Run title")
    ("description", po::value(&description), "Run description")
    ("experiment", po::value(&experiment), "Run experiment (tape dir)")
    ("detectorsSetupId", po::value(&detectorsId), "Detectors Setup Id")
    ("materialsSetupId", po::value(&materialsId), "Materials Setup Id");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " listen [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    DIMData::List params;
    if (vm.count("title"))
        params.emplace_back(EacsRunInfo::TITLE, title);
    if (vm.count("description"))
        params.emplace_back(EacsRunInfo::DESCRIPTION, description);
    if (vm.count("experiment"))
        params.emplace_back(EacsRunInfo::EXPERIMENT, experiment);
    if (vm.count("detectorsSetupId"))
        params.emplace_back(EacsRunInfo::DETECTORS_SETUP_ID, detectorsId);
    if (vm.count("materialsSetupId"))
        params.emplace_back(EacsRunInfo::MATERIALS_SETUP_ID, materialsId);

    EACSCli cli;
    if (!cli.setRunConfig(params))
        EACS_THROW("Failed to setRunConfig", 0);
    std::cout << "Eacs setRunConfig done" << std::endl;
    return 0;
}

int setRunInfo(const std::string &name, VArgs &vargs)
{
    std::string title, description;
    po::options_description desc("ListenMode Options");

    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("title", po::value(&title), "Run title")
    ("description", po::value(&description), "Run description");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " listen [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    DIMData::List params;
    if (vm.count("title"))
        params.emplace_back(EacsRunInfo::TITLE, title);
    if (vm.count("description"))
        params.emplace_back(EacsRunInfo::DESCRIPTION, description);

    EACSCli cli;
    if (!cli.setRunInfo(params))
        EACS_THROW("Failed to setRunInfo", 0);
    std::cout << "Eacs setRunInfo done" << std::endl;
    return 0;
}

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    std::string config;
    std::string configMisc;
    std::string command;

    try
    {
        po::options_description desc("Global Options");
        // clang-format off
        desc.add_options()
        ("help", "produce this help message")
        ("config,c",
            po::value(&config)->default_value(Config::configFile),
            "force the configuration file")
        ("misc-config,m",
            po::value(&configMisc)
                ->default_value(Config::configMiscFile),
            "force the misc configuration file")(
            "command", po::value(&command),
            "Command to execute:\n"
            "  start: start a run\n"
            "  stop: stop an ongoing run\n"
            "  reset: reset eacs\n"
            "  listen: listen for events\n"
            "  setRunConfig: set RunConfig service\n"
            "  setRunInfo: set RunInfo service\n"
        )
        ("verbose", "Turn on maximum verbosity");

        po::options_description extra = desc;
        extra.add_options()
        ("args...", po::value<std::vector<std::string>>(),
            "command specific arguments");
        // clang-format on

        po::positional_options_description pd;
        pd.add("command", 1);
        // ignore unknown positionals, and let it go to the next level
        pd.add("args...", -1);

        po::variables_map vm;
        po::parsed_options parsed = po::command_line_parser(argc, argv)
                                        .options(extra)
                                        .positional(pd)
                                        .allow_unregistered()
                                        .run();
        std::vector<std::string> unparsed = po::collect_unrecognized(
            parsed.options, po::include_positional);
        po::store(parsed, vm);
        po::notify(vm);

        if (vm.count("help"))
        {
            unparsed.push_back("--help");
        }

        if (!command.empty())
        {
            {
                Config::load(config, configMisc);
                Config &conf = Config::instance();
                DimClient::setDnsNode(conf.getDimDns().c_str(),
                                      conf.getDimDnsPort());
            }

            {
                el::Configurations conf;
                conf.setToDefault();
                conf.set(el::Level::Global, el::ConfigurationType::Format,
                         "%level: %msg");
                conf.set(el::Level::Global, el::ConfigurationType::ToFile,
                         "false");
                conf.set(el::Level::Global,
                         el::ConfigurationType::ToStandardOutput, "true");
                conf.set(el::Level::Global, el::ConfigurationType::Enabled,
                         vm.count("verbose") ? "true" : "false");
                el::Loggers::reconfigureAllLoggers(conf);

                conf.set(el::Level::Global, el::ConfigurationType::Enabled,
                         "true");
                conf.set(el::Level::Global, el::ConfigurationType::Format,
                         "%msg");
                el::Logger *l = el::Loggers::getLogger("out", true);
                l->configure(conf);
            }

            if (command == "start")
                return startCmd(argv[0], unparsed);
            else if (command == "stop")
                return stopCmd(argv[0], unparsed);
            else if (command == "reset")
                return resetCmd(argv[0], unparsed);
            else if (command == "listen")
                return listen(argv[0], unparsed);
            else if (command == "setRunInfo")
                return setRunInfo(argv[0], unparsed);
            else if (command == "setRunConfig")
                return setRunConfig(argv[0], unparsed);
        }

        std::cout << "Usage:\n"
                  << argv[0] << " [start|stop|reset|listen...] <opts>\n\n"
                  << desc << std::endl;
        return 0;
    }
    catch (const std::exception &ex)
    {
        std::cerr << "Error: " << ex.what() << std::endl;
        return -1;
    }
    return 0;
}
