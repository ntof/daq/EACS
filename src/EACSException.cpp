/*
 * EACSException.cpp
 *
 * Created on: May 8, 2015
 * Author: mdonze
 */

#include "EACSException.hpp"

#include <sstream>

namespace ntof {
namespace eacs {

EACSException::EACSException(const std::string &Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    NTOFException(Msg, file, Line, error)
{}

EACSException::EACSException(const char *Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    NTOFException(Msg, file, Line, error)
{}

const char *EACSException::what() const noexcept
{
    if (what_.empty())
    {
        std::ostringstream oss;
        oss << "EACS " << getFile() << ":" << getLine()
            << " code:" << getErrorCode() << " exception: " << getMessage();
        what_ = oss.str();
    }
    return what_.c_str();
}

} /* namespace eacs */
} /* namespace ntof */
